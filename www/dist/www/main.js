(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/add-employeraddress-modal/add-address-modal.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/add-employeraddress-modal/add-address-modal.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".more-value {\n margin:0 auto;\n width:80%;\n}\n.more-value form p {\n color: #000000 ;\n}\n.confirm-btn{\n border:2px solid #000000;\n border-radius:0px;\n}\ninput[type=number]::-webkit-inner-spin-button, \ninput[type=number]::-webkit-outer-spin-button { \n    -webkit-appearance: none;\n    -moz-appearance: none;\n    appearance: none;\n    margin: 0; \n}\n"

/***/ }),

/***/ "./src/app/add-employeraddress-modal/add-address-modal.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/add-employeraddress-modal/add-address-modal.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"p-3 font-weight-bold text-white bg-dark\">{{employerDetail.employer.employer_name}}\n    <i class=\"fa fa-close float-right cursor-pointer\" (click)=\"modalClose(null)\"></i></div>\n  <div class=\"p-3 font-weight-bold bg-warning\">{{\"AAM_DIV_CONTACT_DETAILS\" | translate}}</div>\n  <div class=\"row mt-2 ml-2 mr-2\">\n    <div class=\"col-sm-5 col-md-4 col-lg-3 text-secondary font-weight-bold pt-2\">{{\"AAM_DIV_CONTACT_PERSON\" | translate}}</div>\n    <div class=\"col-sm-7 col-md-8 col-lg-9\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.contact_person\" type=\"text\" placeholder=\"{{'AAM_DIV_CONTACT_PERSON' | translate}}\"/>\n    </div>\n  </div>\n  <div class=\"row mt-3 ml-2 mr-2\">\n    <div class=\"col-sm-5 col-md-4 col-lg-3 text-secondary font-weight-bold pt-2\">{{\"AAM_DIV_TITLE\" | translate}}</div>\n    <div class=\"col-sm-7 col-md-8 col-lg-9\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.title\" type=\"text\" placeholder=\"{{'AAM_DIV_TITLE' | translate}}\" />\n    </div>\n  </div>\n  <div class=\"row mt-3 ml-2 mr-2\">\n    <div class=\"col-sm-5 col-md-4 col-lg-3 text-secondary font-weight-bold pt-2\">{{\"AAM_DIV_EMAIL\" | translate}}</div>\n    <div class=\"col-sm-7 col-md-8 col-lg-9\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.email\" type=\"email\" placeholder=\"{{'AAM_DIV_EMAIL' | translate}}\" />\n    </div>\n  </div>\n  <div class=\"row mt-3 ml-2 mr-2\">\n    <div class=\"col-sm-5 col-md-4 col-lg-3 text-secondary font-weight-bold pt-2\">{{ \"AAM_DIV_PHONE_NUMBER\" | translate}}</div>\n    <div class=\"col-sm-7 col-md-8 col-lg-9\">\n      <input class=\"w-100 p-2\" [ngModel]=\"employerDetail.employer.phone_number | phoneNumber\" \n        (ngModelChange)=\"employerDetail.employer.phone_number = $event\" maxlength=\"14\" \n        (keypress)=\"numberOnly($event)\" type=\"text\" placeholder=\"{{'AAM_DIV_PHONE_NUMBER' | translate}}\" />\n    </div>\n  </div>\n  <div class=\"p-3 font-weight-bold bg-warning mt-2\">{{\"AAM_DIV_ADDRESS\" | translate}}</div>\n  <div class=\"row mt-2 ml-2 mr-2\">\n    <div class=\"col-sm-5 col-md-4 col-lg-3 text-secondary font-weight-bold pt-2\">{{\"AAM_DIV_ADDRESS\" | translate}}</div>\n    <div class=\"col-sm-7 col-md-8 col-lg-9\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.address.street1\" type=\"text\" placeholder=\"{{'AAM_DIV_ADDRESS' | translate}}\" />\n    </div>\n  </div>\n  <div class=\"row mt-3 ml-2 mr-2\">\n    <div class=\"col-sm-5 col-md-4 col-lg-3 text-secondary font-weight-bold pt-2\">{{\"AAM_DIV_ADDRESS\" | translate}} 2</div>\n    <div class=\"col-sm-7 col-md-8 col-lg-9\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.address.street2\" type=\"text\" placeholder=\"{{'AAM_DIV_ADDRESS' | translate}} 2\" />\n    </div>\n  </div>\n  <div class=\"row mt-3 ml-2 mr-2\">\n    <div class=\"col-sm-2 text-secondary font-weight-bold text-center pt-2\">{{\"AAM_DIV_COUNTRY\" | translate}}</div>\n    <div class=\"col-sm-4\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.address.country\" type=\"text\" placeholder=\"{{'AAM_DIV_COUNTRY' | translate}}\" />\n    </div>\n    <div class=\"col-sm-2 text-secondary font-weight-bold text-center pt-2\">{{\"AAM_DIV_STATE\" | translate}}</div>\n    <div class=\"col-sm-4\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.address.state\" type=\"text\" placeholder=\"{{'AAM_DIV_STATE' | translate}}\" />\n    </div>\n  </div>\n  <div class=\"row mt-3 ml-2 mr-2\">\n    <div class=\"col-sm-2 text-secondary font-weight-bold text-center pt-2\">{{\"AAM_DIV_CITY\" | translate}}</div>\n    <div class=\"col-sm-4\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.address.city\" type=\"text\" placeholder=\"{{'AAM_DIV_CITY' | translate}}\" />\n    </div>\n    <div class=\"col-sm-2 text-secondary font-weight-bold text-center pt-2\">{{\"AAM_DIV_ZIP_CODE\" | translate}}</div>\n    <div class=\"col-sm-4\">\n      <input class=\"w-100 p-2\" [(ngModel)]=\"employerDetail.employer.address.zipCode\" type=\"number\" placeholder=\"{{'AAM_DIV_ZIP_CODE' | translate}}\" />\n    </div>\n  </div>\n  <div class=\"modal-footer mt-2\">\n    <button type=\"button\" class=\"btn btn-success pl-3 pr-3 mr-3 font-weight-bold\" (click)=\"addEmployerAddress()\"\n      [disabled]=\"!((employerDetail.employer.address.street1 && employerDetail.employer.address.city\n                    && employerDetail.employer.address.state && employerDetail.employer.address.country\n                    && employerDetail.employer.address.zipCode) || \n\n                    (employerDetail.employer.contact_person && employerDetail.employer.title\n                    && employerDetail.employer.email && employerDetail.employer.phone_number))\">{{ \"AAM_DIV_SAVE\" | translate}}</button>\n    <button type=\"button\" class=\"btn btn-danger pl-3 pr-3 font-weight-bold\" (click)=\"modalClose(null)\">{{ \"AAM_DIV_CANCEL\" | translate}}</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/add-employeraddress-modal/add-address-modal.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/add-employeraddress-modal/add-address-modal.component.ts ***!
  \**************************************************************************/
/*! exports provided: AddAddressModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddAddressModalComponent", function() { return AddAddressModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddAddressModalComponent = /** @class */ (function () {
    function AddAddressModalComponent(authService, backend, session, spinnerService, notifierService) {
        this.authService = authService;
        this.backend = backend;
        this.session = session;
        this.spinnerService = spinnerService;
        this.notifierService = notifierService;
        this.closeAddressmodal = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.notifier = notifierService;
    }
    AddAddressModalComponent.prototype.ngOnInit = function () {
        if (!(this.employerDetail.employer.address)) {
            this.employerDetail.employer.address = {
                street1: null,
                street2: null,
                country: null,
                state: null,
                city: null,
                zipCode: null
            };
        }
    };
    AddAddressModalComponent.prototype.modalClose = function (value) {
        this.closeAddressmodal.emit(value);
    };
    AddAddressModalComponent.prototype.addEmployerAddress = function () {
        this.modalClose(this.employerDetail);
    };
    // restrict characters and allow only number
    AddAddressModalComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("employerDetail"),
        __metadata("design:type", Object)
    ], AddAddressModalComponent.prototype, "employerDetail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AddAddressModalComponent.prototype, "closeAddressmodal", void 0);
    AddAddressModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-address-modal',
            template: __webpack_require__(/*! ./add-address-modal.component.html */ "./src/app/add-employeraddress-modal/add-address-modal.component.html"),
            styles: [__webpack_require__(/*! ./add-address-modal.component.css */ "./src/app/add-employeraddress-modal/add-address-modal.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"],
            angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_5__["NotifierService"]])
    ], AddAddressModalComponent);
    return AddAddressModalComponent;
}());



/***/ }),

/***/ "./src/app/add-investment-modal/add-investment-modal.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/add-investment-modal/add-investment-modal.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/add-investment-modal/add-investment-modal.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/add-investment-modal/add-investment-modal.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\" >\n    <button type=\"button\" class=\"close\" (click)=\"modalRef.hide()\">&times;</button>\n  </div>\n  \n  <div class=\"modal-body mt-0 pt-0\">\n    <div class=\"container\">\n      <h1>{{\"AIM_H1_INVESTMENT_MODAL\" | translate}}</h1>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/add-investment-modal/add-investment-modal.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/add-investment-modal/add-investment-modal.component.ts ***!
  \************************************************************************/
/*! exports provided: AddInvestmentModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddInvestmentModalComponent", function() { return AddInvestmentModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddInvestmentModalComponent = /** @class */ (function () {
    function AddInvestmentModalComponent(modalRef) {
        this.modalRef = modalRef;
    }
    AddInvestmentModalComponent.prototype.ngOnInit = function () {
    };
    AddInvestmentModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-investment-modal',
            template: __webpack_require__(/*! ./add-investment-modal.component.html */ "./src/app/add-investment-modal/add-investment-modal.component.html"),
            styles: [__webpack_require__(/*! ./add-investment-modal.component.css */ "./src/app/add-investment-modal/add-investment-modal.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], AddInvestmentModalComponent);
    return AddInvestmentModalComponent;
}());



/***/ }),

/***/ "./src/app/add-loan-modal/add-loan-modal.component.css":
/*!*************************************************************!*\
  !*** ./src/app/add-loan-modal/add-loan-modal.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/add-loan-modal/add-loan-modal.component.html":
/*!**************************************************************!*\
  !*** ./src/app/add-loan-modal/add-loan-modal.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n    <span aria-hidden=\"true\">&times;</span>\n</button>\n<div class=\"modal-body custom-modal\">   \n  <div class=\"container\">\n    <h1>{{\"LM_H1_LOAN_MODAL\" | translate}}</h1>\n  </div> \n</div>"

/***/ }),

/***/ "./src/app/add-loan-modal/add-loan-modal.component.ts":
/*!************************************************************!*\
  !*** ./src/app/add-loan-modal/add-loan-modal.component.ts ***!
  \************************************************************/
/*! exports provided: AddLoanModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddLoanModalComponent", function() { return AddLoanModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddLoanModalComponent = /** @class */ (function () {
    function AddLoanModalComponent(modalRef) {
        this.modalRef = modalRef;
    }
    AddLoanModalComponent.prototype.ngOnInit = function () {
    };
    AddLoanModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-loan-modal',
            template: __webpack_require__(/*! ./add-loan-modal.component.html */ "./src/app/add-loan-modal/add-loan-modal.component.html"),
            styles: [__webpack_require__(/*! ./add-loan-modal.component.css */ "./src/app/add-loan-modal/add-loan-modal.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], AddLoanModalComponent);
    return AddLoanModalComponent;
}());



/***/ }),

/***/ "./src/app/add-property-modal/add-property-modal.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/add-property-modal/add-property-modal.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".custom-modal{\n  min-height: 70vh;\n}\n.property-modal-footer{\n    padding:10px !important;\n    justify-content: normal !important;\n    color: #ffffff;\n    background: #333333;\n    border:1px solid;\n    font-size:14px;\n  }\n.common-font{\n    font-size: 0.9rem;\n  }\n.w-100{\n    width:100%;\n  }\n.cancel{\n    float:right;\n  }\n.back{\n    float:left;\n  }\n.back:hover, .cancel:hover{\n    cursor:pointer;\n  }\n.icon-size{\n    font-size:2.2rem;\n  }\n.plus-icon{\n    font-size:1.2rem;\n  }\n.hover-color{\n    line-height:1.5rem;\n    border:1px solid #999999;\n  }\n.hover-color:hover .fa {\n    color:#008198;\n  }\n.hover-color:hover{\n    border:1px solid #666666;\n  }\n.set-padding{\n    padding-left:5px;\n    padding-right:5px;\n    cursor: pointer;\n    z-index: 9999;\n  }\n.bg-none{\n    background: none;\n  }\n.set-group{\n    border-right:0;\n    border-radius: 0.25rem !important;\n    padding-right: 4px;\n  }\n.set-input{\n    border-left:0;\n    padding-left: 0px;\n    margin-left: -3px;\n  }\n.small-size {\n    font-size: 9px;\n    white-space: nowrap;\n    text-decoration: none;\n  }\n.zillow-img{\n    width: 70px;\n  }\n.no-underline:hover{\n    text-decoration: none !important;\n  }\ninput::-webkit-input-placeholder {\n    color:#d8d2d2;\n  }\ninput::-ms-input-placeholder {\n    color:#d8d2d2;\n  }\ninput::placeholder {\n    color:#d8d2d2;\n  }\n@media screen and (max-width: 575px){\n    .set-padding{\n      margin-top:0.25rem;\n      padding-left:25px;\n      padding-right:25px;\n    }\n    .top-sp {\n      margin-top: 10px;\n    }\n    .top-mp{\n      margin-top:1px;\n    }\n  }\n@media screen and (max-width:350px){\n    .property-modal-footer{\n      font-size:12px;\n    }\n    .padding-r{\n      padding-right:2px;\n    }\n    .padding-l{\n      padding-left:2px;\n    }\n  }\n@media screen and (max-width:480px){\n    .padding-r{\n      padding-right:4px;\n    }\n    .padding-l{\n      padding-left:4px;\n    }\n    .btn-Overview{\n      margin-right: 0 !important;\n      margin-bottom: 16px !important;\n    }\n  }"

/***/ }),

/***/ "./src/app/add-property-modal/add-property-modal.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/add-property-modal/add-property-modal.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-body custom-modal\">\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'selectPropertyType'\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 text-center\">\n        <h4> {{\"APM_H4_ADD_YOUR_PROPERTY\" | translate}}</h4>\n        <p class=\"small\"> {{\"APM_P_ADD_TO_NEXUS\" | translate}}</p>\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <div class=\"col-sm-3 col-md-3 col-lg-3 set-padding\">\n        <div class=\"text-center hover-color pt-3 pb-3\" (click)=\"changeModalDiv('realEstateI')\">\n          <i class=\"fa fa-plus plus-icon\" aria-hidden=\"true\"></i><br> {{\"APM_I_REAL_ESTATE\" | translate}} <br><i class=\"fa fa-home icon-size\" aria-hidden=\"true\"></i>\n        </div>\n      </div>\n      <div class=\"col-sm-3 col-md-3 col-lg-3 set-padding\" (click)=\"changeModalDiv('VehicleI')\">\n        <div class=\"text-center hover-color pt-3 pb-3\">\n          <i class=\"fa fa-plus plus-icon\" aria-hidden=\"true\"></i><br> {{\"APM_I_VEHICLE\" | translate}}  <br><i class=\"fa fa-car icon-size\" aria-hidden=\"true\"></i>\n        </div>\n      </div>\n      <div class=\"col-sm-3 col-md-3 col-lg-3 set-padding\" (click)=\"changeModalDiv('cashOrDebtI')\">\n        <div class=\"text-center hover-color pt-3 pb-3\">\n          <i class=\"fa fa-plus plus-icon\" aria-hidden=\"true\"></i><br> {{\"APM_I_CASH_OR_DEBT\" | translate}}  <br><i class=\"fa fa-usd icon-size\" aria-hidden=\"true\"></i>\n        </div>\n      </div>\n      <div class=\"col-sm-3 col-md-3 col-lg-3 set-padding\" (click)=\"changeModalDiv('otherI')\">\n        <div class=\"text-center hover-color pt-3 pb-3\">\n          <i class=\"fa fa-plus plus-icon\" aria-hidden=\"true\"></i><br> {{\"APM_I_Other\" | translate}} <br><i class=\"fa fa-cube icon-size\" aria-hidden=\"true\"></i>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <!-- Real Estate -->\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'realEstateI'\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 text-center\">\n        <h4>{{\"APM_H4_LET_FIND_THE_VALUE_OF_YOUR_HOME\" | translate}}</h4>\n        <p class=\"small\"> {{\"APM_P_PLEASE_ENTER_YOUR_ADDRESS\" | translate}}.</p>\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <div class=\"col-sm-2\"></div>\n      <div class=\"col-12 col-sm-8\">\n        <input type=\"text\" class=\"form-control\" name=\"realEstateAddress\" [(ngModel)]=\"realEstateData.address\" placeholder=\"{{'APM_DIV_STREET_ADDRESS' | translate}}\">\n      </div>\n      <div class=\"col-sm-2\"></div>\n    </div>\n    <div class=\"row mt-3 mb-3\">\n      <div class=\"col-sm-2\"></div>\n      <div class=\"col-6 col-sm-4 padding-r\">\n        <input type=\"text\" class=\"form-control\" name=\"realEstateCity\" [(ngModel)]=\"realEstateData.city\" placeholder=\"{{'EPM_DIV_CITY' | translate}}\">\n      </div>\n      <div class=\"col-6 col-sm-4 padding-l\">\n        <input type=\"text\" class=\"form-control\" name=\"realEstateState\" [(ngModel)]=\"realEstateData.state\" placeholder=\"{{'EPM_DIV_STATE' | translate}}\">\n      </div>\n      <div class=\"col-sm-2\"></div>\n    </div>\n    <div class=\"row mt-3 mb-3\">\n      <div class=\"col-sm-2\"></div>\n      <div class=\"col-8 col-sm-5 padding-r\">\n        <input type=\"text\" class=\"form-control\" name=\"realEstateCountry\" [(ngModel)]=\"realEstateData.country\" placeholder=\"{{'EPM_DIV_COUNTRY' | translate}}\">\n      </div>\n      <div class=\"col-4 col-sm-3 padding-l\">\n        <input type=\"text\" class=\"form-control\" name=\"realEstateZip\" [(ngModel)]=\"realEstateData.zip\" placeholder=\"{{'APM_DIV_ZIP' | translate}}\">\n      </div>\n      <div class=\"col-sm-2\"></div>\n    </div>\n    <div class=\"row mt-4\">\n      <div class=\"col-sm-2\"></div>\n      <div class=\"col-sm-8 text-center\">\n          {{\"APM_DIV_AUTOMATICALLY_GET\" | translate}} <a>{{\"APM_A_CLICK_HERE\" | translate}}</a>.\n      </div>\n      <div class=\"col-sm-2\"></div>\n    </div>\n    <div class=\"row mt-5\">\n      <div class=\"col-sm-12 text-center\">\n        <button type=\"button\" class=\"btn btn-info\" [disabled]=\"!(realEstateData.address && realEstateData.city && realEstateData.state)\"\n          (click)=\"addressIntegration('realEstateII', realEstateData)\"> {{\"APM_BUTTON_NEXT\" | translate}}</button>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'realEstateII'\">\n    <div class=\"row mb-5\">\n      <div class=\"col-sm-12 text-center mb-5\">\n        <h4> {{\"APM_H4_LOCATING_YOUR_ADDRESS\" | translate}}...</h4>\n        <p class=\"small mt-2 mb-2\"> {{\"APM_H4_SEARCHING_POSSIBLE_MATCHES_ON\" | translate}} </p>\n        <p class=\"mt-0 mb-0\"><img src=\"../assets/zillow.png\" alt=\"\" width=\"125\"></p>\n        <p class=\"mt-1\"><img src=\"../assets/loading-icon.gif\" alt=\"\" width=\"100\"></p>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'addAddressDiv'\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 text-center mb-2\">\n        <h4> {{\"APM_H4_HERE_WHAT_WE_FOUND\" | translate}}</h4>\n        <p class=\"small mb-2\"> {{\"APM_P_PLEASE_SELECT_YOUR_ADDRESS\" | translate}} </p>\n        <p class=\"mt-0 mb-0\"><img src=\"../assets/zillow.png\" alt=\"\" width=\"125\"></p>\n      </div>\n    </div>\n    <div class=\"row mb-5\">\n      <div class=\"col-1 col-sm-1 col-md-1 col-lg-3\"></div>\n      <div class=\"col-10 col-sm-10 col-md-10 col-lg-6 text-center mb-5\">\n        <div class=\"text-center hover-color pt-2\" (click)=\"changeModalDiv('realEstateIII')\">\n          <i class=\"fa fa-plus plus-icon\" aria-hidden=\"true\"></i><br>\n          <p class=\"small\" *ngIf=\"realEstateData.objAddress\">\n            {{realEstateData.objAddress.street}}, {{realEstateData.objAddress.city}}, {{realEstateData.objAddress.state}} {{realEstateData.objAddress.zipcode}}\n          </p>\n        </div>\n      </div>\n      <div class=\"col-1 col-sm-1 col-md-1 col-lg-3\"></div>\n    </div>\n  </div>\n\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'realEstateIII'\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 text-center\">\n        <h4> {{\"APM_H4_EDIT_YOUR_REAL_ESTATE_DETAILS\" | translate}} </h4>\n        <p class=\"small\"> {{\"APM_P_PLEASE_ANSWER_THE_FOLLOWING_QUESTION\" | translate}} n</p>\n      </div>\n    </div>\n    <div class=\"row mt-5 mb-3\">\n      <div class=\"col-lg-1\"></div>\n      <div class=\"col-sm-6 col-md-6 col-lg-5\">\n        <p class=\"common-font\"> {{\"APM_P_WHAT_KIND_OF_RESIDENCE_IS_THIS\" | translate}} </p>\n      </div>\n      <div class=\"col-sm-6 col-md-6 col-lg-5\">\n        <select class=\"form-control custom-select\" name=\"realEstateName\" [(ngModel)]=\"realEstateData.name\">\n          <option> {{\"APM_P_WHAT_KIND_OF_RESIDENCE_IS_THIS\" | translate}} </option>\n          <option> {{\"APM_OPTION_INVESTMENT_PROPERTY\" | translate}} </option>\n          <option> {{\"APM_OPTION_VACATION_HOME\" | translate}} </option>\n          <option>{{\"APM_OPTION_OTHER\" | translate}}</option>\n        </select>\n      </div>\n      <div class=\"col-lg-1\"></div>\n    </div>\n    <div class=\"row \">\n      <div class=\"col-lg-1\"></div>\n      <div class=\"col-sm-6 col-md-6 col-lg-5\">\n        <p class=\"common-font\"> {{\"APM_P_ITS_ESTIMATED_VALUE\" | translate}}?</p>\n      </div>\n      <div class=\"col-8 col-sm-4 col-md-4 col-lg-3 input-group\">\n        <div class=\"input-group-append\">\n          <span class=\"input-group-text bg-none set-group\">$</span>\n        </div>\n        <input type=\"number\" class=\"form-control set-input\" placeholder=\"\" [(ngModel)]=\"realEstateData.estimated_value\" step=\"0.01\" name=\"realEstateEstimatedValue\">\n      </div>\n      <div class=\"col-4 col-sm-2 col-md-2 col-lg-3 pl-0\">\n        <a href=\"https://www.zillow.com/corp/Terms.htm\" target=\"_blank\" class=\"cursor-pointer no-underline\">\n          <img src=\"../assets/zillow.png\" alt=\"\" class=\"zillow-img\">\n          <div class=\"text-secondary small-size\"> {{\"APM_DIV_TERMS_AND_CONDITIONS\" | translate}} </div>\n        </a>\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <div class=\"col-lg-1\"></div>\n      <div class=\"col-sm-6 col-md-6 col-lg-5\">\n        <p class=\"common-font\"> {{\"APM_P_PROPERTY_NAME\" | translate}} </p>\n      </div>\n      <div class=\"col-sm-6 col-md-6 col-lg-5\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"{{'APM_DIV_ITALIAN_HOME' | translate}}\" [(ngModel)]=\"realEstateData.subtype\" name=\"realEstateSubtype\">\n      </div>\n      <div class=\"col-lg-1\"></div>\n    </div>\n    <div class=\"row mt-5\">\n      <div class=\"col-sm-12 text-center\">\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!(realEstateData.name && realEstateData.estimated_value)\" (click)=\"addProperties('successDiv', realEstateData)\">Add IT</button>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'successDiv'\">\n    <div class=\"row mb-3\">\n      <div class=\"col-sm-12 text-center\">\n        <h4> {{\"APM_H4_WE_GOT_YOUR_PROPERTY\" | translate}} </h4>\n        <p class=\"small\"> {{\"APM_P_FINANCIAL_OVERVIEW\" | translate}} </p>\n      </div>\n    </div>\n    <div class=\"row mt-5 mb-5 ml-0 mr-0\">\n      <div class=\"col-sm-12 text-center\">\n        <button type=\"button\" class=\"btn btn-primary btn-Overview text-uppercase common-font mr-4\" style=\"height:40px;\"\n          (click)=\"closeModal()\"> {{\"APM_BUTTON_GO_TO_OVERVIEW\" | translate}} </button>\n        <button type=\"button\" class=\"btn btn-primary text-uppercase common-font\" style=\"height:40px;\"\n          (click)=\"changeModalDiv('selectPropertyType')\"> {{ \"APM_BUTTON_ADD_ANOTHER_PROPERTY\" | translate}} </button>\n      </div>\n    </div>\n  </div>\n\n  <!-- Vehicle -->\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'VehicleI'\">\n    <div class=\"row mb-3\">\n      <div class=\"col-sm-12 text-center\">\n        <h4> {{\"APM_H4_LET_FIND_THE_VALUE_OF_YOUR_VEHICLE\" | translate}}</h4>\n        <p class=\"small\"> {{\"APM_H4_WHAT_YOUD_LIKE_TO_ADD\" | translate}}</p>\n      </div>\n    </div>\n    <div class=\"row mt-3 mb-5\">\n      <div class=\"col-2 col-sm-3\"></div>\n      <div class=\"col-8 col-sm-6\">\n        <select class=\"form-control custom-select\" [(ngModel)]=\"vehicleData.name\" name=\"vehicleName\">\n          <option value=\"Automobile\"> {{\"APM_OPTION_AUTOMOBILE\" | translate}} </option>\n          <option value=\"Boat\"> {{\"APM_OPTION_BOAT\" | translate}} </option>\n          <option value=\"Motorcycle\"> {{\"APM_OPTION_MOTORCYCLE\" | translate}} </option>\n          <option value=\"Snowmobile\"> {{\"APM_OPTION_SNOWMOBILE\" | translate}} </option>\n          <option value=\"Bicycle\"> {{\"APM_OPTION_BICYCLE\" | translate}} </option>\n          <option value=\"Commercial Vehicle\"> {{\"APM_OPTION_COMMERCIAL VEHICLE\" | translate}} </option>\n          <option value=\"Other\"> {{\"APM_OPTION_OTHER\" | translate}} </option>\n        </select>\n      </div>\n      <div class=\"col-2 col-sm-3\"></div>\n    </div>\n    <div class=\"row mt-5\">\n      <div class=\"col-sm-12 text-center mt-5\">\n        <button type=\"button\" [disabled]=\"!(vehicleData.name)\" (click)=\"changeModalDiv('VehicleII')\" class=\"btn btn-primary\">{{\"APM_BUTTON_NEXT\" | translate}}</button>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'VehicleII'\">\n    <div class=\"row mb-3\">\n      <div class=\"col-sm-12 text-center\">\n        <h4> {{\"APM_H4_FIND_THE_VALUE\" | translate}} {{vehicleData.name}}</h4>\n        <p class=\"small\"> {{\"APM_P_PLEASE_ANSWER_QUESTIONS\" | translate}} </p>\n      </div>\n    </div>\n    <form>\n      <div class=\"row\">\n        <div class=\"col-lg-4 col-12 col-sm-4 col-md-4 top-sp\">\n          <select class=\"form-control custom-select\"  [(ngModel)]=\"vehicleData.year\" name=\"vehicleYear\" #yearOptions required>\n            <option disabled=\"disabled\" value=\"year\"> {{\"APM_OPTION_YEAR\" | translate}}</option>\n            <option>2019</option>\n            <option>2018</option>\n            <option>2017</option>\n            <option>2016</option>\n            <option>2015</option>\n            <option>2014</option>\n            <option>2013</option>\n            <option>2012</option>\n            <option>2011</option>\n            <option>2010</option>\n            <option>2009</option>\n            <option>2008</option>\n            <option>2007</option>\n            <option>2006</option>\n            <option>2005</option>\n            <option>2004</option>\n            <option>2003</option>\n            <option>2002</option>\n            <option>2001</option>\n          </select>\n        </div>\n        <div class=\"col-lg-4 col-12 col-sm-4 col-md-4 top-sp\">\n          <select class=\"form-control custom-select\" [(ngModel)]=\"vehicleData.brand\" name=\"vehicleBrand\" #brandOptions required>\n            <option disabled=\"disabled\" value=\"brand\"> {{\"APM_OPTION_MAKE\" | translate}} </option>\n            <option> Acura</option>\n            <option> Alfa Romeo</option>\n            <option> Audi</option>\n            <option> BMW</option>\n            <option> Buick</option>\n            <option> Cadicial</option>\n            <option> Cadicial</option>\n            <option> Chevrolet</option>\n            <option> Chrysler</option>\n            <option> Dodge</option>\n            <option> Flat</option>\n            <option> GMC</option>\n            <option>Honda</option>\n          </select>\n        </div>\n        <div class=\"col-lg-4 col-12 col-sm-4 col-md-4 top-sp\">\n          <select class=\"form-control custom-select\" [(ngModel)]=\"vehicleData.model\" name=\"vehicleModel\" #modelOptions required>\n            <option disabled=\"disabled\" value=\"model\">{{\"APM_OPTION_MODEL\" | translate}}</option>\n            <option>A3</option>\n            <option>A4</option>\n            <option>A5</option>\n            <option>A6</option>\n            <option>A7</option>\n            <option>A8</option>\n            <option>Q3</option>\n            <option>Q4</option>\n            <option>S3</option>\n            <option>S4</option>\n            <option>S5</option>\n            <option>S6</option>\n            <option>S7</option>\n            <option>S8</option>\n          </select>\n        </div>\n      </div>\n      <div class=\"row mt-3\">\n        <div class=\"col-md-8 col-lg-8 col-12 col-sm-8 top-mp\">\n          <select class=\"form-control custom-select\" [(ngModel)]=\"vehicleData.trim\" name=\"vehicleTrim\" #trimOptions required>\n            <option disabled=\"disabled\" value=\"trim\">{{\"APM_OPTION_TRIM\" | translate}}</option>\n            <option>Premium sedan 4D</option>\n            <option>Premium plus sedan 4D</option>\n          </select>\n        </div>\n        <div class=\"col-12 col-sm-4 col-md-4 col-lg-4 top-sp\">\n          <input type=\"text\" class=\"form-control\" placeholder=\"{{'APM_DIV_MILES' | translate}}\" [(ngModel)]=\"vehicleData.miles\" name=\"vehicleMiles\">\n        </div>\n      </div>\n      <div class=\"row mt-4 mb-5\">\n        <div class=\"col-sm-12 text-center\">\n          <!-- <p class=\"small\">Or, Manually track your {{vehicleData.name}}'s value by <a href=\"#\">clicking here</a></p> -->\n        </div>\n      </div>\n      <div class=\"row mt-5\">\n        <div class=\"col text-center mt-3\">\n          <button type=\"button\" (click)=\"changeModalDiv('VehicleIII')\" class=\"btn btn-primary btn-md\"\n            [disabled]=\"yearOptions.value=='year' || brandOptions.value=='brand' || modelOptions.value=='model' || trimOptions.value=='trim'\">{{\"APM_BUTTON_SHOW_ME\" | translate}}</button>\n        </div>\n      </div>\n    </form>\n  </div>\n\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'VehicleIII'\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 text-center\">\n        <h4>{{\"APM_H4_EDIT_THE_DETAILS_OF_YOUR\" | translate}} {{vehicleData.brand}} {{vehicleData.model}}</h4>\n        <p class=\"small\">{{\"APM_P_PLEASE_ANSWER_QUESTIONS\" | translate}} </p>\n      </div>\n    </div>\n    <div class=\"row mt-5 mb-3\">\n      <div class=\"col-sm-1\"></div>\n      <div class=\"col-sm-5\">\n        <p class=\"common-font\">{{\"APM_P_IDENTIFIER_NAME_FOR_PROPERTY\" | translate}}</p>\n      </div>\n      <div class=\"col-sm-5\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"{{'APM_DIV_VEHICLE_NAME' | translate}}\" [(ngModel)]=\"vehicleData.subtype\" name=\"vehicleSubtype\">\n      </div>\n      <div class=\"col-sm-1\"></div>\n    </div>\n    <div class=\"row mb-5\">\n      <div class=\"col-sm-1\"></div>\n      <div class=\"col-sm-5\">\n        <p class=\"common-font\">{{\"APM_P_ESTIMATED_VALUE\" | translate}}</p>\n        <!-- <p class=\"small\">Powered by Kelley Blue Book</p> -->\n      </div>\n      <div class=\"col-sm-5\">\n        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"vehicleData.estimated_value\" name=\"vehicleEstimatedValue\" placeholder=\"$\">\n        <!-- <p class=\"small mt-1\">This is our best estimate based on available data see <a href=\"http://KBB.com\">KBB.com</a> for a detailed estimate</p> -->\n      </div>\n      <div class=\"col-sm-1\"></div>\n    </div>\n    <div class=\"row mt-5\">\n      <div class=\"col-sm-12 text-center\">\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!(vehicleData.estimated_value)\" (click)=\"addProperties('successDiv',vehicleData)\">{{\"APM_BUTTON_ADD_IT\" | translate}}</button>\n      </div>\n    </div>\n  </div>\n\n  <!-- Cash or Debt -->\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'cashOrDebtI'\">\n    <div class=\"row mb-3\">\n      <div class=\"col-sm-12 text-center\">\n        <h4>{{\"APM_H4_VALUE_CASH_AND_DEBT\" | translate}}</h4>\n        <p class=\"small\">{{\"APM_P_PLEASE_SELECT\" | translate}}</p>\n      </div>\n    </div>\n    <div class=\"row mt-3 mb-5\">\n      <div class=\"col-2 col-sm-3\"></div>\n      <div class=\"col-8 col-sm-6\">\n        <select class=\"form-control custom-select\" [(ngModel)]=\"cashOrDebtData.name\" name=\"cashOrDebtName\">\n          <option value = \"Debt\">{{\"APM_OPTION_DEBT\" | translate}}</option>\n          <option value = \"Cash\">{{\"APM_OPTION_CASH\" | translate}}</option>\n        </select>\n      </div>\n      <div class=\"col-2 col-sm-3\"></div>\n    </div>\n    <div class=\"row mt-5\">\n      <div class=\"col-sm-12 text-center mt-5\">\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!(cashOrDebtData.name)\" (click)=\"changeModalDiv('cashOrDebtII')\">{{\"APM_BUTTON_NEXT\" | translate}}</button>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'cashOrDebtII'\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 text-center\">\n        <h4>{{\"APM_H4_EDIT_THE_DETAILS_OF_YOUR\" | translate}} {{cashOrDebtData.name}}</h4>\n        <p class=\"small\">{{\"APM_P_PLEASE_ANSWER_QUESTIONS\" | translate}} </p>\n      </div>\n    </div>\n    <div class=\"row mt-5 mb-3\">\n      <div class=\"col-sm-1\"></div>\n      <div class=\"col-sm-5\">\n        <p class=\"common-font\">{{\"APM_P_IDENTIFIER_NAME_FOR_PROPERTY\" | translate}} </p>\n      </div>\n      <div class=\"col-sm-5\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"{{'APM_DIV_ITALIAN_HOME' | translate}}\" [(ngModel)]=\"cashOrDebtData.subtype\" name=\"cashOrDebtSubtype\">\n      </div>\n      <div class=\"col-sm-1\"></div>\n    </div>\n    <div class=\"row mb-5\">\n      <div class=\"col-sm-1\"></div>\n      <div class=\"col-sm-5\">\n        <p class=\"common-font ns-required\">{{\"APM_P_TOTAL_AMOUNT\" | translate}}</p>\n      </div>\n      <div class=\"col-sm-5\">\n        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"cashOrDebtData.estimated_value\" name=\"cashOrDebtEstimatedValue\" placeholder=\"$\">\n      </div>\n      <div class=\"col-sm-1\"></div>\n    </div>\n    <div class=\"row mt-5\">\n      <div class=\"col-sm-12 text-center\">\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!(cashOrDebtData.estimated_value)\" (click)=\"addCashOrDebtProperty('successDiv',cashOrDebtData)\">{{\"APM_BUTTON_ADD_IT\" | translate}}</button>\n      </div>\n    </div>\n  </div>\n\n  <!-- Other -->\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'otherI'\">\n    <div class=\"row mb-3\">\n      <div class=\"col-sm-12 text-center\">\n        <h4>{{\"APM_H4_LIKE_TO_ADD\" | translate}}</h4>\n        <p class=\"small\">{{\"APM_P_PLEASE_SELECT_BELOW\" | translate}}.</p>\n      </div>\n    </div>\n    <div class=\"row mt-3 mb-5\">\n      <div class=\"col-2 col-sm-3\"></div>\n      <div class=\"col-8 col-sm-6\">\n        <select class=\"form-control custom-select\" [(ngModel)]=\"otherData.name\" name=\"otherName\">\n          <option value=\"Artwork\">{{\"APM_OPTION_ARTWORK\" | translate}}</option>\n          <option>{{\"APM_OPTION_APPLIANCE\" | translate}}</option>\n          <option>{{\"APM_OPTION_BEDDING_DRAPES_LINEN\" | translate}}</option>\n          <option>{{\"APM_OPTION_BOOKS_CDS_DVDS_TAPES\" | translate}}</option>\n          <option>{{\"APM_OPTION_CAMERA\" | translate}}</option>\n          <option>{{\"APM_OPTION_CLOTHING\" | translate}}</option>\n          <option>{{\"APM_OPTION_COLECTIBLE\" | translate}}</option>\n          <option>{{\"APM_OPTION_COLECTIBLE\" | translate}}</option>\n          <option>{{\"APM_OPTION_COOKING\" | translate}}</option>\n          <option>{{\"APM_OPTION_CUTLERY_SILVERWARE\" | translate}}</option>\n          <option>{{\"APM_OPTION_DECORATIONS\" | translate}}</option>\n          <option>{{\"APM_OPTION_DISHES_FINE_CHINA\" | translate}}</option>\n          <option>{{\"APM_OPTION_ELECTRONICS\" | translate}}</option>\n          <option>{{\"APM_OPTION_FURNITURE\" | translate}}</option>\n          <option>{{\"APM_OPTION_GARDENING\" | translate}}</option>\n          <option>{{\"APM_OPTION_GLASS_CRYSTAL\" | translate}}</option>\n          <option>{{\"APM_OPTION_GOLD\" | translate}}</option>\n          <option>{{\"APM_OPTION_MISCELLANEOUS_ITEMS\" | translate}}</option>\n          <option>{{\"APM_OPTION_MUSICAL_INSTRUMENTS\" | translate}}</option>\n          <option>{{\"APM_OPTION_RUG\" | translate}}</option>\n          <option>{{\"APM_OPTION_SPORTS_GOODS\" | translate}}</option>\n          <option>{{\"APM_OPTION_TOOLS\" | translate}}</option>\n          <option>{{\"APM_OPTION_TOYS\" | translate}}</option>\n        </select>\n      </div>\n      <div class=\"col-2 col-sm-3\"></div>\n    </div>\n    <div class=\"row mt-5\">\n      <div class=\"col-sm-12 text-center mt-5\">\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!(otherData.name)\" (click)=\"changeModalDiv('otherII')\">{{\"APM_BUTTON_NEXT\" | translate}}</button>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"container\" *ngIf=\"realEstateCurrentDiv == 'otherII'\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 text-center\">\n        <h4>{{\"APM_H4_EDIT_THE_DETAILS_OF_YOUR\" | translate}} {{otherData.name}}</h4>\n        <p class=\"small\">{{\"APM_P_PLEASE_ANSWER_QUESTIONS\" | translate}}</p>\n      </div>\n    </div>\n    <div class=\"row mt-5 mb-3\">\n      <div class=\"col-sm-1\"></div>\n      <div class=\"col-sm-5\">\n        <p class=\"common-font\">{{\"APM_P_IDENTIFIER_NAME_FOR_PROPERTY\" | translate}} </p>\n      </div>\n      <div class=\"col-sm-5\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"{{'APM_DIV_ITALIAN_HOME' | translate}}\" [(ngModel)]=\"otherData.subtype\" name=\"otherSubtype\">\n      </div>\n      <div class=\"col-sm-1\"></div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-sm-1\"></div>\n      <div class=\"col-sm-5\">\n        <p class=\"common-font\">{{\"APM_P_ESTIMATED_VALUE\" | translate}}?</p>\n      </div>\n      <div class=\"col-sm-3\">\n        <input type=\"number\" class=\"form-control\" [(ngModel)]=\"otherData.estimated_value\" name=\"otherEstimatedValue\" placeholder=\"$\">\n      </div>\n      <div class=\"col-sm-3\"></div>\n    </div>\n    <div class=\"row mt-5 mb-3\">\n      <div class=\"col-sm-1\"></div>\n      <div class=\"col-sm-5\">\n        <p class=\"common-font\">{{\"APM_P_PROPERTY_ASSOCIATED_WITH_LOAN_MORTGAGE_CREDIT\" | translate}} </p>\n      </div>\n      <div class=\"col-sm-4\">\n        <select class=\"form-control custom-select\">\n          <option>N/A</option>\n          <option>{{\"APM_OPTION_DEBT\" | translate}} </option>\n        </select>\n      </div>\n      <div class=\"col-sm-2\"></div>\n    </div>\n    <div class=\"row mt-5\">\n      <div class=\"col-sm-12 text-center\">\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!(otherData.estimated_value)\" (click)=\"addProperties('successDiv',otherData)\">{{\"APM_BUTTON_ADD_IT\" | translate}}</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal-footer property-modal-footer\">\n  <div class=\"text-center w-100\">\n    <span *ngIf=\"!(realEstateCurrentDiv == 'selectPropertyType' || realEstateCurrentDiv == 'successDiv')\" \n      class=\"back\" (click)=\"backDiv()\">{{\"APM_SPAN_BACK\" | translate}}</span>\n    <i class=\"fa fa-lock\" aria-hidden=\"true\"></i>{{ \"APM_I_SECURITY\" | translate}} \n    <span (click)=\"closeModal()\" class=\"cancel\">{{\"APM_SPAN_CANCEL\" | translate}}</span>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/add-property-modal/add-property-modal.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/add-property-modal/add-property-modal.component.ts ***!
  \********************************************************************/
/*! exports provided: AddPropertyModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPropertyModalComponent", function() { return AddPropertyModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common/utils */ "./src/app/common/utils.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};









var AddPropertyModalComponent = /** @class */ (function () {
    function AddPropertyModalComponent(authService, backend, modalService, spinnerService, notifierService, translate) {
        this.authService = authService;
        this.backend = backend;
        this.modalService = modalService;
        this.spinnerService = spinnerService;
        this.notifierService = notifierService;
        this.translate = translate;
        this.childEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.propertiesChild = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.realEstateCurrentDiv = "selectPropertyType";
        this.realEstateData = {
            name: "Primary Residence",
            objAddress: {}
        };
        this.vehicleData = {
            name: "Automobile",
            year: "year",
            brand: "brand",
            model: "model",
            trim: "trim"
        };
        this.cashOrDebtData = {
            name: "Debt"
        };
        this.otherData = {
            name: "Artwork"
        };
        this.propertiesUpdated = false;
        this.notifier = notifierService;
        // this is default langugage
        translate.setDefaultLang('en');
        // this language is to set by your prefered browser language
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang);
    }
    AddPropertyModalComponent.prototype.ngOnInit = function () {
        this.userAttributes = this.authService.getUserProfile();
        this.authResultInfo = this.authService.getAccessTokenAndID();
    };
    AddPropertyModalComponent.prototype.closeModal = function () {
        // Check if properties where updated
        var event = null;
        if (this.propertiesUpdated) {
            event = this.userAttributes.properties;
        }
        this.propertiesChild.emit(event);
    };
    AddPropertyModalComponent.prototype.openAddPropertyModal = function (addPropertyModal) {
        this.modalRef = this.modalService.show(addPropertyModal, Object.assign({}, { class: 'gray modal-lg' }));
    };
    AddPropertyModalComponent.prototype.changeModalDiv = function (divName) {
        this.realEstateCurrentDiv = divName;
        if (divName == 'realEstateI') {
            this.realEstateData.type = 'Real Estate';
        }
        else if (divName == 'VehicleI') {
            this.vehicleData.type = 'Vehicle';
        }
        else if (divName == 'cashOrDebtI') {
            this.cashOrDebtData.type = 'Cash or Debt';
        }
        else if (divName == 'otherI') {
            this.otherData.type = 'Other';
        }
        else if (divName == 'selectPropertyType') {
            this.resetPropertiesValue();
        }
    };
    AddPropertyModalComponent.prototype.resetPropertiesValue = function () {
        this.realEstateData.address = null;
        this.realEstateData.city = null;
        this.realEstateData.state = null;
        this.realEstateData.country = null;
        this.realEstateData.zip = null;
        this.realEstateData.subtype = null;
        this.realEstateData.estimated_value = null;
        this.realEstateData.name = "Primary Residence";
        this.vehicleData.subtype = null;
        this.vehicleData.estimated_value = null;
        this.vehicleData.name = "Automobile";
        this.vehicleData.year = "year";
        this.vehicleData.brand = "brand";
        this.vehicleData.model = "model";
        this.vehicleData.trim = "trim";
        this.otherData.subtype = null;
        this.otherData.estimated_value = null;
        this.otherData.name = "ArtWork";
        this.cashOrDebtData.subtype = null;
        this.cashOrDebtData.estimated_value = null;
        this.cashOrDebtData.name = "Debt";
    };
    AddPropertyModalComponent.prototype.backDiv = function () {
        switch (this.realEstateCurrentDiv) {
            case 'realEstateI':
                this.realEstateCurrentDiv = "selectPropertyType";
                break;
            case 'realEstateII':
                this.realEstateCurrentDiv = "realEstateI";
                break;
            case 'addAddressDiv':
                this.realEstateCurrentDiv = "realEstateI";
                break;
            case 'realEstateIII':
                this.realEstateCurrentDiv = "addAddressDiv";
                break;
            case 'VehicleI':
                this.realEstateCurrentDiv = "selectPropertyType";
                break;
            case 'VehicleII':
                this.realEstateCurrentDiv = "VehicleI";
                break;
            case 'VehicleIII':
                this.realEstateCurrentDiv = "VehicleII";
                break;
            case 'cashOrDebtI':
                this.realEstateCurrentDiv = "selectPropertyType";
                break;
            case 'cashOrDebtII':
                this.realEstateCurrentDiv = "cashOrDebtI";
                break;
            case 'otherI':
                this.realEstateCurrentDiv = "selectPropertyType";
                break;
            case 'otherII':
                this.realEstateCurrentDiv = "otherI";
                break;
            default:
                this.realEstateCurrentDiv = "selectPropertyType";
                break;
        }
        return this.realEstateCurrentDiv;
    };
    AddPropertyModalComponent.prototype.addressIntegration = function (divName, addressData) {
        var _this = this;
        if (divName) {
            this.realEstateCurrentDiv = divName;
        }
        var path = "";
        if (addressData.zip) {
            path = "/properties/realestate?city=" + addressData.city + "&state=" + addressData.state + "&zip=&address=" + addressData.zip + " " + addressData.address;
        }
        else {
            path = "/properties/realestate?city=" + addressData.city + "&state=" + addressData.state + "&zip=&address=" + addressData.address;
        }
        this.backend.get(path, this.authResultInfo.accessToken)
            .subscribe(function (data) {
            var resAddress = data.json();
            if (resAddress && resAddress.length > 0) {
                if ('address' in resAddress[0] && resAddress[0].address && resAddress[0].address.length > 0) {
                    try {
                        for (var _a = __values(resAddress[0].address), _b = _a.next(); !_b.done; _b = _a.next()) {
                            var add = _b.value;
                            if (add.street[0] && add.street.length > 0) {
                                _this.realEstateData.objAddress.street = add.street[0];
                                _this.realEstateData.subtype = add.street[0];
                            }
                            if (add.city[0] && add.city.length > 0) {
                                _this.realEstateData.objAddress.city = add.city[0];
                            }
                            if (add.state[0] && add.state.length > 0) {
                                _this.realEstateData.objAddress.state = add.state[0];
                            }
                            if (add.zipcode[0] && add.zipcode.length > 0) {
                                _this.realEstateData.objAddress.zipcode = add.zipcode[0];
                            }
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                    _this.realEstateData.objAddress.zpid = resAddress[0].zpid[0];
                }
                if ('zestimate' in resAddress[0] && resAddress[0].zestimate && resAddress[0].zestimate.length > 0) {
                    try {
                        for (var _d = __values(resAddress[0].zestimate), _e = _d.next(); !_e.done; _e = _d.next()) {
                            var val = _e.value;
                            var values = Object.values(val.amount[0]);
                            _this.realEstateData.estimated_value = values[0];
                        }
                    }
                    catch (e_2_1) { e_2 = { error: e_2_1 }; }
                    finally {
                        try {
                            if (_e && !_e.done && (_f = _d.return)) _f.call(_d);
                        }
                        finally { if (e_2) throw e_2.error; }
                    }
                }
                console.log("Real Estate", _this.realEstateData);
                // translate error message
                _this.translate.get('APM_ADDRESS_SUCCESS')
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
                _this.realEstateCurrentDiv = 'addAddressDiv';
            }
            else {
                _this.notifier.notify('error', "Address not found!");
                _this.realEstateCurrentDiv = 'realEstateI';
            }
            var e_1, _c, e_2, _f;
        }, function (error) {
            console.log(error);
            var errorVar = error.json();
            // translate error message
            _this.translate.get('APM_ERROR')
                .subscribe(function (value) {
                _this.notifier.notify('error', value + errorVar["message"]);
            });
        });
    };
    AddPropertyModalComponent.prototype.updatePropertiesInCache = function (properties) {
        this.userAttributes = this.authService.getUserProfile();
        this.userAttributes.properties = properties;
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        this.propertiesUpdated = true;
    };
    // This method is called on adding a cash/debt
    AddPropertyModalComponent.prototype.addCashOrDebtProperty = function (divName, propertiesData) {
        // First validate
        var estimated_value = parseInt(propertiesData.estimated_value);
        if (!_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(propertiesData, 'subtype') && propertiesData.subtype.length > 200) {
            this.notifier.notify('error', "Short name cannot be greater than 200 characters");
            return;
        }
        // Negative values not allowed
        if (propertiesData.name === 'Cash' && estimated_value < 0) {
            this.notifier.notify('error', "Estimate value cannot be negative.");
            return;
        }
        // If validation passes then call add properties
        this.addProperties(divName, propertiesData);
    };
    AddPropertyModalComponent.prototype.addProperties = function (divName, propertiesData) {
        var _this = this;
        this.spinnerService.show();
        var data = {
            type: propertiesData.type,
            name: propertiesData.name,
            estimated_value: parseInt(propertiesData.estimated_value),
            subtype: "",
            other_detail: {}
        };
        if (propertiesData.objAddress) {
            data.other_detail = {
                address: propertiesData.objAddress.street,
                city: propertiesData.objAddress.city,
                state: propertiesData.objAddress.state,
                zipid: propertiesData.objAddress.zipcode,
                zpid: propertiesData.objAddress.zpid
            };
        }
        if (propertiesData.country) {
            data.other_detail.country = propertiesData.country;
        }
        if (propertiesData.year) {
            data.other_detail.year = propertiesData.year;
        }
        if (propertiesData.subtype) {
            data.subtype = propertiesData.subtype;
        }
        console.log("Data : ", data);
        var token = this.authResultInfo.accessToken;
        this.backend.post(data, '/properties', token)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["mergeMap"])(function (data) {
            console.log('response from post properties', data);
            return _this.backend.get('/properties', token);
        }))
            .subscribe(function (data) {
            var propertyRes = data.json();
            console.log('Properties: ', propertyRes);
            _this.updatePropertiesInCache(propertyRes.properties);
            // translate error message
            _this.translate.get('APM_ADD_SUCCESS')
                .subscribe(function (value) {
                _this.notifier.notify('success', value);
            });
            _this.realEstateCurrentDiv = divName;
            _this.spinnerService.hide();
        }, function (err) {
            var errorVar = err.json();
            console.log('Error: ', errorVar["message"]);
            // translate error message
            _this.translate.get('APM_ERROR')
                .subscribe(function (value) {
                _this.notifier.notify('error', value + errorVar["message"]);
            });
            _this.spinnerService.hide();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AddPropertyModalComponent.prototype, "childEvent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AddPropertyModalComponent.prototype, "propertiesChild", void 0);
    AddPropertyModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-property-modal',
            template: __webpack_require__(/*! ./add-property-modal.component.html */ "./src/app/add-property-modal/add-property-modal.component.html"),
            styles: [__webpack_require__(/*! ./add-property-modal.component.css */ "./src/app/add-property-modal/add-property-modal.component.css"), __webpack_require__(/*! ../global/global.css */ "./src/app/global/global.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_4__["BackendService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_3__["NotifierService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"]])
    ], AddPropertyModalComponent);
    return AddPropertyModalComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: RoutingComponents, AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingComponents", function() { return RoutingComponents; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _signuppage_signuppage_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signuppage/signuppage.component */ "./src/app/signuppage/signuppage.component.ts");
/* harmony import */ var _congrat_page_congrat_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./congrat-page/congrat-page.component */ "./src/app/congrat-page/congrat-page.component.ts");
/* harmony import */ var _generatereport_page_generatereport_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./generatereport-page/generatereport-page.component */ "./src/app/generatereport-page/generatereport-page.component.ts");
/* harmony import */ var _open_report_open_report_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./open-report/open-report.component */ "./src/app/open-report/open-report.component.ts");
/* harmony import */ var _manage_account_manage_account_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./manage-account/manage-account.component */ "./src/app/manage-account/manage-account.component.ts");
/* harmony import */ var _email_verification_email_verification_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./email-verification/email-verification.component */ "./src/app/email-verification/email-verification.component.ts");
/* harmony import */ var _guage_chart_guage_chart_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./guage-chart/guage-chart.component */ "./src/app/guage-chart/guage-chart.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    { path: '', component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"] },
    { path: 'signup', component: _signuppage_signuppage_component__WEBPACK_IMPORTED_MODULE_4__["SignuppageComponent"] },
    { path: 'congratpage', component: _congrat_page_congrat_page_component__WEBPACK_IMPORTED_MODULE_5__["CongratPageComponent"] },
    { path: 'report/:id', component: _generatereport_page_generatereport_page_component__WEBPACK_IMPORTED_MODULE_6__["GeneratereportPageComponent"] },
    { path: 'report/:id/open', component: _open_report_open_report_component__WEBPACK_IMPORTED_MODULE_7__["OpenReportComponent"] },
    { path: 'manageaccount', component: _manage_account_manage_account_component__WEBPACK_IMPORTED_MODULE_8__["ManageAccountComponent"] },
    { path: 'email-verification', component: _email_verification_email_verification_component__WEBPACK_IMPORTED_MODULE_9__["EmailVerificationComponent"] },
    { path: 'guage-chart', component: _guage_chart_guage_chart_component__WEBPACK_IMPORTED_MODULE_10__["GaugeChartComponent"] }
];
var RoutingComponents = [_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"]];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overflow\">\n  <router-outlet></router-outlet>\n  <ngx-spinner type=\"ball-clip-rotate\"></ngx-spinner>\n  <notifier-container></notifier-container>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = /** @class */ (function () {
    function AppComponent(renderer, authService, notifierService, translate) {
        var _this = this;
        this.renderer = renderer;
        this.authService = authService;
        this.notifierService = notifierService;
        this.translate = translate;
        this.title = 'app';
        // style for mac
        this.styleForMacOnly = function () {
            if (navigator.userAgent.indexOf('Mac OS X') != -1) {
                _this.renderer.addClass(document.body, 'mac');
            }
        };
        this.notifier = notifierService;
        // this is default langugage
        translate.setDefaultLang('en');
        // this language is to set by your prefered browser language
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang);
    }
    AppComponent.prototype.ngOnInit = function () {
        this.styleForMacOnly();
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_2__["NotifierService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: HttpLoaderFactory, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/progressbar */ "./node_modules/ngx-bootstrap/progressbar/fesm5/ngx-bootstrap-progressbar.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_bootstrap_popover__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/popover */ "./node_modules/ngx-bootstrap/popover/fesm5/ngx-bootstrap-popover.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ngx_avatar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-avatar */ "./node_modules/ngx-avatar/fesm5/ngx-avatar.js");
/* harmony import */ var angular_gauge_chart__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! angular-gauge-chart */ "./node_modules/angular-gauge-chart/fesm5/angular-gauge-chart.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./sidebar/sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var _signuppage_signuppage_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./signuppage/signuppage.component */ "./src/app/signuppage/signuppage.component.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var ngx_plaid_link__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-plaid-link */ "./node_modules/ngx-plaid-link/fesm5/ngx-plaid-link.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var _congrat_page_congrat_page_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./congrat-page/congrat-page.component */ "./src/app/congrat-page/congrat-page.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _generatereport_page_generatereport_page_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./generatereport-page/generatereport-page.component */ "./src/app/generatereport-page/generatereport-page.component.ts");
/* harmony import */ var _open_report_open_report_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./open-report/open-report.component */ "./src/app/open-report/open-report.component.ts");
/* harmony import */ var _edit_profile_modal_edit_profile_modal_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./edit-profile-modal/edit-profile-modal.component */ "./src/app/edit-profile-modal/edit-profile-modal.component.ts");
/* harmony import */ var _trends_trends_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./trends/trends.component */ "./src/app/trends/trends.component.ts");
/* harmony import */ var _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./transactions/transactions.component */ "./src/app/transactions/transactions.component.ts");
/* harmony import */ var _report_transaction_report_transaction_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./report-transaction/report-transaction.component */ "./src/app/report-transaction/report-transaction.component.ts");
/* harmony import */ var _report_sidebar_report_sidebar_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./report-sidebar/report-sidebar.component */ "./src/app/report-sidebar/report-sidebar.component.ts");
/* harmony import */ var _add_property_modal_add_property_modal_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./add-property-modal/add-property-modal.component */ "./src/app/add-property-modal/add-property-modal.component.ts");
/* harmony import */ var _common_moneyformatter_pipe__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./common/moneyformatter.pipe */ "./src/app/common/moneyformatter.pipe.ts");
/* harmony import */ var _common_datediffformat_pipe__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./common/datediffformat.pipe */ "./src/app/common/datediffformat.pipe.ts");
/* harmony import */ var _common_filter_pipe__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./common/filter.pipe */ "./src/app/common/filter.pipe.ts");
/* harmony import */ var _common_safehtml_pipe__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./common/safehtml.pipe */ "./src/app/common/safehtml.pipe.ts");
/* harmony import */ var _transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./transactions-modal/transactions-modal.component */ "./src/app/transactions-modal/transactions-modal.component.ts");
/* harmony import */ var _common_dateOrdering_pipe__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./common/dateOrdering.pipe */ "./src/app/common/dateOrdering.pipe.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _common_phoneNumber_pipe__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./common/phoneNumber.pipe */ "./src/app/common/phoneNumber.pipe.ts");
/* harmony import */ var _common_searchFilter_pipe__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./common/searchFilter.pipe */ "./src/app/common/searchFilter.pipe.ts");
/* harmony import */ var _common_empty_pipe__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./common/empty.pipe */ "./src/app/common/empty.pipe.ts");
/* harmony import */ var _qr_generate_modal_qr_generate_modal_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./qr-generate-modal/qr-generate-modal.component */ "./src/app/qr-generate-modal/qr-generate-modal.component.ts");
/* harmony import */ var _common_currencyformatter_pipe__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./common/currencyformatter.pipe */ "./src/app/common/currencyformatter.pipe.ts");
/* harmony import */ var _manage_account_manage_account_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./manage-account/manage-account.component */ "./src/app/manage-account/manage-account.component.ts");
/* harmony import */ var _add_employeraddress_modal_add_address_modal_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./add-employeraddress-modal/add-address-modal.component */ "./src/app/add-employeraddress-modal/add-address-modal.component.ts");
/* harmony import */ var _email_verification_email_verification_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./email-verification/email-verification.component */ "./src/app/email-verification/email-verification.component.ts");
/* harmony import */ var _pdf_pdf_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./pdf/pdf.component */ "./src/app/pdf/pdf.component.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
/* harmony import */ var _add_loan_modal_add_loan_modal_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./add-loan-modal/add-loan-modal.component */ "./src/app/add-loan-modal/add-loan-modal.component.ts");
/* harmony import */ var _add_investment_modal_add_investment_modal_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./add-investment-modal/add-investment-modal.component */ "./src/app/add-investment-modal/add-investment-modal.component.ts");
/* harmony import */ var _nisaan_nisaan_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./nisaan/nisaan.component */ "./src/app/nisaan/nisaan.component.ts");
/* harmony import */ var _guage_chart_guage_chart_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./guage-chart/guage-chart.component */ "./src/app/guage-chart/guage-chart.component.ts");
/* harmony import */ var _verify_loan_data_verify_loan_data_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./verify-loan-data/verify-loan-data.component */ "./src/app/verify-loan-data/verify-loan-data.component.ts");
/* harmony import */ var _common_sum_pipe__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./common/sum.pipe */ "./src/app/common/sum.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























































/* Custom angular notifier options */
var customNotifierOptions = {
    position: {
        horizontal: {
            position: 'middle',
            distance: 12
        },
        vertical: {
            position: 'top',
            distance: 12,
            gap: 10
        }
    },
};
var avatarColors = ["#FFB6C1", "#2c3e50", "#95a5a6", "#f39c12", "#1abc9c"];
// AoT requires an exported function for factories
function HttpLoaderFactory(httpClient) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_52__["TranslateHttpLoader"](httpClient);
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_13__["AppComponent"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_14__["RoutingComponents"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_16__["DashboardComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_17__["HeaderComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_18__["SidebarComponent"],
                _signuppage_signuppage_component__WEBPACK_IMPORTED_MODULE_19__["SignuppageComponent"],
                _congrat_page_congrat_page_component__WEBPACK_IMPORTED_MODULE_25__["CongratPageComponent"],
                _generatereport_page_generatereport_page_component__WEBPACK_IMPORTED_MODULE_27__["GeneratereportPageComponent"],
                _open_report_open_report_component__WEBPACK_IMPORTED_MODULE_28__["OpenReportComponent"],
                _edit_profile_modal_edit_profile_modal_component__WEBPACK_IMPORTED_MODULE_29__["EditProfileModalComponent"],
                _trends_trends_component__WEBPACK_IMPORTED_MODULE_30__["TrendsComponent"],
                _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_31__["TransactionsComponent"],
                _report_transaction_report_transaction_component__WEBPACK_IMPORTED_MODULE_32__["ReportTransactionComponent"],
                _report_sidebar_report_sidebar_component__WEBPACK_IMPORTED_MODULE_33__["ReportSidebarComponent"],
                _add_property_modal_add_property_modal_component__WEBPACK_IMPORTED_MODULE_34__["AddPropertyModalComponent"],
                _common_moneyformatter_pipe__WEBPACK_IMPORTED_MODULE_35__["MoneyFormatterPipe"],
                _common_datediffformat_pipe__WEBPACK_IMPORTED_MODULE_36__["DateDiffFormatPipe"],
                _common_sum_pipe__WEBPACK_IMPORTED_MODULE_58__["Sum"],
                _common_filter_pipe__WEBPACK_IMPORTED_MODULE_37__["Filter"],
                _common_safehtml_pipe__WEBPACK_IMPORTED_MODULE_38__["SafeHtml"],
                _common_empty_pipe__WEBPACK_IMPORTED_MODULE_44__["Empty"],
                _common_phoneNumber_pipe__WEBPACK_IMPORTED_MODULE_42__["PhoneNumberPipe"],
                _transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_39__["TransactionsModalComponent"],
                _common_dateOrdering_pipe__WEBPACK_IMPORTED_MODULE_40__["DateOrdering"],
                _common_searchFilter_pipe__WEBPACK_IMPORTED_MODULE_43__["SearchFilterPipe"],
                _profile_profile_component__WEBPACK_IMPORTED_MODULE_41__["ProfileComponent"],
                _qr_generate_modal_qr_generate_modal_component__WEBPACK_IMPORTED_MODULE_45__["QrGenerateModalComponent"],
                _common_currencyformatter_pipe__WEBPACK_IMPORTED_MODULE_46__["LooseCurrencyPipe"],
                _manage_account_manage_account_component__WEBPACK_IMPORTED_MODULE_47__["ManageAccountComponent"],
                _add_employeraddress_modal_add_address_modal_component__WEBPACK_IMPORTED_MODULE_48__["AddAddressModalComponent"],
                _email_verification_email_verification_component__WEBPACK_IMPORTED_MODULE_49__["EmailVerificationComponent"],
                _pdf_pdf_component__WEBPACK_IMPORTED_MODULE_50__["PdfComponent"],
                _add_loan_modal_add_loan_modal_component__WEBPACK_IMPORTED_MODULE_53__["AddLoanModalComponent"],
                _add_investment_modal_add_investment_modal_component__WEBPACK_IMPORTED_MODULE_54__["AddInvestmentModalComponent"],
                _nisaan_nisaan_component__WEBPACK_IMPORTED_MODULE_55__["NisaanComponent"],
                _guage_chart_guage_chart_component__WEBPACK_IMPORTED_MODULE_56__["GaugeChartComponent"],
                _verify_loan_data_verify_loan_data_component__WEBPACK_IMPORTED_MODULE_57__["VerifyLoanDataComponent"]
            ],
            entryComponents: [
                _transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_39__["TransactionsModalComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_14__["AppRoutingModule"],
                ngx_plaid_link__WEBPACK_IMPORTED_MODULE_21__["NgxPlaidLinkModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_23__["HttpModule"],
                angular_web_storage__WEBPACK_IMPORTED_MODULE_24__["AngularWebStorageModule"],
                ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                angular_gauge_chart__WEBPACK_IMPORTED_MODULE_12__["GaugeChartModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_26__["HttpClientModule"],
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__["ModalModule"].forRoot(),
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_5__["BsDatepickerModule"].forRoot(),
                angular_notifier__WEBPACK_IMPORTED_MODULE_9__["NotifierModule"].withConfig(customNotifierOptions),
                ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_6__["ProgressbarModule"].forRoot(),
                ngx_avatar__WEBPACK_IMPORTED_MODULE_11__["AvatarModule"].forRoot({
                    colors: avatarColors
                }),
                ngx_pagination__WEBPACK_IMPORTED_MODULE_10__["NgxPaginationModule"],
                ngx_bootstrap_popover__WEBPACK_IMPORTED_MODULE_8__["PopoverModule"].forRoot(),
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_51__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_51__["TranslateLoader"],
                        useFactory: HttpLoaderFactory,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_26__["HttpClient"]]
                    }
                }),
                angular_gauge_chart__WEBPACK_IMPORTED_MODULE_12__["GaugeChartModule"]
            ],
            providers: [_auth_auth_service__WEBPACK_IMPORTED_MODULE_20__["AuthService"], _backend_backend_service__WEBPACK_IMPORTED_MODULE_22__["BackendService"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_7__["BsModalRef"], _common_moneyformatter_pipe__WEBPACK_IMPORTED_MODULE_35__["MoneyFormatterPipe"], _common_currencyformatter_pipe__WEBPACK_IMPORTED_MODULE_46__["LooseCurrencyPipe"], _common_dateOrdering_pipe__WEBPACK_IMPORTED_MODULE_40__["DateOrdering"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_13__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var auth0_lock__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! auth0-lock */ "./node_modules/auth0-lock/lib/index.js");
/* harmony import */ var auth0_lock__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(auth0_lock__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var ng2_cookies_ng2_cookies__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-cookies/ng2-cookies */ "./node_modules/ng2-cookies/ng2-cookies.js");
/* harmony import */ var ng2_cookies_ng2_cookies__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_cookies_ng2_cookies__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// auth.service.ts










window.global = window;
var AuthService = /** @class */ (function () {
    function AuthService(router, session, http, spinnerService, backend, notifierService) {
        var _this = this;
        this.router = router;
        this.session = session;
        this.http = http;
        this.spinnerService = spinnerService;
        this.backend = backend;
        this.notifierService = notifierService;
        this.USER_PROFILE = "userProfile";
        this.ACCOUNT = 'accounts';
        this.TOKEN = 'tokens';
        this.HOME_PAGE = '/dashboard';
        this.BASE_64 = 'base64';
        this.auth0Options = {
            container: 'logincontainer',
            theme: {
                logo: '/assets/nexus-log.png',
                primaryColor: '#8DC740'
            },
            languageDictionary: {
                title: "",
                signUpTitle: ""
            },
            rememberLastLogin: false,
            auth: {
                redirectUrl: _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].auth.redirect,
                responseType: 'token id_token',
                audience: "https://" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].auth.domain + "/api/v2/",
                params: {
                    scope: _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].auth.scope
                }
            },
            autoclose: true,
            oidcConformant: true,
        };
        this.lock = new auth0_lock__WEBPACK_IMPORTED_MODULE_3___default.a(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].auth.clientID, _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].auth.domain, this.auth0Options);
        this.notifier = notifierService;
        this.lock.on('authenticated', function (authResult) {
            console.log('authResult Info', authResult);
            _this.lock.hide();
            _this.handleSuccessAuth(authResult);
        }); // ...finish implementing authenticated
        this.lock.on('authorization_error', function (error) {
            console.log('something went wrong', error);
            if (error && 'errorDescription' in error && error.errorDescription) {
                _this.notifier.notify('error', error.errorDescription);
            }
            _this.lock.show();
        });
    }
    AuthService.prototype.handleSuccessAuth = function (authResult) {
        var _this = this;
        this.spinnerService.show();
        this.lock.hide();
        this.setDataInCache(this.TOKEN, authResult);
        var userProfile = {
            id: authResult.idTokenPayload.sub
        };
        // Get userDetails
        var path = "/users/" + authResult.idTokenPayload.sub + '?expand=accounts&sync=true';
        this.backend.get(path, authResult.accessToken)
            .subscribe(function (data) {
            console.log('getUserProfile', data.json());
            userProfile = Object.assign(userProfile, data.json());
            // if pricture has 'NA
            if (userProfile.picture == "NA") {
                var collectName = [];
                if ('first_name' in userProfile && userProfile.first_name) {
                    collectName.push(userProfile.first_name[0]);
                }
                if ('last_name' in userProfile && userProfile.last_name) {
                    collectName.push(userProfile.last_name[0]);
                }
                userProfile.picture = collectName.join('');
            }
            _this.setDataInCache(_this.USER_PROFILE, userProfile);
            _this.spinnerService.hide();
            _this.router.navigate([_this.HOME_PAGE]); // go to the home route
        }, function (error) {
            console.log(error);
            _this.router.navigate(['/']);
            var errorVar = error.json();
            _this.notifier.notify('error', "Message from Server: " + errorVar["message"]);
            _this.spinnerService.hide();
        });
    };
    AuthService.prototype.showLogin = function () {
        this.lock.show();
    };
    AuthService.prototype.checkAuthentication = function (redirect, hash) {
        var that = this;
        this.spinnerService.show();
        this.lock.hide();
        this.lock.checkSession({}, function (err, authResult) {
            // handle error or new tokens
            console.log(err, redirect, hash);
            if (!(hash && hash.includes('access_token'))) {
                if (err) {
                    if (redirect) {
                        that.router.navigate(['/']);
                    }
                    else {
                        that.lock.show();
                        that.spinnerService.hide();
                        return;
                    }
                }
                console.log(authResult);
                if (authResult) {
                    that.handleSuccessAuth(authResult);
                }
            }
        });
    };
    AuthService.prototype.getUserProfile = function () {
        // console.log("Session Call$$$$", this.session.get(this.USER_PROFILE));
        return this.session.get(this.USER_PROFILE);
    };
    AuthService.prototype.getAccessTokenAndID = function () {
        return this.session.get(this.TOKEN);
    };
    AuthService.prototype.getDataFromCache = function (key) {
        return this.session.get(key);
    };
    AuthService.prototype.setDataInCache = function (key, data) {
        this.session.set(key, data, 2, 'h');
    };
    AuthService.prototype.sessionCleared = function () {
        this.session.remove(this.USER_PROFILE);
        this.session.remove(this.ACCOUNT);
        this.session.remove(this.TOKEN);
        this.session.remove(this.HOME_PAGE);
    };
    AuthService.prototype.cookieClear = function () {
        ng2_cookies_ng2_cookies__WEBPACK_IMPORTED_MODULE_9__["Cookie"].delete(this.USER_PROFILE);
        ng2_cookies_ng2_cookies__WEBPACK_IMPORTED_MODULE_9__["Cookie"].delete(this.ACCOUNT);
        ng2_cookies_ng2_cookies__WEBPACK_IMPORTED_MODULE_9__["Cookie"].delete(this.TOKEN);
        ng2_cookies_ng2_cookies__WEBPACK_IMPORTED_MODULE_9__["Cookie"].delete(this.HOME_PAGE);
    };
    AuthService.prototype.getUserLogout = function () {
        this.sessionCleared();
        this.cookieClear();
        this.session.clear();
        this.lock.logout({
            returnTo: _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].auth.redirect
        });
        return;
    };
    AuthService.prototype.login = function () {
        // Auth0 authorize request
        this.lock.show();
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], angular_web_storage__WEBPACK_IMPORTED_MODULE_4__["SessionStorageService"], _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"], ngx_spinner__WEBPACK_IMPORTED_MODULE_6__["NgxSpinnerService"], _backend_backend_service__WEBPACK_IMPORTED_MODULE_7__["BackendService"], angular_notifier__WEBPACK_IMPORTED_MODULE_8__["NotifierService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/backend/backend.service.ts":
/*!********************************************!*\
  !*** ./src/app/backend/backend.service.ts ***!
  \********************************************/
/*! exports provided: BackendService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackendService", function() { return BackendService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BackendService = /** @class */ (function () {
    function BackendService(http, _http) {
        this.http = http;
        this._http = _http;
    }
    BackendService_1 = BackendService;
    BackendService.prototype.post = function (data, path, token) {
        console.log('Inside post');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append("Authorization", token);
        headers.append("content-type", "application/json");
        var body = data;
        var url = BackendService_1.API_BASE_URL + path;
        return this.http.post(url, body, {
            headers: headers
        });
    };
    ;
    BackendService.prototype.postHandlerForMeter = function (data) {
        console.log('Inside post');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append("content-type", "application/json");
        var body = data;
        var url = BackendService_1.NEXUS_SCORE_METER_URL;
        return this.http.post(url, body, {
            headers: headers
        });
    };
    ;
    BackendService.prototype.delete = function (path, token) {
        console.log('Inside delete');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append("Authorization", token);
        headers.append("content-type", "application/json");
        var url = BackendService_1.API_BASE_URL + path;
        return this.http.delete(url, { headers: headers });
    };
    ;
    BackendService.prototype.put = function (data, path, token) {
        console.log('Inside put');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append("Authorization", token);
        var body = data;
        headers.append("content-type", "application/json");
        var url = BackendService_1.API_BASE_URL + path;
        return this.http.put(url, body, {
            headers: headers
        });
    };
    ;
    BackendService.prototype.get = function (path, token) {
        console.log('Inside get');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        if (token) {
            headers.append("Authorization", token);
        }
        headers.append("content-type", "application/json");
        var url = BackendService_1.API_BASE_URL + path;
        return this.http.get(url, {
            headers: headers
        });
    };
    ;
    BackendService.prototype.syncTransaction = function (path, token) {
        console.log('Inside syncTransaction');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append("Authorization", token);
        headers.append("content-type", "application/json");
        headers.append("Access-Control-Allow-Origin", "*");
        headers.append('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
        headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept,X-Requested-With');
        var url = BackendService_1.API_BASE_URL + path;
        return this.http.post(url, { headers: headers });
    };
    ;
    BackendService.API_BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apibaseurl;
    BackendService.NEXUS_SCORE_METER_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].nexusscoremeterurl;
    BackendService = BackendService_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], BackendService);
    return BackendService;
    var BackendService_1;
}());



/***/ }),

/***/ "./src/app/common/currencyformatter.pipe.ts":
/*!**************************************************!*\
  !*** ./src/app/common/currencyformatter.pipe.ts ***!
  \**************************************************/
/*! exports provided: LooseCurrencyPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LooseCurrencyPipe", function() { return LooseCurrencyPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var _NUMBER_FORMAT_REGEXP = /^(\d+)?\.((\d+)(-(\d+))?)?$/;
var LooseCurrencyPipe = /** @class */ (function (_super) {
    __extends(LooseCurrencyPipe, _super);
    function LooseCurrencyPipe() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LooseCurrencyPipe.prototype.transform = function (value, currencyCode, symbolDisplay, digits) {
        if (typeof value === 'number' || _NUMBER_FORMAT_REGEXP.test(value)) {
            return _super.prototype.transform.call(this, value, currencyCode, symbolDisplay, digits);
        }
        else {
            return value;
        }
    };
    LooseCurrencyPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'currencyformatter' })
    ], LooseCurrencyPipe);
    return LooseCurrencyPipe;
}(_angular_common__WEBPACK_IMPORTED_MODULE_1__["CurrencyPipe"]));



/***/ }),

/***/ "./src/app/common/dateOrdering.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/common/dateOrdering.pipe.ts ***!
  \*********************************************/
/*! exports provided: DateOrdering */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateOrdering", function() { return DateOrdering; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DateOrdering = /** @class */ (function () {
    function DateOrdering() {
    }
    DateOrdering.prototype.transform = function (array, args) {
        if (!args[0]) {
            return array;
        }
        var direction = args[0][0];
        var column = args.replace('-', '');
        array.sort(function (a, b) {
            var left = Number(moment__WEBPACK_IMPORTED_MODULE_1__["utc"](a[column]));
            var right = Number(moment__WEBPACK_IMPORTED_MODULE_1__["utc"](b[column]));
            //  if(moment.utc(a[column], 'DD-MM-YYYY').isValid() || (moment.utc(a[column], 'MM-DD-YYYY').isValid())){
            //      if(moment.utc(a[column], 'DD-MM-YYYY').isValid()){
            //          left = Number(moment.utc(a[column], 'DD-MM-YYYY'));
            //          right = Number(moment.utc(b[column], 'DD-MM-YYYY'));
            //      }else if(moment.utc(a[column], 'MM-DD-YYYY').isValid()){
            //         left = Number(moment.utc(a[column], 'MM-DD-YYYY'));
            //         right = Number(moment.utc(b[column], 'MM-DD-YYYY'));
            //      }
            //  }else{
            //      left = Number(moment.utc(a[column]));
            //      right = Number(moment.utc(b[column]));
            // }
            return (direction === "-") ? right - left : left - right;
        });
        return array;
    };
    DateOrdering = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: "orderBydate"
        })
    ], DateOrdering);
    return DateOrdering;
}());



/***/ }),

/***/ "./src/app/common/datediffformat.pipe.ts":
/*!***********************************************!*\
  !*** ./src/app/common/datediffformat.pipe.ts ***!
  \***********************************************/
/*! exports provided: DateDiffFormatPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateDiffFormatPipe", function() { return DateDiffFormatPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DateDiffFormatPipe = /** @class */ (function () {
    function DateDiffFormatPipe(translate) {
        this.translate = translate;
    }
    DateDiffFormatPipe.prototype.transform = function (date) {
        // Start from days
        var now = moment__WEBPACK_IMPORTED_MODULE_1__["utc"]();
        var dateInMoment = moment__WEBPACK_IMPORTED_MODULE_1__["utc"](date);
        var diffInDays = dateInMoment.diff(now, 'days');
        var diffInHours = dateInMoment.diff(now, 'hours');
        if (diffInDays > 0) {
            // Show upto 2 units
            var remainingHours_1 = diffInHours - (diffInDays * 24);
            var formatstring_1;
            this.translate.get("DF_DAYS").subscribe(function (value) {
                formatstring_1 = diffInDays + " " + value;
            });
            if (remainingHours_1 > 0) {
                this.translate.get("DF_HOURS").subscribe(function (value) {
                    formatstring_1 += " " + remainingHours_1 + " " + value;
                });
            }
            return formatstring_1;
        }
        // Check diff in hours
        var diffInMinutes = dateInMoment.diff(now, 'minutes');
        if (diffInHours > 0) {
            // Show upto 2 units
            var remaining_1 = diffInMinutes - (diffInHours * 60);
            var formatstring_2;
            this.translate.get("DF_HOURS").subscribe(function (value) {
                formatstring_2 = diffInHours + " " + value;
            });
            if (remaining_1 > 0) {
                this.translate.get("DF_MINUTES").subscribe(function (value) {
                    formatstring_2 += " " + remaining_1 + " " + value;
                });
            }
            return formatstring_2;
        }
        // Check diff in seconds
        var diffInSecs = dateInMoment.diff(now, 'seconds');
        if (diffInMinutes > 0) {
            // Show upto 2 units
            var formatstring_3;
            var remaining_2 = diffInSecs - (diffInMinutes * 60);
            this.translate.get("DF_MINUTES").subscribe(function (value) {
                formatstring_3 = diffInMinutes + " " + value;
            });
            if (remaining_2 > 0) {
                this.translate.get("DF_SECONDS").subscribe(function (value) {
                    formatstring_3 += " " + remaining_2 + " " + value;
                });
            }
            return formatstring_3;
        }
        this.translate.get("DF_SECONDS").subscribe(function (value) {
            return diffInSecs + " " + value;
        });
    };
    DateDiffFormatPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'datediffformat' }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
    ], DateDiffFormatPipe);
    return DateDiffFormatPipe;
}());



/***/ }),

/***/ "./src/app/common/empty.pipe.ts":
/*!**************************************!*\
  !*** ./src/app/common/empty.pipe.ts ***!
  \**************************************/
/*! exports provided: Empty */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Empty", function() { return Empty; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Empty = /** @class */ (function () {
    function Empty() {
    }
    Empty.prototype.transform = function (value, emptyText) {
        if (emptyText === void 0) { emptyText = 'NO ITEMS'; }
        return value && value.length > 0 ? value : [{ emptyMessage: emptyText }];
    };
    Empty = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'empty'
        })
    ], Empty);
    return Empty;
}());



/***/ }),

/***/ "./src/app/common/filter.pipe.ts":
/*!***************************************!*\
  !*** ./src/app/common/filter.pipe.ts ***!
  \***************************************/
/*! exports provided: Filter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Filter", function() { return Filter; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Filter = /** @class */ (function () {
    function Filter() {
    }
    Filter.prototype.transform = function (items, field, value) {
        if (!items)
            return [];
        if (!value || value.length == 0)
            return items;
        return items.filter(function (it) {
            return it[field].toLowerCase().indexOf(value.toLowerCase()) != -1;
        });
    };
    Filter = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filter'
        })
    ], Filter);
    return Filter;
}());



/***/ }),

/***/ "./src/app/common/moneyformatter.pipe.ts":
/*!***********************************************!*\
  !*** ./src/app/common/moneyformatter.pipe.ts ***!
  \***********************************************/
/*! exports provided: MoneyFormatterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoneyFormatterPipe", function() { return MoneyFormatterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MoneyFormatterPipe = /** @class */ (function () {
    function MoneyFormatterPipe() {
        this.SI_SYMBOL = ["", "k", "M", "G", "T", "P", "E"];
    }
    MoneyFormatterPipe.prototype.transform = function (amount) {
        // what tier? (determines SI symbol)
        var tier = Math.log10(amount) / 3 | 0;
        // if zero, we don't need a suffix
        if (tier == 0)
            return amount;
        // get suffix and determine scale
        var suffix = this.SI_SYMBOL[tier];
        var scale = Math.pow(10, tier * 3);
        // scale the number
        var scaled = amount / scale;
        // format number and add suffix
        return scaled.toFixed(1) + suffix;
    };
    MoneyFormatterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'moneyformatter' })
    ], MoneyFormatterPipe);
    return MoneyFormatterPipe;
}());



/***/ }),

/***/ "./src/app/common/phoneNumber.pipe.ts":
/*!********************************************!*\
  !*** ./src/app/common/phoneNumber.pipe.ts ***!
  \********************************************/
/*! exports provided: PhoneNumberPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneNumberPipe", function() { return PhoneNumberPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var PhoneNumberPipe = /** @class */ (function () {
    function PhoneNumberPipe() {
    }
    PhoneNumberPipe.prototype.transform = function (phoneNum) {
        var cleaned = ('' + phoneNum).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{0,9})$/);
        return match ? '(' + match[1] + ') ' + match[2] + '-' + match[3] : '';
    };
    PhoneNumberPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'phoneNumber'
        })
    ], PhoneNumberPipe);
    return PhoneNumberPipe;
}());



/***/ }),

/***/ "./src/app/common/safehtml.pipe.ts":
/*!*****************************************!*\
  !*** ./src/app/common/safehtml.pipe.ts ***!
  \*****************************************/
/*! exports provided: SafeHtml */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafeHtml", function() { return SafeHtml; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SafeHtml = /** @class */ (function () {
    function SafeHtml(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafeHtml.prototype.transform = function (html) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(html);
    };
    SafeHtml = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'safeHtml' }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"]])
    ], SafeHtml);
    return SafeHtml;
}());



/***/ }),

/***/ "./src/app/common/searchFilter.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/common/searchFilter.pipe.ts ***!
  \*********************************************/
/*! exports provided: SearchFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchFilterPipe", function() { return SearchFilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};

var SearchFilterPipe = /** @class */ (function () {
    function SearchFilterPipe() {
    }
    SearchFilterPipe.prototype.transform = function (items, searchText) {
        var retArray = [];
        if (!items)
            return [];
        if (!searchText) {
            return items;
        }
        else {
            try {
                for (var items_1 = __values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
                    var row = items_1_1.value;
                    if (row.name.trim().toLowerCase().includes(searchText.trim().toLowerCase())) {
                        retArray.push(row);
                    }
                    else if (row.category.trim().toLowerCase().includes(searchText.trim().toLowerCase())) {
                        retArray.push(row);
                    }
                    else if (row.officialName.trim().toLowerCase().includes(searchText.trim().toLowerCase())) {
                        retArray.push(row);
                    }
                    else if (row.amount) {
                        if (row.amount.toString().toLowerCase().includes(searchText.trim().toLowerCase())) {
                            retArray.push(row);
                        }
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (items_1_1 && !items_1_1.done && (_a = items_1.return)) _a.call(items_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            return retArray;
        }
        var e_1, _a;
    };
    SearchFilterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'searchFilter'
        })
    ], SearchFilterPipe);
    return SearchFilterPipe;
}());



/***/ }),

/***/ "./src/app/common/sum.pipe.ts":
/*!************************************!*\
  !*** ./src/app/common/sum.pipe.ts ***!
  \************************************/
/*! exports provided: Sum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sum", function() { return Sum; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};

var Sum = /** @class */ (function () {
    function Sum() {
    }
    Sum.prototype.transform = function (items, field, abs) {
        if (!items)
            return 0;
        var sum = 0;
        try {
            for (var items_1 = __values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
                var item = items_1_1.value;
                sum += item[field];
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (items_1_1 && !items_1_1.done && (_a = items_1.return)) _a.call(items_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        if (abs) {
            return Math.abs(sum);
        }
        return sum;
        var e_1, _a;
    };
    Sum = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'sum'
        })
    ], Sum);
    return Sum;
}());



/***/ }),

/***/ "./src/app/common/utils.ts":
/*!*********************************!*\
  !*** ./src/app/common/utils.ts ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};

// This the common utils classes for some common functions
var Utils = /** @class */ (function () {
    function Utils() {
    }
    // This function is used to filter spending transactions and
    // return transactions which are valid for spending
    Utils.filterSpendingTransactions = function (transactions) {
        // spending calculation
        var ignoreCatog = ['Transfer,Internal Account Transfer', 'Payment,Credit Card'];
        var spendTrans = transactions.filter(function (t) { return t.amount && t.amount > 0 && t.account_type == 'depository' && (!ignoreCatog.includes(t.category)); });
        // credit card
        var ignoredCreditCategories = ["Transfer,Credit"];
        var creditCardTransactions = transactions.filter(function (t) { return t.account_type === 'credit' && (!(ignoredCreditCategories.includes(t.category)) || (ignoredCreditCategories.includes(t.category))) && t.amount > 0; });
        // End of spending calculation
        return spendTrans.concat(creditCardTransactions);
    };
    // This function is used to group chart data by format
    Utils.groupGraphDataByFormat = function (transactions, dates, key, dateFormat) {
        var graphData = Utils.createEmptyGraphDataByDuration(dates);
        if (!dateFormat) {
            dateFormat = Utils.calculateDuration(dates).dateFormat;
        }
        try {
            for (var transactions_1 = __values(transactions), transactions_1_1 = transactions_1.next(); !transactions_1_1.done; transactions_1_1 = transactions_1.next()) {
                var t = transactions_1_1.value;
                var tranxDate = moment__WEBPACK_IMPORTED_MODULE_0__["utc"](t.date);
                var formatedDate = tranxDate.format(dateFormat);
                if (formatedDate in graphData) {
                    graphData[formatedDate].amount += Math.round(Math.abs(t[key]) * 100) / 100;
                    graphData[formatedDate].transactionArr.push(t);
                }
                else {
                    graphData[formatedDate] = {};
                    graphData[formatedDate].amount = Math.round(Math.abs(t[key]) * 100) / 100;
                    graphData[formatedDate].transactionArr = [t];
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (transactions_1_1 && !transactions_1_1.done && (_a = transactions_1.return)) _a.call(transactions_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return graphData;
        var e_1, _a;
    };
    // This function is used to filter incoming transactions and
    // return transactions which are valid for income
    Utils.filterIncomeTransactions = function (transactions) {
        var ignoredCategories = ["Transfer,Internal Account Transfer", "Transfer,Credit"];
        var filterData = [];
        if (transactions && transactions.length > 0) {
            try {
                for (var transactions_2 = __values(transactions), transactions_2_1 = transactions_2.next(); !transactions_2_1.done; transactions_2_1 = transactions_2.next()) {
                    var t = transactions_2_1.value;
                    if (t.category && t.account_type == 'depository' && t.amount < 0 && !(ignoredCategories.includes(t.category))) {
                        filterData.push(t);
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (transactions_2_1 && !transactions_2_1.done && (_a = transactions_2.return)) _a.call(transactions_2);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        return filterData;
        var e_2, _a;
    };
    // This method is used to validate if a key is present in a JSON
    Utils.isNullOrEmpty = function (json, key) {
        if (json && key in json && json[key] != null) {
            if (typeof json[key] == 'string') {
                if (!(json[key] == '')) {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        return true;
    };
    // This function is used to merge address
    Utils.constructAddress = function (addressJSON) {
        var address = [];
        if (!Utils.isNullOrEmpty(addressJSON, 'streetAddress')) {
            address.push(addressJSON.streetAddress);
        }
        if (!Utils.isNullOrEmpty(addressJSON, 'addressLocality')) {
            address.push(addressJSON.addressLocality);
        }
        if (!Utils.isNullOrEmpty(addressJSON, 'addressRegion')) {
            address.push(addressJSON.addressRegion);
        }
        if (!Utils.isNullOrEmpty(addressJSON, 'postalCode')) {
            address.push(addressJSON.postalCode);
        }
        return address.join(', ');
    };
    Utils.cloneJSON = function (json) {
        return JSON.parse(JSON.stringify(json));
    };
    Utils.roundTo2Digit = function (number) {
        return (Math.round(number * 100) / 100);
    };
    Utils.calculateDuration = function (dates) {
        var end_date = moment__WEBPACK_IMPORTED_MODULE_0__["utc"](dates.end_date);
        var start_date = moment__WEBPACK_IMPORTED_MODULE_0__["utc"](dates.start_date);
        var dateFormat = 'MMM YYYY'; // Default to monthy view
        var duration = 'month';
        if ((end_date.diff(start_date, 'months') == 0) || (end_date.diff(start_date, 'days') < 15)) {
            dateFormat = 'MM-DD-YYYY';
            duration = 'days';
        }
        return { dateFormat: dateFormat, duration: duration };
    };
    // This function is used to create empty graph data
    Utils.createEmptyGraphDataByDuration = function (dates) {
        var loop_start_date = moment__WEBPACK_IMPORTED_MODULE_0__["utc"](dates.start_date);
        var end_date = moment__WEBPACK_IMPORTED_MODULE_0__["utc"](dates.end_date);
        var dateAndDuration = Utils.calculateDuration(dates);
        var dateFormat = dateAndDuration.dateFormat; // Default to monthy view
        var interval = -1;
        var duration = dateAndDuration.duration;
        if (duration === 'month') {
            end_date = end_date.endOf('month');
        }
        var graphData = {};
        for (var l = loop_start_date; l.isSameOrBefore(end_date); l.subtract(interval, duration)) {
            var formatedDate = l.format(dateFormat);
            graphData[formatedDate] = { amount: 0, transactionArr: [] };
        }
        return graphData;
    };
    // This function is used to calculate diffrence in years and months
    Utils.diffYearMonthes = function (year, months, days) {
        if (year > 0) {
            var remainingMonths = months - (year * 12);
            var formatstring = void 0;
            if (year <= 1) {
                formatstring = year + ' year';
            }
            else {
                formatstring = year + ' years';
            }
            if (remainingMonths >= 0) {
                formatstring += ' and ' + remainingMonths + ' months';
            }
            return formatstring;
        }
        else {
            return (months + ' months');
        }
    };
    return Utils;
}());
/* harmony default export */ __webpack_exports__["default"] = (Utils);


/***/ }),

/***/ "./src/app/congrat-page/congrat-page.component.css":
/*!*********************************************************!*\
  !*** ./src/app/congrat-page/congrat-page.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".custom-center{\n  width: auto;\n  height: 100px;\n  position: absolute;\n  top:0;\n  bottom: 0;\n  left: 50%;\n  right: 0;\n  margin: auto;\n}\n.star-color{\n  font-size: 28px;\n  color: #1686e8;\n  width: 40px;\n}\n.c-text{\n  color: #1686e8;\n  font-size: 30px;\n}\n.line-txt{\n  color: #797777;\n  font-size: 12px;\n}\n.prize{\n  height: 170px;\n}\n.custom-input-button{\n  border-radius: 0 50rem 50rem 0;\n  border-left: 0px;\n  border: 1px solid #ced4da;\n  border-left: 0px;\n}\n.custom-radius{\n  border-radius: 50rem 0 0 50rem;\n  border-right: 0px;\n}\n.icon-color{\n  color: #a7a6a2;\n}\n.a-texture{\n  color: #1686e8 !important;\n  text-decoration:underline !important;\n}\n.a-texture:hover{\n  cursor:pointer;\n}\n.account-btn{\n  background-color: rgb(0, 133, 228) !important;\n  padding: 12px 30px;\n}\n"

/***/ }),

/***/ "./src/app/congrat-page/congrat-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/congrat-page/congrat-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container h-100\">\n  <div class=\"row h-100 justify-content-center align-items-center\">\n    <div align=\"center\" class=\"mt-4\">\n      <img src=\"assets/trophy.gif\" alt=\"imgpic\" class=\"prize\"/>\n      <p class=\"custom-text-align c-text\">Congratulations!</p>\n      <div class=\"custom-text-align line-txt\">You just link your {{accountsLength}} account! Average users like you link only 5 accounts. Way to go!</div>\n      <div class=\"custom-text-align line-txt\">The more accounts your link, the stronger your profile is</div>\n      <div class=\"custom-text-align\">\n        <label class=\"star-color\" *ngIf=\"!firstStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n        <label class=\"star-color\" *ngIf=\"!secondStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n        <label class=\"star-color\" *ngIf=\"!thirdStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n        <label class=\"star-color\" *ngIf=\"!fourthStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n        <label class=\"star-color\" *ngIf=\"!fifthStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n\n        <label class=\"star-color\" *ngIf=\"firstStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n        <label class=\"star-color\" *ngIf=\"secondStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n        <label class=\"star-color\" *ngIf=\"thirdStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n        <label class=\"star-color\" *ngIf=\"fourthStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n        <label class=\"star-color\" *ngIf=\"fifthStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n      </div>\n      <div class=\"mt-3 mb-3\">\n        <!-- <mr-ngx-plaid-link-button env=\"sandbox\" publicKey=\"58b5868dead2e3931a909a866b60ca\" (Success)=\"onPlaidSuccess($event)\"\n                                  className=\"launch-plaid-link-button\"\n                                  buttonText=\"Add another account\"></mr-ngx-plaid-link-button> -->\n        <button class=\"btn btn-info btn-lg account-btn\">Add another account</button>\n      </div>\n      <div class=\"col-md-12 mt-3\">\n        <p class=\"line-txt\" align=\"right\">I'm done Linking Accounts <a class=\"a-texture\" (click)=\"goToDashboard($event)\">Go to Dashboard</a></p>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/congrat-page/congrat-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/congrat-page/congrat-page.component.ts ***!
  \********************************************************/
/*! exports provided: CongratPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CongratPageComponent", function() { return CongratPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CongratPageComponent = /** @class */ (function () {
    function CongratPageComponent(authService, backend, session, router, spinnerService, notifierService, translate) {
        this.authService = authService;
        this.backend = backend;
        this.session = session;
        this.router = router;
        this.spinnerService = spinnerService;
        this.notifierService = notifierService;
        this.translate = translate;
        this.accounts = {};
        this.firstStar = true;
        this.secondStar = true;
        this.thirdStar = true;
        this.fourthStar = true;
        this.fifthStar = true;
        this.notifier = notifierService;
    }
    CongratPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.spinnerService.show();
        this.user = this.authService.getUserProfile();
        var tokenID = this.authService.getAccessTokenAndID();
        this.accounts = this.user.accounts;
        console.log('accounts', this.accounts);
        this.accountsLength = this.accounts.length;
        if (this.accountsLength === 1) {
            var path = "/users/" + tokenID.idTokenPayload.sub;
            this.backend.get(path, tokenID.accessToken)
                .subscribe(function (data) {
                console.log('getUserProfile', data.json());
                _this.user = Object.assign(_this.user, data.json());
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.user);
                _this.spinnerService.hide();
            }, function (error) {
                console.log(error);
                _this.router.navigate(['/']);
                var errorVar = error.json();
                _this.translate.get('DASH_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + errorVar["message"]);
                });
                _this.spinnerService.hide();
            });
        }
        switch (this.accountsLength) {
            case 1: {
                this.firstStar = false;
                this.secondStar = true;
                this.thirdStar = true;
                this.fourthStar = true;
                this.fifthStar = true;
                this.accountsLength = this.accountsLength + 'st';
                break;
            }
            case 2: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = true;
                this.fourthStar = true;
                this.fifthStar = true;
                this.accountsLength = this.accountsLength + 'nd';
                break;
            }
            case 3: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = false;
                this.fourthStar = true;
                this.fifthStar = true;
                this.accountsLength = this.accountsLength + 'rd';
                break;
            }
            case 4: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = false;
                this.fourthStar = false;
                this.fifthStar = true;
                this.accountsLength = this.accountsLength + 'th';
                break;
            }
            case 5: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = false;
                this.fourthStar = false;
                this.fifthStar = false;
                this.accountsLength = this.accountsLength + 'th';
                break;
            }
            default: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = false;
                this.fourthStar = false;
                this.fifthStar = false;
                this.accountsLength = this.accountsLength + 'th';
                break;
            }
        }
        this.spinnerService.hide();
    };
    CongratPageComponent.prototype.goToDashboard = function (event) {
        console.log(event);
        this.router.navigate(['/dashboard']); // go to the dashboard
    };
    CongratPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-congrat-page',
            template: __webpack_require__(/*! ./congrat-page.component.html */ "./src/app/congrat-page/congrat-page.component.html"),
            styles: [__webpack_require__(/*! ./congrat-page.component.css */ "./src/app/congrat-page/congrat-page.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _backend_backend_service__WEBPACK_IMPORTED_MODULE_3__["BackendService"], angular_web_storage__WEBPACK_IMPORTED_MODULE_4__["SessionStorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"], angular_notifier__WEBPACK_IMPORTED_MODULE_6__["NotifierService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"]])
    ], CongratPageComponent);
    return CongratPageComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".blank-display{\n  position: absolute;\n  background: #353434d4;\n  height: 250%;\n  width: 100%;\n  z-index: 1000;\n  cursor: none;\n}\n.profile-img{\n  width:100%;\n}\n.ul-height{\n  height:43px;\n}\n.underline{\n  text-decoration:underline !important;\n  color: #007bff !important;\n}\n.nav-tabs .active{\n  border-bottom:2px solid #F5BF13;\n  color:#F5BF13 !important;\n}\n.sidebar-bg{\n  /* background: #dcdcdc; */\n}\n.share-profile-text{\n  font-size: 15px;\n  color: #5f5b5b;\n}\n.ml-4{\n  margin-left: 4px;\n}\n.custom-select{\n  width: 30% ;\n  border-radius: 0px ;\n  height: 32px ;\n  font-size: 14px;\n}\n.clock-custom{\n  width:70%;\n}\n.h-style{\n  font-size: 20px;\n  font-weight: 600;\n  line-height:2 ;\n}\n.generate-report{\n  height: 52px;\n  background: #dc8925;\n  color: white;\n  font-size: 14px;\n}\n.mb-10{\n  margin-bottom: 5%;\n}\n.tabs-border{\n  border:0px;\n  margin:5px 0;\n  background: transparent;\n}\n/* .d-icon{\n  margin-right: 5px;\n} */\n.cash-font{\n  font-size:12px;\n  font-weight:600;\n}\n/*toggle css*/\n.switch {\n  position: relative;\n  display: inline-block;\n  width: 60px;\n  height: 34px;\n}\n/* Hide default HTML checkbox */\n.switch input {\n  opacity: 0;\n  width: 0;\n  height: 0;\n}\n/* The slider */\n.slider {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: #ccc;\n  transition: .4s;\n}\n.slider:before {\n  position: absolute;\n  content: \"\";\n  height: 26px;\n  width: 26px;\n  left: 4px;\n  bottom: 4px;\n  background-color: white;\n  transition: .4s;\n}\ninput:checked + .slider {\n  background-color: #46a81b;\n}\ninput:focus + .slider {\n  box-shadow: none;\n}\ninput:checked + .slider:before {\n  -webkit-transform: translateX(26px);\n  transform: translateX(26px);\n}\n/* Rounded sliders */\n.slider.round {\n  border-radius: 34px;\n}\n.slider.round:before {\n  border-radius: 50%;\n}\n/*end toggle css*/\n.report-btn{\n  white-space: nowrap;\n  position: relative;\n  top:46%;\n}\n.report-div{\n  background: #ffffff;\n  border: 1px solid #666666;\n}\n.report-div .row{\n  margin-left: 0;\n  margin-right: 0;\n}\n.email-input{\n  padding: 5px;\n}\n.send-input{\n  padding: 5px 10px 5px 10px;\n}\n.report-div .pl-line{\n  padding-left:12px;\n}\n.icon-size i{\n  font-size: 20px;\n  vertical-align: -webkit-baseline-middle;\n}\n@media (max-width: 767px){\n  .sidebar-bg.pr-0 {\n    padding-right:15px !important;\n  }\n}\n@media (max-width: 575px){\n  .profile-img{\n    width:30%;\n  }\n  .content-padding{\n      padding-left:15px !important;\n      padding-right:15px !important;\n  }\n}\n@media screen and (min-width:320px) and (max-width:411px){\n  .nav-item .nav-link {\n    padding: .5rem 0.3rem;\n  }\n  .width-100{\n    max-width:100% !important;\n  }\n  .width-100 .scan-code{\n    width: 100%;\n  }\n  .width-100 .report-btn{\n    top:0%;\n    margin-left: 12px;\n  }\n}\n.border-primary-none{\n    background-color:#DCDCDC !important;\n}\n.table.table-hover .scroll-tbl{\n  overflow-y: auto;\n  max-height: 360px;\n  display: block;\n}\n.checkbox-top{\n  top:0px;\n}\n.progress-div{\n  background: #4a4949;\n  color: #ffffff;\n  margin-bottom: 0;\n}\n.progress-div .row{\n  margin-left: 0;\n  margin-right: 0;\n}\n.progress{\n  height: 0.6rem;\n}\n.table-faildAccount td:first-child{\n  vertical-align: middle;\n}\n.width-20{\n  width:25%;\n  font-size:larger;\n}\n.border-red-left{\n  border-left:2px solid red !important;\n}\n.coming-soon-icon{\n  color:red;\n  font-size:10px;\n  font-weight:bold;\n}\n@media only screen and (max-width: 600px) {\n  .width-20{\n    font-size:11px ;\n  }\n}\n.nav-position{\n   position:fixed ;\n   width:76%;\n   z-index:1 ;\n   top:61px;\n   /* height: 45px; */\n}\n.upper-margin{\n   margin-top:112px;\n}\n.margin-zero{\n   margin-left:0px;\n   margin-right:0px;\n}\n.font-size{\n   font-size:22px;\n}\n.padding-right{\n   padding-right:0px ;\n}\n.padding-left{\n   padding-left:0px ;\n}\n.Generate-Report-btn{\n   background-color:#46a81b ;\n   border: 0px;\n   padding: 8px;\n}\n.border-right-red{\n   border-right:1px solid #000000 ;\n}\n/* @media only screen and (max-width: 600px) {\n  .nav-position{\n       position:unset;\n       width:unset;\n       z-index:unset;\n       top:unset;\n  }\n  .sidebar-bg{\n    background: unset;\n    margin-top:0px;\n  }\n  .upper-margin{\n    margin-top:0px !important;\n  }\n}  */\n/* style for Congratulations screen */\n.custom-center{\n  width: auto;\n  height: 100px;\n  position: absolute;\n  top:0;\n  bottom: 0;\n  left: 50%;\n  right: 0;\n  margin: auto;\n}\n.star-color{\n  font-size: 28px;\n  color: #1686e8;\n  width: 40px;\n}\n.c-text{\n  color: #1686e8;\n  font-size: 30px;\n}\n.line-txt{\n  color: #797777;\n  font-size: 12px;\n}\n.prize{\n  height: 170px;\n}\n.custom-input-button{\n  border-radius: 0 50rem 50rem 0;\n  border-left: 0px;\n  border: 1px solid #ced4da;\n  border-left: 0px;\n}\n.custom-radius{\n  border-radius: 50rem 0 0 50rem;\n  border-right: 0px;\n}\n.icon-color{\n  color: #a7a6a2;\n}\n.a-texture{\n  color: #1686e8 !important;\n  text-decoration:underline !important;\n  cursor:pointer !important;\n}\n.account-btn{\n  background-color: rgb(0, 133, 228) !important;\n  padding: 12px 30px;\n}\n/* Name Selection Modal Starts */\n.more-value {\n  margin:0 auto;\n  width:80%;\n }\n.more-value form p {\n  color: #000000 ;\n }\n.confirm-btn{\n  border:2px solid #000000;\n  border-radius:0px;\n }\n.small-text{\n   font-size:12px;\n }\n@media only screen and (max-width: 957px) and (min-width: 700px) {\n   .font{\n     font-size:smaller;\n   }\n }\n@media only screen and (min-width: 767px) and (max-width: 816px){\n  .text-padding{\n    padding-left:0;\n  }\n}\n@media only screen and (max-width: 767px){\n  .grey-height{\n    width:97%;\n  }\n  .font-header{\n    font-size:11px !important;\n    white-space:pre;\n  }\n}\n@media only screen and (max-width: 700px){\n  .font-header{\n    width:97%;\n    /* font-size:12px;  */\n  }\n}\n@media only screen and (max-width: 600px){\n  .font-header{\n    font-size: 9px !important;\n  }\n}\n@media only screen and (max-width: 900px){\n  .font-size{\n      font-size:18px !important;\n  }\n}\n@media only screen and (max-width: 575px) and (min-width: 320px){\n  .font-header{\n    width:106%;\n  }\n  .ul-height{\n    height:99px !important;\n  }\n}\n/* background images */\n.bg-text {\n  color: #000000;\n  font-weight: bold;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  text-align: center;\n}\n.bg-common {\n  /* Add the blur effect */\n  filter: blur(7px);\n  -webkit-filter: blur(7px);\n\n  /* Center and scale the image nicely */\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.bg-trend-image {\n  /* The image used */\n  background-image: url(\"/../assets/trend-image.png\");\n  height: 160px;\n}\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"checkPoint\" class=\"blank-display\">\n  <div class=\"spinner-container\">\n    <div class=\"spinner\"></div>\n  </div>\n</div>\n\n<div *ngIf=\"!showScreen.congrateScreen\">\n  <app-header [user]=\"user\" (hitProfile)=\"getHitProfile($event)\"></app-header>\n  <div class=\"row\">\n    <div class=\"col-md-3 col-lg-3 sidebar-bg pr-0\">\n      <app-sidebar (accEmit)=\"receivedAccId($event)\" (propertyDataEmit)=\"receivedPropertyData($event)\"\n        (multipleHandlerEmit)=\"receivedMultipleHandler($event)\" [sidebarReload]=\"sidebarReload\"\n        [reloadScreen]=\"showScreen.reloadScreen\" [callPropertyButton]=\"callPropertyButton\"\n        (booleanValueForSpinner)=\"receivedvalueForSpinner($event)\"></app-sidebar>\n    </div>\n\n    <div id=\"about\" class=\"col-md-9 col-lg-9 bg-gray about-padding\">\n      <!-- <div class=\"\"> -->\n      <div class=\"row border border-primary-none nav-position grey-height font-header\">\n        <ul class=\"nav nav-tabs w-100 text-center font ul-height\">\n          <li class=\"col-12 col-sm-3 col-md-3 col-lg-3\">\n            <button #capturedProfile class=\"active tabs-border cursor-pointer d-inline\" data-toggle=\"tab\"\n              href=\"#profile\">\n              <p class=\"m-0 font-weight-bold\"><i class=\"fa fa-user d-icon\"></i> {{ \"DASH_P_PROFILE\" | translate }} </p>\n            </button>\n          </li>\n          <li class=\"col-12 col-sm-3 col-md-3 col-lg-3\">\n            <button (click)=\"employerSectionHide()\" class=\"tabs-border cursor-pointer d-inline\" data-toggle=\"tab\"\n              href=\"#trends\">\n              <p class=\"m-0 font-weight-bold\"><i class=\"fa fa-bar-chart d-icon\"></i> {{\"DASH_P_TRENDS\" | translate }}\n              </p>\n            </button>\n          </li>\n          <li class=\"col-12 col-sm-3 col-md-3 col-lg-3 text-padding\">\n            <button #transactionButton (click)=\"employerSectionHide()\" class=\"tabs-border cursor-pointer d-inline \"\n              data-toggle=\"tab\" href=\"#transactions\">\n              <p class=\"m-0 font-weight-bold\"><i class=\"fa fa-usd d-icon\"></i> {{ \"DASH_P_TRANSACTIONS\" | translate }}\n              </p>\n            </button>\n          </li>\n          <li class=\"col-12 col-sm-3 col-md-3 col-lg-3 text-padding\">\n            <button (click)=\"employerSectionHide()\" class=\"tabs-border cursor-pointer d-inline\" data-toggle=\"tab\"\n              href=\"#shareProfile\">\n              <p class=\"m-0 font-weight-bold\"><i class=\"fa fa-lock d-icon\"></i> {{  \"DASH_P_SECURE_SHARE\" | translate }}\n              </p>\n            </button>\n          </li>\n          <li class=\"width-20 d-none\">\n            <button #manageAccount (click)=\"employerSectionHide()\" class=\"tabs-border cursor-pointer d-inline\"\n              data-toggle=\"tab\" href=\"#manageAccount\">\n              <p class=\"m-0 font-weight-bold\"><i class=\"fa fa-lock d-icon\"></i>{{\"DASH_P_MANAGE_ACCOUNT\" | translate}}\n              </p>\n            </button>\n          </li>\n        </ul>\n      </div>\n\n      <div class=\"tab-content mt-1\">\n        <!-- Profile Section -->\n        <div id=\"profile\" class=\"tab-pane active upper-margin\">\n          <app-profile [tranxData]=\"tranxData\" [properties]=\"propertiesData\" [reloadScreen]=\"showScreen.reloadScreen\"\n            (headerEvent)=\"headerEvent($event)\"\n            [employerSectionValue]=\"employerSectionHideValue\" (addAccountHandlerEmit)=\"receivedMultipleHandler($event)\"\n            (propertyHandlerEmit)=\"receivedpropertyHandler($event)\"></app-profile>\n        </div>\n        <!-- Trends Section -->\n        <div id=\"trends\" class=\"tab-pane fade upper-margin\">\n          <app-trends [propertyAttribute]=\"propertiesData\" [sidebarReload]=\"sidebarReload\"\n            [reloadScreen]=\"showScreen.reloadScreen\" [controlData]=\"showScreen.controlData\"\n            (tranxDataEmit)=\"receivedTranxData($event)\"\n            (addAccountHandlerEmit)=\"receivedMultipleHandler($event)\"></app-trends>\n        </div>\n\n        <!-- Transactions Section -->\n        <div id=\"transactions\" class=\"tab-pane fade upper-margin\">\n          <app-transactions [accountId]=\"emitAccId\" [reloadScreen]=\"showScreen.reloadScreen\"\n            [controlData]=\"showScreen.controlData\" (addAccountHandlerEmit)=\"receivedMultipleHandler($event)\">\n          </app-transactions>\n        </div>\n\n        <!--Share Profile New UI Starts-->\n        <div id=\"shareProfile\" class=\"tab-pane upper-margin\">\n          <div class=\"border border-dark\">\n            <div class=\"row\">\n              <div class=\"col-md-6 col-sm-12 text-center font-weight-bold padding-right border-right-red\">\n                <div class=\" margin-zero bg-dark text-light p-3 font-size\">\n                  {{ \"DASH_DIV_SHARE_YOUR_PROFILE\" | translate}}</div>\n                <div class=\"p-5\">\n                  <p class=\"mb-2 font-size\">{{ \"DASH_P_THE_REPORT_WILL_EXPIRE_IN\" | translate }}</p>\n                  <div class=\"icon-size font-size\">\n                    <i class=\"fa fa-clock-o mr-2\"></i>\n                    <select class=\"form-control custom-select cursor-pointer mt-2\" name=\"sellist1\"\n                      [(ngModel)]=\"selectedHours\">\n                      <option value=\"30min\">{{\"DASH_OPTION_30_MIN\" | translate}}</option>\n                      <option value=\"1hour\">{{\"DASH_OPTION_1_HOUR\" | translate}}</option>\n                      <option value=\"8hours\">{{\"DASH_OPTION_8_HOURS\" | translate}}</option>\n                      <option value=\"endofday\">{{\"DASH_OPTION_UNTIL_END_OF_DAY\" | translate}}</option>\n                      <option value=\"7days\">{{\"DASH_OPTION_7_DAYS\" | translate}}</option>\n                      <option value=\"14days\">{{\"DASH_OPTION_14_DAYS\" | translate}}</option>\n                      <option value=\"30days\">{{  \"DASH_OPTION_30_DAYS\" | translate}}</option>\n                    </select>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-6 col-sm-12 padding-left\">\n                <div class=\" margin-zero bg-dark text-light p-3 font-size text-center font-weight-bold\">\n                  {{ \"DASH_DIV_SHARE_ADDITIONAL_DETAILS\" | translate }}\n                </div>\n                <div class=\"row p-5\" *ngIf=\"accounts.length == 0\">\n                  <div class=\"col-12\">\n                    <div class=\"bg-trend-image bg-common\"></div>\n                    <div class=\"bg-text\">\n                      <p>Link your account to view the widget</p>\n                      <button type=\"button\" class=\"btn btn-secondary\"\n                        (click)=\"receivedMultipleHandler('callingAddAccountBtn')\">Add Account</button>\n                    </div>\n                  </div>\n                </div>\n\n                <div class=\"row p-5\" *ngIf=\"accounts.length > 0\">\n                  <div class=\"col-4 col-md-4 col-lg-4\">\n                    <div class=\"mb-3\">\n                      <label class=\"switch\">\n                        <input type=\"checkbox\" [(ngModel)]=\"shareTrends\" (change)=\"changedShareTrends()\">\n                        <span class=\"slider round\"></span>\n                      </label>\n                    </div>\n                    <div class=\"\">\n                      <label class=\"switch\">\n                        <input type=\"checkbox\" [(ngModel)]=\"shareTransactions\" (change)=\"changedShareTransactions()\">\n                        <span class=\"slider round\"></span>\n                      </label>\n                    </div>\n                  </div>\n                  <div class=\"col-8 col-md-8 col-lg-8\">\n                    <div class=\"h-style mb-3 font-size\">{{ 'DASH_P_TRENDS' | translate }}</div>\n                    <div class=\"h-style font-size\">{{ 'DASH_P_TRANSACTIONS' | translate }}</div>\n                    <p class=\"mb-2 font-size\">{{ 'DASH_P_DURATION_OF_TRANSACTIONS' | translate }}</p>\n                    <div class=\"icon-size font-size\">\n                      <i class=\"fa fa-calendar mr-2\"></i>\n                      <select class=\"form-control custom-select clock-custom cursor-pointer mt-2\" id=\"sel1\"\n                        name=\"sellist1\" [(ngModel)]=\"selectedBackMonths\">\n                        <option value=\"3\">3 {{ 'DASH_OPTION_MONTHS_BACK' | translate }}</option>\n                        <option value=\"6\">6 {{ 'DASH_OPTION_MONTHS_BACK' | translate }}</option>\n                        <option value=\"12\">12 {{ 'DASH_OPTION_MONTHS_BACK' | translate }}</option>\n                      </select>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"text-center\">\n              <button class=\"font-weight-bold mb-3 mt-3 pl-5 pr-5 cursor-pointer Generate-Report-btn text-white\"\n                (click)=\"generateReport(QRCodeTemplate)\">{{ 'DASH_BUTTON_GENERATE_REPORT' | translate }}</button>\n            </div>\n          </div>\n        </div>\n        <!--Share Profile New UI Ends-->\n\n        <!--Manage Account UI Starts-->\n        <div id=\"manageAccount\" class=\"tab-pane upper-margin\">\n          <app-manage-account [pubToken]=\"publicToken.public_token\" [logoUpdated]=\"showScreen.logoUpdated\"\n            [addAnotherAcc]=\"addAnotherAcc\" [callTheAddAccount]=\"callAddAccount\"\n            (syncChildEvent)=\"callSyncStatusMethod()\" (congratePageScreen)=\"showCongrateScreenSection($event)\"\n            [propertyAttribute]=\"propertiesData\" (propertyHandlerEmit)=\"receivedpropertyHandler($event)\"\n            (employerDivHide)=\"employerSectionHide()\" [hasRealTimeData]=\"hasRealTimeData\"\n            (controlData)=\"gotControlData($event)\"></app-manage-account>\n        </div>\n        <!--Manage Account UI Starts-->\n      </div>\n      <!-- </div> -->\n    </div>\n  </div>\n\n  <div *ngIf=\"failedAcc.length > 0\" class=\"progress-div fixed-bottom\" [ngStyle]=\"hideProgressbar\">\n    <div class=\"row\">\n      <div class=\"col-sm-12 text-right\">\n        <span class=\"cursor-pointer\" (click)=\"closeProgressbar()\">&times;</span>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-sm-6\">\n        <span class=\"ml-5 small\">{{failedAcc.length}} {{ 'DASH_SPAN_ACCOUNTS_NEED_ATTENTION' | translate }}</span>\n      </div>\n      <div class=\"col-sm-6\">\n        <span class=\"cursor-pointer small\" (click)=\"failedAccDetails = !failedAccDetails\">\n          <span *ngIf=\"!failedAccDetails\"><i class=\"fa fa-angle-up\"></i>{{ 'DASH_SPAN_SEE_DETAILS' | translate }}</span>\n          <span *ngIf=\"failedAccDetails\"><i\n              class=\"fa fa-angle-down\"></i>{{ 'DASH_SPAN_HIDE_DETAILS' | translate }}</span>\n        </span>\n        <div class=\"table-responsive\" *ngIf=\"failedAccDetails\">\n          <table class=\"table table-borderless table-sm table-faildAccount\">\n            <tbody>\n              <tr *ngFor=\"let acc of failedAcc\">\n                <td>\n                  {{acc.inst_name}}\n                  <div class=\"small-text\">{{ \"DASH_DIV_TRY_ENTERING_USERNAME\" | translate }} <br />{{acc.inst_name}}\n                  </div>\n                </td>\n                <td>\n                  <button class=\"btn btn-success btn-sm\"\n                    (click)=\"getPublicToken(acc.id)\">{{ 'DASH_BUTTON_TRY_AGAIN' | translate }}</button>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-sm-12\">\n        <div class=\"mt-1 mb-4 ml-5 mr-5\">\n          <progressbar [animate]=\"false\" [max]=\"maxValue\" [value]=\"progressbarPercent\" type=\"danger\"></progressbar>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--Start barcode-generate-modal-->\n<ng-template #QRCodeTemplate>\n  <app-qr-generate-modal (childEvent)=\"editProfileModalClose()\" [qrCodeData]=\"qrCodeModalData\"></app-qr-generate-modal>\n</ng-template>\n<!--End Edit barcode-generate-modal-->\n\n<!-- name selection modal -->\n<ng-template #nameSelection [ngIf]=\"showScreen.nameSelectionScreen\">\n  <div class=\"modal-body\">\n    <div class=\"container\">\n      <div class=\"border border-dark\">\n        <div class=\"p-3 bg-dark text-white font-weight-bold\">{{ 'DASH_DIV_CONFIRM_DISPLAY_NAME' | translate }}</div>\n        <div class=\"text-danger mt-3 mb-3 more-value\"><i class=\"fa fa-info mr-3\"></i>\n          {{ 'DASH_DIV_ERR_MORE_THAN_1_NAME_EXISTS_FOR_YOUR_ACCOUNT' | translate }}.\n        </div>\n        <div class=\"more-value font-weight-bold  p-3\">\n          <form #myform=\"ngForm\" (ngSubmit)=\"updatedName(myform)\">\n            <div *ngFor=\"let item of userDetail.names; let i = index\">\n              <p class=\"mt-2 mb-2\">\n                <input type=\"radio\" name=\"gender\" class=\"cursor-pointer\" [(ngModel)]=\"item[i]\" value=\"{{item}}\"\n                  [checked]=\"userDetail.names[0]\">\n                {{item}}\n              </p>\n            </div>\n            <div class=\"text-center\">\n              <button type=\"submit\"\n                class=\"btn btn-success confirm-btn font-weight-bold pl-5 pr-5 mt-3 mb-3\">{{ 'DASH_DIV_CONFIRM' | translate }}</button>\n            </div>\n          </form>\n        </div>\n        <div class=\"more-value font-weight-bold mt-5 mb-5\">{{ \"DASH_DIV_NOTE_E_PROFILE\" | translate }}</div>\n      </div>\n    </div>\n  </div>\n</ng-template>\n\n<!-- Congratulations screen-->\n<div *ngIf=\"showScreen.congrateScreen\">\n  <div class=\"container h-100\">\n    <div class=\"row h-100 justify-content-center align-items-center\">\n      <div align=\"center\" class=\"mt-4\">\n        <img src=\"assets/trophy.gif\" alt=\"imgpic\" class=\"prize\" />\n        <p class=\"custom-text-align c-text\">{{ 'CP_P_CONGRATULATIONS' | translate }}</p>\n        <div class=\"custom-text-align line-txt\">{{ \"CP_DIV_YOU_JUST_LINK_YOUR\" | translate }} {{accountsLength}}\n          {{ \"CP_DIV_AVERAGE_ACCOUNTS_5\" | translate }}</div>\n        <div class=\"custom-text-align line-txt\">{{ 'CP_DIV_STRONGER_PROFILE' | translate }}</div>\n        <div class=\"custom-text-align\">\n          <label class=\"star-color\" *ngIf=\"!firstStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n          <label class=\"star-color\" *ngIf=\"!secondStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n          <label class=\"star-color\" *ngIf=\"!thirdStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n          <label class=\"star-color\" *ngIf=\"!fourthStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n          <label class=\"star-color\" *ngIf=\"!fifthStar\"><i class=\"fa fa-star\" aria-hidden=\"true\"></i></label>\n\n          <label class=\"star-color\" *ngIf=\"firstStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n          <label class=\"star-color\" *ngIf=\"secondStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n          <label class=\"star-color\" *ngIf=\"thirdStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n          <label class=\"star-color\" *ngIf=\"fourthStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n          <label class=\"star-color\" *ngIf=\"fifthStar\"><i class=\"fa fa-star-o\" aria-hidden=\"true\"></i></label>\n        </div>\n        <div class=\"mt-3 mb-3\">\n          <button class=\"btn btn-info btn-lg account-btn\"\n            (click)=\"addAnotherAccount()\">{{ 'CP_BUTTON_ADD_ANOTHER_ACCOUNT' | translate }}</button>\n        </div>\n        <div class=\"col-md-12 mt-3\">\n          <p class=\"line-txt\" align=\"center\">{{ \"CP_P_DONE_LINKING_ACCOUNTS\" | translate }} <a class=\"a-texture\"\n              (click)=\"goToDashboard()\">{{ \"DASH_DIV_GO_TO_DASHBOARD\" | translate }}</a></p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!-- Start Verify Details modal -->\n<ng-template #verifyDetailsModal>\n  <app-verify-loan-data (verifyChildEvent)=\"verifyDetailsModalClose($event)\"></app-verify-loan-data>\n</ng-template>\n<!--End Verify Details modal-->\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var ngx_plaid_link__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-plaid-link */ "./node_modules/ngx-plaid-link/fesm5/ngx-plaid-link.js");
/* harmony import */ var _realtime_realtime_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../realtime/realtime.service */ "./src/app/realtime/realtime.service.ts");
/* harmony import */ var _websocket_websocket_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../websocket/websocket.service */ "./src/app/websocket/websocket.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../common/utils */ "./src/app/common/utils.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};















var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(authService, backend, session, spinnerService, modalService, router, notifierService, eleRef, plaidLinkService, cdr, translate, realTimeService) {
        var _this = this;
        this.authService = authService;
        this.backend = backend;
        this.session = session;
        this.spinnerService = spinnerService;
        this.modalService = modalService;
        this.router = router;
        this.notifierService = notifierService;
        this.eleRef = eleRef;
        this.plaidLinkService = plaidLinkService;
        this.cdr = cdr;
        this.translate = translate;
        this.realTimeService = realTimeService;
        this.tab = "trends";
        this.selectedHours = '30min';
        this.selectedBackMonths = '3';
        this.shareTrends = false;
        this.shareTransactions = false;
        this.checkSyncStatus = null;
        this.hideProgressbar = {};
        this.maxValue = 100;
        this.failedAcc = [];
        this.failedAccDetails = false;
        this.publicToken = {};
        this.showScreen = {
            congrateScreen: false,
            nameSelectionScreen: false,
            reloadScreen: false,
            controlData: false,
            logoUpdated: false
        };
        this.sidebarReload = false;
        this.firstStar = true;
        this.secondStar = true;
        this.thirdStar = true;
        this.fourthStar = true;
        this.fifthStar = true;
        this.addAnotherAcc = false;
        this.employerSectionHideValue = false;
        this.checkPoint = false;
        this.accounts = [];
        this.userInactive = new rxjs__WEBPACK_IMPORTED_MODULE_12__["Subject"]();
        this.warningMsg = new rxjs__WEBPACK_IMPORTED_MODULE_12__["Subject"]();
        this.notifier = notifierService;
        this.setTimeout();
        this.userInactive.subscribe(function () {
            console.log('user has been inactive for 10 min');
            _this.authService.getUserLogout();
        });
        this.warningMsg.subscribe(function () {
            _this.notifier.notify('warning', "Session will expire after 60 Seconds, please move cursor!!");
        });
        // this is default langugage
        translate.setDefaultLang('en');
        // this language is to set by your prefered browser language
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang);
        realTimeService.messages.subscribe(function (msg) {
            console.log("Response from websocket: " + JSON.stringify(msg));
            if (msg && 'action' in msg && msg.action === 'sync') {
                _this.hasRealTimeData = msg.id;
                _this.showSyncStatus(msg);
            }
            else if (msg && 'action' in msg && msg.action === 'addemployer') {
                _this.userAttributes.employers = Object.assign(_this.userAttributes.employers, msg.emp);
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.userAttributes);
            }
            else if (msg && 'action' in msg && msg.action === 'updateUserIdentity') {
                _this.userAttributes.address = msg.userIdentity.address;
                if ((_this.userAttributes.picture) === 'NA') {
                    var firstAndLastName = [];
                    firstAndLastName = (msg.userIdentity.name).split(' ');
                    if (firstAndLastName.length >= 2) {
                        _this.userAttributes.picture = ((firstAndLastName[0])[0]) + ((firstAndLastName[firstAndLastName.length - 1])[0]);
                    }
                    else {
                        _this.userAttributes.picture = ((msg.userIdentity.name)[0]);
                    }
                    _this.updateUserPicture();
                }
                _this.userAttributes.phone_numbers = msg.userIdentity.phone_number;
                _this.userAttributes.emails = msg.userIdentity.email;
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.userAttributes);
            }
            else if (msg && 'action' in msg && msg.action === 'logoUpdated') {
                _this.appendLogo(msg);
            }
        });
    }
    DashboardComponent.prototype.appendLogo = function (msg) {
        var account = this.userAttributes.accounts.filter(function (acc) { return acc.id === msg.id; });
        var otherAccounts = this.userAttributes.accounts.filter(function (acc) { return acc.id !== msg.id; });
        if (account && account.length > 0) {
            account[0].logo = msg.logo;
            account[0].primaryColor = msg.primaryColor;
            otherAccounts.push(account[0]);
            this.userAttributes.accounts = otherAccounts;
            this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
            // Change message to update accounts
            this.showScreen.logoUpdated = !this.showScreen.logoUpdated;
        }
    };
    DashboardComponent.prototype.setTimeout = function () {
        var _this = this;
        // logout completed 10 min
        this.userActivity = setTimeout(function () { return _this.userInactive.next(undefined); }, 10 * 60000);
        // show warning modal on 9 min completed
        this.warningActivity = setTimeout(function () { return _this.warningMsg.next(undefined); }, 9 * 60000);
    };
    DashboardComponent.prototype.refreshUserState = function () {
        // clear timeout
        clearTimeout(this.userActivity);
        clearTimeout(this.warningActivity);
        // again call the timeout method
        this.setTimeout();
    };
    DashboardComponent.prototype.ngOnInit = function () {
        this.userAttributes = null;
        this.userAttributes = this.authService.getUserProfile();
        this.authResultInfo = this.authService.getAccessTokenAndID();
        if (!_common_utils__WEBPACK_IMPORTED_MODULE_14__["default"].isNullOrEmpty(this.userAttributes, 'account_verified')) {
            if (this.userAttributes.account_verified == 0) {
                try {
                    for (var _a = __values(this.userAttributes.accounts), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var acc = _b.value;
                        if ('accounts' in acc && acc.accounts) {
                            try {
                                for (var _c = __values(acc.accounts), _d = _c.next(); !_d.done; _d = _c.next()) {
                                    var i = _d.value;
                                    if (('mask' in i && i.mask == null) && ('type' in i && i.type == 'loan')) {
                                        this.openQRCodeModal(this.verifyDetailsModal);
                                    }
                                }
                            }
                            catch (e_1_1) { e_1 = { error: e_1_1 }; }
                            finally {
                                try {
                                    if (_d && !_d.done && (_e = _c.return)) _e.call(_c);
                                }
                                finally { if (e_1) throw e_1.error; }
                            }
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_f = _a.return)) _f.call(_a);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
        }
        this.accounts = [];
        try {
            for (var _g = __values(this.userAttributes.accounts), _h = _g.next(); !_h.done; _h = _g.next()) {
                var acc = _h.value;
                if ('accounts' in acc && acc.accounts) {
                    try {
                        for (var _j = __values(acc.accounts), _k = _j.next(); !_k.done; _k = _j.next()) {
                            var i = _k.value;
                            if ((i.type == "credit" && i.status == 'ACTIVE') || i.type == "depository" && i.status == 'ACTIVE') {
                                this.accounts.push(i);
                            }
                        }
                    }
                    catch (e_3_1) { e_3 = { error: e_3_1 }; }
                    finally {
                        try {
                            if (_k && !_k.done && (_l = _j.return)) _l.call(_j);
                        }
                        finally { if (e_3) throw e_3.error; }
                    }
                }
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (_h && !_h.done && (_m = _g.return)) _m.call(_g);
            }
            finally { if (e_4) throw e_4.error; }
        }
        var e_2, _f, e_1, _e, e_4, _m, e_3, _l;
    };
    DashboardComponent.prototype.getHitProfile = function (event) {
        if (event == "hitprofile") {
            console.log(' this.capturedProfile.nativeElement', this.capturedProfile);
            this.capturedProfile.nativeElement.click();
        }
    };
    DashboardComponent.prototype.receivedvalueForSpinner = function (event) {
        // if(event){
        //   this.checkPoint = event;
        //   this.cdr.detectChanges();
        //   timer(15000).subscribe(val =>  this.checkPoint = false);
        // }
    };
    DashboardComponent.prototype.receivedAccId = function (e) {
        this.emitAccId = e;
        this.transactionButton.nativeElement.click();
    };
    DashboardComponent.prototype.updateUserPicture = function () {
        var _this = this;
        this.spinnerService.show();
        var path = "/users/" + this.authResultInfo.idTokenPayload.sub;
        var data = {
            picture: this.userAttributes.picture
        };
        this.backend.put(data, path, this.authService.getAccessTokenAndID()).subscribe(function (res) {
            var response = res.json();
            console.log('Response: ', response);
            _this.spinnerService.hide();
        }, function (err) {
            var errorVar = err.json();
            console.log('Error: ', errorVar["message"]);
            _this.spinnerService.hide();
        });
    };
    DashboardComponent.prototype.receivedTranxData = function (e) {
        this.tranxData = e;
    };
    DashboardComponent.prototype.receivedPropertyData = function (e) {
        this.propertiesData = e;
    };
    DashboardComponent.prototype.showSyncStatus = function (msg) {
        // Get the account 
        if (msg) {
            var fa = this.userAttributes.accounts.filter(function (a) { return a.id == msg.id; });
            if (fa && fa.length > 0 && fa[0].sync_status != msg.status) {
                fa[0].sync_status = msg.status;
                if (msg.status === 'failed' && msg.last_sync_retry >= 3) {
                    this.failedAcc.push(fa[0]);
                }
                // Update user_profile
                this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
                if (this.failedAcc.length > 0) {
                    if (this.hideProgressbar.display == 'none') {
                        this.hideProgressbar.display = 'block';
                    }
                    this.progressbarPercent = (this.failedAcc.length / this.userAttributes.accounts.length) * 100;
                }
                console.log("failedAcc ", this.failedAcc);
                this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
            }
        }
    };
    DashboardComponent.prototype.showTab = function (tabs) {
        this.tab = tabs;
    };
    /* Edit Modal Code */
    DashboardComponent.prototype.openQRCodeModal = function (editProfileModal) {
        this.modalRef = this.modalService.show(editProfileModal, Object.assign({}, { class: 'gray modal-lg' }));
    };
    DashboardComponent.prototype.editProfileModalClose = function () {
        this.modalRef.hide();
    };
    DashboardComponent.prototype.changedShareTrends = function () {
        if (this.shareTrends) {
            this.shareTrends = true;
        }
        else {
            this.shareTrends = false;
        }
    };
    DashboardComponent.prototype.changedShareTransactions = function () {
        if (this.shareTransactions) {
            this.shareTransactions = true;
        }
        else {
            this.shareTransactions = false;
        }
    };
    DashboardComponent.prototype.shareDateCalculation = function (value) {
        var dateVal;
        switch (value) {
            case '30min':
                dateVal = moment__WEBPACK_IMPORTED_MODULE_6__["utc"]().add(30, 'minutes');
                return '30min';
            case '1hour':
                dateVal = moment__WEBPACK_IMPORTED_MODULE_6__["utc"]().add(1, 'hours');
                return '1hour';
            case '8hours':
                dateVal = moment__WEBPACK_IMPORTED_MODULE_6__["utc"]().add(8, 'hours');
                return '8hours';
            case 'endofday':
                dateVal = moment__WEBPACK_IMPORTED_MODULE_6__["utc"]().endOf('day');
                return 'endofday';
            case '7days':
                dateVal = moment__WEBPACK_IMPORTED_MODULE_6__["utc"]().add(7, 'days');
                return '7days';
            case '14days':
                dateVal = moment__WEBPACK_IMPORTED_MODULE_6__["utc"]().add(14, 'days');
                return '14days';
            case '30days':
                dateVal = moment__WEBPACK_IMPORTED_MODULE_6__["utc"]().add(30, 'days');
                return '30days';
        }
    };
    DashboardComponent.prototype.generateReport = function (value) {
        var _this = this;
        this.spinnerService.show();
        var data = {
            validTill: this.shareDateCalculation(this.selectedHours),
            shareTrends: this.shareTrends,
            shareTrans: this.shareTransactions,
            trans: {
                interval: parseInt(this.selectedBackMonths),
                duration: 'months'
            }
        };
        this.backend.post(data, "/reports", this.authResultInfo.accessToken).subscribe(function (res) {
            _this.qrCodeModalData = res.json();
            _this.openQRCodeModal(value);
            _this.spinnerService.hide();
        }, function (err) {
            _this.spinnerService.hide();
            var errorVar = err.json();
            console.log('Error: ', errorVar["message"]);
            // translate error message
            _this.translate.get('DASH_ERROR')
                .subscribe(function (value) {
                _this.notifier.notify('error', value + errorVar["message"]);
            });
        });
    };
    DashboardComponent.prototype.closeProgressbar = function () {
        this.hideProgressbar = {
            display: 'none'
        };
    };
    DashboardComponent.prototype.headerEvent = function (e) {
        this.user = this.authService.getUserProfile();
    };
    DashboardComponent.prototype.getPublicToken = function (accountId) {
        var _this = this;
        this.spinnerService.show();
        var path = "/accounts/" + accountId + "/public_token";
        this.backend.get(path, this.authResultInfo.accessToken)
            .subscribe(function (res) {
            _this.publicToken = res.json();
            _this.spinnerService.hide();
        }, function (error) {
            var errorVar = error.json();
            // translate error message
            _this.translate.get('DASH_ERROR')
                .subscribe(function (value) {
                _this.notifier.notify('error', value + errorVar["message"]);
            });
            _this.spinnerService.hide();
        });
    };
    DashboardComponent.prototype.callSyncStatusMethod = function () {
        this.hideProgressbar = {
            display: 'none'
        };
        this.failedAccDetails = false;
        //this.showSyncStatus();
    };
    DashboardComponent.prototype.showCongrateScreenSection = function (event) {
        console.log('Got reload event', event);
        if (event === 'reload') {
            this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
        }
        else if (event == 'sidereload') {
            this.sidebarReload = !this.sidebarReload;
        }
        else {
            this.congrateScreenOnLoad();
            this.showScreen.congrateScreen = true;
            this.receivedAccountData(event);
        }
    };
    DashboardComponent.prototype.gotControlData = function (event) {
        if (event == 'notactive') {
            this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
            this.showScreen.controlData = !this.showScreen.controlData;
        }
        if (event == 'active') {
            this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
        }
    };
    DashboardComponent.prototype.congrateScreenOnLoad = function () {
        var _this = this;
        this.user = this.authService.getUserProfile();
        var tokenID = this.authService.getAccessTokenAndID();
        this.accountsLength = this.user.accounts.length;
        if (this.accountsLength === 1) {
            var path = "/users/" + tokenID.idTokenPayload.sub;
            this.spinnerService.show();
            this.backend.get(path, tokenID.accessToken)
                .subscribe(function (data) {
                _this.user = Object.assign(_this.user, data.json());
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.user);
                _this.spinnerService.hide();
            }, function (error) {
                _this.router.navigate(['/']);
                var errorVar = error.json();
                // translate error message
                _this.translate.get('DASH_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + errorVar["message"]);
                });
                _this.spinnerService.hide();
            });
        }
        switch (this.accountsLength) {
            case 1: {
                this.firstStar = false;
                this.secondStar = true;
                this.thirdStar = true;
                this.fourthStar = true;
                this.fifthStar = true;
                this.accountsLength = this.accountsLength + 'st';
                break;
            }
            case 2: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = true;
                this.fourthStar = true;
                this.fifthStar = true;
                this.accountsLength = this.accountsLength + 'nd';
                break;
            }
            case 3: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = false;
                this.fourthStar = true;
                this.fifthStar = true;
                this.accountsLength = this.accountsLength + 'rd';
                break;
            }
            case 4: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = false;
                this.fourthStar = false;
                this.fifthStar = true;
                this.accountsLength = this.accountsLength + 'th';
                break;
            }
            case 5: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = false;
                this.fourthStar = false;
                this.fifthStar = false;
                this.accountsLength = this.accountsLength + 'th';
                break;
            }
            default: {
                this.firstStar = false;
                this.secondStar = false;
                this.thirdStar = false;
                this.fourthStar = false;
                this.fifthStar = false;
                this.accountsLength = this.accountsLength + 'th';
                break;
            }
        }
        this.spinnerService.hide();
    };
    DashboardComponent.prototype.goToDashboard = function () {
        // i just add this number for change callAddAccount variable value, to prevent recall plaid calling
        this.callAddAccount = 5;
        this.showScreen.congrateScreen = false;
        this.addAnotherAcc = false;
    };
    DashboardComponent.prototype.addAnotherAccount = function () {
        this.showScreen.congrateScreen = false;
        this.addAnotherAcc = true;
    };
    DashboardComponent.prototype.receivedMultipleHandler = function (referenceName) {
        if (referenceName) {
            if (referenceName == 'manageAccountSection') {
                this.manageAccount.nativeElement.click();
            }
            else {
                // exchange the data to make clickable and prevent passing the same value
                this.callAddAccount == 'callingAddAccount' ? this.callAddAccount = 'swapVariable' : this.callAddAccount = 'callingAddAccount';
            }
        }
    };
    DashboardComponent.prototype.receivedpropertyHandler = function (propertyName) {
        if (propertyName) {
            this.callPropertyButton == 'callingAddProperty' ? this.callPropertyButton = 'swapManageV' : this.callPropertyButton = 'callingAddProperty';
        }
    };
    DashboardComponent.prototype.openModal = function (nameS) {
        // prevent closing when user click outside of modal
        var ngbModalOptions = {
            backdrop: 'static',
            keyboard: false
        };
        this.modalRef = this.modalService.show(nameS, ngbModalOptions);
    };
    // UPDATE THE NAME
    DashboardComponent.prototype.updatedName = function (namePassed) {
        var _this = this;
        console.log('selectedName', namePassed.value.gender);
        if (namePassed.value.gender === 'H') {
            return;
        }
        var data = {};
        if ('gender' in namePassed.value && namePassed.value.gender) {
            data.first_name = namePassed.value.gender;
            var splitted = [];
            splitted = (namePassed.value.gender).split(' ');
            try {
                if (splitted.length >= 2) {
                    data.picture = splitted[0][0] + splitted[splitted.length - 1][0];
                }
                else {
                    data.picture = splitted[0][0];
                }
                this.userAttributes.picture = data.picture;
            }
            catch (e) {
                console.log("Error logged", e);
            }
            if (this.userAttributes.first_name) {
                this.userAttributes.first_name = data.first_name;
            }
        }
        else {
            data.first_name = this.userAttributes.first_name;
        }
        var path = "/users/" + this.authResultInfo.idTokenPayload.sub;
        this.spinnerService.show();
        this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(function (res) {
            _this.spinnerService.hide();
            // translate error message
            _this.translate.get('DASH_UPDATE_SUCCESS')
                .subscribe(function (value) {
                _this.notifier.notify('success', value);
            });
            _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.userAttributes);
            _this.modalRef.hide();
            _this.showScreen.nameSelectionScreen = false;
            _this.showScreen.congrateScreen = true;
        }, function (err) {
            var errorVar = err.json();
            console.log('Error: ', errorVar["message"]);
            _this.spinnerService.hide();
            // translate error message
            _this.translate.get('DASH_ERROR')
                .subscribe(function (value) {
                _this.notifier.notify('error', value + errorVar["message"]);
            });
            _this.modalRef.hide();
        });
    };
    // when new user signup and add account; that time user gets multiple name
    DashboardComponent.prototype.receivedAccountData = function (event) {
        console.log('accountData', event);
        this.ngOnInit();
        this.userDetail = event;
        if (this.userDetail) {
            if (('names' in this.userDetail && this.userDetail.names.length > 0)
                && ('notify_for_names_selection' in this.userDetail && this.userDetail.notify_for_names_selection)) {
                this.showScreen.nameSelectionScreen = true;
                this.showScreen.congrateScreen = false;
                this.openModal(this.nameSelection);
                this.spinnerService.hide();
            }
            else {
                this.showScreen.nameSelectionScreen = false;
                this.showScreen.congrateScreen = true;
                this.spinnerService.hide();
            }
        }
    };
    // getScoreGraphData(e: any) {
    //   if ('TippingPoints' in e && e.TippingPoints) {
    //     this.scoreGraphDataFromTrend = e.TippingPoints;
    //   }else{
    //     if('err' in  e){
    //       delete e.err;
    //       this.scoreGraphDataFromTrend = e;
    //     }
    //   }
    // }
    // Employer section hide on click
    DashboardComponent.prototype.employerSectionHide = function () {
        this.employerSectionHideValue = !this.employerSectionHideValue;
    };
    // verify details modal close
    DashboardComponent.prototype.verifyDetailsModalClose = function (event) {
        if (event === 'reload') {
            this.modalRef.hide();
            this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
            this.ngOnInit();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('transactionButton'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], DashboardComponent.prototype, "transactionButton", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('nameSelection'),
        __metadata("design:type", Object)
    ], DashboardComponent.prototype, "nameSelection", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('manageAccount'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], DashboardComponent.prototype, "manageAccount", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('capturedProfile'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], DashboardComponent.prototype, "capturedProfile", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('verifyDetailsModal'),
        __metadata("design:type", Object)
    ], DashboardComponent.prototype, "verifyDetailsModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:mousemove'),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:click'),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:keydown'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], DashboardComponent.prototype, "refreshUserState", null);
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")],
            providers: [_websocket_websocket_service__WEBPACK_IMPORTED_MODULE_11__["WebsocketService"], _realtime_realtime_service__WEBPACK_IMPORTED_MODULE_10__["RealTimeService"]]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"],
            angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["BsModalService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_8__["NotifierService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"],
            ngx_plaid_link__WEBPACK_IMPORTED_MODULE_9__["NgxPlaidLinkService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_13__["TranslateService"],
            _realtime_realtime_service__WEBPACK_IMPORTED_MODULE_10__["RealTimeService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/edit-profile-modal/edit-profile-modal.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/edit-profile-modal/edit-profile-modal.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".white{\n  color:#ffffff;\n}\n.modal-head{\n  margin-left: -16px;\n  margin-right: -16px;\n}\n.change-image{\n  width:70%;\n}\n.underline{\n  text-decoration: underline !important;\n}\n.bg-orange{\n  background: #FCE55C;\n  border-top: 1px solid #918f8f;\n  border-bottom: 1px solid #918f8f;\n}\n.no-break{\n  white-space:nowrap;\n}\n.save-employee-btn{\n  padding: 12px 0 12px 0;\n  width: 100px;\n}\n.border-none{\n  border-top:none;\n}\n.bg-none{\n  background: none;\n}\ninput::-webkit-input-placeholder {\n  color:#d8d2d2;\n}\ninput::-ms-input-placeholder {\n  color:#d8d2d2;\n}\ninput::placeholder {\n  color:#d8d2d2;\n}\n.image-fixed-size{\n  width: 130px;\n  height: 130px;\n}\n@media screen and (max-width: 990px){\n  .change-image {\n    width: 100%;\n  }\n}\n@media screen and (max-width: 575px){\n  .change-image {\n    width: 50%;\n  }\n  .p-t-0{\n    padding-top: 0 !important;\n  }\n  .left-text{\n    text-align: left !important;\n  }\n  .small-screen-mt{\n    margin-top:1rem;\n  }\n}\n"

/***/ }),

/***/ "./src/app/edit-profile-modal/edit-profile-modal.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/edit-profile-modal/edit-profile-modal.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Edit Profile Modal-->\n<div class=\"modal-body pt-0\">\n  <form name=\"editForm\" (ngSubmit)=\"updateUserProfile()\" #f>\n    <div class=\"row bg-dark white modal-head\">\n      <div class=\"col-sm-12 pt-2 pb-2\"> {{\"EPM_DIV_EDIT_PROFILE\" | translate}} <a class=\"float-right cursor-pointer\"\n          (click)=\"closeModal('')\"><i class=\"fa fa-close\"></i></a></div>\n    </div>\n\n    <div class=\"row mt-2\">\n      <div class=\"col-sm-4 text-center\">\n        <div>\n          <div class=\"image-fixed-size text-center d-inline-block\">\n            <img *ngIf=\"(userAttributes.picture.includes('http')) || (userAttributes.picture.includes('data'))\"\n              [src]=\"userAttributes.picture\" class=\"rounded-circle mw-100 h-100 text-center cursor-pointer\"\n              alt=\"userPic\" (click)=\"imagePicker.click()\">\n            <ngx-avatar size=\"125\"\n              *ngIf=\"!(userAttributes.picture.includes('http')) && !(userAttributes.picture.includes('data'))\"\n              name=\"{{userAttributes.first_name}}\"></ngx-avatar>\n          </div>\n        </div>\n        <div class=\"text-primary text-center mt-1\">\n          <input #imagePicker class=\"d-none\" id=\"userProfilePic\" type=\"file\" accept=\"image/*\"\n            (change)=\"updateSource($event)\">\n          <label for=\"userProfilePic\" class=\"cursor-pointer underline\"> {{\"EPM_DIV_CHANGE\" | translate}} </label>\n        </div>\n      </div>\n      <div class=\"col-sm-8\">\n        <div class=\"row mt-3\">\n          <div class=\"col-sm-5 col-lg-5 pt-2 p-t-0 text-secondary ns-required\">\n            {{\"EPM_DIV_FIRST_NAME\" | translate}}\n          </div>\n          <div class=\"col-sm-7 col-lg-7 \">\n            <input name=\"firstName\" (keypress)=\"alphabetOnly($event)\" placeholder=\"{{'EPM_DIV_FIRST_NAME' | translate}}\"\n              [(ngModel)]=\"userAttributes.first_name\" maxlength=\"30\" class=\"form-control text-secondary\" type=\"text\" required>\n          </div>\n        </div>\n        <div class=\"row mt-3\">\n          <div class=\"col-sm-5 col-lg-5 pt-2 p-t-0 text-secondary\">\n            {{\"EPM_DIV_LAST_NAME\" | translate}}\n          </div>\n          <div class=\"col-sm-7 col-lg-7\">\n            <input name=\"lastName\" (keypress)=\"alphabetOnly($event)\" placeholder=\"{{'EPM_DIV_LAST_NAME' | translate}}\" [(ngModel)]=\"userAttributes.last_name\" maxlength=\"30\"\n              class=\"form-control text-secondary\" type=\"text\" required>\n          </div>\n        </div>\n        <div class=\"row mt-3\">\n          <div class=\"col-sm-5 col-lg-5 pt-2 p-t-0 text-secondary\">\n            {{\"EPM_DIV_DATE_OF_BIRTH\" | translate}}\n          </div>\n          <div class=\"col-sm-7 col-lg-7 input-group\">\n            <input type=\"text\" name=\"DOB\" placeholder=\"{{'EPM_DIV_DATE_OF_BIRTH' | translate}}\"\n              [(ngModel)]=\"userAttributes.dobInDate\" class=\"form-control text-secondary\"\n              [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" [maxDate]=\"maxDate\" placement=\"bottom\" #dp=\"bsDatepicker\" bsDatepicker>\n            <div class=\"input-group-append\">\n              <span class=\"input-group-text bg-none\" (click)=\"dp.show()\" [attr.aria-expanded]=\"dp.isOpen\"><i\n                  class=\"fa fa-calendar\"></i></span>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row bg-orange modal-head mt-2\">\n      <div class=\"col-sm-12 pt-2 pb-1\"></div>\n    </div>\n\n    <div class=\"row mt-2\">\n      <div class=\"col-sm-12\"> {{\"EPM_DIV_CONTACT_DETAILS\" | translate}}</div>\n    </div>\n    <div class=\"row mt-2\">\n      <div class=\"col-sm-4 col-lg-3 text-secondary pt-2 p-t-0\"> {{\"EPM_DIV_PREFERRED_EMAIL\" | translate}}</div>\n      <div class=\"col-sm-8 col-lg-9\">\n        <input name=\"email\" placeholder=\"{{'EPM_DIV_PREFERRED_EMAIL' | translate}}\" [(ngModel)]=\"userAttributes.emails\"\n          class=\"form-control text-secondary\" type=\"email\" required (blur)=\"emailValidateMethod()\">\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <div class=\"col-sm-4 col-lg-3 text-secondary pt-2 p-t-0\"> {{\"EPM_DIV_MOBILE_PHONE_NUMBER\" | translate}}</div>\n      <div class=\"col-sm-8 col-lg-9\">\n        <input name=\"phone\" placeholder=\"{{'EPM_DIV_MOBILE_PHONE_NUMBER' | translate}}\" (keypress)=\"numberOnly($event)\"\n          (blur)=\"phoneLength()\" [ngModel]=\"userAttributes.phone_numbers | phoneNumber\"\n          (ngModelChange)=\"userAttributes.phone_numbers = $event\" class=\"form-control text-secondary\" type=\"text\"\n          maxlength=\"14\" required>\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <div class=\"col-sm-4 col-lg-3 text-secondary pt-2 p-t-0\"> {{\"EPM_DIV_HOME_PHONE_NUMBER\" | translate}}</div>\n      <div class=\"col-sm-8 col-lg-9\">\n        <input name=\"homePhone\" placeholder=\"{{'EPM_DIV_HOME_PHONE_NUMBER' | translate}}\" (keypress)=\"numberOnly($event)\"\n          (blur)=\"phoneLength()\" [ngModel]=\"userAttributes.home_phone | phoneNumber\"\n          (ngModelChange)=\"userAttributes.home_phone = $event\" class=\"form-control text-secondary\" type=\"text\"\n          maxlength=\"14\" required>\n      </div>\n    </div>\n\n    <div class=\"row bg-orange modal-head mt-2\">\n      <div class=\"col-sm-12 pt-2 pb-1\"></div>\n    </div>\n\n    <div class=\"row mt-2\">\n      <div class=\"col-sm-12\"> {{\"EPM_DIV_ADDRESS\" | translate}}</div>\n    </div>\n    <div class=\"row mt-2\">\n      <div class=\"col-sm-4 col-lg-3 text-secondary pt-2 p-t-0\"> {{\"EPM_DIV_STREET_ADDRESS_1\" | translate}}</div>\n      <div class=\"col-sm-8 col-lg-9\">\n        <input name=\"address1\" placeholder=\"{{'EPM_DIV_STREET_ADDRESS_1' | translate}}\"\n          [(ngModel)]=\"userAttributes.address.street\" maxlength=\"45\" class=\"form-control text-secondary\" type=\"text\"\n          required>\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <div class=\"col-sm-4 col-lg-3 text-secondary pt-2 p-t-0\"> {{\"EPM_DIV_STREET_ADDRESS_2\" | translate}}</div>\n      <div class=\"col-sm-8 col-lg-9\">\n        <input name=\"address2\" placeholder=\"{{'EPM_DIV_STREET_ADDRESS_2' | translate}}\"\n          [(ngModel)]=\"userAttributes.address.street2\" maxlength=\"45\" class=\"form-control text-secondary\" type=\"text\"\n          required>\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <!-- <div class=\"col-sm-2 col-lg-2 text-center left-text text-secondary pt-2 p-t-0\"> {{\"EPM_DIV_COUNTRY\" | translate}}</div>\n      <div class=\"col-sm-4 col-lg-4\">\n        <input name=\"country\" placeholder=\"Country\" [(ngModel)]=\"userAttributes.address.country\" class=\"form-control text-secondary\" \n          type=\"text\" required>\n      </div> -->\n      <div class=\"col-sm-2 col-lg-2 text-center left-text text-secondary pt-2 p-t-0\">{{\"EPM_DIV_CITY\" | translate}}\n      </div>\n      <div class=\"col-sm-4 col-lg-4\">\n        <input name=\"city\" placeholder=\"{{'EPM_DIV_CITY' | translate}}\" [(ngModel)]=\"userAttributes.address.city\" maxlength=\"20\"\n          class=\"form-control text-secondary\" type=\"text\" required>\n      </div>\n      <div class=\"col-sm-2 col-lg-2 text-center left-text text-secondary pt-2 p-t-0 small-screen-mt\">\n        {{\"EPM_DIV_STATE\" | translate}}</div>\n      <div class=\"col-sm-4 col-lg-4\">\n        <input name=\"state\" placeholder=\"{{'EPM_DIV_STATE' | translate}}\" [(ngModel)]=\"userAttributes.address.state\" maxlength=\"20\"\n          class=\"form-control text-secondary\" type=\"text\" required>\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <div class=\"col-sm-2 col-lg-2 text-center left-text text-secondary no-break pt-2 p-t-0\">\n        {{\"EPM_DIV_ZIP_CODE\" | translate}}</div>\n      <div class=\"col-sm-4 col-lg-4\">\n        <input name=\"zipcode\" (change)=\"verifyZipCode()\" placeholder=\"{{'EPM_DIV_ZIP_CODE' | translate}}\"\n          [(ngModel)]=\"userAttributes.address.zip\" class=\"form-control text-secondary\" type=\"text\" required>\n      </div>\n\n      <div class=\"col-sm-2 col-lg-2 text-center left-text text-secondary pt-2 p-t-0 small-screen-mt\"> {{\"EPM_DIV_COUNTRY\" | translate}}\n      </div>\n      <div class=\"col-sm-4 col-lg-4\">\n        <input name=\"country\" placeholder=\"{{'EPM_DIV_COUNTRY' | translate}}\"\n          [(ngModel)]=\"userAttributes.address.country\" maxlength=\"20\" class=\"form-control text-secondary\" type=\"text\" required>\n      </div>\n\n\n    </div>\n    <div class=\"modal-footer mt-4 border-none\">\n      <button type=\"submit\" class=\"btn btn-success save-employee-btn mr-3 cursor-pointer font-weight-bold\"\n        [disabled]=\"buttonSave\"> {{\"EPM_DIV_SAVE\" | translate}}</button>\n      <button type=\"button\" class=\"btn btn-danger save-employee-btn cursor-pointer font-weight-bold\"\n        (click)=\"closeModal('')\"> {{\"EPM_DIV_CANCEL\" | translate}}</button>\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/edit-profile-modal/edit-profile-modal.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/edit-profile-modal/edit-profile-modal.component.ts ***!
  \********************************************************************/
/*! exports provided: EditProfileModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfileModalComponent", function() { return EditProfileModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common/utils */ "./src/app/common/utils.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var EditProfileModalComponent = /** @class */ (function () {
    //@Output() onChange: EventEmitter<File> = new EventEmitter<File>();
    function EditProfileModalComponent(authService, backend, session, spinnerService, modalService, translate, notifierService) {
        this.authService = authService;
        this.backend = backend;
        this.session = session;
        this.spinnerService = spinnerService;
        this.modalService = modalService;
        this.translate = translate;
        this.notifierService = notifierService;
        this.childEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.DATE_FORMAT = 'YYYY-MM-DD';
        this.buttonSave = false;
        this.clicked = false;
        this.maxDate = new Date();
        this.notifier = notifierService;
        // this is default langugage
        translate.setDefaultLang('en');
        // this language is to set by your prefered browser language
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang);
    }
    EditProfileModalComponent.prototype.ngOnInit = function () {
        // Clone this
        this.userAttributes = _common_utils__WEBPACK_IMPORTED_MODULE_9__["default"].cloneJSON(this.authService.getUserProfile());
        if (this.userAttributes.dob) {
            this.userAttributes.dobInDate = moment__WEBPACK_IMPORTED_MODULE_6__["utc"](this.userAttributes.dob).toDate();
        }
        else {
            this.userAttributes.dobInDate = null;
        }
        if (!this.userAttributes.address) {
            this.userAttributes.address = {
                street: null,
                city: null,
                street2: null,
                country: null,
                state: null,
                zip: null
            };
        }
        this.authResultInfo = this.authService.getAccessTokenAndID();
    };
    EditProfileModalComponent.prototype.closeModal = function (event) {
        this.childEvent.emit(event);
    };
    //imagePicker event
    EditProfileModalComponent.prototype.updateSource = function ($event) {
        this.projectImage($event.target['files'][0]);
        this.clicked = true;
    };
    EditProfileModalComponent.prototype.projectImage = function (file) {
        var _this = this;
        var reader = new FileReader;
        reader.onload = function (e) {
            console.log("Data string url Base64", e.target.result);
            _this.userAttributes.picture = e.target.result;
        };
        reader.readAsDataURL(file);
    };
    /* Update profile Code */
    EditProfileModalComponent.prototype.updateUserProfile = function () {
        var _this = this;
        this.spinnerService.show();
        var data = {};
        var requiredField = true;
        if (this.userAttributes.first_name) {
            data.first_name = this.userAttributes.first_name;
        }
        else {
            requiredField = false;
        }
        if ('last_name' in this.userAttributes) {
            data.last_name = this.userAttributes.last_name;
            if (data.last_name == '') {
                data.last_name = null;
            }
        }
        if (this.userAttributes.dobInDate) {
            data.dob = moment__WEBPACK_IMPORTED_MODULE_6__["utc"](this.userAttributes.dobInDate).format(this.DATE_FORMAT);
            this.userAttributes.dob = data.dob;
        }
        if (this.userAttributes.address) {
            data.address = JSON.parse(JSON.stringify(this.userAttributes.address));
        }
        if (this.userAttributes.emails) {
            data.emails = this.userAttributes.emails;
        }
        if (this.userAttributes.phone_numbers) {
            data.phone_numbers = this.userAttributes.phone_numbers;
        }
        if (this.userAttributes.picture && this.clicked) {
            data.picture = this.userAttributes.picture;
        }
        console.log("Data : ", data);
        var path = "/users/" + this.authResultInfo.idTokenPayload.sub;
        if (requiredField) {
            this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(function (res) {
                // translate error message
                _this.translate.get('EPM_UPDATE_PROFILE')
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
                var response = res.json();
                console.log('Response: ', response);
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.userAttributes);
                _this.closeModal(_this.userAttributes);
                _this.spinnerService.hide();
            }, function (err) {
                var errorVar = err.json();
                console.log('Error: ', errorVar["message"]);
                // translate error message
                _this.translate.get('EPM_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + errorVar["message"]);
                });
                _this.spinnerService.hide();
            });
        }
        else {
            // translate error message
            this.translate.get('EPM_FIRSTNAME_REQUIRED')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
            this.spinnerService.hide();
        }
    };
    EditProfileModalComponent.prototype.verifyZipCode = function () {
        var _this = this;
        var re = /^[0-9]*[-\s]*[0-9]*$/;
        console.log("Zip code now", this.userAttributes.address.zip, re.test(this.userAttributes.address.zip));
        if (!re.test((this.userAttributes.address.zip).toString())) {
            // translate error message
            this.translate.get('EPM_VALID_ZIPCODE')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
            this.buttonSave = true;
        }
        else {
            this.buttonSave = false;
        }
    };
    EditProfileModalComponent.prototype.emailValidateMethod = function () {
        var _this = this;
        var re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
        if (!re.test(this.userAttributes.emails)) {
            if (this.userAttributes.emails == '') {
                this.buttonSave = false;
            }
            else {
                if (this.userAttributes.emails) {
                    // translate error message
                    this.translate.get('EPM_VALID_EMAIL')
                        .subscribe(function (value) {
                        _this.notifier.notify('error', value);
                    });
                    this.buttonSave = true;
                }
                else {
                    this.buttonSave = false;
                }
            }
        }
        else {
            this.buttonSave = false;
        }
    };
    // check the phone number length 
    EditProfileModalComponent.prototype.phoneLength = function () {
        var _this = this;
        if ('phone_numbers' in this.userAttributes && this.userAttributes.phone_numbers && this.userAttributes.phone_numbers.length < 14) {
            // translate error message
            this.translate.get('EPM_VALID_PHONENUMBER')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
            this.buttonSave = true;
        }
        else {
            this.buttonSave = false;
        }
    };
    // restrict alpha character and allow only number
    EditProfileModalComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    EditProfileModalComponent.prototype.alphabetOnly = function (event) {
        var regex = new RegExp("^[a-zA-Z ]*$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], EditProfileModalComponent.prototype, "childEvent", void 0);
    EditProfileModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-profile-modal',
            template: __webpack_require__(/*! ./edit-profile-modal.component.html */ "./src/app/edit-profile-modal/edit-profile-modal.component.html"),
            styles: [__webpack_require__(/*! ./edit-profile-modal.component.css */ "./src/app/edit-profile-modal/edit-profile-modal.component.css"), __webpack_require__(/*! ../global/global.css */ "./src/app/global/global.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"],
            angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["BsModalService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]])
    ], EditProfileModalComponent);
    return EditProfileModalComponent;
}());



/***/ }),

/***/ "./src/app/email-verification/email-verification.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/email-verification/email-verification.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button-right{\n    float:right;\n}\n.img-size{\n    width:30%;\n    height:80%;\n}"

/***/ }),

/***/ "./src/app/email-verification/email-verification.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/email-verification/email-verification.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#myModal\">\n  Open modal\n</button>\n<!-- The Modal -->\n<div class=\"modal\" id=\"myModal\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n\n      <!-- Modal Header -->\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Verify your email</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n\n      <!-- Modal body -->\n      <div class=\"col-md-12 col-sm-12 col-12 text-center\">\n        We have sent verification link on your email.\n        Please Check!!\n      </div>\n      <!-- <div class=\"col-md-4 col-sm-4 col-4\"></div> -->\n      <div class=\"row\">\n        <div class=\"col d-flex justify-content-center\">\n          <img src=\"../assets/Verify-Logo_stacked-2017.png\" class=\"img-size\">\n        </div>\n      </div>\n      <!-- <div class=\"col-md-4 col-sm-4 col-4\"></div> -->\n\n\n      <!-- Modal footer -->\n      <div class=\"float-right\">\n        <button type=\"button\" class=\"btn btn-success button-right mb-3 mr-3\" data-dismiss=\"modal\" (click)=\"resend()\">Resend Email</button>\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/email-verification/email-verification.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/email-verification/email-verification.component.ts ***!
  \********************************************************************/
/*! exports provided: EmailVerificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailVerificationComponent", function() { return EmailVerificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmailVerificationComponent = /** @class */ (function () {
    function EmailVerificationComponent(router) {
        this.router = router;
        this.statusMessageHeading = 'EV_VERIFIED';
    }
    EmailVerificationComponent.prototype.ngOnInit = function () {
    };
    EmailVerificationComponent.prototype.resend = function () {
        this.router.navigate(['/']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('promptModal'),
        __metadata("design:type", Object)
    ], EmailVerificationComponent.prototype, "modal", void 0);
    EmailVerificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email-verification',
            template: __webpack_require__(/*! ./email-verification.component.html */ "./src/app/email-verification/email-verification.component.html"),
            styles: [__webpack_require__(/*! ./email-verification.component.css */ "./src/app/email-verification/email-verification.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], EmailVerificationComponent);
    return EmailVerificationComponent;
}());



/***/ }),

/***/ "./src/app/generatereport-page/generatereport-page.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/generatereport-page/generatereport-page.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".gen-report{\n  height:125px;\n}\n.profile-pic{\n  height:90px;\n  vertical-align: top;\n}\n.profile-content{\n  display:inline-block;\n}\n.zoom-checkbox{\n  zoom:2;\n  margin-left:-2rem;\n}\n.form-check-label span{\n  vertical-align:sub;\n  font-size:1.3rem;\n}\n.open-report-btn{\n  white-space:nowrap;\n  font-weight: 600;\n}\nngx-avatar {\n  display:inline-block;\n}\n/* Custom checkbox */\n.zoom-checkbox {\n  -webkit-appearance: none;\n  -moz-appearance: none;\n}\n.zoom-checkbox::-ms-check {\n  display: none;\n}\n.zoom-checkbox {\n  position: relative;\n  width: 1em;\n  height: 1em;\n  border: 1px solid green;\n  vertical-align: middle;\n  color: green;\n  right:5px;\n}\n.zoom-checkbox::before {\n  content: \"✔\";\n  position: absolute;\n  font-size: 1.2em;\n  left: 0;\n  top: -0.35em;\n  visibility: hidden;\n}\n.zoom-checkbox:checked::before {\n  /* Use `visibility` instead of `display` to avoid recalculating layout */\n  visibility: visible;\n}\n.link-expire{\n  background: rgb(255, 231, 213);\n  color: rgb(230, 135, 69);\n}\n.link-expire div{\n  font-size: 14px;\n}\n@media screen and (max-width:480px){\n  .gen-report{\n    width:100%;\n  }\n  .font-check, .font-check .form-check-label span{\n    font-size:12px !important;\n  }\n  .gen-button{\n    padding-left:0px;\n    padding-right:0px;\n    font-size:12px !important;\n  }\n}\n\n"

/***/ }),

/***/ "./src/app/generatereport-page/generatereport-page.component.html":
/*!************************************************************************!*\
  !*** ./src/app/generatereport-page/generatereport-page.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" *ngIf=\"report\">\n  <div class=\"row justify-content-center align-items-center\">\n    <div align=\"center\" class=\"mt-3\">\n      <div class=\"row ml-0 mr-0\">\n        <div class=\"col-sm-12 logo-size\">\n            <img src=\"assets/nexus-log.png\" alt=\"\" class=\"gen-report\"/>\n        </div>\n      </div>\n      <div class=\"row ml-0 mr-0 mt-3 text-left\">\n        <div class=\"col-sm-12\">\n            <img *ngIf=\"(report.picture.includes('http')) || (report.picture.includes('data'))\" class=\"rounded-circle profile-pic\" [src]=\"report.picture\" alt=\"\" />\n            <ngx-avatar size=\"125\" *ngIf=\"!(report.picture.includes('http')) && !(report.picture.includes('data'))\" name=\"{{report.first_name}}\"></ngx-avatar>\n          <div class=\"pl-3 profile-content\">\n            <h2 class=\"text-left font-check\">{{report.first_name}} {{report.last_name}}</h2>\n            <h2 class=\"text-left font-check\">{{ \"GRP_H2_FINANCIAL_REPORT\" | translate}}</h2>\n          </div>\n        </div>\n      </div>\n      <div class=\"row mr-0 ml-0 mt-4 mb-3 text-left\">\n        <div class=\"col-sm-12 pl-2\">\n          <div class=\"form-check font-check\">\n            <label class=\"form-check-label\" for=\"check1\">\n              <img src=\"assets/tick.png\" alt=\"\" class=\"mr-2\"/>\n              <!-- <input type=\"checkbox\" class=\"form-check-input zoom-checkbox\" id=\"check1\" checked> -->\n              <span>{{\"GRP_SPAN_REAL_TIME\" | translate}}</span>\n            </label>\n          </div>\n          <div class=\"form-check font-check\">\n            <label class=\"form-check-label\" for=\"check2\">\n              <img src=\"assets/tick.png\" alt=\"\" class=\"mr-2\"/>\n              <!-- <input type=\"checkbox\" class=\"form-check-input zoom-checkbox\" id=\"check2\" checked> -->\n              <span>{{\"GRP_SPAN_SECURE\" | translate}}</span>\n            </label>\n          </div>\n          <div class=\"form-check font-check\">\n            <label class=\"form-check-label\">\n              <img src=\"assets/tick.png\" alt=\"\" class=\"mr-2\"/>\n              <!-- <input type=\"checkbox\" class=\"form-check-input zoom-checkbox\" id=\"check3\" checked> -->\n              <span>{{\"GRP_SPAN_FREE\" | translate}}</span>\n            </label>\n          </div>\n        </div>\n      </div>\n      <div class=\"row mr-0 ml-0\">\n        <div class=\"col-sm-12\">\n          <button type=\"button\" class=\"btn btn-lg btn-success open-report-btn pl-5 pr-5 mt-3 mb-2 gen-button\" *ngIf=\"report\" (click)=\"open()\">{{ \"GRP_BUTTON_OPEN_REPORT\" | translate}}</button>\n        </div>\n      </div>\n      <!-- <div class=\"row mr-0 ml-0 mt-2 mb-4\" *ngIf=\"report\">\n        <div class=\"col-sm-12 link-expire text-center pt-1 pb-1\">This link will expire in {{report.valid_till | datediffformat}}</div>\n      </div> -->\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/generatereport-page/generatereport-page.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/generatereport-page/generatereport-page.component.ts ***!
  \**********************************************************************/
/*! exports provided: GeneratereportPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneratereportPageComponent", function() { return GeneratereportPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GeneratereportPageComponent = /** @class */ (function () {
    function GeneratereportPageComponent(router, route, spinnerService, notifierService, translate, backend) {
        this.router = router;
        this.route = route;
        this.spinnerService = spinnerService;
        this.notifierService = notifierService;
        this.translate = translate;
        this.backend = backend;
        this.id = null;
        this.report = null;
        this.notifier = notifierService;
        {
            // this is default langugage
            translate.setDefaultLang('en');
            // this language is to set by your prefered browser language
            var browserLang = translate.getBrowserLang();
            translate.use(browserLang);
        }
    }
    GeneratereportPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.id = this.route.snapshot.paramMap.get("id");
        console.log('report', this.id);
        this.spinnerService.show();
        // Get report details
        this.backend.get("/reports/" + this.id + "?expand=users", null)
            .subscribe(function (data) {
            _this.report = data.json();
            console.log('report Data', _this.report);
            _this.translate.get('GP_TIME START')
                .subscribe(function (value) {
                _this.notifier.notify('success', value);
            });
            _this.spinnerService.hide();
        }, function (error) {
            console.log('error', error);
            _this.spinnerService.hide();
            var errorDetails = error.json();
            _this.notifier.notify('error', errorDetails.message);
        });
    };
    GeneratereportPageComponent.prototype.open = function () {
        this.router.navigate(["/report/" + this.id + "/open"]);
    };
    GeneratereportPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-generatereport-page',
            template: __webpack_require__(/*! ./generatereport-page.component.html */ "./src/app/generatereport-page/generatereport-page.component.html"),
            styles: [__webpack_require__(/*! ./generatereport-page.component.css */ "./src/app/generatereport-page/generatereport-page.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_4__["NotifierService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_2__["BackendService"]])
    ], GeneratereportPageComponent);
    return GeneratereportPageComponent;
}());



/***/ }),

/***/ "./src/app/global/global.css":
/*!***********************************!*\
  !*** ./src/app/global/global.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ns-font-16{\n    font-size: 16px;\n}\n.ns-font-13{\n    font-size: 13px;\n}\n.ns-font-23{\n    font-size: 23px;\n}\n.ns-font-34{\n    font-size: 34px;\n}\n.ns-font-14{\n    font-size: 14px;\n}\n.ns-font-12{\n    font-size: 12px;\n}\n.ns-font-20{\n    font-size: 20px;\n}\n.ns-font-22{\n    font-size: 22px;\n}\n.ns-font-28{\n    font-size: 28px;\n}\n.ns-font-18{\n    font-size: 18px;\n}\n.ns-color-white{\n    color: white;\n}\n.ns-font-weight-600{\n    font-weight: 600;\n}\n.ns-font-weight-bold{\n    font-weight: bold;\n}\n.ns-display-center{\n    display: flex;\n    justify-content: center;\n    align-items: center;\n}\n.ns-display-center-items{\n    display: flex;\n    align-items: center;\n}\n.ns-visibilty-hidden{\n    visibility: hidden;\n}\n.ns-cursor-pointer{\n    cursor: pointer;\n}\n.ns-width-100p{\n    width:100%;\n}\n.ns-border-none{\n    border:none;\n}\n.ns-bg-none{\n    background: none;\n}\n.ns-required:before{\n    color: red;\n    content: \"*\";\n    position: absolute;\n    margin-left: -10px;\n }\n.ns-justify-text{\n     justify-content:center;\n }\n.ns-text-center{\n     text-align:center;\n }\n.ns-text-overflow-ellipsis{\n    text-overflow: ellipsis;\n    overflow: hidden;\n    white-space: nowrap;\n }"

/***/ }),

/***/ "./src/app/guage-chart/guage-chart.component.css":
/*!*******************************************************!*\
  !*** ./src/app/guage-chart/guage-chart.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "*{ font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.gauge-chart {\n  display: flex;\n  flex-direction: column;\n  text-align: center;\n}\n\n.gauge-chart__label { font-weight: bold; }"

/***/ }),

/***/ "./src/app/guage-chart/guage-chart.component.html":
/*!********************************************************!*\
  !*** ./src/app/guage-chart/guage-chart.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"gauge-chart\" [style.width.px]=\"canvasWidth\">\n  <span\n    [style.font-size.px]=\"nameFont\"\n    [style.margin-bottom.px]=\"nameMargin\">\n    {{name}}\n  </span>\n  <div #gaugeArea></div>\n  <span\n    class=\"gauge-chart__label\"\n    [style.font-size.px]=\"bottomLabelFont\"\n    [style.margin-top.px]=\"bottomLabelMargin\">\n    {{bottomLabel}}\n  </span>\n</div>\n"

/***/ }),

/***/ "./src/app/guage-chart/guage-chart.component.ts":
/*!******************************************************!*\
  !*** ./src/app/guage-chart/guage-chart.component.ts ***!
  \******************************************************/
/*! exports provided: GaugeChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaugeChartComponent", function() { return GaugeChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var gauge_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gauge-chart */ "./node_modules/gauge-chart/dist/bundle.js");
/* harmony import */ var gauge_chart__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(gauge_chart__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
 * GaugeChart Component
 */
var GaugeChartComponent = /** @class */ (function () {
    function GaugeChartComponent() {
    }
    GaugeChartComponent.prototype.ngOnInit = function () {
        // calculate styles for name and bottomLabel
        if (this.name) {
            if (!this.nameFont) {
                this.nameFont = '' + Math.round(this.canvasWidth / 15);
            }
            this.nameMargin = '' + Math.round(+this.nameFont / 4);
        }
        if (this.bottomLabel) {
            if (!this.bottomLabelFont) {
                this.bottomLabelFont = '' + Math.round(this.canvasWidth / 10);
            }
            this.bottomLabelMargin = '-' + this.bottomLabelFont;
        }
        if (this.optionsCheck()) {
            this.element = this.gaugeArea.nativeElement;
            this.options.centralLabel = this.centralLabel;
            // Drawing and updating the chart
            this.gaugeChart = gauge_chart__WEBPACK_IMPORTED_MODULE_1__["gaugeChart"](this.element, this.canvasWidth, this.options);
            this.gaugeChart.updateNeedle(this.needleValue);
        }
    };
    GaugeChartComponent.prototype.optionsCheck = function () {
        if (this.canvasWidth == null) {
            console.warn('gauge-chart warning: canvasWidth is not specified!');
            return false;
        }
        else if (this.needleValue == null) {
            console.warn('gauge-chart warning: needleValue is not specified!');
            return false;
        }
        if (this.centralLabel == null) {
            this.centralLabel = '';
        }
        return true;
    };
    GaugeChartComponent.prototype.ngOnChanges = function (changes) {
        if (changes.needleValue && !changes.needleValue.firstChange) {
            if (changes.needleValue.currentValue !== changes.needleValue.previousValue) {
                this.needleValue = changes.needleValue.currentValue;
                this.gaugeChart.updateNeedle(this.needleValue);
            }
        }
        if (changes.centralLabel && !changes.centralLabel.firstChange) {
            if (changes.centralLabel.currentValue !== changes.centralLabel.previousValue) {
                this.gaugeChart.removeGauge();
                this.centralLabel = changes.centralLabel.currentValue;
                this.options.centralLabel = this.centralLabel;
                this.gaugeChart = gauge_chart__WEBPACK_IMPORTED_MODULE_1__["gaugeChart"](this.element, this.canvasWidth, this.options);
                this.gaugeChart.updateNeedle(this.needleValue);
            }
        }
        if (changes.centralLabel && !changes.centralLabel.firstChange) {
            if (changes.bottomLabel.currentValue !== changes.bottomLabel.previousValue) {
                console.log(changes.bottomLabel.currentValue);
            }
        }
        if (changes.canvasWidth && !changes.canvasWidth.firstChange && changes.canvasWidth.currentValue != changes.canvasWidth.previousValue) {
            this.gaugeChart.removeGauge();
            this.gaugeChart = gauge_chart__WEBPACK_IMPORTED_MODULE_1__["gaugeChart"](this.element, this.canvasWidth, this.options);
            this.gaugeChart.updateNeedle(this.needleValue);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('gaugeArea'),
        __metadata("design:type", Object)
    ], GaugeChartComponent.prototype, "gaugeArea", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], GaugeChartComponent.prototype, "canvasWidth", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], GaugeChartComponent.prototype, "needleValue", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], GaugeChartComponent.prototype, "centralLabel", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], GaugeChartComponent.prototype, "options", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], GaugeChartComponent.prototype, "name", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], GaugeChartComponent.prototype, "nameFont", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], GaugeChartComponent.prototype, "bottomLabel", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], GaugeChartComponent.prototype, "bottomLabelFont", void 0);
    GaugeChartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ns-gauge-chart',
            template: __webpack_require__(/*! ./guage-chart.component.html */ "./src/app/guage-chart/guage-chart.component.html"),
            styles: [__webpack_require__(/*! ./guage-chart.component.css */ "./src/app/guage-chart/guage-chart.component.css")],
        })
    ], GaugeChartComponent);
    return GaugeChartComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar{\n  background:#393939;\n}\n.user-icon{\n  width:35px;\n  height:35px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius:50%;\n  display: inline-block;\n}\n.navbar-nav .dropdown-menu {\n  position: absolute;\n  right:0 !important;\n}\n.white{\n  color:#ffffff;\n}\n.navbar-brand{\n  display:contents;\n}\n.pic-logo-icon{\n  padding-bottom:10px;\n  width:130px;\n}\n.add-account{\n  font-size: 15px;\n  color: white!important;\n  margin-right: 6px;\n}\n.padding-account{\n  padding:.4rem .1rem;\n}\n.custom-display{\n  display: inline-block;\n}\n.p-width{\n  width:80px;\n}\n.dropdown-size{\n  min-width:0px;\n}\n.dropdown-signout{\n  font-size: 12px;\n  padding: 0px;\n  display: flex;\n  justify-content: center;\n}\n.blank-display{\n  position: absolute;\n  background: #353434;\n  height: 100%;\n  width: 100%;\n  z-index: 999;\n}\n@media screen and (max-width:375px){\n  .p-width{\n    width:125px;\n  }\n  .dropdown-menu{\n    left:-30px;\n  }\n}\n@media screen and (max-width:320px){\n  .p-width{\n    width:120px;\n  }\n  .logo-icon{\n    width:70px;\n  }\n}\n.account-number-font{\n        height: 25px;\n        background-color: red;\n        width: 25px;\n        border-radius: 50%;\n        position: absolute;\n        bottom: 34px;\n        right: 82px;\n        font-size: 12px;\n        font-weight: bold;\n        color: #ffffff;\n        padding-right: 10px;\n        padding-top: 4px;\n}\n.navbar-position{\n  position:fixed;\n  z-index:999;\n  width:100%;\n}\n"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngClass] = \"user.accounts.length == 0 ? 'blank-display' : ''\"></div>\n<nav class=\"navbar navbar-light navbar-position\">\n  <a  (click)=\"profileSection()\" class=\"navbar-brand cursor-pointer\"><img class=\"pic-logo-icon\" src=\"../../assets/Nexus-new-logo.png\" alt=\"...\"></a>\n  <!-- navbar right -->\n  <ul class=\"nav navbar-nav ml-auto flex-row text-right responsive-top\">\n    <li class=\"nav-item d-none\">\n      <button class=\"plaid-button1\"><i class=\"fas fa-plus-square mr-2\"></i>{{\"H_BUTTON_ADD_ACCOUNT\" | translate}}</button>\n      <div class=\"account-number-font\">5</div>\n    </li>\n    <li class=\"nav-item dropdown p-width\">\n      <a class=\"nav-link p-0 clear\" data-toggle=\"dropdown\">\n        <span class=\"avatar w-32 white cursor-pointer\" *ngIf=\"(user.picture) != 'NA'\">\n          <img *ngIf=\"user.picture && ((user.picture).includes('http') || (user.picture.includes('data')))\" class=\"user-icon\" [src]=\"user.picture\" alt=\"...\"> {{user.nickname}}\n          <ngx-avatar size=\"40\" class=\"user-icon\" *ngIf=\"!user.picture || !(user.picture.includes('http')) && !(user.picture.includes('data'))\" name=\"{{user.first_name}}\"></ngx-avatar> {{user.nickname}}\n          <i class=\"fa fa-caret-down ml-2\" aria-hidden=\"true\"></i>\n        </span>\n      </a>\n      <div class=\"dropdown-menu dropdown-menu-overlay ng-scope cursor-pointer dropdown-size\">\n        <!-- <a class=\"dropdown-item\" ng-href=\"#!/profile\">My Profile</a> -->\n        <a class=\"dropdown-item dropdown-signout\" (click)=\"logout()\">{{\"H_A_SIGN_OUT\" | translate}}</a>\n      </div>\n    </li>\n  </ul>\n  <!-- navbar right -->\n</nav>\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.hitProfile = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    HeaderComponent.prototype.ngOnInit = function () {
        console.log('inside header');
        this.userToken = this.authService.getDataFromCache('tokens');
        if (!this.userToken) {
            this.router.navigate(['/']);
        }
        console.log('userInfo', this.user);
    };
    HeaderComponent.prototype.logout = function () {
        this.authService.getUserLogout();
    };
    HeaderComponent.prototype.ngOnChanges = function (changes) {
        console.log("In header ngOnChanges");
        this.user = this.authService.getUserProfile();
    };
    HeaderComponent.prototype.profileSection = function () {
        this.hitProfile.emit('hitprofile');
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "user", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "hitProfile", void 0);
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-page{\n  border:1px solid #cecece;\n  /*width:25%;*/\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n.bg-blue{\n  background:#5862D3;\n}\n.white{\n  color:#ffffff;\n}\n.bold{\n  font-weight:600;\n}\n.sign-in{\n  padding: 7px 0 7px 0;\n  font-size: 14px;\n}\n.sign-in-form{\n  padding:20px;\n}\n.remember{\n  font-size: 13px;\n  color: #999999;\n}\n.width-100{\n  width:100%;\n}\n.btn-size{\n  font-size:18px;\n}\n.icon-size{\n  font-size:28px;\n  vertical-align:middle;\n  margin-right: 10px;\n}\n.text-size{\n  font-size:12px;\n  color:#999999;\n  margin-top: 10px;\n  margin-bottom: 10px !important;\n}\n.text-size1{\n  font-size:14px;\n  margin-bottom: 5px !important;\n}\nhr{\n  margin-top: 0rem;\n  margin-bottom: 0.3rem;\n  border-top: 1px solid rgba(0,0,0,0.3);\n}\n.bg-signUp{\n  background:#F89B2A;\n  padding: .25rem .75rem;\n  font-size: 14px;\n}\n.m-b{\n  margin-bottom:0.6rem;\n}\ninput::-webkit-input-placeholder {\n  color: #999999;\n}\ninput::-ms-input-placeholder {\n  color: #999999;\n}\ninput::placeholder {\n  color: #999999;\n}\n.input-element{\n  border-radius:0 !important;\n  border:none;\n  border-bottom: 1px solid #ced4da;\n  padding:0;\n}\n.input-element:focus{\n  box-shadow:none;\n  border-bottom:2px solid #3AB9B2;\n}\n/*.placeholder-move{\n  position:relative;\n}\n.floating-placeholder {\n  color: #999999;\n  position: absolute;\n  pointer-events: none;\n  left: 0px;\n  top: 10px;\n  transition: 0.2s ease all;\n}\n.floating-placeholder1 {\n  color: #999999;\n  position: absolute;\n  pointer-events: none;\n  left: 0px;\n  top: 10px;\n  transition: 0.2s ease all;\n}\ninput.input-element:focus + .floating-placeholder, input.input-element:not(:focus):invalid + .floating-placeholder{\n  color:#3AB9B2;\n  transform: scale(0.75) translateY(-125%) translateX(-7px);\n}\ninput.input-element:focus + .floating-placeholder1, input.input-element:not(:focus):invalid + .floating-placeholder{\n  color:#3AB9B2;\n  transform: scale(0.75) translateY(-125%) translateX(-13px);\n}*/\n@media (min-width: 768px){\n  .login-page {\n      width:35%;\n  }\n}\n@media (min-width: 1100px){\n  .login-page {\n      width:25%;\n  }\n}\n.bg-image{\n  background-image:url(\"/assets/visa-card.png\");\n  background-size:100% 100% ;\n  height:97vh;\n}\n"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"bg-image\" id=\"logincontainer\">\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, route, notifierService) {
        this.authService = authService;
        this.route = route;
        this.notifierService = notifierService;
        this.routeHash = null;
        this.notifier = notifierService;
        this.routeHash = route.snapshot.fragment;
        // console.log('fragment',this.routeHash);
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (response) {
            console.log('response', response);
            if ('success' in response && response.success && 'message' in response) {
                _this.notifier.notify('success', response.message);
            }
        });
        this.authService.showLogin();
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], angular_notifier__WEBPACK_IMPORTED_MODULE_3__["NotifierService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/manage-account/manage-account.component.css":
/*!*************************************************************!*\
  !*** ./src/app/manage-account/manage-account.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".add-account-btns{\n  background:#337ab7;\n  color: #fff;\n  font-size: 15px;\n  padding: 5px 10px 5px 10px;\n  border: 0px;\n}\n.manage-accounts{\n  font-size:25px;\n  display: flex;\n  justify-content: space-between;\n}\n.bank-name{\n  font-size:20px;\n  background:#dcdcdc;\n  color:#666666;\n  font-weight: bold;\n  border-bottom: 1px solid #000;\n  padding-left: 1.2rem !important;\n}\n.white{\n  color:#ffffff !important;\n}\n.main-account-check{\n  font-size:15px;\n}\n.amount-dollar{\n  color:#46a81b;\n}\n.save-account-btn{\n  background:#337ab7;\n  color: #fff;\n  font-size: 15px;\n  padding: 5px 10px 5px 10px;\n  border: 0px;\n  width: 120px;\n}\n.delete-account-btn{\n  background:red;\n  color: #fff;\n  font-size: 15px;\n  padding: 5px 10px 5px 10px;\n  border: 0px;\n  width: 120px;\n}\n.info-message{\n  color:red;\n  font-size:17px;\n}\n.name-details{\n  width:250px;\n}\n.zestimate{\n  color: #46a81b;\n  text-decoration:underline;\n  font-size:14px;\n  font-weight:bold;\n}\n.zillow-img{\n  width:20%;\n}\n.zillow-text{\n  font-size:14px;\n  color:#A9A9A9;\n}\n.image-logo{\n  width:50px;\n  height:50px;\n}\n.gray-bg{\n  background:#f2f1f2;\n  margin-left:1px;\n  margin-right:0px;\n}\n.button-align{\n  display:inline;\n}\n.button-height{\n  height:36px;\n}\n.number-span {\n  position: relative;\n  bottom: 42px;\n  height: 25px;\n  width: 25px;\n  background: #337ab7;\n  border-radius: 50%;\n  font-size: 12px;\n  padding: 4px 9px;\n  color:#ffffff;\n}\n.margin-cog{\n  margin-top:-20px;\n}\n@media only screen and (max-width: 600px) {\n  .manage-accounts {\n    font-size:15px;\n    margin-left:10px;\n  }\n .info-message {\n    display:none;\n  }\n}\n.dropdown-position{\n   z-index:1;\n   min-width:0px;\n   left:50px;\n   -webkit-transform: translate3d(-70px,26px, 0px) !important;\n           transform: translate3d(-70px,26px, 0px) !important;\n}\n.instname-color{\n  color:#ffffff;\n}\n.dropdown-position1{\n  z-index:1;\n  min-width:0px;\n  left:50px;\n}\n.position1{\n  position:relative;\n}\n.position2{\n  position:absolute;\n  top:0;\n  right:0\n}\n"

/***/ }),

/***/ "./src/app/manage-account/manage-account.component.html":
/*!**************************************************************!*\
  !*** ./src/app/manage-account/manage-account.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"manage-accounts\">\n    <div>{{\"MA_DIV_MANAGE_ACCOUNTS\" | translate}}</div>\n    <button #invokeAddAccount class=\"float-right add-account-btns font-weight-bold cursor-pointer mr-3\"\n      (click)=\"addNewAccount()\"><i class=\"fa fa-plus mr-1\"></i>{{\"MA_BUTTON_ADD_ACCOUNT\" | translate}}</button>\n  </div>\n  <div class=\"border border-dark mt-1\" *ngFor=\"let accounts of user.accounts; let i = index\">\n    <div class=\"p-2 bank-name\" [ngStyle]=\"{'background': accounts.primaryColor}\">\n      <img [src]=\"'data:image/jpg;base64,'+accounts.logo | safeHtml\" class=\"image-logo\">\n      <span class=\"instname-color ml-2\">{{accounts.inst_name}}</span>\n      <span class=\"float-right m-2 white\">\n        <div class=\"dropdown\">\n          <i class=\"fa fa-cog cursor-pointer\" id=\"dropdownMenuButton\" data-target=\"dropdownnotifyButton\"\n            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"></i>\n          <div id=\"dropdownnotifyButton\" class=\"dropdown-menu dropdown-position\" aria-labelledby=\"dropdownMenuButton\">\n            <a class=\"dropdown-item cursor-pointer\" (click)=\"editAccounts(accounts.id)\">{{\"MA_A_EDIT\" | translate}}</a>\n            <a class=\"dropdown-item cursor-pointer\" data-toggle=\"modal\" data-target=\"#deleteIndividually\"\n              (click)=\"passDltValue(accounts.id, i, 'ACCOUNTS')\">{{\"MA_A_DELETE\" | translate}}</a>\n          </div>\n        </div>\n        <div class=\"number-span d-none\" *ngIf=\"accounts.subAccountsLength\">{{accounts.subAccountsLength.length}}</div>\n      </span>\n    </div>\n    <div class=\"gray-bg\">\n      <div *ngFor=\"let subAccount of accounts.accounts; let j = index\">\n        <div class=\"p-3\"\n          *ngIf=\"subAccount.type == 'depository' || subAccount.type == 'credit' || subAccount.type == 'brokerage'\">\n          <div class=\"row ml-3 mr-3\">\n            <div class=\"col-md-9 col-sm-8 col-8\">\n              <div class=\"cursor-pointer main-account-check font-weight-bold\">\n                <span (click)=\"showAccount(accounts.accounts, j)\" class=\"cursor-pointer\">\n                  <i class=\"fa fa-angle-right mr-2 font-weight-bold\" *ngIf=\"!subAccount.showDetails\"></i>\n                  <i class=\"fa fa-angle-down mr-2 font-weight-bold\" *ngIf=\"subAccount.showDetails\"></i>\n                  {{subAccount.official_name}}\n                </span>\n              </div>\n            </div>\n            <div class=\"col-md-3 col-sm-4 col-4 amount-dollar text-right\">\n              {{subAccount.balances.current | currency}}\n            </div>\n          </div>\n          <div class=\"ml-4 mt-2\" *ngIf=\"subAccount.showDetails\">\n            <div class=\"row ml-3 mr-3\">\n              <div class=\"col-lg-4 col-md-6 col-sm-6 col-7\">\n                {{\"MA_DIV_NAME\" | translate}}\n                <div>\n                  <input class=\"form-control\" [(ngModel)]=\"subAccount.official_name\" placeholder=\"\" />\n                </div>\n              </div>\n              <div class=\"col-lg-4 col-md-4 col-sm-6 col-5\">\n                {{\"MA_DIV_STATUS\" | translate}}\n                <div>\n                  <select class=\"form-control cursor-pointer\" name=\"status{{j+1}}\" [(ngModel)]=\"subAccount.status\">\n                    <option value=\"ACTIVE\">{{\"MA_OPTION_ACTIVE\" | translate}}</option>\n                    <option value=\"INACTIVE\">{{\"MA_OPTION_NOT_ACTIVE\" | translate}}</option>\n                  </select>\n                </div>\n              </div>\n              <div class=\"col-lg-4 col-md-2\">\n                <button class=\"float-right save-account-btn font-weight-bold cursor-pointer mt-4 button-height\"\n                  (click)=\"setStatusOfAccount(accounts.id, subAccount)\">{{\"MA_BUTTON_SAVE\" | translate}}</button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"border border-dark mt-3\">\n    <div class=\"p-3 bank-name bg-info instname-color\">\n      {{\"MA_DIV_PROPERTIES\" | translate}}\n      <span class=\"float-right white\">\n        <div class=\"dropdown\">\n          <i class=\"fa fa-cog cursor-pointer\" id=\"dropdownMenuButton\" data-target=\"dropdownnotifyButton\"\n            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"></i>\n          <div id=\"dropdownnotifyButton\" class=\"dropdown-menu dropdown-position1\" aria-labelledby=\"dropdownMenuButton\">\n            <a class=\"dropdown-item cursor-pointer\"\n              (click)=\"propertyHandler('callingAddProperty')\">{{\"MA_A_ADD_PROPERTY\" | translate}}</a>\n            <a class=\"dropdown-item cursor-pointer\" *ngIf=\"(user.properties.length > 0)\" data-toggle=\"modal\"\n              data-target=\"#exampleModalCenter\"\n              (click)=\"deleteAll('All_Property')\">{{\"MA_A_DELETE_ALL\" | translate}}</a>\n          </div>\n        </div>\n      </span>\n    </div>\n    <div class=\"text-center pt-2 pb-2\" *ngIf=\"!(user.properties.length > 0)\">{{\"MA_DIV_NO_PROPERTY_ADDED\" | translate}}\n    </div>\n    <div class=\"p-3 gray-bg\" *ngFor=\"let value of user.properties; let i = index\">\n      <div class=\"row ml-3 mr-3\">\n        <div class=\"col-md-9 col-sm-8 col-8\">\n          <div class=\"cursor-pointer main-account-check font-weight-bold\">\n            <span (click)=\"showAccount(user.properties, i)\" class=\"cursor-pointer\">\n              <i class=\"fa fa-angle-right mr-2 font-weight-bold\" *ngIf=\"!value.showDetails\"></i>\n              <i class=\"fa fa-angle-down mr-2 font-weight-bold\" *ngIf=\"value.showDetails\"></i>\n              <span *ngIf=\"!value.subtypeToUpdate\">{{value.type}} {{value.nameToUpdate}}</span>\n              <span *ngIf=\"value.subtypeToUpdate\">{{value.type}} ({{value.subtypeToUpdate}})</span>\n            </span>\n          </div>\n        </div>\n        <div class=\"col-md-3 col-sm-4 col-4 amount-dollar text-right\">\n          {{value.estimated_value | currency}}\n        </div>\n      </div>\n      <div class=\"ml-4 mt-2 position1\" *ngIf=\"value.showDetails\">\n        <div class=\"row ml-3 mr-3\">\n          <div class=\"col-sm-12\">\n            {{\"MA_DIV_NAME\" | translate}}\n            <div *ngIf=\"!value.subtypeToUpdate\">\n              <input class=\"form-control name-details button-align\" [(ngModel)]=\"value.name\" placeholder=\"\" />\n              <button class=\"float-right save-account-btn font-weight-bold cursor-pointer button-height\"\n              (click)=\"updatePropertyName(value, i)\">{{\"MA_BUTTON_SAVE\" | translate}}</button>\n            <button class=\"float-right delete-account-btn font-weight-bold cursor-pointer mr-3 button-height\"\n              data-toggle=\"modal\" data-target=\"#deleteIndividually\"\n              (click)=\"passDltValue(value, i, 'PROPERTY')\">{{\"MA_BUTTON_DELETE\" | translate}}</button>\n            </div>\n            <div *ngIf=\"value.subtypeToUpdate\">\n              <input class=\"form-control name-details button-align\" [(ngModel)]=\"value.subtype\" placeholder=\"\" />\n              <button class=\"float-right save-account-btn font-weight-bold cursor-pointer button-height\"\n                (click)=\"updatePropertyName(value, i)\">{{\"MA_BUTTON_SAVE\" | translate}}</button>\n              <button class=\"float-right delete-account-btn font-weight-bold cursor-pointer mr-3 button-height\"\n                data-toggle=\"modal\" data-target=\"#deleteIndividually\"\n                (click)=\"passDltValue(value, i, 'PROPERTY')\">{{\"MA_BUTTON_DELETE\" | translate}}</button>\n\n            </div>\n          </div>\n        </div>\n        <div class=\"row ml-3 mr-3 mt-3\" *ngIf=\"(value.type == 'Real Estate')\">\n          <div class=\"col-sm-12\" *ngIf=\"value.other_detail\">\n            <p>{{\"MA_P_ADDRESS\" | translate}}</p>\n            <p class=\"font-weight-bold\">\n              {{value.other_detail.address}} {{value.other_detail.city}} {{value.other_detail.state}}\n              {{value.other_detail.country}} {{value.other_detail.zipid}}\n            </p>\n          </div>\n        </div>\n        <div class=\"row ml-3 mr-3 mt-3\" *ngIf=\"(value.type == 'Real Estate')\">\n          <div class=\"col-sm-4\">\n            <div class=\"zestimate cursor-pointer\">{{\"MA-DIV_ZESTIMATE\" | translate}}</div>\n            <div class=\"mt-2\">{{value.estimated_value | currency}}</div>\n          </div>\n          <div class=\"col-sm-8\">\n            <div><img src=\"../assets/zillow.png\" class=\"zillow-img\"></div>\n            <div class=\"zillow-text\">{{\"MA_DIV_T_AND_C\" | translate}}</div>\n          </div>\n        </div>\n\n        <div class=\"row ml-3 mr-3 mt-3 position2\" *ngIf=\"value.prices && value.prices.length > 0\">\n          <div>\n            <span class=\"font-weight-bold\">{{\"MA_SPAN_DATE_UPDATED\" | translate}}</span> {{value.prices[i].dateUpdated}}\n          </div>\n          <div class=\"pl-4\">\n            <span class=\"font-weight-bold\">{{\"MA_SPAN_ONGOING_PRICE\" | translate}}\n            </span>{{value.prices[i].zEstimate | currency}}\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <!-- Delete confirmation modal -->\n  <div class=\"modal fade\" id=\"exampleModalCenter\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n    aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLabel\"> {{\"MA_H5_CONFIRM\" | translate}}</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          {{ \"MA_DIV_CONFIRMATION_D_ALL\" | translate}} {{dynamicText}}?\n        </div>\n        <div class=\"modal-footer\">\n          <button #closeBtn type=\"button\" class=\"btn btn-primary\"\n            data-dismiss=\"modal\">{{\"MA_BUTTON_CANCEL\" | translate}}</button>\n          <button type=\"button\" class=\"btn btn-danger\"\n            (click)=\"callDeleteAll(dynamicText)\">{{\"MA_BUTTON_DELETE\" | translate}}</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"modal fade\" id=\"deleteIndividually\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n    aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLabel\"> {{\"MA_H5_CONFIRM\" | translate}}</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          {{ \"MA_DIV_CONFIRMATION_D_THIS\" | translate}} {{dynamicText}}?\n        </div>\n        <div class=\"modal-footer\">\n          <button #propertBtnClose type=\"button\" class=\"btn btn-primary\"\n            data-dismiss=\"modal\">{{\"MA_BUTTON_CANCEL\" | translate}}</button>\n          <button type=\"button\" class=\"btn btn-danger\"\n            (click)=\"deleteIndividual(propertyVariable.value, propertyVariable.index, dynamicText)\">{{\"MA_BUTTON_DELETE\" | translate}}</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/manage-account/manage-account.component.ts":
/*!************************************************************!*\
  !*** ./src/app/manage-account/manage-account.component.ts ***!
  \************************************************************/
/*! exports provided: ManageAccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageAccountComponent", function() { return ManageAccountComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var ngx_plaid_link__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-plaid-link */ "./node_modules/ngx-plaid-link/fesm5/ngx-plaid-link.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __read = (undefined && undefined.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (undefined && undefined.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};










var ManageAccountComponent = /** @class */ (function () {
    function ManageAccountComponent(session, backend, authService, router, plaidLinkService, spinnerService, translate, notifierService) {
        this.session = session;
        this.backend = backend;
        this.authService = authService;
        this.router = router;
        this.plaidLinkService = plaidLinkService;
        this.spinnerService = spinnerService;
        this.translate = translate;
        this.notifierService = notifierService;
        this.syncChildEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.congratePageScreen = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.controlData = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.propertyHandlerEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.employerDivHide = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.propertyVariable = { value: null, index: null };
        this.CONGRATS_PAGE = '/congratpage';
        this.dynamicText = null;
        this.notifier = notifierService;
        // this is default langugage
        translate.setDefaultLang('en');
        // this language is to set by your prefered browser language
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang);
    }
    ManageAccountComponent.prototype.ngOnInit = function () {
        this.userAttributes = this.authService.getUserProfile();
        this.userToken = this.authService.getDataFromCache('tokens');
        if (!this.userToken) {
            this.router.navigate(['/']);
        }
        this.user = this.authService.getUserProfile();
        this.controllActiveInactiveAccount(this.user.accounts);
        if ('properties' in this.user && this.user.properties && this.user.properties.length > 0) {
            try {
                for (var _a = __values(this.user.properties), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var pro = _b.value;
                    if ('subtype' in pro && pro.subtype) {
                        pro.subtypeToUpdate = pro.subtype;
                    }
                    else if ('name' in pro && pro.name) {
                        pro.nameToUpdate = pro.name;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        console.log('manage-accounts', this.user.accounts);
        if ('accounts' in this.user && this.user.accounts) {
            try {
                for (var _d = __values(this.user.accounts), _e = _d.next(); !_e.done; _e = _d.next()) {
                    var acc = _e.value;
                    if ('accounts' in acc && acc.accounts) {
                        acc.subAccountsLength = acc.accounts.filter(function (e) { return (e.type == 'depository' || e.type == 'credit' || e.type == 'brokerage'); });
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_e && !_e.done && (_f = _d.return)) _f.call(_d);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        var e_1, _c, e_2, _f;
    };
    /* start Plaid */
    ManageAccountComponent.prototype.ngOnChanges = function (changes) {
        console.log('manage-changes', changes);
        if ('publicToken' in changes && changes.publicToken) {
            var getPublicToken = changes.publicToken;
            console.log('Public Token ', getPublicToken.currentValue);
            if (getPublicToken.currentValue) {
                this.openPlaidWidget(getPublicToken.currentValue, false);
            }
        }
        if ('propertyAttribute' in changes && changes.propertyAttribute && 'currentValue' in changes.propertyAttribute && changes.propertyAttribute.currentValue) {
            this.ngOnInit();
        }
        if ('callTheAddAccount' in changes && changes.callTheAddAccount && 'currentValue' in changes.callTheAddAccount && (typeof (changes.callTheAddAccount.currentValue) == 'string')) {
            this.invokeAddAccount.nativeElement.click();
        }
        if ('addAccByCongratePage' in changes && changes.addAccByCongratePage && 'currentValue' in changes.addAccByCongratePage && changes.addAccByCongratePage.currentValue) {
            this.invokeAddAccount.nativeElement.click();
        }
        if ('hasRealTimeData' in changes && changes.hasRealTimeData && 'currentValue' in changes.hasRealTimeData && changes.hasRealTimeData.currentValue) {
            console.log('changes.hasRealTimeData.currentValue', changes.hasRealTimeData.currentValue);
        }
        if ('logoUpdated' in changes && changes.logoUpdated && 'currentValue' in changes.logoUpdated && changes.logoUpdated.currentValue != changes.logoUpdated.previousValue) {
            this.ngOnInit();
        }
    };
    ManageAccountComponent.prototype.openPlaidWidget = function (public_token, varForNewAccount) {
        var _this = this;
        if (varForNewAccount) {
            public_token = null;
        }
        var params = {
            apiVersion: "v2",
            env: _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].placid.environment,
            product: _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].placid.productArray,
            key: _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].placid.apiid,
            webhook: _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].apibaseurl + '/updatewehbook',
            onSuccess: function (token, metadata) { return _this.onSuccess(token, metadata, public_token); },
            onExit: function (error, metadata) { return _this.onExit(error, metadata, public_token); }
        };
        if (public_token) {
            params.token = typeof public_token == "object" ? public_token.public_token : public_token;
        }
        this.plaidLinkService.createPlaid(params)
            .then(function (handler) {
            _this.plaidLinkHandler = handler;
            if (!_this.user.accounts || _this.user.accounts.length == 0) {
                _this.open();
            }
            if (public_token || varForNewAccount) {
                _this.open();
            }
        });
    };
    ManageAccountComponent.prototype.ngAfterViewInit = function () {
        this.openPlaidWidget(null, false);
    };
    ManageAccountComponent.prototype.onExit = function (error, metadata, public_token) {
        console.log("We exited:", error);
        console.log("We got metadata:", metadata);
        if (!this.user.accounts || this.user.accounts.length == 0) {
            this.ngAfterViewInit();
        }
        if (public_token) {
            console.log('Inside public token success');
            this.syncChildEvent.emit();
        }
        this.employerDivHide.emit();
    };
    ManageAccountComponent.prototype.onSuccess = function (token, metadata, public_token) {
        console.log("We got metadata:", metadata);
        if (public_token) {
            console.log('Inside public token success');
            this.syncChildEvent.emit();
        }
        else {
            this.onPlaidSuccess({ token: token, metadata: metadata });
        }
    };
    ManageAccountComponent.prototype.addNewAccount = function () {
        this.openPlaidWidget(null, true);
    };
    ManageAccountComponent.prototype.open = function () {
        this.spinnerService.hide();
        this.plaidLinkHandler.open();
        return;
    };
    ManageAccountComponent.prototype.exit = function () {
        this.plaidLinkHandler.exit();
    };
    ManageAccountComponent.prototype.onPlaidSuccess = function (event) {
        var _this = this;
        console.log('onPlaidSuccess', event);
        this.spinnerService.show();
        var post_data = {
            "public_token": event.token || event.public_token,
            "inst_id": event.metadata.institution.institution_id,
            "inst_name": event.metadata.institution.name
        };
        var path = "/accounts";
        this.backend.post(post_data, path, this.userToken.accessToken)
            .subscribe(function (data) {
            var resAccount = data.json();
            console.log('successfully post your account', resAccount);
            // Need to update session also and then navigate
            _this.user = _this.authService.getUserProfile();
            if (!_this.user.accounts) {
                _this.user.accounts = [];
            }
            _this.user.accounts.push(data.json());
            _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.user);
            // this.router.navigate([this.CONGRATS_PAGE]);
            _this.congratePageScreen.emit(resAccount);
            _this.spinnerService.hide();
        }, function (error) {
            _this.spinnerService.hide();
            var errMsg = JSON.parse(error._body);
            if ('message' in errMsg && errMsg.message && errMsg.message == 'ITEM_LOGIN_REQUIRED') {
                // translate error message
                _this.translate.get('MA_WIDGET')
                    .subscribe(function (value) {
                    _this.notifier.notify('warning', value);
                });
            }
            else if ('message' in errMsg && errMsg.message == "Account is already added!") {
                _this.translate.get('MA_ACCOUNT_ALREADY_ADDED')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value);
                });
            }
            else {
                _this.translate.get('MA_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value);
                });
            }
            // Check if 0 accounts then open plaid again
            if (!_this.user.accounts || _this.user.accounts.length === 0) {
                // This interval is added because without it notification was closing too fast
                setTimeout(function () {
                    _this.open();
                }, 3000);
            }
        });
    };
    /* end Plaid */
    // collect the failed account
    ManageAccountComponent.prototype.showSyncStatus = function (msg) {
        var _this = this;
        if (msg && msg.id) {
            this.spinnerService.show();
            var path = "/accounts/" + msg.id + "/public_token";
            this.backend.get(path, this.userToken.accessToken)
                .subscribe(function (res) {
                var publicToken = res.json();
                _this.openPlaidWidget(publicToken, false);
            }, function (error) {
                var errorVar = error.json();
                _this.translate.get('MA_ERRORMSG')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value);
                });
                _this.spinnerService.hide();
            });
        }
    };
    ;
    ManageAccountComponent.prototype.showAccount = function (accounts, index) {
        if (accounts[index].showDetails) {
            accounts[index].showDetails = false;
        }
        else {
            accounts[index].showDetails = true;
        }
    };
    // controll div through active and inactive option
    ManageAccountComponent.prototype.controllActiveInactiveAccount = function (accounts) {
        var mappedData;
        var statusCollection = [];
        try {
            for (var accounts_1 = __values(accounts), accounts_1_1 = accounts_1.next(); !accounts_1_1.done; accounts_1_1 = accounts_1.next()) {
                var acc = accounts_1_1.value;
                mappedData = acc.accounts.map(function (item) { return item.status; });
                statusCollection.push(mappedData);
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (accounts_1_1 && !accounts_1_1.done && (_a = accounts_1.return)) _a.call(accounts_1);
            }
            finally { if (e_3) throw e_3.error; }
        }
        statusCollection = [].concat.apply([], __spread(statusCollection));
        if (!(statusCollection.includes('ACTIVE'))) {
            this.controlData.emit('notactive');
        }
        else {
            this.congratePageScreen.emit('reload');
        }
        var e_3, _a;
    };
    ManageAccountComponent.prototype.setStatusOfAccount = function (accountId, accountObj) {
        var _this = this;
        var data = {};
        if ('official_name' in accountObj && accountObj.official_name) {
            data.officialName = accountObj.official_name;
        }
        if ('status' in accountObj && accountObj.status) {
            data.status = accountObj.status;
        }
        // update account
        var userDetail = this.authService.getDataFromCache(this.authService.USER_PROFILE);
        try {
            for (var _a = __values(userDetail.accounts), _b = _a.next(); !_b.done; _b = _a.next()) {
                var account = _b.value;
                try {
                    for (var _c = __values(account.accounts), _d = _c.next(); !_d.done; _d = _c.next()) {
                        var acc = _d.value;
                        if (acc.account_id == accountObj.account_id) {
                            acc.status = accountObj.status;
                        }
                    }
                }
                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                finally {
                    try {
                        if (_d && !_d.done && (_e = _c.return)) _e.call(_c);
                    }
                    finally { if (e_4) throw e_4.error; }
                }
            }
        }
        catch (e_5_1) { e_5 = { error: e_5_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_f = _a.return)) _f.call(_a);
            }
            finally { if (e_5) throw e_5.error; }
        }
        var path = "/accounts/" + accountId + "/subAccount/" + accountObj.account_id;
        if ('officialName' in data && data.officialName) {
            this.spinnerService.show();
            this.backend.put(data, path, this.userToken.accessToken).subscribe(function (res) {
                delete accountObj.showDetails;
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, userDetail);
                _this.controllActiveInactiveAccount(userDetail.accounts);
                // this.ngOnInit();
                _this.spinnerService.hide();
                _this.translate.get("MA_UPDATE_ACCOUNT")
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
            }, function (err) {
                var errorVar = err.json();
                _this.translate.get('DASH_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + ": " + errorVar["message"]);
                });
                _this.spinnerService.hide();
            });
        }
        else {
            this.translate.get('MA_RQR_FIELD_MISSING')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
        }
        var e_5, _f, e_4, _e;
    };
    //edit account method
    ManageAccountComponent.prototype.editAccounts = function (accountId) {
        var _this = this;
        this.spinnerService.show();
        var path = "/accounts/" + accountId + "/public_token";
        this.backend.get(path, this.userToken.accessToken)
            .subscribe(function (res) {
            _this.spinnerService.hide();
            var responce = res.json();
            if (responce && 'public_token' in responce && responce.public_token) {
                _this.openPlaidWidget(responce.public_token, false);
            }
        }, function (error) {
            var errorVar = error.json();
            _this.translate.get('DASH_ERROR')
                .subscribe(function (value) {
                _this.notifier.notify('error', value + ": " + errorVar["message"]);
            });
            _this.spinnerService.hide();
        });
    };
    // deleteAll Generic method
    ManageAccountComponent.prototype.deleteAll = function (stringValue) {
        var _this = this;
        if (stringValue == "All_Property") {
            this.translate.get("MA_PROPERTIES").subscribe(function (value) { return _this.dynamicText = value; });
        }
    };
    ManageAccountComponent.prototype.callDeleteAll = function (stringValue) {
        var parsedPropties;
        this.translate.get("MA_PROPERTIES").subscribe(function (value) { return parsedPropties = value; });
        if (stringValue == parsedPropties) {
            this.deleteAllProperty();
        }
    };
    //delete account method
    ManageAccountComponent.prototype.deleteAccounts = function (accountId, index) {
        var _this = this;
        if (accountId) {
            var path = '/accounts/' + accountId;
            this.spinnerService.show();
            this.backend.delete(path, this.userToken.accessToken).subscribe(function (res) {
                _this.translate.get('MA_ACC_DELETED')
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
                _this.spinnerService.hide();
                _this.propertBtnClose.nativeElement.click();
                // Read the data from cache again then remove the account and set
                // This is done because in cache other component might have updated data like profile name etc
                var userDataFromCache = _this.authService.getDataFromCache(_this.authService.USER_PROFILE);
                userDataFromCache.accounts.splice(index, 1);
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, userDataFromCache);
                _this.congratePageScreen.emit('sidereload');
                _this.ngOnInit();
            }, function (err) {
                _this.spinnerService.hide();
                var errorVar = err.json();
                _this.translate.get('DASH_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + ": " + errorVar["message"]);
                });
            });
        }
        else {
            this.translate.get('MA_RQR_FIELD_MISSING')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
        }
    };
    ManageAccountComponent.prototype.updatePropertyName = function (item, index) {
        var _this = this;
        var data = {};
        if ('subtype' in item && item.subtype) {
            data.subtype = item.subtype;
            this.user.properties[index].subtype = item.subtype;
        }
        else if ('name' in item && item.name) {
            data.name = item.name;
            this.user.properties[index].name = item.name;
        }
        var path = "/properties/" + item.id;
        if (('name' in data && data.name) || ('subtype' in data && data.subtype)) {
            this.spinnerService.show();
            this.backend.put(data, path, this.userToken.accessToken).subscribe(function (res) {
                delete item.showDetails;
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.user);
                _this.congratePageScreen.emit('sidereload');
                _this.ngOnInit();
                _this.spinnerService.hide();
                _this.translate.get('MA_PROPERTY_ADDED')
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
            }, function (err) {
                var errorVar = err.json();
                _this.translate.get('DASH_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + ": " + errorVar["message"]);
                });
                _this.spinnerService.hide();
            });
        }
        else {
            this.translate.get('MA_RQR_FIELD_MISSING')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
        }
    };
    // delete propety method
    ManageAccountComponent.prototype.deleteIndividual = function (propertyValue, index, event) {
        var _this = this;
        console.log(event);
        var parsedAcc;
        var parsedProp;
        this.translate.get("MA_ACCOUNT").subscribe(function (value) { return parsedAcc = value; });
        this.translate.get("MA_PROPERTY").subscribe(function (value) { return parsedProp = value; });
        if (event == parsedAcc) {
            this.deleteAccounts(propertyValue, index);
        }
        if (event == parsedProp) {
            if (propertyValue.id) {
                var path = "/properties/" + propertyValue.id;
                this.spinnerService.show();
                this.backend.delete(path, this.userToken.accessToken).subscribe(function (res) {
                    _this.spinnerService.hide();
                    _this.translate.get('MA_PROPERTY_DELETED')
                        .subscribe(function (value) {
                        _this.notifier.notify('success', value);
                    });
                    // Read the data from cache again then remove the account and set
                    // This is done because in cache other component might have updated data like profile name etc
                    var userDataFromCache = _this.authService.getDataFromCache(_this.authService.USER_PROFILE);
                    userDataFromCache.properties.splice(index, 1);
                    _this.authService.setDataInCache(_this.authService.USER_PROFILE, userDataFromCache);
                    _this.propertBtnClose.nativeElement.click();
                    _this.congratePageScreen.emit('sidereload');
                    _this.ngOnInit();
                }, function (err) {
                    _this.spinnerService.hide();
                    var errorVar = err.json();
                    _this.translate.get('DASH_ERROR')
                        .subscribe(function (value) {
                        _this.notifier.notify('error', value + ": " + errorVar["message"]);
                    });
                });
            }
            else {
                this.translate.get('MA_RQR_FIELD_MISSING')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value);
                });
            }
        }
    };
    ManageAccountComponent.prototype.passDltValue = function (value, index, event) {
        var _this = this;
        if (event == 'PROPERTY') {
            this.propertyVariable.value = null;
            this.propertyVariable.index = null;
            this.dynamicText = null;
            // this.dynamicText = 'property'
            this.translate.get('MA_PROPERTY').subscribe(function (value) { return _this.dynamicText = value; });
            this.propertyVariable.value = value;
            this.propertyVariable.index = index;
        }
        if (event == 'ACCOUNTS') {
            this.propertyVariable.value = null;
            this.propertyVariable.index = null;
            this.dynamicText = null;
            //this.dynamicText = 'account' 
            this.translate.get('MA_ACCOUNT').subscribe(function (value) { return _this.dynamicText = value; });
            this.propertyVariable.value = value;
            this.propertyVariable.index = index;
        }
    };
    ManageAccountComponent.prototype.deleteAllProperty = function () {
        var _this = this;
        var path = "/properties/" + -1;
        this.spinnerService.show();
        this.backend.delete(path, this.userToken.accessToken).subscribe(function (res) {
            _this.translate.get('MA_PROPERTIES_DELETED')
                .subscribe(function (value) {
                _this.notifier.notify('success', value);
            });
            _this.closeBtn.nativeElement.click();
            // Read the data from cache again then remove the account and set
            // This is done because in cache other component might have updated data like profile name etc
            var userDataFromCache = _this.authService.getDataFromCache(_this.authService.USER_PROFILE);
            userDataFromCache.properties.splice(0, _this.user.properties.length);
            _this.authService.setDataInCache(_this.authService.USER_PROFILE, userDataFromCache);
            _this.congratePageScreen.emit('sidereload');
            _this.ngOnInit();
            _this.spinnerService.hide();
        }, function (err) {
            var errorVar = err.json();
            _this.translate.get('DASH_ERROR')
                .subscribe(function (value) {
                _this.notifier.notify('error', value + ": " + errorVar["message"]);
            });
            _this.closeBtn.nativeElement.click();
            _this.spinnerService.hide();
        });
    };
    ManageAccountComponent.prototype.propertyHandler = function (PropertyName) {
        this.propertyHandlerEmit.emit(PropertyName);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("addAnotherAcc"),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "addAccByCongratePage", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("pubToken"),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "publicToken", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("propertyAttribute"),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "propertyAttribute", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("callTheAddAccount"),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "callTheAddAccount", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("hasRealTimeData"),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "hasRealTimeData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("logoUpdated"),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "logoUpdated", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "syncChildEvent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "congratePageScreen", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "controlData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "propertyHandlerEmit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ManageAccountComponent.prototype, "employerDivHide", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('invokeAddAccount'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ManageAccountComponent.prototype, "invokeAddAccount", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('closeBtn'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ManageAccountComponent.prototype, "closeBtn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('propertBtnClose'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ManageAccountComponent.prototype, "propertBtnClose", void 0);
    ManageAccountComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manage-account',
            template: __webpack_require__(/*! ./manage-account.component.html */ "./src/app/manage-account/manage-account.component.html"),
            styles: [__webpack_require__(/*! ./manage-account.component.css */ "./src/app/manage-account/manage-account.component.css")],
        }),
        __metadata("design:paramtypes", [angular_web_storage__WEBPACK_IMPORTED_MODULE_1__["SessionStorageService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_2__["BackendService"],
            _auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            ngx_plaid_link__WEBPACK_IMPORTED_MODULE_6__["NgxPlaidLinkService"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]])
    ], ManageAccountComponent);
    return ManageAccountComponent;
}());



/***/ }),

/***/ "./src/app/nisaan/nisaan.component.css":
/*!*********************************************!*\
  !*** ./src/app/nisaan/nisaan.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span, .txt-color{\n  color:#636060;\n}\n.font-color-black {\n  color:#000 ;\n}\n.font-14 {\n  font-size:16px;\n}\n.font-16 {\n  font-size:16px;\n}\n.input-custm{\n  vertical-align:middle ;\n}\n.cursor-pointer{\n  cursor:pointer;\n}\n.input-border{\n  border-top:0px;\n  border-left:0px;\n  border-right:0px;\n  border-bottom:1px solid #000;\n}\n.ml-none{\n  margin-left:0 !important;\n}\n.mr-none{\n  margin-right:0 !important;\n}\n.main-div-pdf input {\n  font-weight: 600;\n}\n.xs-font{\n  font-size: x-small;\n}\n"

/***/ }),

/***/ "./src/app/nisaan/nisaan.component.html":
/*!**********************************************!*\
  !*** ./src/app/nisaan/nisaan.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-div-pdf\">\n  <div class=\"p-5\">\n  <div class=\"row\">\n    <div class=\"col-md-12 text-right\">\n      <h5 class=\"font-weight-bold font-color-black\">Nissan Canada Financial Services Inc.</h5>\n    </div>\n  </div>\n    <div class=\"row\">\n      <div class=\"col-md-2 \">\n        <img src=\"../../assets/Nissan-logo.png\" style=\"height:100px ;\">\n      </div>\n      <div class=\"col-md-10 pt-3\">\n        <h2 class=\"font-weight-bold font-color-black\">CREDIT APPLICATION</h2>\n      </div>\n    </div>\n    <div class=\"row mt-3\">\n      <div class=\"col-md-12\">\n        <h6 class=\"font-14\">PLEASE PRINT</h6>\n      </div>\n    </div>\n    <div class=\"row mt-2\">\n      <div class=\"col-md-3\">\n        <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\">LEASE</span>\n        </span>\n      </div>\n      <div class=\"col-md-4\">\n        <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\">PERSONAL</span>\n        </span>\n      </div>\n      <div class=\"col-md-5\">\n        <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\">NEW CUSTOMER</span>\n        </span>\n      </div>\n    </div>\n\n    <div class=\"row mt-2\">\n      <div class=\"col-md-3\">\n        <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\">CONDITIONAL SALES CONTRACT</span>\n        </span>\n      </div>\n      <div class=\"col-md-4\">\n        <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\">BUSINESS CO-APPLICANT/GUARANTOR</span>\n        </span>\n      </div>\n      <div class=\"col-md-5\">\n        <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\">PRESENT CUSTOMER A/C # <input class=\"input-border\"></span>\n        </span>\n      </div>\n    </div>\n\n    <div class=\"row mt-2\">\n      <div class=\"col-md-3\">\n      </div>\n      <div class=\"col-md-4\">\n      </div>\n      <div class=\"col-md-5\">\n        <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\">FORMER CUSTOMER A/C # <input class=\"input-border\"></span>\n        </span>\n      </div>\n    </div>\n    <div class=\"row mt-2\">\n      <div class=\"col-md-12\">\n        <h6 class=\"font-weight-bold\">1) <span class=\"ml-5\">VEHICLE INFORMATION</span></h6>\n      </div>\n    </div>\n<div class=\"border border-dark\">\n  <div class=\"row ml-none mr-none\">\n    <div class=\"col-md-4 pl-3\">\n      <span class=\"font-14\">Dealer\n        <span><input class=\"w-50 border border-0\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-1\" style=\"border-left:1px solid #000;\">\n      <span class=\"font-14\">No.\n        <span><input class=\"border border-0\" style=\"width:50px;\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-3\" style=\"border-left:1px solid #000;\">\n      <span class=\"font-14\">Year\n        <span><input class=\"border border-0\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-4\" style=\"border-left:1px solid #000;\">\n      <span class=\"font-14\">Selling Price\n        <span><input class=\"border border-0\"></span>\n      </span>\n    </div>\n  </div>\n\n  <div class=\"row ml-none mr-none\">\n    <div class=\"col-md-3 pl-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Telephone\n        <span><input class=\"w-50 border border-0\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Fax\n        <span><input class=\"border border-0\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Make\n        <span><input class=\"border border-0\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-4\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Cash Down\n        <span><input class=\"border border-0\"></span>\n      </span>\n    </div>\n  </div>\n\n  <div class=\"row ml-none mr-none\">\n    <div class=\"col-md-5 pl-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Contact\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Model\n        <span><input class=\"border border-0\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-4\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Trade-in (Net)\n        <span><input class=\"border border-0\"></span>\n      </span>\n    </div>\n  </div>\n\n  <div class=\"row ml-none mr-none\" style=\"height:50px;\">\n    <div class=\"col-md-5 pl-3\" style=\"border-top:1px solid #000;\">\n      <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\"> Individual </span>\n        <input type=\"checkbox\" class=\"input-custm cursor-pointer ml-3\">\n          <span class=\"font-14 ml-2\"> Joint-with whom: <input class=\"input-border w-50\"> </span>\n        </span>\n    </div>\n    <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Model No. / Option Code\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-4\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Lease Vehicle Amount/Amount Financed\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n    </div>\n  </div>\n\n  <div class=\"row ml-none mr-none\" style=\"height:50px;\">\n    <div class=\"col-md-5 pl-3\" style=\"border-top:1px solid #000;\">\n      <span>\n          <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n          <span class=\"font-14 ml-2\"> Personal </span>\n        <input type=\"checkbox\" class=\"input-custm cursor-pointer ml-3\">\n          <span class=\"font-14 ml-2\"> Business</span>\n        </span>\n    </div>\n    <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\" style=\"font-size:15px;\">MSRP\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\" style=\"font-size:15px;\">Term\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n    </div>\n    <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\" style=\"font-size:15px;\">Payment\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n    </div>\n  </div>\n\n</div>\n\n    <div class=\"row mt-2 \">\n      <div class=\"col-md-12\">\n        <h6 class=\"font-weight-bold\">2) <span class=\"ml-5\">INFORMATION REGARDING APPLICANT(S)</span></h6>\n      </div>\n    </div>\n    <div class=\"border border-dark\">\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-4 pl-3\">\n      <span class=\"font-14\">Applicant (1) Full Name (Middle initial(s) required)\n        <span><input type=\"text\" class=\"w-100 border border-0\" [(ngModel)]=\"user.name\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;\">\n      <span class=\"font-14\">Birth Date (DD-MM-YYYY)\n        <span><input class=\"border border-0 w-100\" [(ngModel)]=\"user.birthDate\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-3\" style=\"border-left:1px solid #000;\">\n      <span class=\"font-14\">Social Insurance Number\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-3\" style=\"border-left:1px solid #000;\">\n      <span class=\"font-14\">No. of Dependants\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-4 pl-3\" style=\"border-top:1px solid #000;\">\n          <span class=\"font-14\">Current Address\n            <span style=\"color:#212529;\" class=\"d-block font-weight-bold\" [ngClass]=\"{'xs-font': user.address.streetAddress.length > 62}\" *ngIf=\"changeText\" (click)=\"changeText=false\">{{user.address.streetAddress}}</span>\n            <span *ngIf=\"!changeText || user.address.streetAddress == 'null' || user.address.streetAddress == '' || !user.address.streetAddress\"><input class=\"w-100 border border-0\" [(ngModel)]=\"user.address.streetAddress\" (focusout)=\"changeText=true\"></span>\n          </span>\n        </div>\n        <div class=\"col-md-2 pr-2 d-none\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\" style=\"height: 20px;\">Street\n\n      </span>\n        </div>\n        <div class=\"col-md-1\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">City\n        <span><input class=\"border border-0 w-100\" [(ngModel)]=\"user.address.addressLocality\" style=\"font-size:12px\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1 pl-0 pr-2\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Province\n        <span><input class=\"border border-0 w-100\" [(ngModel)]=\"user.address.addressRegion\" style=\"font-size:12px\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Postal Code\n        <span><input class=\"border border-0 w-100\" [(ngModel)]=\"user.address.postalCode\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n          <div class=\"txt-color\">How long?</div>\n      <div class=\"row\">\n        <div class=\"col-md-3 txt-color\">\n          Years\n        </div>\n        <div class=\"col-md-3\">\n          <input class=\"border border-0\" style=\"width:36px;\">\n        </div>\n        <div class=\"col-md-3 txt-color\">\n          Mos.\n        </div>\n        <div class=\"col-md-3\">\n          <input class=\"border border-0\" style=\"width:36px;\">\n        </div>\n      </div>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Home Telephone\n        <span><input class=\"border border-0 w-100\" [ngModel]=\"user.telephone | phoneNumber\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-6 pl-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Previous Address (if less than 3 years at current address)\n        <span><input class=\"w-100 border border-0\" [(ngModel)]=\"user['previous addresses'][0].fullAddress\"></span>\n      </span>\n        </div>\n\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Postal Code\n        <span><input class=\"border border-0 w-100\" [(ngModel)]=\"user['previous addresses'][0].postalCode\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n          <div class=\"txt-color\">How long?</div>\n          <div class=\"row\">\n            <div class=\"col-md-3 txt-color\">\n              Years\n            </div>\n            <div class=\"col-md-3\">\n              <input class=\"border border-0\" style=\"width:36px;\">\n            </div>\n            <div class=\"col-md-3 txt-color\">\n              Mos.\n            </div>\n            <div class=\"col-md-3\">\n              <input class=\"border border-0\" style=\"width:36px;\">\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Home Telephone\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-5 pl-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Employer’s Name\n        <span><input class=\"w-100 border border-0\" [(ngModel)]=\"user.worksFor.name\"></span>\n      </span>\n        </div>\n\n        <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Telephone\n        <span><input class=\"border border-0 w-100\" [ngModel]=\"user.worksFor.phone_number | phoneNumber\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n          <div class=\"txt-color\">How long?</div>\n          <div class=\"row\">\n            <div class=\"col-md-3 txt-color\">\n              Years\n            </div>\n            <div class=\"col-md-3\">\n              <input class=\"border border-0\" style=\"width:36px;\">\n            </div>\n            <div class=\"col-md-3 txt-color\">\n              Mos.\n            </div>\n            <div class=\"col-md-3\">\n              <input class=\"border border-0\" style=\"width:36px;\">\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Occupation\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-8 pl-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Employer’s Address\n        <span><input class=\"w-100 border border-0\" [(ngModel)]=\"user.worksFor.empAddress\"></span>\n      </span>\n        </div>\n\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n          <div class=\"txt-color\">Self-employed </div>\n          <div class=\"row\">\n            <div class=\"col-md-2\">\n              <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n            </div>\n            <div class=\"col-md-4 txt-color\">\n              Yes\n            </div>\n            <div class=\"col-md-2\">\n              <input type=\"checkbox\" class=\"input-custm cursor-pointer\">\n            </div>\n            <div class=\"col-md-4 txt-color\">\n              No\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Nature of Business\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-5 pl-3\" style=\"border-top:1px solid #000;\">\n      <span style=\"font-size:13px;\">Previous Employer’s Name and Address (if less than 3 years at current employer)\n        <span><input class=\"w-100 border border-0\" [(ngModel)]=\"user.preEmployer.name\"></span>\n      </span>\n        </div>\n\n        <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Telephone\n        <span><input class=\"border border-0 w-100\" [ngModel]=\"user.preEmployer.phone_number | phoneNumber\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n          <div class=\"txt-color\">How long?</div>\n          <div class=\"row\">\n            <div class=\"col-md-3 txt-color\">\n              Years\n            </div>\n            <div class=\"col-md-3\">\n              <input class=\"border border-0\" style=\"width:36px;\">\n            </div>\n            <div class=\"col-md-3 txt-color\">\n              Mos.\n            </div>\n            <div class=\"col-md-3\">\n              <input class=\"border border-0\" style=\"width:36px;\">\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Occupation\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-3 pl-3\" style=\"border-top:1px solid #000;\">\n      <div class=\"txt-color\" style=\"font-size:13px;\">\n        Applicant (1) Gross Salary/Wages\n      </div>\n          <div class=\"row\">\n            <div class=\"col-md-6 txt-color\" style=\"font-size:13px;\">Income Per Month <span class=\"ml-2 font-14 \">$</span></div>\n            <div class=\"col-md-6 pl-0\"><input class=\"border border-0 w-75\"></div>\n          </div>\n        </div>\n        <div class=\"col-md-1 \" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <div class=\"txt-color\" style=\"font-size:12px;\">Other Income</div>\n      <span style=\"font-size:14px;\">$\n        <span><input class=\"w-75 border border-0\"></span>\n      </span>\n        </div>\n\n        <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Describe Other Income\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-3\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n          <div class=\"txt-color\" style=\"font-size:13px;\">\n            Applicant (2) Gross Income\n          </div>\n          <div class=\"row\">\n            <div class=\"col-md-6 txt-color\" style=\"font-size:13px;\">Per Month <span class=\"ml-2 font-14 \">$</span></div>\n            <div class=\"col-md-6 pl-0\"><input class=\"border border-0 w-75\"></div>\n          </div>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <div class=\"txt-color\">Total Income </div>\n          <span style=\"font-size:14px;\">$\n        <span><input class=\"w-75 border border-0 ml-2\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-3 pl-3\" style=\"border-top:1px solid #000;\">\n           <span style=\"font-size:12px;\">Applicant (2) Full Name (Middle initial(s) required)\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1 pl-0 pr-0\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n         <span style=\"font-size:9px;\">Birth Date (DD-MM-YYYY)\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n\n        <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n     <span>Social Insurance Number\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-5\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n         <div class=\"row\">\n           <div class=\"col-md-4 txt-color\">\n             Applicant (2)\n           </div>\n           <div class=\"col-md-4\">\n             <span><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3\"> Co-applicant</span>\n           </div>\n           <div class=\"col-md-4\">\n             <span><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3\"> Guarantor </span>\n           </div>\n         </div>\n\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-2 pl-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Current Address\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Street\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">City\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Province\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Postal Code\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n          <div class=\"txt-color\">How long?</div>\n          <div class=\"row\">\n            <div class=\"col-md-3 txt-color\">\n              Years\n            </div>\n            <div class=\"col-md-3\">\n              <input class=\"border border-0\" style=\"width:36px;\">\n            </div>\n            <div class=\"col-md-3 txt-color\">\n              Mos.\n            </div>\n            <div class=\"col-md-3\">\n              <input class=\"border border-0\" style=\"width:36px;\">\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Home Telephone\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-3 pl-3\" style=\"border-top:1px solid #000;\">\n           <span>Applicant (2) Employer\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1 pr-0\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <div class=\"txt-color\" style=\"font-size:12px;\">How long?</div>\n          <div class=\"row\" style=\"font-size:12px;\">\n            <div class=\"col-md-6 txt-color\">\n              Years\n              <div><input class=\"border border-0\" style=\"width:36px;\"></div>\n            </div>\n            <div class=\"col-md-6 txt-color\">\n              Mos.\n              <div>\n                <input class=\"border border-0\" style=\"width:28px;\">\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"col-md-3\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n     <span>Occupation\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-5\" style=\"border-left:1px solid #000; border-top:1px solid #000;\">\n          <div class=\"txt-color\">Has Either Applicant Ever Declared Bankruptcy? </div>\n          <div class=\"row\">\n            <div class=\"col-md-9\">\n              <span><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3\"> Yes - Date (DD-MM-YYYY) <input class=\"w-25 border border-0\"></span>\n            </div>\n            <div class=\"col-md-3\">\n              <span><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3\"> No </span>\n            </div>\n          </div>\n\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-2 pl-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Relative Not Living With applicant\n      </span>\n        </div>\n        <div class=\"col-md-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Name\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-3\" style=\"border-top:1px solid #000;\">\n      <span class=\"font-14\">Address\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Relationship\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n      <span class=\"font-14\">Home Telephone\n        <span><input class=\"border border-0 w-100\"></span>\n      </span>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row mt-2 \">\n      <div class=\"col-md-12\">\n        <h6 class=\"font-weight-bold\">3) <span class=\"ml-5\">FINANCIAL INFORMATION – ALL LOANS, LEASES AND OTHER OBLIGATIONS (INCLUDING ALIMONY, CHILD SUPPORT, MAINTENANCE)</span></h6>\n      </div>\n    </div>\n\n    <div class=\"border border-dark\">\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-2\">\n          <div class=\"font-14 txt-color\">Residence</div>\n          <span class=\"font-14\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3\"> Buying or Own </span>\n        </div>\n        <div class=\"col-md-3\"  style=\"border-left:1px solid #000;\">\n          <span>Lienholder or Landlord’s Name\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1\" style=\"border-left:1px solid #000;\">\n           <span>Est. Value\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;\">\n           <span>Original Balance\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;\">\n           <span>Balancing Owing\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;\">\n           <span>Payment Per Month\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-2\">\n          <div class=\"font-14\"><span class=\"font-14\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3\"> Rent </span></div>\n          <span class=\"font-14\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3\">  With Parents/Other </span>\n        </div>\n        <div class=\"col-md-4\"  style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <span>Address\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-3\" style=\"border-top:1px solid #000;\">\n          <span>Name and Account No. (Creditors only)\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-3\"  style=\"border-top:1px solid #000;\">\n          <span>Address\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-3\" style=\"border-top:1px solid #000;\">\n          <span>Name and Account No. (Creditors only)\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-3\"  style=\"border-top:1px solid #000;\">\n          <span>Address\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-3\" style=\"border-top:1px solid #000;\">\n          <span>Name and Account No. (Creditors only)\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-3\"  style=\"border-top:1px solid #000;\">\n          <span>Address\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-2\" style=\"border-top:1px solid #000;\">\n          <div class=\"txt-color\">Previous Vehicle Was</div>\n          <div class=\"row\">\n            <div class=\"col-md-6\">\n              <span style=\"font-size:14px;\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-1\"> Leased </span>\n            </div>\n            <div class=\"col-md-6 pl-0\">\n              <span style=\"font-size:14px;\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-1\"> Purchased </span>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-4\"  style=\"border-top:1px solid #000;border-left:1px solid #000\">\n          <span>Name of Lessor or Financing Creditor\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>$\n        <span><input class=\"w-100 border border-0 ml-2\"></span>\n      </span>\n        </div>\n      </div>\n\n\n    </div>\n    <div class=\"row mt-2\">\n      <div class=\"col-md-12\">\n        <h6 class=\"font-weight-bold\">4) <span class=\"ml-5\">BANKING INFORMATION</span></h6>\n      </div>\n    </div>\n    <div class=\"border border-dark\">\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-3\">\n          <div class=\"row\">\n            <div class=\"col-md-4\">\n              <span style=\"font-size:12px;\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-1\"> Chequing </span>\n            </div>\n            <div class=\"col-md-5 pl-0 pr-0\">\n              <span style=\"font-size:12px;\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-1\"> Chequing/Savings  </span>\n            </div>\n            <div class=\"col-md-3 pl-0 pr-0\">\n              <span style=\"font-size:12px;\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-1\"> Savings </span>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-3\"  style=\"border-left:1px solid #000\">\n          <span>Name and Branch\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;\">\n           <span>Telephone\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;\">\n          <span>Account No.\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;\">\n          <div class=\"txt-color\">Balance</div>\n          <span>\n        <span>$<input class=\"w-50 border border-0\"></span>\n      </span>\n        </div>\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-3\" style=\"border-top:1px solid #000;\">\n          <div class=\"row\">\n            <div class=\"col-md-4\">\n              <span style=\"font-size:12px;\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-1\"> Chequing </span>\n            </div>\n            <div class=\"col-md-5 pl-0 pr-0\">\n              <span style=\"font-size:12px;\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-1\"> Chequing/Savings  </span>\n            </div>\n            <div class=\"col-md-3 pl-0 pr-0\">\n              <span style=\"font-size:12px;\"><input type=\"checkbox\" class=\"input-custm cursor-pointer mr-1\"> Savings </span>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-3\"  style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <span>Name and Branch\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span>Telephone\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <span>Account No.\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <div class=\"txt-color\">Balance</div>\n          <span>\n        <span>$<input class=\"w-50 border border-0 ml-2\"></span>\n      </span>\n        </div>\n      </div>\n\n <div class=\"row ml-none mr-none\" style=\"border-top:1px solid #000;height:50px;\">\n     <div class=\"col-md-12 ml-3\" >\n       <span>Has either Applicant Ever Obtained Credit Under a Different Name? <input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3 ml-3\"> No\n       <input type=\"checkbox\" class=\"input-custm cursor-pointer mr-3 ml-3\"> Yes (List Name)\n       </span>\n     </div>\n </div>\n\n    </div>\n    <div class=\"row mt-2\">\n      <div class=\"col-md-12\">\n        <h6 class=\"font-weight-bold\">5) <span class=\"ml-5\">LIST ALL VEHICLE OPERATORS IN ORDER OF MOST FREQUENT USE</span></h6>\n      </div>\n    </div>\n    <div class=\"border border-dark\">\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-5\">\n          <span>1. Name and Address\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1\" style=\"border-left:1px solid #000;\">\n           <span style=\"font-size:12px;\">% Of Vehicle Use\n        <span><input class=\"w-50 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2 pr-0\" style=\"border-left:1px solid #000;\">\n          <span>Birth Date (DD-MM-YYYY)\n        <span><input class=\"w-75 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;\">\n          <span>Operator’s License No.\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1\" style=\"border-left:1px solid #000;\">\n          <span>Prov.\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1 pr-0\" style=\"border-left:1px solid #000;\">\n          <span style=\"font-size:12px;\">Years Licensed\n        <span><input class=\"w-50 border border-0\"></span>\n      </span>\n        </div>\n\n\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-5\" style=\"border-top:1px solid #000;\">\n          <span>1. Name and Address\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n           <span style=\"font-size:12px;\">% Of Vehicle Use\n        <span><input class=\"w-50 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2 pr-0\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <span>Birth Date (DD-MM-YYYY)\n        <span><input class=\"w-75 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <span>Operator’s License No.\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <span>Prov.\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1 pr-0\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <span style=\"font-size:12px;\">Years Licensed\n        <span><input class=\"w-50 border border-0\"></span>\n      </span>\n        </div>\n\n\n      </div>\n\n      <div class=\"row ml-none mr-none\">\n        <div class=\"col-md-2 pr-0\" style=\"border-top:1px solid #000;font-size:14px\">\n          <span>Garaging Address If Other Than Address Above\n        <span><input class=\"w-25 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-3\" style=\"border-top:1px solid #000;\">\n           <span>Number and Street\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2 pr-0\" style=\"border-top:1px solid #000;\">\n          <span>City\n        <span><input class=\"w-75 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2\" style=\"border-top:1px solid #000;\">\n          <span>Province\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-1 pl-0\" style=\"border-top:1px solid #000;\">\n          <span>Postal Code\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n        <div class=\"col-md-2 pr-0\" style=\"border-left:1px solid #000;border-top:1px solid #000;\">\n          <span style=\"font-size:14px;\">Telephone\n        <span><input class=\"w-100 border border-0\"></span>\n      </span>\n        </div>\n\n\n      </div>\n\n\n\n\n\n\n\n\n\n    </div>\n\n    <div style=\"font-size:14px\" class=\"mt-3\">\n      <div class=\"row\">\n        <div class=\"col-md-12\">\n          1. Do not sign this application before you have read it or if it contains any blanks to be filled or any errors. You confirm that the information you have given is true and complete and that you have not\n          withheld any information, and you authorize us to rely on and use the information you have given us for the purposes of making a decision on your application for credit; determining whether to enter into a\n          conditional sales contract or lease with you on the vehicle you have chosen; and where we have entered into such an agreement with you, for the purpose of administering that agreement.\n        </div>\n      </div>\n\n      <div class=\"row mt-3\">\n        <div class=\"col-md-12\">\n          2. Where this application is executed electronically, you agree to conduct this transaction in electronic form and that your electronic signature signifies your acceptance to the terms and conditions of this\n          application.\n        </div>\n      </div>\n\n      <div class=\"row mt-3\">\n        <div class=\"col-md-12\">\n          3. By signing below, you also authorize us, from time to time, to (a) disclose your personal information to Nissan Canada Financial Services Inc. and its affiliates, (i) for the purpose of, after the removal\n          of your personal identifiers, creating market analyses and other statistical studies, and (ii) to the extent such disclosure may incidentally result from the shared use by Nissan Canada Financial Services Inc.\n          and its affiliates of certain information systems on which your personal information is collected, stored and transferred, and provided that such personal information shall not be used or disclosed by Nissan\n          Canada Financial Services Inc. and its affiliates for any purpose other than (i) above; (b) collect from you, credit bureaus, other credit granters, the relevant dealer(s), and your employer(s), your credit and\n          other relevant personal information, and to use such information, as required for the above-noted purpose or as required by law; (c) disclose your credit and other relevant personal information to credit\n          bureaus and other credit granters as required for the purposes of such parties making a decision on your applications for credit, and, after the removal of your personal identifiers, creating market analyses\n          and other statistical studies; and as required by law inclusive of safety regulations; (d) disclose your personal information to third party service providers for the purposes of debt collection and the registration\n          of liens and other security interests relating to credit provided to you, and the confirmation of insurance details on the vehicle you have chosen; (e) disclose relevant personal information to the applicable\n          dealer(s) in order to (i) provide you with information relating to the purpose set out in 1. above, and (ii) in order to ensure that you comply with the requirements of your financing agreement(s).\n        </div>\n      </div>\n\n      <div class=\"row mt-3\">\n        <div class=\"col-md-12\">\n          4. In addition to the above, by signing below you authorize us to disclose your credit and other relevant personal information to Nissan Canada Financial Services Inc. and its affiliates, and to dealers, for\n          the purpose of sending to you additional information regarding their products and additional services. Please check the box below should you not wish such credit and other personal information to be\n          disclosed for such purposes:\n        </div>\n      </div>\n\n      <div class=\"row mt-3\">\n        <div class=\"col-md-12\">\n          Please do not disclose my personal information for the purpose set out in 4. above: <span><input type=\"checkbox\" class=\"input-custm cursor-pointer ml-3\"></span>\n        </div>\n      </div>\n\n      <div class=\"row mt-3\">\n        <div class=\"col-md-12\">\n          Should you have questions as to the collection, use and disclosure of your credit and other personal information, contact Customer Services at 1-800-268-6499.\n        </div>\n      </div>\n\n    </div>\n    <div class=\"row ml-none mr-none mt-5\">\n      <div class=\"col-md-3\">\n        <input _ngcontent-c2=\"\" class=\"input-border\">\n        <div class=\"font-weight-bold \">Applicant (1) Signature</div>\n      </div>\n      <div class=\"col-md-3\">\n        <input _ngcontent-c2=\"\" class=\"input-border\">\n        <div class=\"font-weight-bold \">DD-MM-YYYY </div>\n      </div>\n      <div class=\"col-md-3\">\n        <input _ngcontent-c2=\"\" class=\"input-border\">\n        <div class=\"font-weight-bold \">Applicant (2) Signature</div>\n      </div>\n      <div class=\"col-md-3\">\n        <input _ngcontent-c2=\"\" class=\"input-border\">\n        <div class=\"font-weight-bold \">DD-MM-YYYY </div>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/nisaan/nisaan.component.ts":
/*!********************************************!*\
  !*** ./src/app/nisaan/nisaan.component.ts ***!
  \********************************************/
/*! exports provided: NisaanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NisaanComponent", function() { return NisaanComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/utils */ "./src/app/common/utils.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NisaanComponent = /** @class */ (function () {
    function NisaanComponent() {
        this.changeText = true;
    }
    NisaanComponent.prototype.ngOnInit = function () {
    };
    NisaanComponent.prototype.ngOnChanges = function (changes) {
        if ('applicationJson' in changes && 'currentValue' in changes.applicationJson && changes.applicationJson.currentValue) {
            this.user = changes.applicationJson.currentValue;
            if (!_common_utils__WEBPACK_IMPORTED_MODULE_1__["default"].isNullOrEmpty(this.user, 'familyName')) {
                this.user.name = this.user.name + ' ' + this.user.familyName;
            }
            if (!('birthDate' in this.user && this.user.birthDate)) {
                this.user.birthDate = "";
            }
            if ('address' in this.user && this.user.address && 'street2' in this.user.address && this.user.address.street2) {
                this.user.address.streetAddress = this.user.address.streetAddress + ', ' + this.user.address.street2;
            }
            // for previous address
            this.user['previous addresses'][0].fullAddress = "";
            if (!_common_utils__WEBPACK_IMPORTED_MODULE_1__["default"].isNullOrEmpty(this.user, 'previous addresses') && this.user['previous addresses'].length > 0) {
                this.user['previous addresses'][0].fullAddress = _common_utils__WEBPACK_IMPORTED_MODULE_1__["default"].constructAddress(this.user['previous addresses'][0]);
            }
            // for current employer
            this.user.worksFor.empAddress = "";
            if ('worksFor' in this.user && 'address' in this.user.worksFor && Object.keys(this.user.worksFor.address).length > 0) {
                this.user.worksFor.empAddress = _common_utils__WEBPACK_IMPORTED_MODULE_1__["default"].constructAddress(this.user.worksFor.address);
            }
            // for previous employer
            this.user.preEmployer = {
                name: '',
                phone_number: ''
            };
            if ('worksFor' in this.user && 'setDuration' in this.user.worksFor && this.user.worksFor.setDuration > 2) {
                if (this.user.workedAt && this.user.workedAt.length > 0) {
                    var workedAtByDuration = this.user.workedAt.sort(function (a, b) { return (a.setDuration - b.setDuration); });
                    if (workedAtByDuration && workedAtByDuration.length > 0) {
                        this.user.preEmployer.name = workedAtByDuration[0].name + ', ' + _common_utils__WEBPACK_IMPORTED_MODULE_1__["default"].constructAddress(workedAtByDuration[0].address);
                        this.user.preEmployer.phone_number = workedAtByDuration[0].phone_number;
                    }
                }
            }
            console.log('User', this.user);
        }
        if ('invokePdf' in changes && 'currentValue' in changes.invokePdf && changes.invokePdf.currentValue && typeof changes.invokePdf.currentValue == 'string') {
            this.printPdf();
        }
    };
    // for print 
    NisaanComponent.prototype.printPdf = function () {
        window.print();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("applicationJson"),
        __metadata("design:type", Object)
    ], NisaanComponent.prototype, "applicationJson", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("invokePdf"),
        __metadata("design:type", Object)
    ], NisaanComponent.prototype, "invokePdf", void 0);
    NisaanComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nisaan',
            template: __webpack_require__(/*! ./nisaan.component.html */ "./src/app/nisaan/nisaan.component.html"),
            styles: [__webpack_require__(/*! ./nisaan.component.css */ "./src/app/nisaan/nisaan.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NisaanComponent);
    return NisaanComponent;
}());



/***/ }),

/***/ "./src/app/open-report/open-report.component.css":
/*!*******************************************************!*\
  !*** ./src/app/open-report/open-report.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".link-expire{\n  background: rgb(255, 231, 213);\n  color: rgb(230, 135, 69);\n}\n.custom-green {\n  color: rgb(0, 194, 86);\n}\n.custom-red {\n  color: rgb(234, 104, 109);\n}\n.link-expire div{\n  font-size: 14px;\n}\n.box-1{\n  border: 1px solid black;\n  min-height: 22vh;\n}\n.align-dropdown{\n  left:47px !important;\n}\n.dry-img{\n  height: 70px;\n  width: 70px;\n  vertical-align: super!important;\n}\n.header-space{\n  white-space: pre;\n}\n.work{\n  width: 45px;\n}\n.f-20{\n  font-size: 20px;\n}\n.f-35{\n  font-size: 28px;\n}\n.f-14{\n  font-size: 14px;\n}\n.custom-sidebar{\n  background: rgb(86, 86, 86);\n  color: white;\n}\n.custom-sidebar ul li{\n  list-style-type: none;\n}\n.canvas-div{\n  border: 1px solid black;\n  padding: 15px;\n}\n.nav-tabs .nav-link.active{\n  background: rgb(38, 129, 208);\n  border: 1px solid rgb(38, 129, 208);\n  color: #ffffff;\n}\n.margin-none-upper{\n  margin-top:-1px;\n}\n.bgclr{\n  background:#eae8e8;\n}\n.bgclr > div, .bgclr > div > ul{\n  width: 100%;\n}\n.bgclr > div > ul > li {\n  width: 25%;\n  display: block;\n}\n.left{\n  margin-left:21%;\n}\n.button-width{\n  margin:0 auto;\n}\n.set-border{\n  border:0px;\n  background:0px;\n  text-align: center;\n}\n/* For header */\n.change-clr{\n  color:white;\n}\n.navbar{\n  background:#393939;\n  padding: 0.2rem 1rem;\n}\n.user-icon{\n  width:35px;\n  border-radius:50%;\n }\n.navbar-nav .dropdown-menu {\n  position: absolute;\n  right:0 !important;\n}\n.white{\n  color:#ffffff;\n}\n.navbar-brand{\n  display:contents;\n}\n.pic-logo-icon{\n  padding-bottom:10px;\n  width:130px;\n}\n.p-width{\n  width:80px;\n}\n.blank-display{\n  position: absolute;\n  background: #353434;\n  height: 100%;\n  width: 100%;\n  z-index: 999;\n}\n.close-btn-customize{\n  float: right;\n  margin-right: 45px;\n}\n.centering{\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  height: 80vh;\n}\n@media screen and (max-width:375px){\n  .p-width{\n    width:125px;\n  }\n  .dropdown-menu{\n    left:-30px;\n  }\n\n  .or-text-align{\n    text-align: center;\n  }\n}\n@media screen and (max-width:380px){\n  .p-width{\n    width:120px;\n  }\n  .logo-icon{\n    width:70px;\n  }\n  /* div .d-icon{\n    display: block;\n  } */\n}\n.account-number-font{\n        height: 25px;\n        background-color: red;\n        width: 25px;\n        border-radius: 50%;\n        position: absolute;\n        bottom: 34px;\n        right: 82px;\n        font-size: 12px;\n        font-weight: bold;\n        color: #ffffff;\n        padding-right: 10px;\n        padding-top: 4px;\n}\n@media only screen and (max-width: 708px) {\n  .navbar-position {\n    position:unset;\n  }\n .report-font > ul{\n      font-size:small;\n      white-space:pre;\n    }\n  .button-font{\n    font-size:9px !important;\n  }\n}\n/* added style */\n.p-t-b-2{\n  padding-top:0.6rem;\n  padding-bottom:0.6rem;\n}\n.nav-tabs .active{\n  /* border-bottom: 2px solid #F5BF13; */\n  color: #F5BF13 !important;\n}\n.tab-visible{\n  visibility: hidden;\n}\n/* @media only screen and (max-width: 480px){\n  .header-block{\n    display:block;\n    width:100%;\n  }\n} */\n.dropdown-menu .dropdown-item:hover {\n  background-color: #f0f7ed;\n  cursor: pointer;\n}\n.btn-success:focus {\n  box-shadow: unset !important;\n}\n.dropdown-menu{\n  min-width: 14.5rem !important;\n  padding: 0 !important;\n  -webkit-transform: translate3d(16%, 54%, 0) !important;\n          transform: translate3d(16%, 54%, 0) !important;\n  font-size: 14px;\n}\n.bg-common {\n  /* Add the blur effect */\n  filter: blur(7px);\n  -webkit-filter: blur(7px);\n\n  /* Center and scale the image nicely */\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.bg-report-image {\n   /* The image used  */\n  background-image: url(\"/../assets/ReportPageUI.png\");\n  height: 800px;\n}\n.report-profile{\n  margin-left: 30px;\n  margin-right: 30px;\n}"

/***/ }),

/***/ "./src/app/open-report/open-report.component.html":
/*!********************************************************!*\
  !*** ./src/app/open-report/open-report.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- header -->\n<nav class=\"navbar navbar-light navbar-position\">\n  <a routerLink=\"/dashboard\" class=\"navbar-brand\">\n    <img class=\"pic-logo-icon\" src=\"../../assets/Nexus-new-logo.png\" alt=\"...\">\n  </a>\n  <div class=\"col-md-8 col-12 col-lg-8 col-sm-8 pt-1 pb-1 change-clr or-text-align\">\n    <span *ngIf=\"report\"> {{\"OR_SPAN_LINK_EXPIRE\" | translate}} {{report.valid_till | datediffformat}}</span>\n  </div>\n</nav>\n<div *ngIf=!reportPage>\n<div class=\"justify-content-around mx-auto\" *ngIf=\"report\">\n  <div class=\"row bgclr\">\n    <div class=\"col-md-12 p-t-b-2 report-font\">\n      <ul class=\"nav nav-tabs button-width header-space header-block\">\n        <li class=\"col-md-2 col-sm-2 col-12 text-center\">\n          <button class=\"active tabs-border cursor-pointer d-inline set-border set-font\" data-toggle=\"tab\" href=\"#profile\" *ngIf=\"!showPdf\">\n            <div class=\"m-0 font-weight-bold\"><i class=\"fa fa-user d-icon\"></i>&nbsp;{{\"OR_DIV_PROFILE\" | translate}}</div>\n          </button>\n        </li>\n        <li [ngClass]=\"{'tab-visible': !(report.share_trends == 1)}\" class=\"col-md-3 col-sm-3 col-12 text-center\">\n          <button class=\"tabs-border cursor-pointer d-inline set-border set-font\" data-toggle=\"tab\" href=\"#trends\" *ngIf=\"!showPdf\">\n            <div class=\"m-0 font-weight-bold\"><i class=\"fa fa-bar-chart d-icon\"></i> {{\"OR_DIV_TRENDS\" | translate}}</div>\n          </button>\n        </li>\n        <li [ngClass]=\"{'tab-visible': !(report.share_trans == 1)}\" class=\"col-md-3 col-sm-3 col-12 text-center\">\n          <button class=\"tabs-border cursor-pointer d-inline set-border set-font\" data-toggle=\"tab\" href=\"#transactions\" *ngIf=\"!showPdf\">\n            <div class=\"m-0 font-weight-bold\"><i class=\"fa fa-usd d-icon\"></i> {{\"OR_DIV_TRANSACTIONS\" | translate}}</div>\n          </button>\n        </li>\n        <li class=\"col-md-4 col-sm-4 col-12 text-center noprint\">\n          <div class=\"dropdown\" *ngIf=\"!showPdf\">\n            <button type=\"button\" class=\"btn btn-sm btn-success button-font dropdown-toggle\" data-toggle=\"dropdown\">\n              {{\"OR_BUTTON_GENERATE_APPLICATION_FORM\" | translate}}\n            </button>\n            <div class=\"dropdown-menu align-dropdown\">\n              <a class=\"dropdown-item\" (click)=\"applicationFormJson('regular')\">Regular Loan Application</a>\n              <a class=\"dropdown-item\" (click)=\"applicationFormJson('nissan')\">Nissan Loan Application</a>\n            </div>\n          </div>\n\n          <button class=\"btn btn-sm btn-danger ml-2\" (click)=\"closingPdf()\" *ngIf=\"showPdf\"\n                [ngClass]=\"showPdf ? 'close-btn-customize' : ''\">\n                     <i class=\"fa fa-window-close\" aria-hidden=\"true\"></i>\n          </button>\n\n          <button class=\"btn btn-sm btn-dark float-right printBtn\" (click)= \"callPrintPdf()\" *ngIf=\"showPdf\"\n          [ngClass]=\"showPdf ? 'float-right' : ''\">\n            <i class=\"fa fa-print\" aria-hidden=\"true\"></i>\n          </button>\n        </li>\n      </ul>\n    </div>\n  </div>\n\n  <div class=\"tab-content mt-0\" *ngIf=\"!showPdf\">\n    <!-- Profile section  -->\n    <div id=\"profile\" class=\"report-profile tab-pane active\">\n      <app-profile  [tranxData] = \"tranxData\" [properties]=\"propertiesData\" [report]=\"report\"\n        [scoreGraphObj]=\"scoreValue\" [updatedReport] = \"reportData\" style=\"margin-top:50px;\"></app-profile> \n    </div>\n\n    <!-- Trends Section -->\n    <div id=\"trends\" class=\"tab-pane fade report-trend\">\n      <app-trends [report]=\"report\" [updatedReport] = \"reportData\" (tranxDataEmit)=\"receivedTranxData($event)\"></app-trends>\n      <div class=\"pt-3 pl-3\" *ngIf=\"report.share_trends == 0\">{{ \"OR_DIV_NOT_SHARED_TRENDS_DETAILS\" | translate}}</div>\n    </div>\n\n    <!-- Transactions Section -->\n    <div id=\"transactions\" class=\"tab-pane fade\">\n      <app-report-transaction *ngIf=\"report.share_trans == 1\" [accounts]=\"accounts\" [report]=\"report\"></app-report-transaction>\n      <div class=\"pt-3 pl-3\" *ngIf=\"report.share_trans == 0\">{{  \"OR_DIV_NOT_SHARED_TRANSACTIONS_DETAILS\" | translate}}</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"showPdf\" id=\"contentToConvert\" >\n      <app-pdf *ngIf=\"loanType.name === 'regular'\" [applicationJson] = \"applicationJson\" [invokePdf] = \"invokePdf\"></app-pdf>\n      <app-nisaan *ngIf=\"loanType.name === 'nissan'\" [applicationJson]=\"applicationJson\" [invokePdf] = \"invokePdf\"></app-nisaan>\n  </div>\n\n</div>\n\n<div class=\"centering\" *ngIf=\"expiredDiv\">\n  <div class=\"text-center\">\n    <h4 class=\"m-0 p-0\"> {{\"OR_H4_THIS_LINK_HAS_EXPIRED\" | translate}}</h4>\n    <h5>{{\"OR_H5_PLEASE_CONTACT\" | translate}}</h5>\n  </div>\n</div>\n </div> \n<div *ngIf=reportPage>\n    <div class=\"bg-report-image bg-common\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/open-report/open-report.component.ts":
/*!******************************************************!*\
  !*** ./src/app/open-report/open-report.component.ts ***!
  \******************************************************/
/*! exports provided: OpenReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OpenReportComponent", function() { return OpenReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};








var OpenReportComponent = /** @class */ (function () {
    function OpenReportComponent(router, route, spinnerService, notifierService, backend, authService) {
        this.router = router;
        this.route = route;
        this.spinnerService = spinnerService;
        this.notifierService = notifierService;
        this.backend = backend;
        this.authService = authService;
        this.report = null;
        this.accounts = null;
        this.applicationJson = {};
        this.showPdf = false;
        this.reportPage = true;
        this.scoreValue = {};
        this.loanType = {
            'name': ''
        };
        this.notifier = notifierService;
    }
    OpenReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Got obj value", this.subscription);
        this.id = this.route.snapshot.paramMap.get("id");
        console.log('report', this.id);
        this.spinnerService.show();
        var path = "/reports/" + this.id;
        // Get report details
        this.backend.put(null, path, this.authService.getAccessTokenAndID())
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["mergeMap"])(function (data) {
            _this.report = data.json();
            console.log('generateReport', _this.report);
            _this.report.valid_till = parseInt(_this.report.valid_till);
            console.log('ipdateGenerateReport', _this.report);
            // this.spinnerService.hide();
            return _this.backend.get('/accounts', _this.report.token);
        }))
            .subscribe(function (accountsResponse) {
            console.log('Got accounts', accountsResponse);
            _this.accounts = [];
            if (accountsResponse) {
                _this.accounts = accountsResponse.json().accounts;
                console.log('this.accounts', _this.accounts);
                var accountsObj = Object.create({});
                accountsObj['accounts'] = _this.accounts;
                _this.reportData = Object.assign(_this.report, accountsObj);
            }
            _this.reportPage = !_this.reportPage;
            _this.spinnerService.hide();
            _this.backend.get('/users/points', _this.report.token).subscribe(function (res) {
                if (res) {
                    var points = res.json();
                    _this.scoreValue = points['data'];
                    console.log('Tipping Points', _this.scoreValue);
                }
            }, function (error) {
                var errorMsg = error.json();
                _this.notifier.notify('error', errorMsg.message);
            });
        }, function (error) {
            console.log('error', error);
            _this.reportPage = !_this.reportPage;
            _this.spinnerService.hide();
            var errorDetails = error.json();
            if (errorDetails.message == "Report has expired") {
                _this.expiredDiv = true;
            }
            _this.notifier.notify('error', errorDetails.message);
        });
    };
    OpenReportComponent.prototype.receivedTranxData = function (e) {
        this.tranxData = e;
    };
    OpenReportComponent.prototype.applicationFormJson = function (value) {
        this.showPdf = true;
        this.loanType.name = value;
        this.invokePdf = 10;
        this.applicationJson = {
            "@context": "http://schema.org",
            "@type": "Person",
            "address": {
                "@type": "PostalAddress",
                "addressLocality": "",
                "addressRegion": "",
                "postalCode": "",
                "streetAddress": "",
                "from": "",
                "to": ""
            },
            "previous addresses": [
                {
                    "@type": "PostalAddress",
                    "addressLocality": "",
                    "addressRegion": "",
                    "postalCode": "",
                    "streetAddress": "",
                    "from": "",
                    "to": ""
                }
            ],
            "name": "",
            "jobTitle": "",
            "image": "",
            "alternateName": "",
            "additionalName": "",
            "givenName": "",
            "familyName": "",
            "birthPlace": "",
            "birthDate": "",
            "email": [],
            "telephone": "",
            "mobilephone": "",
            "url": "",
            "sameAs": [],
            "worksFor": {
                "@type": "Organization",
                "name": "",
                "url": "",
                "from": "",
                "to": "",
                "annualSalary": "",
                "address": {
                    "@type": "PostalAddress",
                    "addressLocality": "",
                    "postalCode": "",
                    "streetAddress": "",
                    "from": "",
                    "to": "",
                    "annualSalary": ""
                }
            },
            "workedAt": []
        };
        if (this.report && (typeof this.report === 'object')) {
            this.applicationJson.name = this.report.first_name;
            this.applicationJson.givenName = this.report.first_name;
            this.applicationJson.familyName = this.report.last_name;
            this.applicationJson.email.push(this.report.emails);
            this.applicationJson.telephone = this.report.phone_numbers;
            this.applicationJson.image = this.report.picture;
            if ('dob' in this.report && this.report.dob) {
                this.applicationJson.birthDate = moment__WEBPACK_IMPORTED_MODULE_5__["utc"](this.report.dob).format('DD-MM-YYYY');
            }
            else {
                this.applicationJson.birthDate = "";
            }
        }
        if ('address' in this.report && this.report.address) {
            this.applicationJson.address.addressLocality = this.report.address.city;
            this.applicationJson.address.addressRegion = this.report.address.state;
            this.applicationJson.address.postalCode = this.report.address.zip;
            this.applicationJson.address.streetAddress = this.report.address.street;
            this.applicationJson.address.street2 = this.report.address.street2;
        }
        if ('employers' in this.report && this.report.employers.length > 0) {
            var employerWithPreferredSet = this.report.employers.filter(function (e) { return e.preferred; });
            if (!(employerWithPreferredSet && employerWithPreferredSet.length > 0)) {
                var employersByAmount = this.report.employers.sort(function (a, b) { return (b.income - a.income); });
                if (employersByAmount && employersByAmount.length > 0 && 'income' in employersByAmount[0] && employersByAmount[0].income) {
                    employersByAmount[0].preferred = true;
                }
                else {
                    this.report.employers[0].preferred = true;
                }
            }
            try {
                for (var _a = __values(this.report.employers), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var employer = _b.value;
                    if ('preferred' in employer && employer.preferred) {
                        var data = {
                            'address': {}
                        };
                        data.name = employer.employer_name;
                        data.from = employer.from_duration;
                        data.to = employer.to_duration;
                        data.annualSalary = employer.income;
                        data.preferred = employer.preferred;
                        if ('address' in employer && employer.address && employer.address != null) {
                            data.address.addressLocality = employer.address.city;
                            data.address.postalCode = employer.address.zipCode;
                            data.address.streetAddress = employer.address.street1;
                        }
                        if ('phone_number' in employer && employer.phone_number) {
                            data.phone_number = employer.phone_number;
                        }
                        if ('from_duration' in employer && employer.from_duration) {
                            var presentEmployer = moment__WEBPACK_IMPORTED_MODULE_5__["utc"]();
                            var fromDuration = moment__WEBPACK_IMPORTED_MODULE_5__["utc"](employer.from_duration);
                            if (presentEmployer.diff(fromDuration, 'months') > 24) {
                                data.setDuration = 3;
                            }
                            else {
                                data.setDuration = 2;
                            }
                        }
                        this.applicationJson.worksFor = Object.assign(this.applicationJson.worksFor, data);
                    }
                    else {
                        var dataWorkedAt = {
                            "@type": "Organization",
                            address: {
                                "@type": "PostalAddress"
                            }
                        };
                        dataWorkedAt.name = employer.employer_name;
                        dataWorkedAt.url = "";
                        dataWorkedAt.preferred = employer.preferred;
                        if ('phone_number' in employer && employer.phone_number) {
                            dataWorkedAt.phone_number = employer.phone_number;
                        }
                        if ('address' in employer && employer.address) {
                            dataWorkedAt.address.addressLocality = employer.address.city;
                            dataWorkedAt.address.postalCode = employer.address.zipCode;
                            dataWorkedAt.address.streetAddress = employer.address.street1;
                        }
                        else {
                            dataWorkedAt.address.streetAddress = "";
                            dataWorkedAt.address.postalCode = "";
                            dataWorkedAt.address.addressLocality = "";
                        }
                        if (!('preferred' in employer && employer.preferred)) {
                            if ('to_duration' in employer && employer.to_duration) {
                                var presentEmployer = moment__WEBPACK_IMPORTED_MODULE_5__["utc"]();
                                var toDuration = moment__WEBPACK_IMPORTED_MODULE_5__["utc"](employer.to_duration);
                                dataWorkedAt.setDuration = presentEmployer.diff(toDuration, 'months');
                            }
                        }
                        this.applicationJson.workedAt.push(dataWorkedAt);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        console.log("Generate Application JSON", this.applicationJson);
        var e_1, _c;
    };
    OpenReportComponent.prototype.closingPdf = function () {
        this.showPdf = false;
    };
    OpenReportComponent.prototype.callPrintPdf = function () {
        this.invokePdf == 'callingPdf' ? this.invokePdf = 'swapVariable' : this.invokePdf = 'callingPdf';
    };
    OpenReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-open-report',
            template: __webpack_require__(/*! ./open-report.component.html */ "./src/app/open-report/open-report.component.html"),
            styles: [__webpack_require__(/*! ./open-report.component.css */ "./src/app/open-report/open-report.component.css"), __webpack_require__(/*! ../global/global.css */ "./src/app/global/global.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_3__["NotifierService"], _backend_backend_service__WEBPACK_IMPORTED_MODULE_4__["BackendService"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]])
    ], OpenReportComponent);
    return OpenReportComponent;
}());



/***/ }),

/***/ "./src/app/pdf/pdf.component.css":
/*!***************************************!*\
  !*** ./src/app/pdf/pdf.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span{\n    color: #636060;\n}\n.wrapup-container{\n    padding-left: 5%;\n    padding-right: 5%;\n}\n.custom-border{\n    border: 4px solid black;\n}\n.cus-bot-border{\n    border-bottom: 3px solid black;\n    font-size: 19px;\n    font-weight: 800;\n}\n.font-15{\n    font-size: 15px;\n}\n.nexus-log{\n    padding-bottom:10px;\n    width:130px;\n}\n.signature-box{\n    width: 195px;\n    border-bottom: 1px solid;\n    height: 25px;\n    text-align: right;\n}\n.s-box-1{\n    float: left;\n    margin-left: 80px;\n}\n.s-box-2{\n    float: right;\n    margin-right: 80px;\n}\n.signature-name > div:first-child{\n    margin-left: 21%;\n}\n.signature-name > div:last-child{\n    margin-left: 33%;\n}\nh5.a-application-form{\n    border-top: 3px solid;\n    border-bottom: 3px solid;\n    margin-left: 5px;\n    margin-right: 5px;\n    font-size: 22px;\n    font-weight: 800;\n    background: #80808094;\n    margin-top: 13px;\n    padding-left: 10px;\n}\n.arrangement-application{\n    width: 99% !important;\n    margin-left: 6px;\n}\n.border-width-2px{\n    border-width: 1px !important;\n}\ndiv{\n    overflow: hidden;\n}\ninput[type=text], span.occupied{\n    border: transparent;\n    font-weight: 800;\n    width: 100%;\n}\n.declearation{\n    padding-left: 14px;\n    font-weight: 800;\n    font-size: 18px;\n    line-height: 22px;\n    margin-top: 10px;\n}\n.dealer-section{\n    color: #80808094;\n    font-size: 4.5rem;\n    font-weight: 800;\n    line-height: 1.2;\n}\n.year-row > div> span{\n    display: block;\n    font-size: 15px;\n}\n.w-99{\n    width: 99%;\n}\n.agreement{\n    margin-top:30px !important;\n}\nh5.agreement + div, span.notices + div{\n    text-align: justify;\n}\nspan.notices{\n    text-align: center;\n    margin-top: 30px \n}\nspan.notices > h5{\n    border-bottom: 3px solid;\n    font-size: 22px;\n    font-weight: 800;\n    padding: 0;\n    display: inline-block;\n}\n.font-22{\n    font-weight: 600;\n}\n.names-and-address{\n    width: 48%;\n    display: inline-block;\n    margin-top: 8%;\n}\n.names-and-address > input[type=\"text\"]{\n    width: 100%;\n    border-bottom: 2px solid !important;\n    height: 10px;\n}\n.final-certify{\n    border-top: 3px solid;\n    padding-top: 16px \n}\n.final-certify > h5{\n    border: 3px solid;\n    font-size: 16px;\n    font-weight: 800;\n    background: #80808040;\n    padding-top: 5px;\n    padding-bottom: 5px;\n    padding-left: 10px;\n    font-style: italic;\n    text-align: center;\n}\n.final-certify > div > div > input[type=\"text\"]{\n    height: 75px;\n}\n.border-bottom-2px{\n    border-bottom: 2px solid !important;\n}\n.margin-top-40{\n    margin-top : 80px;\n}\n.vermont, .Ohio{\n    margin-top :40px;\n}\n.new-york{\n    margin-top : 130px;\n}\n.margin-attached{\n    margin-top: 100px;\n    border-top: 1px solid;\n}\n.time-at-address{\n    width: 50px !important;\n    height: 15px;\n    border-bottom: 2px solid black !important;\n}\n.w-80{\n    width:70% !important;\n    margin-left: 5px !important;\n}\n@media only screen and (min-width: 1300px) {\n    .margin-top-40{\n        margin-top : 150px;\n    }\n    .Ohio{\n        margin-top :20px;\n    }\n    .vermont{\n        margin-top : 80px !important;\n    }\n  }\n"

/***/ }),

/***/ "./src/app/pdf/pdf.component.html":
/*!****************************************!*\
  !*** ./src/app/pdf/pdf.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid pb-4\">\n  <div class=\"row\">\n\n    <div class=\"section-1\">\n      <!-- logo section -->\n      <div class=\"container-fluid mt-4\">\n        <div class=\"row\">\n          <div class=\"col-md-6\">\n            <span class=\"d-inline-block\">{{\"PDF_SPAN_DEALER_NAME\" | translate}}: </span> <input class=\"w-80\" type=\"text\">\n            <span class=\"d-inline-block\">{{\"PDF_SPAN_DEALER_PHONE\" | translate}} #: </span> <input class=\"w-80\" type=\"text\">\n            <span class=\"d-inline-block\">{{  \"PDF_SPAN_DEALER_FAX\" | translate}} # </span> <input class=\"w-80\" type=\"text\">\n          </div>\n          <div class=\"col-md-6 text-right\">\n            <img class=\"nexus-log\" src=\"../../assets/nexus-log.png\" alt=\"...\">\n          </div>\n        </div>\n      </div>\n\n      <div class=\"container-fluid ml-2 mr-2 custom-border p-0 pb-3 overflow-hidden w-99\">\n\n        <!-- header section -->\n        <div class=\"row ml-1 mr-1\">\n\n          <!-- instruction section -->\n          <h5 class=\"cus-bot-border w-100\">{{ \"PDF_H5_PLEASE_PRINT_INCOMPLETE\" | translate}}</h5>\n\n          <div class=\"col-md-6 pl-0\">\n            <span class=\"d-block\">{{\"PDF_SPAN_INSTRUCTIONS\" | translate}} </span>\n            <span class=\"d-block\">{{\"PDF_SPAN_ALONE_NAME\" | translate}}</span>\n\n            <div class=\"font-15\">\n              <span class=\"d-block\">{{\"PDF_SPAN_PLEASE_INDICATE_WHETHER_YOU_ARE_APPLYING_FOR\" | translate}} </span>\n              <div class=\"mb-3 ml-3\">\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{ \"PDF_SPAN_INDIVIDUAL_CREDIT\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{ \"PDF_SPAN_JOIN_CREDIT\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_COMMUNITY_PROPERLY_STATE\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_BUSINESS_APPLICATION\" | translate}}</span>\n              </div>\n\n              <span class=\"d-block\">(2) <input class=\"ml-1\" type=\"checkbox\">\n              {{\"PDF_SPAN_APPLYING_FOR_INDIVIDUAL_CREDIT\" | translate}}\n              </span>\n            </div>\n          </div>\n\n          <div class=\"col-md-6 pl-0 font-15\">\n            <div class=\"mb-3\">\n              <span class=\"d-block\">(3) <input class=\"ml-1\" type=\"checkbox\">\n                <span>\n                 {{  \"PDF_SPAN_COMPLETE_A_B\" | translate}}\n                </span>\n              </span>\n              <span class=\"ml-5\">\n                {{\"PDF_SPAN_APPLY_FOR_JOINT_CREDIT\" | translate}}\n              </span>\n              <div class=\"signature-space\">\n                <div class=\"signature-box s-box-1 d-inline\"></div>\n                <div class=\"signature-box s-box-2 d-inline\"></div>\n              </div>\n\n              <div class=\"signature-name\">\n                <div class=\"d-inline\">{{\"PDF_DIV_APPLICANT\" | translate}}</div>\n                <div class=\"d-inline\">{{  \"PDF_DIV_CO_APPLICANT\" | translate}}</div>\n              </div>\n            </div>\n\n            <span>\n             {{ \"PDF_SPAN_MARITAL_STATUS\" | translate}}\n            </span>\n          </div>\n\n        </div>\n\n        <!-- A.Application Information -->\n        <h5 class=\"a-application-form mb-0\">{{  \"PDF_H5_A_APPLICANT_INFORMATION\" | translate}}</h5>\n        <div class=\"arrangement-application\">\n\n          <!-- last name row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-8 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n                <div class=\"col-md-4\">\n                  <span>\n                    {{\"PDF_SPAN_LAST_NAME\" | translate}}\n                  </span>\n                  <input type=\"text\" [(ngModel)]=\"user.familyName\">\n                </div>\n                <div class=\"col-md-4\">\n                  <span>\n                    {{\"PDF_SPAN_FIRST_NAME\" | translate}}\n                  </span>\n                  <input type=\"text\" [(ngModel)]=\"user.name\">\n                </div>\n                <div class=\"col-md-4\">\n                  <span>\n                    {{\"PDF_SPAN_MIDDLE_INITIAL\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n              </div>\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px overflow-hidden\">\n              <span>{{\"PDF_SPAN_SOCIAL_SECURITY_NUMBER\" | translate}}</span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 overflow-hidden\">\n              <span>{{\"PDF_SPAN_BIRTH_DATE\" | translate}}</span>\n              <input type=\"text\" [(ngModel)]=\"user.birthDate\">\n            </div>\n          </div>\n\n          <!-- Address row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-8 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n                <div class=\"col-md-6 border-right border-dark border-width-2px\">\n                  <span class=\"d-block\">\n                    {{\"PDF_SPAN_ADDRESS\" | translate}}\n                  </span>\n                  <input type=\"text\" [(ngModel)]=\"user.address.streetAddress\">\n                </div>\n                <div class=\"col-md-2 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_APT_SUITE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-2 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_PO_BOX\" | translate}} \n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-2\">\n                  <span>\n                    {{\"PDF_SPAN_RURAL_ROUTE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n              </div>\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px overflow-hidden\">\n              <span>{{\"PDF_SPAN_CITY\" | translate}}</span>\n              <input type=\"text\" [(ngModel)]=\"user.address.addressLocality\">\n            </div>\n            <div class=\"col-md-1 p-1 border-right border-dark border-width-2px overflow-hidden\">\n              <span>\n                {{\"PDF_SPAN_STATE\" | translate}}\n              </span>\n              <input type=\"text\" [(ngModel)]=\"user.address.addressRegion\" style=\"font-size:12px\">\n            </div>\n\n            <div class=\"col-md-1 pl-1\">\n              <span>\n                {{\"PDF_SPAN_ZIP\" | translate}}\n              </span>\n              <input type=\"text\" [(ngModel)]=\"user.address.postalCode\" style=\"font-size:12px\">\n            </div>\n          </div>\n\n          <!-- Home Phone row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-8 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_HOME_PHONE\" | translate}}\n                  </span>\n                  <input type=\"text\" [ngModel]=\"user.telephone | phoneNumber\">\n                </div>\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_CELL_PHONE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-6\">\n                  <span>\n                    {{\"PDF_SPAN_RESEDENTIAL_STATUS\" | translate}}\n                  </span>\n                  <div class=\"\">\n                    <input class=\"ml-1\" type=\"checkbox\">\n                    <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_HOMEOWNER\" | translate}}</span>\n                    <input class=\"ml-1\" type=\"checkbox\">\n                    <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_RENT\" | translate}}</span>\n                    <input class=\"ml-1\" type=\"checkbox\">\n                    <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_FAMILY\" | translate}}</span>\n                    <input class=\"ml-1\" type=\"checkbox\">\n                    <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_OTHER\" | translate}}</span>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px overflow-hidden\">\n              <span>{{\"PDF_SPAN_TIME_AT_ADDRESS\" | translate}}</span>\n              <input class=\"time-at-address\" type=\"text\"> YR <input class=\"time-at-address\" type=\"text\"> Mos.\n            </div>\n            <div class=\"col-md-2 overflow-hidden\">\n              <input class=\"d-block\" type=\"text\">\n              <span>{{\"PDF_SPAN_RENTIMTG_PMT\" | translate}}</span>\n            </div>\n          </div>\n\n          <!-- Email Address row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-5 border-right border-dark border-width-2px\">\n              <span class=\"d-block\">\n                {{  \"PDF_SPAN_EMAIL_ADDRESS\" | translate}}\n              </span>\n              <input type=\"text\"[(ngModel)]=\"user.userEmail\">\n            </div>\n            <div class=\"col-md-7 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n                <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                  <span class=\"d-block\">\n                    {{ \"PDF_SPAN_DRIVERS_LICENCE_NUMBER\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                  <span>\n                    {{  \"PDF_SPAN_DRIVERS_LICENCE_STATE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-4\">\n                  <span>\n                    {{  \"PDF_SPAN_TIME_AT_PREVIOUS_ADDRESS\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <!-- Previous Full Address row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-8 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n                <div class=\"col-md-6 border-right border-dark border-width-2px\">\n                  <span class=\"d-block\">\n                  {{\"PDF_SPAN_PREVIOUS_FULL_ADDRESS\" | translate}}\n                  </span>\n                  <input type=\"text\" [(ngModel)]=\"user['previous addresses'][0].fullAddress\">\n                </div>\n                <div class=\"col-md-2 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_APT_SUITE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-2 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_PO_BOX\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-2\">\n                  <span>\n                    {{\"PDF_SPAN_RURAL_ROUTE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n              </div>\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px overflow-hidden\">\n              <span> {{\"PDF_SPAN_CITY\" | translate}}</span>\n              <input type=\"text\" [(ngModel)]=\"user['previous addresses'][0].addressLocality\">\n            </div>\n            <div class=\"col-md-1  border-right border-dark border-width-2px overflow-hidden\">\n              <span>\n                {{\"PDF_SPAN_STATE\" | translate}}\n              </span>\n              <input type=\"text\" [(ngModel)]=\"user['previous addresses'][0].addressRegion\">\n            </div>\n\n            <div class=\"col-md-1\">\n              <span>\n                {{\"PDF_SPAN_ZIP\" | translate}}\n              </span>\n              <input type=\"text\" [(ngModel)]=\"user['previous addresses'][0].postalCode\">\n            </div>\n          </div>\n\n          <!-- Employer Name row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-4 border-right border-dark border-width-2px\">\n              <span class=\"d-block\">\n                {{\"PDF_SPAN_EMPLOYER_NAME\" | translate}}\n              </span>\n              <input type=\"text\" [(ngModel)]=\"user.worksFor.name\">\n            </div>\n            <div class=\"col-md-8 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_EMPLOYER_TYPE\" | translate}}\n              </span>\n              <div class=\"\">\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\"> {{\"PDF_SPAN_EMPLOYED\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\"> {{\"PDF_SPAN_UNEMPLOYED\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_SELF_EMPLOYED\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_MILITARY\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_RETORED\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_STUDENT\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_OTHER\" | translate}}</span>\n              </div>\n            </div>\n          </div>\n\n          <!-- Salary row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-6 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-9\">\n                  <span>\n                    {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                  </span>\n                  <div class=\"\">\n                    <input class=\"ml-1\" type=\"checkbox\">\n                    <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_WEEKLY\" | translate}}</span>\n                    <input class=\"ml-1\" type=\"checkbox\">\n                    <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_BI_WEEKLY\" | translate}}</span>\n                    <input class=\"ml-1\" type=\"checkbox\">\n                    <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_MONTHLY\" | translate}}</span>\n                    <input class=\"ml-1\" type=\"checkbox\">\n                    <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_ANNUALLY\" | translate}}</span>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-md-6 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n                <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-4\">\n                  <span>\n                    {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <!--Previous Employer Name row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-4 border-right border-dark border-width-2px\">\n              <span class=\"d-block\">\n                {{\"PDF_SPAN_PREVIOUS_EMPLOYER_NAME\" | translate}}\n              </span>\n              <input type=\"text\" [(ngModel)]=\"user.preEmployer.name\">\n            </div>\n            <div class=\"col-md-8 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_PREVIOUS_EMPLOYEE_TYPE\" | translate}}\n              </span>\n              <div class=\"\">\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_EMPLOYED\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_UNEMPLOYED\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_SELF_EMPLOYED\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_MILITARY\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_RETORED\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_STUDENT\" | translate}}</span>\n                <input class=\"ml-1\" type=\"checkbox\">\n                <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_OTHER\" | translate}}</span>\n              </div>\n            </div>\n          </div>\n\n          <!--Previous Occupation row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-4 border-right border-dark border-width-2px\">\n              <span class=\"d-block\">\n                {{\"PDF_SPAN_PREVIOUS_OCCUPATION\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-3 border-right border-dark border-width-2px\">\n              <span class=\"d-block\">\n                {{\"PDF_SPAN_LENGTH_OF_EMPLOYMENT\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-5 border-right border-dark border-width-2px\">\n              <span class=\"d-block\">\n                {{\"PDF_SPAN_PREVIOUS_WORK_PHONE_NUMBER\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n          </div>\n\n          <!-- Alimony Child support -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <span class=\"pl-4\">\n              <small class=\"font-weight-bold\">\n               {{  \"PDF_SMALL_ALIMONY\" | translate}}\n              </small>\n            </span>\n          </div>\n\n          <!-- Alimony Child support -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-3 border-right border-dark border-width-2px\">\n              <span class=\"d-block\">\n              {{\"PDF_SPAN_OTHER_INCOME\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-9 border-right border-dark border-width-2px\">\n              <span class=\"d-block\">\n              {{\"PDF_SPAN_SOURCE_OF_OTHER_INCOME\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n          </div>\n\n\n        </div>\n\n          <!-- B.Application Information -->\n          <h5 class=\"a-application-form mb-0\">{{  \"PDF_H5_B_APPLICANT_INFORMATION\" | translate}}</h5>\n          <div class=\"arrangement-application\">\n  \n            <!-- last name row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-8 border-right border-dark border-width-2px\">\n                <div class=\"row\">\n                  <div class=\"col-md-4\">\n                    <span>\n                      {{\"PDF_SPAN_LAST_NAME\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-4\">\n                    <span>\n                      {{\"PDF_SPAN_FIRST_NAME\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-4\">\n                    <span>\n                      {{\"PDF_SPAN_MIDDLE_INITIAL\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-2 border-right border-dark border-width-2px overflow-hidden\">\n                <span>{{\"PDF_SPAN_SOCIAL_SECURITY_NUMBER\" | translate}}</span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-2 overflow-hidden\">\n                <span>{{\"PDF_SPAN_BIRTH_DATE\" | translate}}</span>\n                <input type=\"text\">\n              </div>\n            </div>\n  \n            <!-- Address row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-8 border-right border-dark border-width-2px\">\n                <div class=\"row\">\n                  <div class=\"col-md-6 border-right border-dark border-width-2px\">\n                    <span class=\"d-block\">\n                      {{\"PDF_SPAN_ADDRESS\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-2 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_APT_SUITE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-2 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_PO_BOX\" | translate}} \n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-2\">\n                    <span>\n                      {{\"PDF_SPAN_RURAL_ROUTE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-2 border-right border-dark border-width-2px overflow-hidden\">\n                <span>{{\"PDF_SPAN_CITY\" | translate}}</span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-1  border-right border-dark border-width-2px overflow-hidden\">\n                <span>\n                  {{\"PDF_SPAN_STATE\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n  \n              <div class=\"col-md-1\">\n                <span>\n                  {{\"PDF_SPAN_ZIP\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n            </div>\n  \n            <!-- Home Phone row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-8 border-right border-dark border-width-2px\">\n                <div class=\"row\">\n                  <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_HOME_PHONE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_CELL_PHONE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-6\">\n                    <span>\n                      {{\"PDF_SPAN_RESEDENTIAL_STATUS\" | translate}}\n                    </span>\n                    <div class=\"\">\n                      <input class=\"ml-1\" type=\"checkbox\">\n                      <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_HOMEOWNER\" | translate}}</span>\n                      <input class=\"ml-1\" type=\"checkbox\">\n                      <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_RENT\" | translate}}</span>\n                      <input class=\"ml-1\" type=\"checkbox\">\n                      <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_FAMILY\" | translate}}</span>\n                      <input class=\"ml-1\" type=\"checkbox\">\n                      <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_OTHER\" | translate}}</span>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-2 border-right border-dark border-width-2px overflow-hidden\">\n                <span>{{\"PDF_SPAN_TIME_AT_ADDRESS\" | translate}}</span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-2 overflow-hidden\">\n                <input class=\"d-block\" type=\"text\">\n                <span>{{\"PDF_SPAN_RENTIMTG_PMT\" | translate}}</span>\n              </div>\n            </div>\n  \n            <!-- Email Address row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-5 border-right border-dark border-width-2px\">\n                <span class=\"d-block\">\n                  {{  \"PDF_SPAN_EMAIL_ADDRESS\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-7 border-right border-dark border-width-2px\">\n                <div class=\"row\">\n                  <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                    <span class=\"d-block\">\n                      {{ \"PDF_SPAN_DRIVERS_LICENCE_NUMBER\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                    <span>\n                      {{  \"PDF_SPAN_DRIVERS_LICENCE_STATE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-4\">\n                    <span>\n                      {{  \"PDF_SPAN_TIME_AT_PREVIOUS_ADDRESS\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                </div>\n              </div>\n            </div>\n  \n            <!-- Previous Full Address row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-8 border-right border-dark border-width-2px\">\n                <div class=\"row\">\n                  <div class=\"col-md-6 border-right border-dark border-width-2px\">\n                    <span class=\"d-block\">\n                    {{\"PDF_SPAN_PREVIOUS_FULL_ADDRESS\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-2 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_APT_SUITE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-2 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_PO_BOX\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-2\">\n                    <span>\n                      {{\"PDF_SPAN_RURAL_ROUTE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-2 border-right border-dark border-width-2px overflow-hidden\">\n                <span> {{\"PDF_SPAN_CITY\" | translate}}</span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-1  border-right border-dark border-width-2px overflow-hidden\">\n                <span>\n                  {{\"PDF_SPAN_STATE\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n  \n              <div class=\"col-md-1\">\n                <span>\n                  {{\"PDF_SPAN_ZIP\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n            </div>\n  \n            <!-- Employer Name row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                <span class=\"d-block\">\n                  {{\"PDF_SPAN_EMPLOYER_NAME\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-8 border-right border-dark border-width-2px\">\n                <span>\n                  {{\"PDF_SPAN_EMPLOYER_TYPE\" | translate}}\n                </span>\n                <div class=\"\">\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\"> {{\"PDF_SPAN_EMPLOYED\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\"> {{\"PDF_SPAN_UNEMPLOYED\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_SELF_EMPLOYED\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_MILITARY\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_RETORED\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_STUDENT\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_OTHER\" | translate}}</span>\n                </div>\n              </div>\n            </div>\n  \n            <!-- Salary row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-6 border-right border-dark border-width-2px\">\n                <div class=\"row\">\n                  <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-9\">\n                    <span>\n                      {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                    </span>\n                    <div class=\"\">\n                      <input class=\"ml-1\" type=\"checkbox\">\n                      <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_WEEKLY\" | translate}}</span>\n                      <input class=\"ml-1\" type=\"checkbox\">\n                      <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_BI_WEEKLY\" | translate}}</span>\n                      <input class=\"ml-1\" type=\"checkbox\">\n                      <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_MONTHLY\" | translate}}</span>\n                      <input class=\"ml-1\" type=\"checkbox\">\n                      <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_ANNUALLY\" | translate}}</span>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-6 border-right border-dark border-width-2px\">\n                <div class=\"row\">\n                  <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                    <span>\n                      {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                  <div class=\"col-md-4\">\n                    <span>\n                      {{\"PDF_SPAN_SALARY_TYPE\" | translate}}\n                    </span>\n                    <input type=\"text\">\n                  </div>\n                </div>\n              </div>\n            </div>\n  \n            <!--Previous Employer Name row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                <span class=\"d-block\">\n                  {{\"PDF_SPAN_PREVIOUS_EMPLOYER_NAME\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-8 border-right border-dark border-width-2px\">\n                <span>\n                  {{\"PDF_SPAN_PREVIOUS_EMPLOYEE_TYPE\" | translate}}\n                </span>\n                <div class=\"\">\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_EMPLOYED\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_UNEMPLOYED\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_SELF_EMPLOYED\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_MILITARY\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_RETORED\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_STUDENT\" | translate}}</span>\n                  <input class=\"ml-1\" type=\"checkbox\">\n                  <span class=\"ml-1 mr-1\">{{\"PDF_SPAN_OTHER\" | translate}}</span>\n                </div>\n              </div>\n            </div>\n  \n            <!--Previous Occupation row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                <span class=\"d-block\">\n                  {{\"PDF_SPAN_PREVIOUS_OCCUPATION\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                <span class=\"d-block\">\n                  {{\"PDF_SPAN_LENGTH_OF_EMPLOYMENT\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-5 border-right border-dark border-width-2px\">\n                <span class=\"d-block\">\n                  {{\"PDF_SPAN_PREVIOUS_WORK_PHONE_NUMBER\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n            </div>\n  \n            <!-- Alimony Child support -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <span class=\"pl-4\">\n                <small class=\"font-weight-bold\">\n                 {{  \"PDF_SMALL_ALIMONY\" | translate}}\n                </small>\n              </span>\n            </div>\n  \n            <!-- Other Income row -->\n            <div class=\"row border-bottom border-dark border-width-2px\">\n              <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                <span class=\"d-block\">\n                {{\"PDF_SPAN_OTHER_INCOME\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n              <div class=\"col-md-9 border-right border-dark border-width-2px\">\n                <span class=\"d-block\">\n                {{\"PDF_SPAN_SOURCE_OF_OTHER_INCOME\" | translate}}\n                </span>\n                <input type=\"text\">\n              </div>\n            </div>\n  \n  \n          </div>\n          \n          \n          \n          <!-- Comments row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-12\">\n              <span class=\"d-block\">\n                {{  \"PDF_SPAN_COMMENTS\" | translate}}\n              </span>\n              <textarea class=\"border-0\" name=\"\" id=\"\" cols=\"30\" rows=\"2\"></textarea>\n            </div>\n          </div>\n\n\n        </div>\n\n      <!-- </div> -->\n      <!--end of closing -->\n\n      <!-- declearation section -->\n      <div class=\"declearation\">\n       {{  \"PDF_DIV_USER_CONCENT\" | translate}}\n      </div>\n    </div>\n\n    <!-- DEALER SECTION -------------------------------------------------------------------------------->\n    <div class=\"section-2\">\n      <div class=\"container-fluid mt-4\">\n        <div class=\"row\">\n          <div class=\"col-md-8\">\n            <h1 class=\"dealer-section\">{{\"PDF_H1_DEALER_SECTION\" | translate}}</h1>\n          </div>\n          <div class=\"col-md-4 text-right\">\n              <img class=\"nexus-log\" src=\"../../assets/nexus-log.png\" alt=\"...\">\n          </div>\n        </div>\n      </div>\n\n      <div class=\"container-fluid ml-2 mr-2 custom-border p-0 pb-3 overflow-hidden w-99\">\n\n        <h5 class=\"a-application-form mb-0\">{{\"PDF_H1_DEALER_SECTION\" | translate}}</h5>\n\n        <!-- Dealer Information -->\n        <div class=\"arrangement-application\">\n\n          <!-- Dealer row -->\n          <div class=\"row border-bottom border-dark border-width-2px\">\n            <div class=\"col-md-6 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_DEALER\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_MILEAGE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_VEHICLE_TYPE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-3\">\n                  <span>\n                    {{\"PDF_SPAN_STOCK_NUMBER\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n\n              </div>\n            </div>\n            <div class=\"col-md-6 border-right border-dark border-width-2px\">\n              <div class=\"row\">\n                <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                  <span>\n                    {{  \"PDF_SPAN_NUMBER\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-4 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_SOURCE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-4\">\n                  <span>\n                    {{\"PDF_SPAN_CERTIFIED_PRE_OWNED\" | translate}}\n                  </span>\n                  <input type=\"checkbox\">\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <!-- year row -->\n          <div class=\"row border-bottom border-dark border-width-2px year-row\">\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{\"APM_OPTION_YEAR\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-11 border-right border-dark border-width-2px\">\n              <div class=\"row year-row\">\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{ \"APM_OPTION_TRIM\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{ \"PDF_SPAN_MAKE\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_MODEL\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n                <div class=\"col-md-3 border-right border-dark border-width-2px\">\n                  <span>\n                    {{\"PDF_SPAN_VIN\" | translate}}\n                  </span>\n                  <input type=\"text\">\n                </div>\n              </div>\n            </div>\n          </div>\n          \n          <!-- Term row -->\n          <div class=\"row border-bottom border-dark border-width-2px year-row\">\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_TERM\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n                {{ \"PDF_SPAN_ CASH_SELLING_PRICE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{ \"PDF_SPAN_SALES_TAX\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_T_L\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{ \"PDF_SPAN_CASH_DOWN\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_FRONTEND_FEES\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_REBATE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{ \"PDF_SPAN_NET_TRADE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_ACQ_FEE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n              {{ \"PDF_SPAN_UNPAID_BALANCE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n          </div>\n\n          <!-- Accident row -->\n          <div class=\"row border-bottom border-dark border-width-2px year-row\">\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n               {{\"PDF_SPAN_ACCIDENT_HEALTH_INS\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n                {{ \"PDF_SPAN_CREDIT_LIFE_INSURANCE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_GAP\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n                {{ \"PDF_SPAN_SERVICE_PLAN\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n            {{\"PDF_SPAN_BACK_END_FEES\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n               {{\"PDF_SPAN_EST_AMT_FINANCED\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n          </div>\n          \n           <!-- MSRP row -->\n          <div class=\"row border-bottom border-dark border-width-2px year-row\">\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_MSRP\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_INVOICE_WHOLESALE_VALUE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n              {{\"PDF_SPAN_WHOLESALE_SOURCE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-1 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_RETAIL_VALUE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_RETAIL_SOURCE\" |translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n               {{\"PDF_SPAN_ESTIMATED_PAYMENT\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n            {{\"PDF_SPAN_REQUESTED_APR\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n          </div>\n\n          <!-- Vehicle row -->\n          <div class=\"row border-bottom border-dark border-width-2px year-row\">\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span class=\"d-inline\">\n                {{\"PDF_SPAN_VEHICLE_BOOKOUT\" | translate}}\n              </span>\n              <input type=\"checkbox\" class=\"pl-2\">\n            </div>\n            <div class=\"col-md-2 border-right border-dark border-width-2px\">\n              <span>\n                {{  \"PDF_SPAN_BOOKOUT_DATE\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>\n            <div class=\"col-md-8 border-right border-dark border-width-2px\">\n              <span>\n                {{\"PDF_SPAN_LENDER_PROGRAM\" | translate}}\n              </span>\n              <input type=\"text\">\n            </div>          \n          </div>\n\n          <!-- Comments row -->\n          <div class=\"row\">\n            <div class=\"col-md-12\">\n              <span class=\"d-block\">\n                {{\"PDF_SPAN_VEHICLE_OPTIONS\" | translate}}\n              </span>\n              <textarea class=\"border-0\" name=\"\" id=\"\" cols=\"30\" rows=\"4\"></textarea>\n            </div>\n          </div>\n\n\n        </div>\n        <!--Dealer of closing -->\n\n      </div>\n\n      <h5 class=\"a-application-form mb-0 border-0 text-center bg-white agreement\">{{\"PDF_H5_AGRRE\" | translate}}</h5>\n      <div class=\"ml-4 mr-4\">\n        <span>\n          \n         {{\"PDF_SPAN_AGREEMENT\" | translate}}\n        </span>\n      </div>\n      \n    </div>\n    <!--section-2closing -->\n\n    <div class=\"section-3\">\n      \n        <div class=\"container-fluid mt-4\">\n            <div class=\"row\">\n              <div class=\"col-md-12 text-right\">\n                <img class=\"nexus-log\" src=\"../../assets/nexus-log.png\" alt=\"...\">\n              </div>\n            </div>\n          </div>\n\n      <span class=\"d-block notices\">\n        <h5>{{\"PDF_H5_FEDERAL_NOTICES\" | translate}}</h5>\n      </span>\n      <div class=\"ml-4 mr-4\">\n        <span>{{\"PDF_SPAN_IMPORTANT_INFORMATION\" | translate}} </span>{{  \"PDF_DIV_IDENTIFYING_DOCUMENTS\" | translate}}\n      </div>\n\n      <span class=\"d-block notices\">\n        <h5>{{ \"PDF_H5_STATE_NOTICES\" | translate}}</h5>\n      </span>\n      <div class=\"ml-4 mr-4\">\n       \n        <div class=\"mt-4\">\n             <span class=\"font-22\">{{\"PDF_SPAN_CALIFORNIA_RESIDENTS\" | translate}}</span>  {{\"PDF_DIV_MARRIED_APPLICANT\" | translate}}\n        </div>\n        <div class=\"mt-4\">\n             <span class=\"font-22\">{{ \"PDF_SPAN_MAINE_AND_TENNESSEE RESIDENTS\" | translate}}</span>  {{\"PDF_DIV_FOR_ALEASE\" | translate}}\n        </div>\n\n        <div class=\"mt-4\">\n            <span class=\"font-22\">{{ \"PDF_SPAN_NEW_HAMPSHIRE_RESIDENTS\" | translate}}</span>  {{ \"PDF_DIV_BALLOON_PAYMENT_CONTRACT\" | translate}}\n        </div>\n\n        <div class=\"mt-4\">\n            <span class=\"font-22\">{{ \"PDF_SPAN_NEW_YORK_RESIDENTS\" | translate}}</span>  {{ \"PDF_DIV_APPLICATION_FOR_CREDIT\" | translate}}.\n        </div>\n\n        <div class=\"mt-4\">\n            <span class=\"font-22\">{{  \"PDF_SPAN_OHIO_RESIDENTS\" | translate}}</span>  {{\"PDF_DIV_LAWS_AGAINST_DISCRIMINATION\" | translate}}\n        </div>\n\n        <div class=\"mt-4\">\n            <span class=\"font-22\">{{\"PDF_SPAN_RHODE_ISLAND_RESIDENTS\" | translate}}</span>  {{\"PDF_DIV_CONSUMER_REPORTS\"| translate }} \n        </div>\n\n        <div class=\"mt-4\">\n            <span class=\"font-22\">{{\"PDF_SPAN_VERMONT_RESIDENTS\" | translate}}</span>  {{\"PDF_DIV_AUTHORIZE\" | translate}}\n        </div>\n\n        <div class=\"mt-4\">\n            <span class=\"font-22\">{{\"PDF_SPAN_MARRIED_WISCONSIN_RESIDENTS\" | translate}}</span>  {{\"PDF_DIV_PROVISION_OF_MARITAL_PROPERTY\" | translate}}\n        </div>\n\n        <div>\n          <small>\n              {{\"PDF_SMALL_APPLICATION_SUBMITTED\" | translate}}\n          </small>\n          <span class=\"d-inline-block names-and-address\">\n             <input type=\"text\">\n          </span>\n        </div>\n\n        <div class=\"mt-4 final-certify\">\n            <h5 class=\"a-application-form m-0\">{{\"PDF_H5_SIGNING\" | translate}}</h5>\n        \n        <div class=\"row text-center\">\n          <div class=\"col-md-4\">\n           <input type=\"text\" class=\"d-block w-100 border-bottom-2px\">\n           <span>{{\"PDF_SPAN_APPLICANTS_SIGNATURE\" | translate}}</span>\n          </div>\n          <div class=\"col-md-2\">\n              <input type=\"text\" class=\"d-block w-100 border-bottom-2px\">\n              <span>{{\"PDF_SPAN_DATE\" | translate}}</span>\n          </div>\n          <div class=\"col-md-4\">\n              <input type=\"text\" class=\"d-block w-100 border-bottom-2px\">\n              <span>{{\"PDF_SPAN_CO_APPLICANTS_SIGNATURE\" | translate}}</span>\n          </div>\n          <div class=\"col-md-2\">\n              <input type=\"text\" class=\"d-block w-100 border-bottom-2px\">\n              <span>{{\"PDF_SPAN_DATE\" | translate}}</span>\n          </div>\n        </div>\n        \n        </div>\n\n      </div>\n\n    </div>\n    <!--section-3 closing -->\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pdf/pdf.component.ts":
/*!**************************************!*\
  !*** ./src/app/pdf/pdf.component.ts ***!
  \**************************************/
/*! exports provided: PdfComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfComponent", function() { return PdfComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/utils */ "./src/app/common/utils.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PdfComponent = /** @class */ (function () {
    function PdfComponent(spinnerService) {
        this.spinnerService = spinnerService;
    }
    PdfComponent.prototype.ngOnInit = function () {
    };
    PdfComponent.prototype.ngOnChanges = function (changes) {
        if ('applicationJson' in changes && 'currentValue' in changes.applicationJson && changes.applicationJson.currentValue) {
            this.user = changes.applicationJson.currentValue;
            if (!('birthDate' in this.user && this.user.birthDate)) {
                this.user.birthDate = "";
            }
            // for email
            this.user.userEmail = "";
            if (!_common_utils__WEBPACK_IMPORTED_MODULE_2__["default"].isNullOrEmpty(this.user, 'email') && this.user.email.length > 0) {
                this.user.userEmail = this.user.email[0];
            }
            if ('address' in this.user && this.user.address && 'street2' in this.user.address && this.user.address.street2) {
                this.user.address.streetAddress = this.user.address.streetAddress + ', ' + this.user.address.street2;
            }
            // for previous address
            this.user['previous addresses'][0].fullAddress = "";
            if (!_common_utils__WEBPACK_IMPORTED_MODULE_2__["default"].isNullOrEmpty(this.user, 'previous addresses') && this.user['previous addresses'].length > 0) {
                this.user['previous addresses'][0].fullAddress = _common_utils__WEBPACK_IMPORTED_MODULE_2__["default"].constructAddress(this.user['previous addresses'][0]);
            }
            // for current employer
            this.user.worksFor.empAddress = "";
            if ('worksFor' in this.user && 'address' in this.user.worksFor && Object.keys(this.user.worksFor.address).length > 0) {
                this.user.worksFor.empAddress = _common_utils__WEBPACK_IMPORTED_MODULE_2__["default"].constructAddress(this.user.worksFor.address);
            }
            // for previous employer
            this.user.preEmployer = {
                name: '',
            };
            if ('worksFor' in this.user && 'setDuration' in this.user.worksFor && this.user.worksFor.setDuration > 2) {
                if (this.user.workedAt && this.user.workedAt.length > 0) {
                    var workedAtByDuration = this.user.workedAt.sort(function (a, b) { return (a.setDuration - b.setDuration); });
                    if (workedAtByDuration && workedAtByDuration.length > 0) {
                        this.user.preEmployer.name = workedAtByDuration[0].name;
                    }
                }
            }
            console.log('User', this.user);
        }
        if ('invokePdf' in changes && 'currentValue' in changes.invokePdf && changes.invokePdf.currentValue && typeof changes.invokePdf.currentValue == 'string') {
            this.printPdf();
        }
    };
    // for print
    PdfComponent.prototype.printPdf = function () {
        window.print();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("applicationJson"),
        __metadata("design:type", Object)
    ], PdfComponent.prototype, "applicationJson", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("invokePdf"),
        __metadata("design:type", Object)
    ], PdfComponent.prototype, "invokePdf", void 0);
    PdfComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pdf',
            template: __webpack_require__(/*! ./pdf.component.html */ "./src/app/pdf/pdf.component.html"),
            styles: [__webpack_require__(/*! ./pdf.component.css */ "./src/app/pdf/pdf.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_spinner__WEBPACK_IMPORTED_MODULE_1__["NgxSpinnerService"]])
    ], PdfComponent);
    return PdfComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/*!***********************************************!*\
  !*** ./src/app/profile/profile.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".no-break{\n  white-space: nowrap;\n}\n.break-address{\n  word-break:break-word; \n}\n.box-flex{\n  display:flex;\n}\n.div-width{\n  width:100% !important;\n}\n.profile-img{\n    width:100%;\n}\n.border-div{\n  border:2px solid black;\n}\n.div-flex{\n  display:flex;\n}\n.nexus-width{\n  width: 100% !important;;\n}\n/* @media only screen and (min-width: 992px){\n  .remove-padding{\n    padding-left: 0px !important;\n    padding-right:0px !important;\n  }\n\n} */\n@media only screen and (max-width: 992px){\n  .div-top{\n    margin-top:6px;\n  }\n}\n@media only screen and (max-width: 767px){\n  .margin-view{\n    margin-left:1px !important;\n    margin-right:1px !important; \n  }\n}\n.underline{\n    /* text-decoration:underline !important; */\n    right: 24%;\n    color: #007bff !important;\n    position: absolute;\n    top: 34px;\n}\n.padding-div{\n  padding-bottom:6px;\n}\n.container-width{\n  margin:auto;\n  width:50%;\n}\n.user-card{\n    border-radius: 4px;\n    background:#49c75c;\n    color: #fff;\n}\n.box-align{\n  display:flex;\n}\n.box-width{\n  width:100%;\n}\n.info-list{\n    list-style: none;\n}\n.emp-text{\n    width: 38%;\n    border-radius: 4px;\n    outline: none;\n    border: none;\n    background: #36A148;\n    color: #fff;\n}\n.user-card input[type=\"text\"]::-webkit-input-placeholder {\n    color: #FEFFF8;\n    text-align: center;\n    font-size: 12px;\n  }\n.help-icon{\n    position: absolute;\n    color: #49c75c;\n    font-size: 12px;\n    font-weight: bold;\n    left: 71%;\n    width: 15px;\n    height: 15px;\n    text-align: center;\n    border-radius: 81%;\n    background: #fff;\n    top: -10px;\n}\n@media (max-width: 768px){\n  .container-width{\n    width:100% !important;\n  }\n}\n@media (max-width: 575px){\n    .profile-img{\n        width:30%;\n    }\n    .content-padding{\n        padding-left:15px !important;\n        padding-right:15px !important;\n    }\n}\n/* .profile-image{\n  background-position: center;\n  background-repeat: no-repeat;\n} */\n.user-image{\n  border-radius:50%;\n  width:112px;\n  height:112px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.element-font {\n    position: absolute;\n    top: 50%;\n    -webkit-transform: translate(50%, -50%);\n            transform: translate(50%, -50%);\n}\n/* .image-border{\n    border-right:2px solid #000000;\n} */\n.text-center-font{\n    overflow: hidden;\n    width: 81%;\n}\n.image-media{\n    position: absolute;\n    top: 50%;\n    -webkit-transform: translate(0%, -50%);\n            transform: translate(0%, -50%);\n  }\n.padding-left{\n    padding-left:0px ;\n}\n.padding-right{\n    padding-right:0px ;\n}\n.employer-details{\n    border-bottom:1px solid #000000;\n    background-color: #343a40 !important;\n    color: #f8f9fa !important;\n}\n.details-employee{\n    border-bottom:1px solid #000000;\n}\n.center{\n  text-align: center !important;\n}\n.text-left{\n  text-align: left !important;\n}\n.box{\n  align-items: center;\n  display: flex;\n  justify-content: center;\n  height:100%;\n  width:100%;\n}\n.set-height{\n  height:130px;\n  text-align:center;\n}\n.float-value{\n  left:5px;\n  left:12px;\n  text-align:right;\n  position:relative;\n}\n.margin-inspect{\n    margin-right:-9px !important;\n    margin-left:-9px !important;\n}\n.loading-text{\n  justify-content: center; \n  display:flex;\n  align-items: center;\n  font-size: 1.5em;\n  height:82%;\n\n}\n.edit-color{\n    color:#2eb8b8;\n}\n.enter-start-date{\n    background-color:#005160;\n    color:#FFFFFF;\n    border:0px;\n    font-size: 14px;\n    height: 24px;\n}\n.info-color{\n    color:#EB0000;\n}\n/* @media only screen and (max-width: 900px) {\n  .padding-zero{\n    padding-right:0px !important;\n  }\n  .padding-zero-none{\n    padding-left:0px !important ;\n  }\n} */\n.gray-bg-color{\n    margin-left:1px !important;\n    margin-right:1px !important;\n    background-color:#E3E3E3;\n}\n.emp-height{\n  min-height:170px;\n}\n.chart-height{\n  min-height:436px;\n}\n.property-height{\n  min-height:165px;\n}\n.income-height{\n  min-height:348px;\n}\n.fa-building{\n    color:white;\n}\n.edit-profile{\n    text-decoration:underline;\n    margin: 0 !important;\n}\n.pencil-icon{\n  position: relative;\n  top: -142px;\n  right: -19px;\n}\n.nexus-score{\n    font-size:25px;\n}\n.score-size{\n  font-size:20px;\n}\n.calculate-score{\n    background-color:#46a81b;\n    border: 0px;\n    color: #ffffff;\n    padding: 5px;\n    font-size: 15px;\n}\n.source-income{\n    color:#4b5eff;\n}\n@media only screen and (max-width: 600px) {\n  .font-change{\n    font-size:12px ;\n  }\n}\n.employer-income{\n  border-bottom:1px solid #000000;\n  font-size:25px;\n}\n.employer-name{\n  background-color:#343a40;\n  border:1px solid #343a40;\n  color: #f8f9fa !important;\n  font-size:100%;\n}\n.add-employer{\n  color:#378dff;\n}\n.note-drag{\n  color:tomato;\n}\n.save-employee-btn{\n  background-color: #46a81b;\n  border: 0px;\n  color: #ffffff;\n  float:right;\n  padding: 5px 10px;\n}\n.Cancel-employee-btn{\n  background-color:tomato !important;\n}\n.employer-table td {\n  width:10%;\n}\n.employer-table tbody td:first-child {\n  vertical-align: middle;\n}\n.employer-table tbody td.v-align {\n  vertical-align: middle;\n}\n.employer-table td.bt0{\n  border-top:none;\n  padding-top: 0px;\n\n}\n.see-transection{\n  color:#46a81b;\n}\n.tippingPoints-tm{\n  min-width: 94px;\n  height: 20px;\n  display: inline-block;\n  text-align: center;\n  position: relative;\n  top: 52px;\n  left: 25%;\n}\n.tippingPoints-tb{\n  min-width: 94px;\n  height: 20px;\n  display: inline-block;\n  position: relative;\n  top: 96px;\n  left: 34%;\n  text-align: left;\n}\n.tippingPoints-tt{\n  min-width: 94px;\n  height: 20px;\n  display: inline-block;\n  position: relative;\n  /* text-align: right; */\n  top: 100px;\n  left: 14%;\n}\nspan.text-1{\n    font-size: 12px;\n    font-weight: 800;\n    top: 51%;\n    right: 69%;\n    border-bottom: 4px solid rgb(238, 90, 69);\n    color: rgb(238, 90, 69);\n    width: 173px;\n    text-align: left;\n}\nspan.text-2{\n  left: 5%;\n  display: block;\n  width: 177px;\n  font-size: 12px;\n  top: 10%;\n  border-bottom: 4px solid rgb(241, 128, 72);\n  color: rgb(241, 128, 72);\n  font-weight: 700;\n  text-align: left;\n}\nspan.text-3{\n  font-size: 12px;\n  font-weight: 800;\n  top: 53%;\n  right:-0.5%;\n  border-bottom: 4px solid rgb(79, 149, 120);\n  color: rgb(79, 149, 120);\n  display: block;\n  text-align: right;\n  width: 177px;\n}\nspan.text-4{\n  right: 8.8%;\n  display: block;\n  width: 164px;\n  font-size: 12px;\n  bottom: 76.5%;\n  border-bottom: 4px solid rgb(247, 182, 69);\n  color: rgb(247, 182, 69);\n  font-weight: 700;\n  text-align: right;\n  \n}\n.cus-stick {\n  width: 58px;\n  position: absolute;\n  display: block;\n}\n.cus-pos-1{\n    border-bottom: 4px solid rgb(241, 128, 72);\n    -webkit-transform: rotate(70deg);\n            transform: rotate(70deg);\n    top: 33%;\n    left: 32%;\n}\n.cus-pos-2{\n  border-bottom: 4px solid rgb(247, 182, 69);\n  -webkit-transform: rotate(105deg);\n          transform: rotate(105deg);\n  top: 32%;\n  left: 57%;\n}\n/* The container */\n.custom-mark {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n/* Hide the browser's default checkbox */\n.custom-mark input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0;\n}\n/* Create a custom checkbox */\n.checkmark {\n  position: absolute;\n  top: -17px;\n  left: 0;\n  height: 25px;\n  width: 25px;\n  border:2px solid #ccc6c6;\n  border-radius:5px;\n}\n/* On mouse-over, add a grey background color */\n.custom-mark:hover input ~ .checkmark {\n  background-color: #fff;\n}\n/* When the checkbox is checked, add a blue background */\n.custom-mark input:checked ~ .checkmark {\n  background-color: #ffffff;\n  border: 2px solid #46a81b;\n  border-radius: 5px;\n}\n/* Create the checkmark/indicator (hidden when not checked) */\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n/* Show the checkmark when checked */\n.custom-mark input:checked ~ .checkmark:after {\n  display: block;\n}\n/* Style the checkmark/indicator */\n.custom-mark .checkmark:after {\n  left: 9px;\n  top: -8px;\n  width: 12px;\n  height: 23px;\n  border: solid #46a81b;\n  border-width: 0 5px 5px 0;\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg);\n}\n.employ-starts{\n  font-size:80%;\n  font-weight:600;\n}\n/* The container */\n.radio-body {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-left:15px ;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n/* Hide the browser's default radio button */\n.radio-body input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n}\n/* Create a custom radio button */\n.checkradio {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 32px;\n  width: 32px;\n  border:2px solid #000000;\n  border-radius: 50%;\n}\n/* On mouse-over, add a grey background color */\n.radio-body:hover input ~ .checkradio {\n  background-color: #ffffff;\n}\n/* When the radio button is checked, add a blue background */\n.container input:checked ~ .checkradio {\n  background-color: #ffffff;\n  border:3px solid #46a81b;\n}\n/* Create the indicator (the dot/circle - hidden when not checked) */\n.checkradio:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n/* Show the indicator (dot/circle) when checked */\n.radio-body input:checked ~ .checkradio:after {\n  display: block;\n}\n/* Style the indicator (dot/circle) */\n.radio-body .checkradio:after {\n \t left: 7px;\n    top: 1px;\n    width: 10px;\n    height: 18px;\n    border: solid #46a81b;\n    border-width: 0 4px 4px 0;\n    -webkit-transform: rotate(45deg);\n    transform: rotate(45deg);\n}\n.back-btn{\n   font-size:20px !important;\n   width:120px;\n   padding:5px;\n   border:0px;\n}\n.margin-top-div{\n   margin-top:122px;\n}\n.control-scroll{\n  height: 140px;\n  overflow: auto;\n  overflow-x: hidden; \n}\n/* The container */\n.custom-radio-btn {\n  position: relative;\n  padding-left: 35px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n/* Hide the browser's default radio button */\n.custom-radio-btn input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n}\n/* Create a custom radio button */\n.checkmark-radio-btn {\n  position: absolute;\n  top: -9px;\n  left: 0;\n  height: 25px;\n  width: 25px;\n  border:2px solid #ccc6c6;\n  border-radius: 50%;\n}\n/* When the radio button is checked, add a blue background */\n.custom-radio-btn input:checked ~ .checkmark-radio-btn {\n  background-color: #ffffff;\n  border:2px solid #46a81b;\n}\n/* Create the indicator (the dot/circle - hidden when not checked) */\n.checkmark-radio-btn:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n/* Show the indicator (dot/circle) when checked */\n.custom-radio-btn input:checked ~ .checkmark-radio-btn:after {\n  display: block;\n}\n/* Style the indicator (dot/circle) */\n.custom-radio-btn .checkmark-radio-btn:after {\n \ttop: 3px;\n\tleft: 3px;\n\twidth: 15px;\n\theight: 15px;\n\tborder-radius: 50%;\n\tbackground: #46a81b;\n}\n.image-fixed-size{\n  position: relative;\n  text-align: center !important;\n  height: 125px;\n}\n.add-address{\n  background:transparent;\n  color:#007bff;\n  font-weight:600;\n  border:0px;\n}\n.bottom-line{\n  border-bottom:1px solid #ddd4d4;\n}\n.spacewrap{\n  white-space:nowrap; \n}\n.div-position1{\n  margin-left: -15px;\n  margin-right: 4px;\n  border: 2px solid;\n  /* flex: 0 0 32.333333%;\n  max-width: 33.333333%; */\n}\n.div-position2{\n  /* flex: 0 0 69.333333% !important; */\n  margin-left: 10px;\n  border: 2px solid;\n  /* max-width:65.733333% !important; */\n}\n/* .div-height{\n  height:66px;\n} */\n/* .icon-margin{\n  margin-left:-8px; \n} */\n/* .income-left{\n  margin-left: -1.9%;\n}\n.credit-div{\n  margin-left:-1.5%;\n}\n.property-left{\n  margin-left:-1.5%;\n} */\n.adjust-image{\n  height:162px;\n  vertical-align: middle;\n  padding: 20px 0 0 0;\n}\n.class-1{\n  font-size:18px;\n}\n.score-div{\n  height: 270px;\n  position: relative;\n  width:582px;\n  margin-left: auto;\n  margin-right: auto;\n}\n.font-text{\n  font-size:14px;\n}\n.img-center-adjust{\n  -webkit-transform: translate(25%, 10%);\n          transform: translate(25%, 10%);\n\n}\n.input-color{\n  color:black;\n}\n.align-value td{\n  vertical-align: middle;\n}\n@media only screen and (max-width: 400px) and (min-width: 320px){\n  .text-nowrap{\n    white-space:inherit !important; \n  }\n  .profile-font{\n    font-size:12px !important;\n  }\n}\n@media only screen and (max-width: 1200px) and (min-width: 1024px){\n  .email-font{\n    font-size:12px;\n  }\n  .div-padding{\n    padding-bottom:16px;\n  }\n  .score-div{\n    width:488px !important;\n  }\n  .tippingPoints-tm{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    text-align: center;\n    position: relative;\n    top: 18px;\n    left: 21%;\n  }\n  .tippingPoints-tb{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 70px;\n    left: 32%;\n    text-align: left;\n  }\n  .tippingPoints-tt{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 64px;\n    left: 18%;\n  }\n  \n  span.text-1{\n    font-size: 11px;\n    font-weight: 800;\n    top: 34%;\n    right: 64%;\n    border-bottom: 4px solid rgb(238, 90, 69);\n    color: rgb(238, 90, 69);\n    width: 173px;\n    text-align: left;\n  }\n  \n  span.text-2{\n    \n    left: 4.7%;\n    display: block;\n    width: 177px;\n    font-size: 11px;\n    top: 1%;\n    border-bottom: 4px solid rgb(241, 128, 72);\n    color: rgb(241, 128, 72);\n    font-weight: 700;\n    text-align: left;\n\n  }\n  \n  \n  span.text-3{\n    \n    font-size: 11px;\n    font-weight: 800;\n    top: 42%;\n    right: 2.5%;\n    border-bottom: 4px solid rgb(79, 149, 120);\n    color: rgb(79, 149, 120);\n    display: block;\n    text-align: right;\n    width: 155px;\n\n  }\n  \n  span.text-4{\n  \n    right: 7.5%;\n    display: block;\n    width: 164px;\n    font-size: 11px;\n    bottom: 85.5%;\n    border-bottom: 4px solid rgb(247, 182, 69);\n    color: rgb(247, 182, 69);\n    font-weight: 700;\n    text-align: right;\n  }\n  \n  .cus-pos-1{\n    border-bottom: 4px solid rgb(241, 128, 72);\n    -webkit-transform: rotate(70deg);\n            transform: rotate(70deg);\n    top: 23%;\n    left: 37%;\n  }\n  .cus-pos-2{\n    border-bottom: 4px solid rgb(247, 182, 69);\n    -webkit-transform: rotate(105deg);\n            transform: rotate(105deg);\n    top: 23%;\n    left: 52%;\n  }\n  \n}\n@media only screen and (max-width: 1023px) and (min-width: 768px){\n  .email-font{\n    font-size:12px;\n  }\n  .score-div{\n    width:488px !important;\n  }\n  .div-padding{\n    padding-bottom:15px;\n  }\n  .tippingPoints-tm{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    text-align: center;\n    position: relative;\n    top: 18px;\n    left: 21%;\n  }\n  .tippingPoints-tb{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 70px;\n    left: 32%;\n    text-align: left;\n  }\n  .tippingPoints-tt{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 64px;\n    left: 18%;\n  }\n  \n  span.text-1{\n   \n    font-size: 12px;\n    font-weight: 800;\n    top: 40%;\n    right: 69%;\n    border-bottom: 4px solid rgb(238, 90, 69);\n    color: rgb(238, 90, 69);\n    width: 173px;\n    text-align: left;\n  }\n  \n  span.text-2{\n    \n   \n    left: 2%;\n    display: block;\n    width: 177px;\n    font-size: 12px;\n    top: 6%;\n    border-bottom: 4px solid rgb(241, 128, 72);\n    color: rgb(241, 128, 72);\n    font-weight: 700;\n    text-align: left;\n  }\n  \n  \n  span.text-3{\n    \n    font-size: 12px;\n    font-weight: 800;\n    top: 48%;\n    right: -5.5%;\n    border-bottom: 4px solid rgb(79, 149, 120);\n    color: rgb(79, 149, 120);\n    display: block;\n    text-align: right;\n    width: 177px;\n  }\n  \n  span.text-4{\n  \n   \n    right: 4%;\n    display: block;\n    width: 164px;\n    font-size: 12px;\n    bottom: 79.5%;\n    border-bottom: 4px solid rgb(247, 182, 69);\n    color: rgb(247, 182, 69);\n    font-weight: 700;\n    text-align: right;\n  }\n  \n  .cus-pos-1{\n    border-bottom: 4px solid rgb(241, 128, 72);\n    -webkit-transform: rotate(74deg);\n            transform: rotate(74deg);\n    top: 25%;\n    left: 35%;\n  }\n  .cus-pos-2{\n    border-bottom: 4px solid rgb(247, 182, 69);\n    -webkit-transform: rotate(107deg);\n            transform: rotate(107deg);\n    top: 25%;\n    left: 58%;\n}\n  .cus-stick {\n    width: 35px;\n    position: absolute;\n    display: block;\n  \n}\n}\n@media only screen and (max-width: 767px) and (min-width: 520px){\n  .email-font{\n    font-size:12px;\n  }\n  .score-div{\n    width:460px !important;\n  }\n  .tippingPoints-tm{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    text-align: center;\n    position: relative;\n    top: 11px;\n    left: 20%;\n  }\n  .tippingPoints-tb{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 70px;\n    left: 32%;\n    text-align: left;\n  }\n  .tippingPoints-tt{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 64px;\n    left: 18%;\n  }\n  \n  span.text-1{\n    font-size: 9px;\n    font-weight: 800;\n    top: 40%;\n    right: 65%;\n    border-bottom: 4px solid rgb(238, 90, 69);\n    color: rgb(238, 90, 69);\n    width: 165px;\n    text-align: left;\n  }\n  \n  span.text-2{\n    left: 9%;\n    display: block;\n    width: 154px;\n    font-size: 9px;\n    top: 4%;\n    border-bottom: 4px solid rgb(241, 128, 72);\n    color: rgb(241, 128, 72);\n    font-weight: 700;\n    text-align: left;\n\n  }\n  \n  \n  span.text-3{\n    font-size: 9px;\n    font-weight: 800;\n    top: 41%;\n    right: 3.5%;\n    border-bottom: 4px solid rgb(79, 149, 120);\n    color: rgb(79, 149, 120);\n    display: block;\n    text-align: right;\n    width: 135px;\n\n  }\n  \n  span.text-4{\n  \n    right: 11.7%;\n    display: block;\n    width: 139px;\n    font-size: 9px;\n    bottom: 84.5%;\n    border-bottom: 4px solid rgb(247, 182, 69);\n    color: rgb(247, 182, 69);\n    font-weight: 700;\n    text-align: right;\n\n  }\n  \n  .cus-pos-1{\n    border-bottom: 4px solid rgb(241, 128, 72);\n    -webkit-transform: rotate(70deg);\n            transform: rotate(70deg);\n    top: 21%;\n    left: 39%;\n  }\n  .cus-pos-2{\n    border-bottom: 4px solid rgb(247, 182, 69);\n    -webkit-transform: rotate(105deg);\n            transform: rotate(105deg);\n    top: 21%;\n    left: 53%;\n  }\n  .cus-stick {\n    width: 42px;\n    position: absolute;\n    display: block;\n  }\n  \n}\n@media only screen and (max-width: 519px) and (min-width: 371px){\n  .email-font{\n    font-size:12px;\n  }\n  .score-div{\n    width:239px !important;\n  }\n  .tippingPoints-tm{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    text-align: center;\n    position: relative;\n    top: 23px;\n    left: -7%;\n  }\n  .tippingPoints-tb{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 61px;\n    left: 81%;\n    text-align: left;\n  }\n  .tippingPoints-tt{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 82px;\n    left: -2%;\n  }\n  \n  span.text-1{\n    font-size: 9px;\n    font-weight: 800;\n    top: 40%;\n    right: 77%;\n    border-bottom: 4px solid rgb(238, 90, 69);\n    color: rgb(238, 90, 69);\n    width: 93px;\n    text-align: left;\n  }\n  \n  span.text-2{\n    left: -12%;\n    display: block;\n    width: 112px;\n    font-size: 9px;\n    top: 6%;\n    border-bottom: 4px solid rgb(241, 128, 72);\n    color: rgb(241, 128, 72);\n    font-weight: 700;\n    text-align: left;\n\n  }  \n  span.text-3{\n    font-size: 9px;\n    font-weight: 800;\n    top: 41%;\n    right: -17.5%;\n    border-bottom: 4px solid rgb(79, 149, 120);\n    color: rgb(79, 149, 120);\n    display: block;\n    text-align: right;\n    width: 82px;\n  }\n  \n  span.text-4{\n  \n    right: -11.3%;\n    display: block;\n    width: 99px;\n    font-size: 9px;\n    bottom: 78.5%;\n    border-bottom: 4px solid rgb(247, 182, 69);\n    color: rgb(247, 182, 69);\n    font-weight: 700;\n    text-align: right;\n\n  }\n  \n  .cus-pos-1{\n    border-bottom: 4px solid rgb(241, 128, 72);\n    -webkit-transform: rotate(70deg);\n            transform: rotate(70deg);\n    top: 27%;\n    left: 29%;\n  }\n  .cus-pos-2{\n    border-bottom: 4px solid rgb(247, 182, 69);\n    -webkit-transform: rotate(105deg);\n            transform: rotate(105deg);\n    top: 27%;\n    left: 59%;\n  }\n  .cus-stick {\n    width: 42px;\n    position: absolute;\n    display: block;\n  }\n  \n}\n@media only screen and (max-width: 371px) and (min-width: 320px){\n  .email-font{\n    font-size:12px;\n  }\n  .score-div{\n    width:239px !important;\n  }\n  .tippingPoints-tm{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    text-align: center;\n    position: relative;\n    top: 18px;\n    left: -7%;\n  }\n  .tippingPoints-tb{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 52px;\n    left: 71%;\n    text-align: left;\n  }\n  .tippingPoints-tt{\n    min-width: 94px;\n    height: 20px;\n    display: inline-block;\n    position: relative;\n    top: 70px;\n    left: 2%;\n  }\n  \n  span.text-1{\n    font-size: 8px;\n    font-weight: 800;\n    top: 32%;\n    right: 72%;\n    border-bottom: 4px solid rgb(238, 90, 69);\n    color: rgb(238, 90, 69);\n    width: 62px;\n    text-align: left;\n  }\n  \n  span.text-2{\n    left: 11%;\n    display: block;\n    width: 62px;\n    font-size: 9px;\n    top: 4%;\n    border-bottom: 4px solid rgb(241, 128, 72);\n    color: rgb(241, 128, 72);\n    font-weight: 700;\n    text-align: left;\n\n  }  \n  span.text-3{\n    font-size: 9px;\n    font-weight: 800;\n    top: 33%;\n    right: 0.5%;\n    border-bottom: 4px solid rgb(79, 149, 120);\n    color: rgb(79, 149, 120);\n    display: block;\n    text-align: right;\n    width: 51px;\n  }\n  \n  span.text-4{\n  \n    right: 10.7%;\n    display: block;\n    font-size:8px;\n    width: 58px;\n    bottom: 75.5%;\n    border-bottom: 4px solid rgb(247, 182, 69);\n    color: rgb(247, 182, 69);\n    font-weight: 700;\n    text-align: right\n  }\n  \n  .cus-pos-1{\n    border-bottom: 4px solid rgb(241, 128, 72);\n    -webkit-transform: rotate(70deg);\n    transform: rotate(70deg);\n    top: 27%;\n    left: 33%;\n  }\n  .cus-pos-2{\n    border-bottom: 4px solid rgb(247, 182, 69);\n    -webkit-transform: rotate(105deg);\n    transform: rotate(105deg);\n    top: 27%;\n    left: 59%;\n  }\n  .cus-stick {\n    width: 25px;\n    position: absolute;\n    display: block;\n  }\n  \n}\n/* background images */\n.bg-text {\n  color: #000000;\n  font-weight: bold;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  text-align: center;\n}\n.bg-text .dash-hide{\n  display: none;\n}\n.bg-common {\n  /* Add the blur effect */\n  filter: blur(7px);\n  -webkit-filter: blur(7px);\n\n  /* Center and scale the image nicely */\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.bg-income-image {\n  /* The image used */\n  background-image: url(\"/../assets/netincome.png\");\n  height: 286px; \n}\n.bg-score-image {\n  background-image: url(\"/../assets/scoregraph.png\");\n  height: 300px;\n}\n.bg-debt-image {\n  background-image: url(\"/../assets/debtgraph.png\");\n  height: 380px;\n}\n.bg-account-image {\n  background-image: url(\"/../assets/account-image.png\");\n  height: 180px;\n}\n.bg-credit-image {\n  background-image: url(\"/../assets/credit-image.png\");\n  height: 180px;\n}\n.bg-employer-image {\n  background-image: url(\"/../assets/employer-image.png\");\n  height: 129px;\n}\n.bg-total-income {\n  background-image: url(\"/../assets/total-income.png\");\n  height: 129px;\n}\n.bg-loan-income {\n  background-image: url(\"/../assets/loan-income.png\");\n  height: 179px;\n}\n.bg-property-image {\n  background-image: url(\"/../assets/property-image.png\");\n  height: 180px;\n}\n"

/***/ }),

/***/ "./src/app/profile/profile.component.html":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--New UI Code-->\n<div class=\"info-section\" [hidden]=\"!employerSectionDiv\">\n  <div class=\" row profile-image profile-section margin-view\">\n    <div class=\"col-12 col-sm-12 col-md-12 col-lg-4 image-border font-change spited-div box-flex\">\n      <div class=\"border-div div-padding div-width\">\n        <div class=\"row mt-2 text-center adjust-image flex-image\">\n          <div class=\"image-fixed-size col-md-12 text-center\">\n            <div class=\"ns-display-center\">\n              <img *ngIf=\"userAttributes.picture && ((userAttributes.picture).includes('http') || (userAttributes.picture.includes('data')))\"\n                class=\"user-image\" [src]=\"userAttributes.picture\" alt=\"\" />\n                <ngx-avatar size=\"125\" value=\"20%\"\n                *ngIf=\"!userAttributes.picture || !(userAttributes.picture.includes('http')) && !(userAttributes.picture.includes('data'))\"\n                name=\"{{userAttributes.first_name}}\"></ngx-avatar>\n            </div>\n            <div class=\"edit-profile mt-2 mb-2 cursor-pointer pencil-icon\"><i class=\"fa fa-pencil-square-o underline\"\n                aria-hidden=\"true\" (click)=\"openEditModal(ChangeProfileTemplate)\"></i></div>\n          </div>\n          <!-- <div class=\"edit-profile mt-2 mb-2 cursor-pointer\"><i class=\"fa fa-pencil-square-o underline\" aria-hidden=\"true\"\n              (click)=\"openEditModal(ChangeProfileTemplate)\"></i></div> -->\n        </div>\n        <div class=\" col-md-12 icon-margin\">\n          <div class=\"font-weight-bold mt-2 text-center email-font\">{{userAttributes.first_name}}\n            {{userAttributes.last_name}}\n          </div>\n          <div class=\"row ns-display-center-items\" *ngIf=\"userAttributes.dob\">\n            <i class=\"fa fa-birthday-cake col-md-1 col-sm-1 col-1 mb-2 font-text email-font\"></i>\n            <span class=\"col-md-9 col-sm-11 col-11 font-text\">{{userAttributes.dob | date: 'MM/dd/yyyy'}}</span>\n          </div>\n\n          <div class=\"row ns-display-center-items\">\n            <i class=\"fa fa-home col-md-1 col-sm-1 col-1 font-text\"></i>\n            <span class=\"col-md-9 col-sm-10 col-10 font-text email-font\" *ngIf=\"userAttributes.address\">\n              <span class=\"break-address\"*ngIf=\"userAttributes.address.street\"> {{userAttributes.address.street}},</span>\n              <span class=\"break-address\" *ngIf=\"userAttributes.address.street2\"> {{userAttributes.address.street2}},</span>\n              <span *ngIf=\"userAttributes.address.city\"> {{userAttributes.address.city}},</span>\n              <span *ngIf=\"userAttributes.address.state\"> {{userAttributes.address.state}}</span>\n              <span *ngIf=\"userAttributes.address.zip\"> {{userAttributes.address.zip}}</span>\n              <span *ngIf=\"userAttributes.address.country\"> {{userAttributes.address.country}}</span>\n            </span>\n          </div>\n          <div class=\"row ns-display-center-items\">\n            <i\n              class=\"fa fa-phone mr-3 col-md-1 col-sm-1 col-1 font-text email-font\"></i>{{userAttributes.phone_numbers | phoneNumber}}\n          </div>\n          <div class=\"row ns-display-center-items\">\n            <i class=\"fa fa-envelope col-md-1 col-sm-1 col-1 font-text\"></i>\n            <div class=\"col-md-9 col-sm-10 col-10 font-text email-font\">{{userAttributes.emails}}</div>\n          </div>\n        </div>\n      </div>\n\n    </div>\n    <!--  -->\n    <div class=\"col-12 col-sm-12 col-md-12 col-lg-8 font-change spited-div div-top div-flex\">\n      <div class=\"border-div padding-div nexus-width\">\n        <!-- <div *ngIf=\"loadingScoreGraph\">\n            <img src=\"../../assets/threeDotLoad.gif\" alt=\"\" height=\"60\">\n        </div> -->\n        \n        <div *ngIf=\"filteredDeposAcc.length > 0\">\n          <div class=\"text-center\">\n            <div class=\"score-size font-weight-bold font-change mt-1\"> {{\"PC_DIV_YOUR_NEXUS_SCORE\" | translate }}<sup\n                class=\"term\">TM</sup></div>\n          </div>\n          <div class=\"score-div\" id=\"ss\">\n            <span class=\"tippingPoints-tt\" *ngIf=\"graphTippingPoints.tt >=0\"><b>${{graphTippingPoints.tt}}/\n                {{\"PC_B_MONTH\" | translate}}</b></span>\n            <span class=\"tippingPoints-tm\" *ngIf=\"graphTippingPoints.tm >=0\"><b>${{graphTippingPoints.tm}}/\n                {{\"PC_B_MONTH\" | translate}}</b></span>\n            <span class=\"tippingPoints-tb\" *ngIf=\"graphTippingPoints.tb >=0\"><b>${{graphTippingPoints.tb}}/\n                {{\"PC_B_MONTH\" | translate}}</b></span>\n\n            <span\n              *ngIf=\"!((graphTippingPoints.tt >= 0) || (graphTippingPoints.tm >= 0) || (graphTippingPoints.tb >= 0))\">\n              <span class=\"tippingPoints-tt\"><b>$0/ {{\"PC_B_MONTH\" | translate}}</b></span>\n              <span class=\"tippingPoints-tm\"><b>$0/ {{\"PC_B_MONTH\" | translate}}</b></span>\n              <span class=\"tippingPoints-tb\"><b>$0/ {{\"PC_B_MONTH\" | translate}}</b></span>\n            </span>\n\n            <span class=\"position-absolute text-1\"> {{\"PC_SPAN_NOT_L_P\" | translate }}</span>\n            <span class=\"position-absolute text-2\">\n              {{\"PC_SPAN_MONTHLY_PAYMENT_MIGHT_BE_AT_MEDIUM_RISK\" | translate }}</span>\n            <span class=\"position-absolute text-3\"> {{\"PC_SPAN_VERY_LIKELY_TO_PAY_IN_FULL\" | translate }}</span>\n            <span class=\"position-absolute text-4\"> {{\"PC_SPAN_LIKELY_TO_PAY_IN_FULL\" | translate }}</span>\n            <span class=\"cus-stick cus-pos-1\"></span>\n            <span class=\"cus-stick cus-pos-2\"></span>\n\n\n            <ns-gauge-chart #nexusscore [canvasWidth]=\"canvasWidth\" [needleValue]=\"needleValue\" [options]=\"options\">\n            </ns-gauge-chart>\n            <div *ngIf=\"!((graphTippingPoints.tt >= 0) || (graphTippingPoints.tm >= 0) || (graphTippingPoints.tb >= 0))\"\n              class=\"text-center\">\n              <div class=\"pt-3 pb-3 p-2 pr-2\"> {{\"PC_DIV_NO_ASSOCIATED_INCOME\" | translate }}\n              </div>\n            </div>\n            <!-- <div class=\"text-center\"\n              *ngIf=\"(graphTippingPoints.tt == 0) || (graphTippingPoints.tm == 0) || (graphTippingPoints.tb == 0)\">\n              <div class=\"pt-3 pb-3 p-2 pr-2\"> {{\"PC_DIV_YOUR_TRENDING_INCOME_SHOW\" | translate }} <b>\n                  {{\"PC_B_YOU_ARE_NOT_ELIGIBLE_FOR_LOAN\" | translate }} </b>\n              </div>\n            </div> -->\n\n          </div>\n          <div class=\"row div-height\"></div>\n        </div>\n\n        <div *ngIf=\"filteredDeposAcc.length == 0\">\n          <div class=\"bg-score-image bg-common\"></div>\n          <div class=\"bg-text\">\n            <p class=\"report-hide\">Link your account to view the widget</p>\n            <p class=\"dash-hide\">User does not have any active account</p>\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add\n              Account</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row mt-2 margin-view profile-font\">\n    <div class=\"col-sm-12 col-md-12 col-lg-4 mb-2 padding-zero remove-padding\">\n      <div class=\"profile-image emp-height gray-bg-color\">\n        <div class=\"p-2 employer-details font-weight-bold\">\n          {{\"PC_DIV_EMPLOYER\" | translate }}\n          <a (click)=\"employerSection()\"><i\n              class=\"fa fa-edit text-light cursor-pointer mt-1 source-income float-right\"></i></a>\n        </div>\n        <div class=\"row\" *ngIf=\"userAttributes.employers.length == 0\">\n          <div class=\"col-12\">\n            <div class=\"bg-employer-image bg-common\"></div>\n            <div class=\"bg-text\">\n              <p class=\"report-hide\">Link your account to view the widget</p>\n              <p class=\"dash-hide\">User does not have any active account</p>\n              <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add\n                Account</button>\n            </div>\n          </div>\n        </div>\n\n        <div *ngIf=\"userAttributes.employers.length > 0\">\n          <div *ngIf=\"!(userAttributes.employers) || (userAttributes.employers.length == 0)\"\n            class=\"pt-3 pb-3 p-2 pr-2 box set-height\">\n            {{\"PC_DIV_NO_EMPLOYER_PRESENT\" | translate }}\n          </div>\n          <div *ngIf=\"userAttributes.employers\">\n            <div class=\"text-center\">\n              <div *ngFor=\"let emp of userAttributes.employers\">\n                <div class=\"mt-3\" *ngIf=\"(emp.preferred && emp.years) || (emp.preferred && emp.months)\">\n                  <span *ngIf=\"emp.years\">{{emp.years}} {{\"PC_DIV_YEARS_AND\" | translate }} </span> {{emp.months}}\n                  {{\"PC_DIV_MONTH_AT\" | translate }}:\n                </div>\n                <div class=\"nexus-score font-weight-bold mt-2\" *ngIf=\"emp.preferred\"\n                  [ngClass]=\"emp.employer_name.lenght > 19 ? '' : 'class-1'\">\n                  {{emp.employer_name}}\n                </div>\n              </div>\n\n              <div class=\"mt-1 mb-2\" *ngIf=\"userAttributes.employers > 1 || (otherEmployersData && otherEmployersData.income > 0)\">\n                {{PC_DIV_AND | translate }}\n                {{(userAttributes.employers.length - 1) + (otherEmployersData?1:0)}} {{\"PC_SPAN_OTHER\" | translate }}\n                <span class=\"source-income cursor-pointer\"\n                  (click)=\"employerSection()\"><u><span>{{\"PC_SPAN_SOURCE\" | translate}}</span>\n                    {{\"PC_U_OF_INCOME\" | translate}}</u></span></div>\n            </div>\n            <div *ngIf=\"userAttributes.showNoIncomeFound && (userAttributes.employers.length === 0)\"\n              class=\"pt-3 pb-3 p-2 pr-2 box\">\n              {{\"PC_DIV_NO_EMPLOYER_FOUND\" | translate }}\n            </div>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"profile-image mt-2 gray-bg-color emp-height\">\n        <div class=\"p-2 employer-details font-weight-bold\">\n          {{\"PC_DIV_ANNUAL_INCOME\" | translate }}\n        </div>\n        <div class=\"row\" *ngIf=\"filteredDeposAcc.length == 0\">\n          <div class=\"col-12\">\n            <div class=\"bg-total-income bg-common\"></div>\n            <div class=\"bg-text\">\n              <p class=\"report-hide\">Link your account to view the widget</p>\n              <p class=\"dash-hide\">User does not have any active account</p>\n              <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add\n                Account</button>\n            </div>\n          </div>\n        </div>\n\n        <div *ngIf=\"filteredDeposAcc.length > 0\">\n          <div *ngIf=\"employersData.totalTransArr && employersData.totalTransArr.length > 0\"\n            class=\"text-center\">\n            <div class=\"nexus-score font-weight-bold mt-3\"><i\n                class=\"mr-2\"></i>{{employersData.totalTransArr| sum:'amount':true | currency:'$':'symbol':'1.0-0'}}</div>\n            <div class=\"mt-1\">{{\"PC_DIV_SEE_INCOME\" | translate}}<span class=\"source-income cursor-pointer\"\n                (click)=\"transactionHandler(employersData.totalTransArr)\"><u>\n                  {{\"PC_U_TRANSACTIONS\" | translate }}</u></span></div>\n          </div>\n          <div *ngIf=\"!employersData.totalTransArr || employersData.totalTransArr.length === 0\"\n            class=\"set-height\">\n            <div class=\"pt-3 pb-3 p-2 pr-2 box\"> {{\"PC_DIV_NO_INCOME_SOURCE_FOUND\" | translate }}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"col-sm-12 col-md-12 col-lg-8 padding-zero-none income-left\">\n      <div class=\"profile-image gray-bg-color income-height\">\n        <div class=\"p-2 employer-details font-weight-bold\">\n          {{\"PC_DIV_NET_INCOME\" | translate }}\n        </div>\n        <div *ngIf=\"filteredDeposAcc.length > 0 || filteredCreditAcc.length > 0\">\n          <div *ngIf=\"graphs.net_income_over_time.load\">\n            <canvas baseChart height=\"309\" [datasets]=\"graphs.net_income_over_time.data\" class=\"report-bar-graph\"\n              [labels]=\"graphs.net_income_over_time.labels\" [options]=\"graphs.net_income_over_time.barOptions\"\n              [chartType]=\"graphs.net_income_over_time.type\" [colors]=\"graphs.net_income_over_time.colors\"\n              (chartClick)=\"onChartClick($event)\"></canvas>\n          </div>\n          <div class=\"text-center p-2\" *ngIf=\"!(graphs.net_income_over_time.load)\">\n            {{\"PC_DIV_ALL_ACCOUNTS_ARE_INACTIVE\" | translate}}\n          </div>\n        </div>\n\n        <div *ngIf=\"filteredDeposAcc.length == 0 && filteredCreditAcc.length == 0\">\n          <div class=\"bg-income-image bg-common\"></div>\n          <div class=\"bg-text\">\n            <p class=\"report-hide\">Link your account to view the widget</p>\n            <p class=\"dash-hide\">User does not have any active account</p>\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add\n              Account</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row mt-2 margin-view profile-font\">\n    <div class=\"col-sm-12 col-md-12 col-lg-6 padding-zero box-align remove-padding\">\n      <div class=\"profile-image gray-bg-color box-width\">\n        <div class=\"p-2 employer-details font-weight-bold\"> {{\"PC_DIV_CURRENT_DEBT_DISTRIBUTION\" | translate }} </div>\n        <div *ngIf=\"graphs.current_debt_distribution.load\">\n          <canvas baseChart height=\"413\" [datasets]=\"graphs.current_debt_distribution.data\"\n            [labels]=\"graphs.current_debt_distribution.labels\" [options]=\"graphs.current_debt_distribution.barOptions\"\n            [chartType]=\"graphs.current_debt_distribution.type\"\n            [colors]=\"graphs.current_debt_distribution.colors\"></canvas>\n        </div>\n\n        <div *ngIf=\"!(graphs.current_debt_distribution.load)\">\n          <div class=\"bg-debt-image bg-common\"></div>\n          <div class=\"bg-text\">\n            <p class=\"report-hide\">Link your account to view the widget</p>\n            <p class=\"dash-hide\">User does not have any active account</p>\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add\n              Account</button>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-sm-12 col-md-12 col-lg-6 padding-zero-none credit-div\">\n      <div class=\"profile-image gray-bg-color\">\n        <div class=\"row p-2 font-weight-bold margin-inspect gray-bg-color employer-details div-top\">\n          <div class=\"col-4 font-change\"> {{ \"PC_DIV_CREDIT_CARDS\" | translate}} </div>\n          <div class=\"col-5 \">\n            <div class=\"font-change text-nowrap center\"> {{totalSum.creditSum | currency}} </div>\n          </div>\n          <div class=\"col-3\">\n            <div class=\"text-left font-change\"> {{totalSum.limitSum | currency}}</div>\n          </div>\n        </div>\n        <div *ngIf=\"filteredCreditAcc.length > 0\">\n          <div class=\"row p-2 font-weight-bold margin-inspect gray-bg-color div-top\">\n            <div class=\"col-4 details-employee font-change\"> {{ \"PC_DIV_CREDIT_CARDS\" | translate}} </div>\n            <div class=\"col-4 details-employee\">\n              <div class=\"text-right font-change text-nowrap\"> {{\"PC_DIV_CURRENT_BALANCE\" | translate }} </div>\n            </div>\n            <div class=\"col-4 details-employee\">\n              <div class=\"text-right font-change\"> {{\"PC_DIV_CREDIT_LIMIT\" | translate }} </div>\n            </div>\n          </div>\n          <div class=\"control-scroll report-credit\">\n            <div class=\"row p-2 gray-bg-color text-center pt-2 pb-2 d-block\"\n              *ngIf=\"filteredCreditAcc && filteredCreditAcc.length == 0\"> {{\"PC_DIV_NO_CREDIT_CARD_ADDED\" | translate }}\n            </div>\n            <div class=\"row p-2 gray-bg-color bottom-line\" *ngFor=\"let credit of filteredCreditAcc\">\n              <div class=\"col-4 text-nowrap\" *ngIf=\"credit.official_name\" placement=\"right\"\n                popover=\"{{credit.official_name}}\" triggers=\"mouseenter:mouseleave\">\n                {{credit.official_name ? (credit.official_name.length > 15 ? credit.official_name.substring(0, 14)+\"...\" : credit.official_name) : \"\"}}\n              </div>\n              <div class=\"col-4 text-right\">\n                {{credit.balances.current | currency }}\n              </div>\n              <div class=\"col-4 text-right\">\n                {{credit.balances.limit | currency}}\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"row\" *ngIf=\"filteredCreditAcc.length == 0\">\n          <div class=\"col-12\">\n            <div class=\"bg-credit-image bg-common\"></div>\n            <div class=\"bg-text\">\n              <p class=\"report-hide\">Link your account to view the widget</p>\n              <p class=\"dash-hide\">User does not have any active account</p>\n              <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add\n                Account</button>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"profile-image mt-2 gray-bg-color report-loans\">\n        <div class=\"row p-2 font-weight-bold margin-inspect gray-bg-color employer-details\">\n          <div class=\"col-5\"> {{\"PC_DIV_ACTIVE_LOANS\" | translate }} </div>\n          <div class=\"col-7\">\n            <div class=\"float-right\"> {{totalSum.loanSum | currency}}</div>\n          </div>\n        </div>\n        <div *ngIf=\"filteredLoanAcc.length > 0\">\n          <div class=\"row p-2 font-weight-bold margin-inspect gray-bg-color\">\n            <div class=\"col-5 details-employee\"> {{\"PC_DIV_INSTITUTIONS\" | translate }} </div>\n            <div class=\"col-7 details-employee\">\n              <div class=\"float-right\"> {{\"PC_DIV_CURRENT_BALANCE\" | translate }}</div>\n            </div>\n          </div>\n          <div class=\"control-scroll\">\n            <div class=\"row p-2 gray-bg-color text-center pt-2 pb-2 d-block\"\n              *ngIf=\"filteredLoanAcc && filteredLoanAcc.length == 0\"> {{\"PC_DIV_ACC_NOT_ADDED\" | translate }} </div>\n            <div class=\"row p-2 gray-bg-color bottom-line\" *ngFor=\"let loan of filteredLoanAcc\">\n\n              <div class=\"col-6\" *ngIf=\"loan.official_name\" placement=\"right\" popover=\"{{loan.official_name}}\"\n                triggers=\"mouseenter:mouseleave\">\n                {{loan.official_name ? (loan.official_name.length > 20 ? loan.official_name.substring(0, 13)+\"...\" : loan.official_name) : \"\"}}\n              </div>\n              <div class=\"col-6 float-value\">\n                {{loan.balances.current > 0 ? ((loan.balances.current) | currency ) : (loan.balances.current * -1) | currency}}\n              </div>\n              <div class=\"col-6 float-value\">\n                {{loan.balances.limit | currency}}\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"row\" *ngIf=\"filteredLoanAcc.length == 0\">\n          <div class=\"col-12\">\n            <div class=\"bg-loan-income bg-common\"></div>\n            <div class=\"bg-text\">\n              <p class=\"report-hide\">Link your account to view the widget</p>\n              <p class=\"dash-hide\">User does not have any active account</p>\n              <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add\n                Account</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row mt-2 margin-view profile-font\">\n    <div class=\"col-sm-12 col-md-12 col-lg-6 padding-zero remove-padding\">\n      <div class=\"profile-image gray-bg-color property-height\">\n        <div class=\"row p-2 font-weight-bold margin-inspect gray-bg-color employer-details\">\n          <div class=\"col-5\"> {{\"PC_DIV_BANK_ACCOUNTS\" | translate }}</div>\n          <div class=\"col-7\">\n            <div class=\"float-right\">{{totalSum.depoSum | currency}} </div>\n          </div>\n        </div>\n        <div *ngIf=\"filteredDeposAcc.length > 0\">\n          <div class=\"row p-2 font-weight-bold margin-inspect gray-bg-color\">\n            <div class=\"col-5 details-employee\"> {{\"PC_DIV_ACCOUNTS\" | translate }}</div>\n            <div class=\"col-7 details-employee\">\n              <div class=\"float-right\"> {{\"PC_DIV_CURRENT_BALANCE\" | translate }}</div>\n            </div>\n          </div>\n          <div class=\"control-scroll\">\n            <div class=\"row p-2 gray-bg-color text-center pt-2 pb-2 d-block\"\n              *ngIf=\"filteredDeposAcc && filteredDeposAcc.length  == 0\">\n              {{\"PC_DIV_NO_BANK_ACCOUNTS_ADDED\" | translate }}\n            </div>\n            <div class=\"row p-2 gray-bg-color bottom-line\" *ngFor=\"let depos of filteredDeposAcc\">\n              <div style=\"width:128px\" class=\"col-6\" *ngIf=\"depos.official_name\" placement=\"right\"\n                popover=\"{{depos.official_name}}\" triggers=\"mouseenter:mouseleave\">\n                <div class=\"mw-100 text-nowrap\" *ngIf=\"!depos.matchOfficialName\">\n                  {{depos.official_name ? (depos.official_name.length > 20 ? depos.official_name.substring(0, 20)+\"...\" : depos.official_name) : \"updating soon...\"}}\n                </div>\n\n                <div class=\"mw-100 text-nowrap\" *ngIf=\"depos.matchOfficialName\">\n                  {{depos.name ? (depos.name.length > 20 ? depos.name.substring(0, 20)+\"...\" : depos.name) : \"\"}}\n                </div>\n              </div>\n              <div class=\"col-6 pr-0\">\n                <div>\n                  <span class=\"float-right\"><i\n                      class=\"cursor-pointer mr-2\"></i>{{depos.balances.current | currency}}</span>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"row\" *ngIf=\"filteredDeposAcc.length == 0\">\n          <div class=\"col-12\">\n            <div class=\"bg-account-image bg-common\"></div>\n            <div class=\"bg-text\">\n              <p class=\"report-hide\">Link your account to view the widget</p>\n              <p class=\"dash-hide\">User does not have any active account</p>\n              <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add\n                Account</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"col-sm-12 col-md-12 col-lg-6 padding-zero-none property-left box-align div-top\">\n      <div class=\"profile-image gray-bg-color property-height box-width\">\n        <div class=\"row p-2 font-weight-bold margin-inspect gray-bg-color employer-details\">\n          <div class=\"col-7\"> {{\"PC_DIV_PROPERTY\" | translate }}</div>\n          <div class=\"col-5\">\n            <div class=\"float-right\"> {{totalSum.propertSum | currency}}</div>\n          </div>\n        </div>\n        <div *ngIf=\"allProperty.length > 0\">\n          <div class=\"row p-2 font-weight-bold margin-inspect gray-bg-color\">\n            <div class=\"col-7 details-employee\"> {{\"PC_DIV_PROPERTY\" | translate }}</div>\n            <div class=\"col-5 details-employee\">\n              <div class=\"float-right\"> {{\"PC_DIV_VALUE\" | translate }}</div>\n            </div>\n          </div>\n          <div class=\"control-scroll\">\n            <div class=\"row p-2 gray-bg-color text-center pt-2 pb-2 d-block\"\n              *ngIf=\"allProperty && allProperty.length == 0\">\n              {{\"PC_DIV_NO_PROPERTIES_ADDED\" | translate }}</div>\n            <div class=\"row p-2 gray-bg-color bottom-line\" *ngFor=\"let item of allProperty\">\n              <div class=\"col-7\" *ngIf=\"item.name\" placement=\"right\" popover=\"{{item.name}} {{item.type}}\"\n                triggers=\"mouseenter:mouseleave\">\n                <div class=\"spacewrap\">\n                  {{item.name}}-{{item.type ? (item.type.length > 10 ? item.type.substring(0, 8)+\"...\" : item.type) : \"\"}}\n                </div>\n              </div>\n                <div class=\"col-5\">\n                  <div><span class=\"float-right\"><i\n                        class=\"cursor-pointer mr-2\"></i>{{item.estimated_value | currency}}</span>\n                  </div>\n                </div>\n              <!-- </div> -->\n            </div>\n          </div>\n        </div>\n\n        <div class=\"row\" *ngIf=\"allProperty.length == 0\">\n          <div class=\"col-12\">\n            <div class=\"bg-property-image bg-common\"></div>\n            <div class=\"bg-text\">\n              <p class=\"report-hide\">Link your account to view the widget</p>\n              <p class=\"dash-hide\">User does not have any property</p>\n              <button type=\"button\" class=\"btn btn-secondary\" (click)=\"propertyHandler('callingAddProperty')\">Add\n                Property</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!-- Employer Section -->\n<div class=\"employer-section\" [hidden]=\"employerSectionDiv\">\n  <div class=\"border border-dark\">\n    <div class=\"font-weight-bold pt-3 pb-3 pl-3 employer-income\"> {{\"PC_DIV_EMPLOYER_AND_INCOME\" | translate }}\n      <button #employerBackButton class=\"float-right cursor-pointer font-weight-bold mr-3 back-btn\"\n        (click)=\"profileSection()\">{{\"PC_BUTTON_BACK\" | translate}}</button>\n    </div>\n    <div class=\"pt-3 pb-3 pl-3\"> {{\"PC_DIV_INCOME_DURING\" | translate }}</div>\n\n    <div class=\"table-responsive\">\n      <table class=\"table employer-table\">\n        <thead class=\"employer-name\">\n          <tr>\n            <th> {{\"PC_TH_EMPLOYER_NAME\" | translate }}</th>\n            <th> {{\"PC_TH_PRIMARY\" | translate }}</th>\n            <th> {{\"PC_TH_PRESENT_EMPLOYER\" | translate }}</th>\n            <th> {{\"PC_TH_EMPLOYMENT_STARTS\" | translate }}</th>\n            <th> {{\"PC_TH_EMPLOYMENT_ENDS\" | translate }}</th>\n            <th> {{\"PC_TH_INCOME\" | translate }}</th>\n            <th> {{\"PC_TH_TRANSACTIONS\" | translate }}</th>\n          </tr>\n        </thead>\n        <tbody *ngFor=\"let empDetails of userAttributes.employers; let i = index\">\n          <tr class=\"align-value\">\n            <td class=\"font-weight-bold\">{{empDetails.employer_name}}</td>\n            <td>\n              <label class=\"container custom-radio-btn\">\n                <input type=\"checkbox\" [(ngModel)]=\"empDetails.preferred\"\n                  (change)=\"setRadioBtnValue(empDetails.preferred, i)\">\n                <span class=\"checkmark-radio-btn\"\n                  [ngClass]=\"empDetails.preferred ? 'make-it-shown' : 'make-it-hidden'\"></span>\n              </label>\n            </td>\n            <td>\n              <div class=\"pl-3\">\n                <label class=\"container custom-mark\" [ngClass]=\"empDetails.present ? '' : 'make-mark'\">\n                  <input type=\"checkbox\" [(ngModel)]=\"empDetails.present\">\n                  <span class=\"checkmark\" [ngClass]=\"empDetails.present ? 'make-it-shown' : 'make-it-hidden'\"></span>\n                </label>\n              </div>\n            </td>\n            <td>\n              <input type=\"text\" name=\"fromDate{{i+1}}\" id=\"fromDate{{i+1}}\" placeholder=\"{{'PC_TD_FROM' | translate}}\"\n                [(ngModel)]=\"empDetails.from_duration_in_date\" class=\"form-control input-color\" placement=\"bottom\"\n                #dp=\"bsDatepicker\" bsDatepicker [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                [ngClass]=\"empDetails.from_duration_in_date ? 'bdr-transaparent' : 'not-visible'\">\n            </td>\n            <td>\n              <span class=\"font-weight-bold\" *ngIf=\"empDetails.present\"> {{\"PC_SPAN_PRESENT\" | translate }} </span>\n              <span [ngClass]=\"{'make-it-hidden' : !empDetails.to_duration_in_date}\">\n                <input *ngIf=\"!(empDetails.present)\" type=\"text\" name=\"toDate{{i+1}}\" id=\"toDate{{i+1}}\"\n                  placeholder=\"{{'PC_SPAN_TO' | translate}}\" [(ngModel)]=\"empDetails.to_duration_in_date\"\n                  class=\"form-control input-color\" placement=\"bottom\" #dp=\"bsDatepicker\" bsDatepicker\n                  [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\"\n                  [ngClass]=\"{'bdr-transaparent' : !(empDetails.present)}\">\n              </span>\n            </td>\n            <td class=\"font-weight-bold v-align\"><i class=\"mr-2\"></i>{{empDetails.income | currency}}</td>\n            <td class=\"cursor-pointer see-transection no-break v-align\" *ngIf=\"empDetails.seeTransaction && empDetails.seeTransaction.length>0\"\n              (click)=\"transactionHandler(empDetails.seeTransaction)\"><u> {{\"PC_TD_SEE_TRANSACTIONS\" | translate }}</u>\n            </td>\n          </tr>\n          <tr [ngClass]=\"{'make-it-hidden' : !empDetails.address}\">\n            <td colspan=\"7\" class=\"bt0 font-weight-bold\">\n              <button\n                *ngIf=\"!(empDetails.address || empDetails.contact_person || empDetails.title || empDetails.email || empDetails.phone_number)\"\n                (click)=\"addAddressModal(addressModalTemplate, empDetails, i)\"\n                class=\"add-address no-break cursor-pointer\">\n                <i class=\"fa fa-plus mr-2\"></i> {{\"PC_I_ADD_ADDRESS\" | translate }}\n              </button>\n              <span *ngIf=\"empDetails.contact_person\">\n                {{empDetails.contact_person}},\n              </span>\n              <span *ngIf=\"empDetails.title\">\n                {{empDetails.title}};\n              </span>\n              <span *ngIf=\"empDetails.email\">\n                {{empDetails.email}};\n              </span>\n              <span *ngIf=\"empDetails.phone_number\">\n                +{{empDetails.phone_number | phoneNumber}}\n              </span> <br />\n              <span *ngIf=\"empDetails.address\">\n                <span *ngIf=\"empDetails.address.street1\">{{empDetails.address.street1}},</span>\n                <span *ngIf=\"empDetails.address.street2\"> {{empDetails.address.street2}},</span>\n                <span *ngIf=\"empDetails.address.city\"> {{empDetails.address.city}},</span>\n                <span *ngIf=\"empDetails.address.state\"> {{empDetails.address.state}}</span>\n                <span *ngIf=\"empDetails.address.zipCode\"> {{empDetails.address.zipCode}}</span>\n                <span *ngIf=\"empDetails.address.country\"> {{empDetails.address.country}}</span>\n              </span>\n              &#x000A0;&#x000A0;\n              <button\n                *ngIf=\"empDetails.contact_person || empDetails.title || empDetails.email || empDetails.phone_number || empDetails.address\"\n                (click)=\"addAddressModal(addressModalTemplate, empDetails, i)\"\n                class=\"btn btn-success cursor-pointer pl-2 pr-2 pt-0 pb-0 make-it-hidden\">\n                {{\"PC_BUTTON_EDIT\" | translate }}</button>\n            </td>\n          </tr>\n        </tbody>\n        <tr *ngIf=\"otherEmployersData.income && otherEmployersData.seeTransaction.length > 0\">\n          <td colspan=\"2\" class=\"font-weight-bold\">{{otherEmployersData.employer_name}}</td>\n          <td></td>\n          <td></td>\n          <td></td>\n          <td class=\"font-weight-bold v-align\"><i class=\"mr-2\"></i>{{otherEmployersData.income | currency}}</td>\n          <td class=\"cursor-pointer see-transection no-break v-align\"\n            (click)=\"transactionHandler(otherEmployersData.seeTransaction)\"><u>\n              {{\"PC_TD_SEE_TRANSACTIONS\" | translate }}</u></td>\n        </tr>\n      </table>\n    </div>\n    <div class=\"text-center make-it-hidden\">\n      <button class=\"cursor-pointer font-weight-bold mr-3 back-btn bg-success text-white\"\n        (click)=\"setEmployerDetails()\"> {{\"PC_BUTTON_SAVE\" | translate}}</button>\n    </div>\n    <div class=\"mt-5 mb-3 ml-3 make-it-hidden\">\n      <div> {{\"PC_DIV_WARINING_YOUR_EMPLOYER_MSG\" | translate }}</div>\n    </div>\n  </div>\n</div>\n\n<!--Start Edit modal-->\n<ng-template #ChangeProfileTemplate>\n  <app-edit-profile-modal (childEvent)=\"editProfileModalClose($event)\"></app-edit-profile-modal>\n</ng-template>\n<!--End Edit modal-->\n\n<!--Start Edit modal-->\n<ng-template #addressModalTemplate>\n  <app-add-address-modal [employerDetail]=\"employerDetail\" (closeAddressmodal)=\"addressModalClose($event)\">\n  </app-add-address-modal>\n</ng-template>\n<!--End Edit modal-->"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var chart_piecelabel_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! chart.piecelabel.js */ "./node_modules/chart.piecelabel.js/src/Chart.PieceLabel.js");
/* harmony import */ var chart_piecelabel_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(chart_piecelabel_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../transactions-modal/transactions-modal.component */ "./src/app/transactions-modal/transactions-modal.component.ts");
/* harmony import */ var _common_currencyformatter_pipe__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../common/currencyformatter.pipe */ "./src/app/common/currencyformatter.pipe.ts");
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! html2canvas */ "./node_modules/html2canvas/dist/html2canvas.js");
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../common/utils */ "./src/app/common/utils.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __read = (undefined && undefined.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (undefined && undefined.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};














var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(authService, backend, session, spinnerService, modalService, notifierService, translate, currency) {
        this.authService = authService;
        this.backend = backend;
        this.session = session;
        this.spinnerService = spinnerService;
        this.modalService = modalService;
        this.notifierService = notifierService;
        this.translate = translate;
        this.currency = currency;
        this.employerSectionDiv = true;
        this.properties = [];
        this.report = null;
        this.headerEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.addAccountHandlerEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.propertyHandlerEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.DATE_FORMAT = 'YYYY-MM-DD';
        this.filteredCreditAcc = [];
        this.filteredDeposAcc = [];
        this.filteredLoanAcc = [];
        this.employersData = { totalTransArr: [] };
        this.totalSum = { creditSum: 0, limitSum: 0, depoSum: 0, propertSum: 0, loanSum: 0 };
        this.otherEmployersData = {};
        this.employerDetail = {};
        this.filteredAccForBlur = [];
        this.loadingScoreGraph = false;
        // for score graph
        this.canvasWidth = 350;
        this.needleValue = 0;
        this.options = {
            hasNeedle: false,
            arcColors: ["rgb(238,90,69)", "rgb(241,128,72)", "rgb(247,182,69)", "rgb(79,149,120)"],
            arcDelimiters: [25, 50, 75],
            rangeLabel: []
        };
        this.graphTippingPoints = {};
        this.notifier = notifierService;
        var G_NO_DEBTS;
        var G_NO_DATA;
        translate.get('G_NO_DEBTS').subscribe(function (value) { return G_NO_DEBTS = value; });
        translate.get('G_NO_DATA').subscribe(function (value) { return G_NO_DATA = value; });
        var G_TRANSACTIONS;
        translate.get('G_TRANSACTIONS').subscribe(function (value) { return G_TRANSACTIONS = value; });
        this.graphs = {
            "net_income_over_time": {
                name: 'Net Income Over Time',
                load: false,
                barOptions: {
                    maintainAspectRatio: false,
                    scaleShowHorizontalLines: true,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 30 //set that fits the best
                        }
                    },
                    scales: {
                        xAxes: [{
                                stacked: true
                            }],
                        yAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true,
                                    autoSkip: false,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) < 0 || parseInt(value) >= 0) {
                                            return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                        }
                                    }
                                }
                            }]
                    },
                    tooltips: {
                        titleFontSize: 16,
                        bodyFontSize: 14,
                        bodySpacing: 4,
                        xPadding: 16,
                        yPadding: 4,
                        xAlign: "center",
                        yAlign: "top",
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            if (('dataPoints' in tooltip && tooltip.dataPoints && tooltip.dataPoints[0].datasetIndex)) {
                                tooltip.opacity = 1;
                            }
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            beforeTitle: function (tooltipItem, data) {
                                if (data.datasets[tooltipItem[0].datasetIndex].label) {
                                    var title = data.datasets[tooltipItem[0].datasetIndex].label;
                                    return title;
                                }
                            },
                            title: function () { return null; },
                            beforeLabel: function (tooltipItem, data) {
                                var modifiedLabels = data.labels[tooltipItem.index];
                                return modifiedLabels;
                            },
                            label: function (tooltipItem, data) {
                                if (tooltipItem && tooltipItem.datasetIndex >= 0) {
                                    return '$' + tooltipItem.yLabel.toLocaleString();
                                }
                                return '';
                            },
                            footer: function (tooltipItem, data) {
                                var index = tooltipItem[0].index;
                                if (data.datasets[tooltipItem[0].datasetIndex].transactions[index] && data.datasets[tooltipItem[0].datasetIndex].transactions[index].length > 0) {
                                    var t = data.datasets[tooltipItem[0].datasetIndex].transactions[index].length;
                                    return t + ("" + G_TRANSACTIONS);
                                }
                            }
                        }
                    },
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length)
                                e.target.style.cursor = 'pointer';
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                },
                colors: [
                    {
                        backgroundColor: '#717171'
                    },
                    {
                        backgroundColor: 'rgb(0, 194, 86)'
                    },
                    {
                        backgroundColor: 'rgb(255, 107, 113)'
                    }
                ],
                labels: [],
                type: 'bar',
                data: []
            },
            "current_debt_distribution": {
                name: 'Spending By Catagory',
                load: false,
                barOptions: {
                    maintainAspectRatio: false,
                    scaleShowHorizontalLines: true,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 30 //set that fits the best
                        }
                    },
                    tooltips: {
                        bodyFontSize: 18,
                        bodySpacing: 4,
                        xPadding: 12,
                        yPadding: 14,
                        xAlign: "center",
                        yAlign: 'top',
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            if (('dataPoints' in tooltip && tooltip.dataPoints && tooltip.dataPoints[0].datasetIndex)) {
                                tooltip.opacity = 1;
                            }
                            tooltip.displayColors = false;
                            return tooltip;
                        },
                        callbacks: {
                            beforeLabel: function (tooltipItem, data) {
                                var modifiedLabels = data.labels[tooltipItem.index].split('deleting')[0];
                                return modifiedLabels;
                            },
                            label: function (tooltipItem, data) {
                                return '$' + data.datasets[0].data[tooltipItem.index].toLocaleString();
                            }
                        }
                    },
                    cutoutPercentage: 60,
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length) {
                                e.target.style.cursor = 'default';
                            }
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                    legend: {
                        display: false,
                        labels: {
                            fontColor: '#000',
                            fontSize: 12
                        },
                        position: 'bottom'
                    },
                    pieceLabel: {
                        segment: true,
                        position: 'outside',
                        overlap: false,
                        fontColor: '#000',
                        fontSize: 12,
                        textMargin: 5,
                        outsidePadding: 10,
                        render: function (args) {
                            if (args.label) {
                                var modifiedLabel = args.label.split('deleting')[0];
                                var cuttedLabel = modifiedLabel ? (modifiedLabel.length > 12 ? modifiedLabel.substring(0, 10) + "..." : modifiedLabel) : "";
                                return cuttedLabel;
                            }
                        }
                    },
                    elements: {
                        center: {
                            text: '$',
                            color: '#36A2EB',
                            fontStyle: 'Helvetica',
                            sidePadding: 15 //Default 20 (as a percentage)
                        }
                    }
                },
                labels: [],
                type: 'pie',
                data: [],
            }
        };
        Chart.Tooltip.positioners.cursor = function (chartElements, coordinates) {
            return coordinates;
        };
        // for chart purpose
        Chart.pluginService.register({
            beforeDatasetsUpdate: function (chart) {
                // console.log('Inside after update');
                if (chart.data.datasets.length > 0) {
                    var nodata = true;
                    try {
                        for (var _a = __values(chart.data.datasets), _b = _a.next(); !_b.done; _b = _a.next()) {
                            var dataset = _b.value;
                            if ('data' in dataset && dataset.data) {
                                try {
                                    for (var _c = __values(dataset.data), _d = _c.next(); !_d.done; _d = _c.next()) {
                                        var d = _d.value;
                                        if (d != 0) {
                                            nodata = false;
                                        }
                                    }
                                }
                                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                                finally {
                                    try {
                                        if (_d && !_d.done && (_e = _c.return)) _e.call(_c);
                                    }
                                    finally { if (e_1) throw e_1.error; }
                                }
                            }
                        }
                    }
                    catch (e_2_1) { e_2 = { error: e_2_1 }; }
                    finally {
                        try {
                            if (_b && !_b.done && (_f = _a.return)) _f.call(_a);
                        }
                        finally { if (e_2) throw e_2.error; }
                    }
                    if (nodata) {
                        chart.data.datasets = [];
                    }
                }
                var e_2, _f, e_1, _e;
            },
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                    //console.log('chart.data.datasets',chart.data.datasets);
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    var total = 0;
                    if (chart.data.datasets && chart.data.datasets.length > 0 && 'data' in chart.data.datasets[0]) {
                        total = chart.data.datasets[0].data.reduce(function (a, b) { return (a + b); });
                    }
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = '$' + ((total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    var color = '#000';
                    var sidePadding = centerConfig.sidePadding || 20;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2);
                    //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;
                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;
                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);
                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);
                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    (total == 0) ? (fontSizeToUse = 15) : fontSizeToUse;
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;
                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            },
            afterDraw: function (chart) {
                if (chart.data.datasets.length == 0) {
                    // No data is present
                    chart.data.datasets = [];
                    var ctx = chart.chart.ctx;
                    var width = chart.chart.width;
                    var height = chart.chart.height;
                    chart.clear();
                    ctx.fillStyle = '#000';
                    ctx.save();
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    ctx.font = "16px normal 'Helvetica Nueue'";
                    chart.chart.config.type == 'pie' ? ctx.fillText("" + G_NO_DEBTS, width / 2, height / 2) : ctx.fillText("" + G_NO_DATA, width / 2, height / 2);
                    ctx.restore();
                }
            }
        });
    }
    ProfileComponent.prototype.onChartClick = function (event) {
        var _this = this;
        if (event && event.active
            && event.active.length > 0
            && event.active[0]._chart.config.type === 'bar'
            && event.active[0]._chart.config.data
            && event.active[0]._chart.config.data.datasets
            && event.active[0]._chart.config.data.datasets.length > 0) {
            if (event.active[0]._chart.config.type === 'bar') {
                var chart = event.active[0]._chart;
                var activePoints = chart.getElementAtEvent(event.event);
                var clickedElementIndex = activePoints[0]._datasetIndex;
                var initialState = { transactions: event.active[clickedElementIndex]._chart.config.data.datasets[clickedElementIndex].transactions[event.active[clickedElementIndex]._index] };
                if ('transactions' in initialState && initialState.transactions && initialState.transactions.length > 0) {
                    this.modalRef = this.modalService.show(_transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_9__["TransactionsModalComponent"], Object.assign({}, { class: 'gray modal-lg', initialState: initialState }));
                }
                else {
                    this.translate.get('G_NO_TRANS')
                        .subscribe(function (value) {
                        _this.notifier.notify('warning', value);
                    });
                }
            }
        }
    };
    ;
    // deep cloning 
    ProfileComponent.prototype.genericCloneMethod = function (arrayData) {
        var arrr = [];
        if (arrayData && arrayData.length > 0) {
            try {
                for (var arrayData_1 = __values(arrayData), arrayData_1_1 = arrayData_1.next(); !arrayData_1_1.done; arrayData_1_1 = arrayData_1.next()) {
                    var obj = arrayData_1_1.value;
                    var b = null;
                    b = Object.assign({}, obj);
                    arrr.push(b);
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (arrayData_1_1 && !arrayData_1_1.done && (_a = arrayData_1.return)) _a.call(arrayData_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
        }
        return arrr;
        var e_3, _a;
    };
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userAttributes = null;
        this.userAttributes = this.authService.getUserProfile();
        this.allAccounts = [];
        this.allAccounts = this.userAttributes.accounts;
        this.authResultInfo = this.authService.getAccessTokenAndID();
        this.formatPhoneNumber(this.userAttributes.phone_numbers);
        this.allProperty = [];
        this.allProperty = this.authService.getUserProfile().properties;
        this.translate.get('G_SPENDING').subscribe(function (value) { return _this.G_Spending = value; });
        this.translate.get('G_INCOME').subscribe(function (value) { return _this.G_Income = value; });
        this.translate.get('G_NET_INCOME').subscribe(function (value) { return _this.G_Net_Income = value; });
        if ('employers' in this.userAttributes && this.userAttributes.employers) {
            try {
                for (var _a = __values(this.userAttributes.employers), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var index = _b.value;
                    if (index.from_duration && index.to_duration) {
                        var from = moment__WEBPACK_IMPORTED_MODULE_7__(index.from_duration);
                        var to = moment__WEBPACK_IMPORTED_MODULE_7__(index.to_duration);
                        var diffDate = to.diff(from, 'months');
                        index.years = Math.floor(diffDate / 12);
                        index.months = diffDate % 12;
                    }
                    else if (index.from_duration && index.present) {
                        var from = moment__WEBPACK_IMPORTED_MODULE_7__(index.from_duration);
                        var to = moment__WEBPACK_IMPORTED_MODULE_7__["utc"]();
                        var diffDate = to.diff(from, 'months');
                        index.years = Math.floor(diffDate / 12);
                        index.months = diffDate % 12;
                    }
                    else {
                        index.years = 0;
                        index.months = 0;
                    }
                    if (index.from_duration) {
                        index.from_duration_in_date = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](index.from_duration).toDate();
                    }
                    else {
                        index.from_duration_in_date = null;
                    }
                    if (index.to_duration) {
                        index.to_duration_in_date = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](index.to_duration).toDate();
                    }
                    else {
                        index.to_duration_in_date = null;
                    }
                    if (index.present) {
                        index.present = true;
                    }
                    else {
                        index.present = false;
                    }
                    if ('preferred' in index && index.preferred) {
                        index.preferred = true;
                    }
                    else {
                        index.preferred = false;
                    }
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_4) throw e_4.error; }
            }
        }
        // credit and depos type acc calculation
        this.filteredCreditAcc = [];
        this.filteredDeposAcc = [];
        this.filteredLoanAcc = [];
        this.totalSum = { creditSum: 0, limitSum: 0, depoSum: 0, propertSum: 0, loanSum: 0 };
        this.filteredAccForBlur = [];
        try {
            for (var _d = __values(this.allAccounts), _e = _d.next(); !_e.done; _e = _d.next()) {
                var acc = _e.value;
                // check official name duplicate or not
                if ('accounts' in acc && acc.accounts) {
                    for (var i = 0; i < acc.accounts.length; i++) {
                        if (acc.accounts[i].type == "depository") {
                            for (var j = 0; j < acc.accounts.length; j++) {
                                if (acc.accounts[j].type == "depository") {
                                    if (!(i === j)) {
                                        if (!(acc.accounts[i].official_name === acc.accounts[j].official_name)) {
                                            if (!(acc.accounts[i].matchOfficialName)) {
                                                acc.accounts[i].matchOfficialName = false;
                                            }
                                        }
                                        else {
                                            acc.accounts[i].matchOfficialName = true;
                                        }
                                    }
                                }
                            }
                        }
                        else if (acc.accounts[i].type == "credit" && acc.accounts[i].status == 'ACTIVE') {
                            this.filteredCreditAcc.push(acc.accounts[i]);
                            this.totalSum.creditSum += acc.accounts[i].balances.current;
                            var limitAmount = acc.accounts[i].balances.limit ? (typeof (acc.accounts[i].balances.limit) == "string" ? parseInt(acc.accounts[i].balances.limit) : acc.accounts[i].balances.limit) : 0;
                            this.totalSum.limitSum += limitAmount;
                        }
                    }
                    try {
                        for (var _f = __values(acc.accounts), _g = _f.next(); !_g.done; _g = _f.next()) {
                            var i = _g.value;
                            if (i.type == "depository" && i.status == 'ACTIVE') {
                                this.filteredDeposAcc.push(i);
                                this.totalSum.depoSum += i.balances.current;
                            }
                            if (i.type == "loan" && i.status == 'ACTIVE') {
                                if ('official_name' in i && !(i.official_name) && 'inst_name' in i && i.inst_name) {
                                    i.official_name = i.inst_name;
                                }
                                this.filteredLoanAcc.push(i);
                                this.totalSum.loanSum += i.balances.current;
                            }
                            if (i.type == "loan" && i.status == 'ACTIVE' && i.mask == null) {
                                this.filteredAccForBlur.push(i);
                            }
                        }
                    }
                    catch (e_5_1) { e_5 = { error: e_5_1 }; }
                    finally {
                        try {
                            if (_g && !_g.done && (_h = _f.return)) _h.call(_f);
                        }
                        finally { if (e_5) throw e_5.error; }
                    }
                }
            }
        }
        catch (e_6_1) { e_6 = { error: e_6_1 }; }
        finally {
            try {
                if (_e && !_e.done && (_j = _d.return)) _j.call(_d);
            }
            finally { if (e_6) throw e_6.error; }
        }
        // property clculation
        if (this.allProperty && this.allProperty.length > 0) {
            var totalPropertySum = this.allProperty.map(function (item) { return item.estimated_value; });
            this.totalSum.propertSum = totalPropertySum.reduce(function (a, b) { return a + b; });
        }
        var arrayOfAccount = __spread(this.filteredCreditAcc, this.filteredDeposAcc, this.filteredLoanAcc);
        this.createDebtDistributionGraph(arrayOfAccount);
        this.createNexusScoreGraph();
        this.spinnerService.hide();
        var e_4, _c, e_6, _j, e_5, _h;
    };
    // This function is used to calculate other employers transactions
    ProfileComponent.prototype.calculateOtherEmployers = function (employers, transactions) {
        var _this = this;
        this.otherEmployersData = { employer_name: '', income: 0, seeTransaction: [] };
        this.translate.get('PC_OTHER').subscribe(function (value) {
            _this.otherEmployersData.employer_name = value;
        });
        if (transactions) {
            var incomeTransactions = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].filterIncomeTransactions(transactions);
            try {
                for (var incomeTransactions_1 = __values(incomeTransactions), incomeTransactions_1_1 = incomeTransactions_1.next(); !incomeTransactions_1_1.done; incomeTransactions_1_1 = incomeTransactions_1.next()) {
                    var t = incomeTransactions_1_1.value;
                    // Let's start with employer not found with this income transaction
                    var employerFound = false;
                    try {
                        for (var employers_1 = __values(employers), employers_1_1 = employers_1.next(); !employers_1_1.done; employers_1_1 = employers_1.next()) {
                            var employer = employers_1_1.value;
                            if (t.name.toLowerCase().includes(employer.employer_name.toLowerCase())) {
                                employer.income += Math.abs(t.amount);
                                employer.income = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].roundTo2Digit(employer.income);
                                if (!employer.seeTransaction) {
                                    employer.seeTransaction = [];
                                }
                                employer.seeTransaction.push(t);
                                this.employersData.totalTransArr.push(t);
                                employerFound = true;
                            }
                        }
                    }
                    catch (e_7_1) { e_7 = { error: e_7_1 }; }
                    finally {
                        try {
                            if (employers_1_1 && !employers_1_1.done && (_a = employers_1.return)) _a.call(employers_1);
                        }
                        finally { if (e_7) throw e_7.error; }
                    }
                    // Check if employer not found means it is from other source
                    if (!employerFound) {
                        this.otherEmployersData.income += Math.abs(t.amount);
                        this.otherEmployersData.income = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].roundTo2Digit(this.otherEmployersData.income);
                        this.otherEmployersData.seeTransaction.push(t);
                        this.employersData.totalTransArr.push(t);
                    }
                }
            }
            catch (e_8_1) { e_8 = { error: e_8_1 }; }
            finally {
                try {
                    if (incomeTransactions_1_1 && !incomeTransactions_1_1.done && (_b = incomeTransactions_1.return)) _b.call(incomeTransactions_1);
                }
                finally { if (e_8) throw e_8.error; }
            }
        }
        var e_8, _b, e_7, _a;
    };
    ProfileComponent.prototype.ngOnChanges = function (changes) {
        console.log('profile-changes', changes);
        // for properties
        if ('properties' in changes && 'currentValue' in changes.properties && changes.properties.currentValue && typeof (changes.properties.currentValue == 'Object') && changes.properties.currentValue.length > 0) {
            var receivedProperties = changes.properties.currentValue;
            this.allProperty = receivedProperties;
            if (this.allProperty && this.allProperty.length > 0) {
                var totalPropertySum = this.allProperty.map(function (item) { return item.estimated_value; });
                this.totalSum.propertSum = totalPropertySum.reduce(function (a, b) { return a + b; });
            }
        }
        // update profile
        if ('updatedReport' in changes && 'currentValue' in changes.updatedReport && changes.updatedReport.currentValue) {
            this.spinnerService.show();
            this.userAttributes = null;
            this.userAttributes = this.updatedReport;
            this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
            this.ngOnInit();
        }
        // for transactions
        if ('tranxData' in changes && 'currentValue' in changes.tranxData && changes.tranxData.currentValue && changes.tranxData.currentValue.transactions.length > 0) {
            var transactionData = changes.tranxData.currentValue.transactions;
            var clonedForGraph = this.genericCloneMethod(transactionData);
            var end_date = moment__WEBPACK_IMPORTED_MODULE_7__["utc"]().format(this.DATE_FORMAT);
            var start_date = moment__WEBPACK_IMPORTED_MODULE_7__["utc"]().subtract(11, 'months').startOf('month').format(this.DATE_FORMAT);
            var dates = { start_date: start_date, end_date: end_date };
            this.createTotalOverTimeGraph(dates, clonedForGraph);
            if (this.userAttributes && this.userAttributes.employers && this.userAttributes.employers.length >= 0) {
                try {
                    // First reset the income and transactions for each employers
                    for (var _a = __values(this.userAttributes.employers), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var employer = _b.value;
                        if ('income' in employer && employer.income) {
                            employer.income = 0;
                        }
                        if ('seeTransaction' in employer && employer.seeTransaction) {
                            employer.seeTransaction = [];
                        }
                    }
                }
                catch (e_9_1) { e_9 = { error: e_9_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                    }
                    finally { if (e_9) throw e_9.error; }
                }
                this.employersData.totalTransArr = [];
                if (changes.tranxData.currentValue.transactions) {
                    this.calculateOtherEmployers(this.userAttributes.employers, changes.tranxData.currentValue.transactions);
                }
                // set default employer
                // Check if preferred is set or not
                var employerWithPreferredSet = this.userAttributes.employers.filter(function (e) { return e.preferred; });
                if (!(employerWithPreferredSet && employerWithPreferredSet.length > 0)) {
                    // First try to get the preferred employees by income
                    var employersByAmount = this.userAttributes.employers.filter(function (e) { return e.income; }).sort(function (a, b) { return (b.income - a.income); });
                    if (employersByAmount && employersByAmount.length > 0 && 'income' in employersByAmount[0] && employersByAmount[0].income) {
                        employersByAmount[0].preferred = true;
                    }
                    else {
                        if ('employers' in this.userAttributes && this.userAttributes.employers.length > 0) {
                            this.userAttributes.employers[0].preferred = true;
                        }
                    }
                }
                this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
            }
        }
        else if ('tranxData' in changes && 'currentValue' in changes.tranxData && changes.tranxData.currentValue && changes.tranxData.currentValue.transactions.length == 0) {
            this.graphs.net_income_over_time.load = false;
            this.employersData.totalTransArr = [];
            this.userAttributes['showNoIncomeFound'] = true;
            try {
                for (var _d = __values(this.userAttributes.employers), _e = _d.next(); !_e.done; _e = _d.next()) {
                    var employer = _e.value;
                    if ('income' in employer && employer.income) {
                        delete employer.income;
                    }
                    if ('seeTransaction' in employer && employer.seeTransaction) {
                        delete employer.seeTransaction;
                    }
                    employer.preferred = false;
                }
            }
            catch (e_10_1) { e_10 = { error: e_10_1 }; }
            finally {
                try {
                    if (_e && !_e.done && (_f = _d.return)) _f.call(_d);
                }
                finally { if (e_10) throw e_10.error; }
            }
            if (!_common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].isNullOrEmpty(this.userAttributes, 'employers') && this.userAttributes.employers.length > 0 && this.userAttributes.employers[0].title == 'csvEmployer') {
                this.userAttributes['showNoIncomeFound'] = false;
                this.userAttributes.employers[0].preferred = true;
            }
            this.otherEmployersData = {};
            this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        }
        // reload page
        if ('reloadScreen' in changes && 'currentValue' in changes.reloadScreen && (changes.reloadScreen.currentValue != changes.reloadScreen.previousValue)) {
            this.ngOnInit();
        }
        if ('sidebarReload' in changes && changes.sidebarReload != null) {
            this.ngOnInit();
        }
        if ('employerSectionValue' in changes && 'currentValue' in changes.employerSectionValue && (changes.employerSectionValue.currentValue != changes.employerSectionValue.previousValue)) {
            this.employerBackButton.nativeElement.click();
        }
        var e_9, _c, e_10, _f;
    };
    //widget
    ProfileComponent.prototype.onResize = function () {
        this.canvasWidthFunction();
    };
    ProfileComponent.prototype.canvasWidthFunction = function () {
        this.innerWidth = window.innerWidth;
        if ((innerWidth > 480) && (innerWidth < 767)) {
            this.canvasWidth = 250;
            console.log('updatedcanvasWidth', this.canvasWidth);
        }
        if ((innerWidth < 480) && (innerWidth >= 320)) {
            this.canvasWidth = 200;
            console.log('updatedcanvasWidth', this.canvasWidth);
        }
        if (innerWidth > 768) {
            this.canvasWidth = 350;
        }
        if ((innerWidth > 992) && (innerWidth < 1200)) {
            this.canvasWidth = 250;
        }
    };
    ProfileComponent.prototype.createScoreGraph = function (item) {
        console.log('nexusScorePoint', item);
        if (!(Object.keys(item).length == 0)) {
            if (!('type' in item && item.type == 'report')) {
                if ('tt' in item && (item.tt || item.tt == 0)) {
                    this.graphTippingPoints.tt = item.tt <= 0 ? 0 : (item.tt).toFixed(0);
                }
                if ('tm' in item && (item.tm || item.tm == 0)) {
                    this.graphTippingPoints.tm = item.tm <= 0 ? 0 : (item.tm).toFixed(0);
                }
                if ('tb' in item && (item.tb || item.tb == 0)) {
                    this.graphTippingPoints.tb = item.tb <= 0 ? 0 : (item.tb).toFixed(0);
                }
                this.capturedDoc();
                if (this.graphTippingPoints) {
                    this.backend.post(this.graphTippingPoints, "/users/points", this.authResultInfo.accessToken).subscribe(function (res) {
                        console.log("Tipping Points added!");
                    }, function (err) {
                        var errorVar = err.json();
                        console.log("Message from Server: " + errorVar["message"]);
                    });
                }
                else {
                    console.log("Tipping Points not available!");
                }
            }
            else {
                if ('tt' in item && (item.tt || item.tt == 0)) {
                    this.graphTippingPoints.tt = item.tt < 0 ? 0 : item.tt;
                }
                if ('tm' in item && (item.tm || item.tm == 0)) {
                    this.graphTippingPoints.tm = item.tm < 0 ? 0 : item.tm;
                }
                if ('tb' in item && (item.tb || item.tb == 0)) {
                    this.graphTippingPoints.tb = item.tb < 0 ? 0 : item.tb;
                }
            }
        }
    };
    //nexusScore widget screenshot
    ProfileComponent.prototype.capturedDoc = function () {
        var _this = this;
        var data = document.getElementById('ss');
        if (data) {
            html2canvas__WEBPACK_IMPORTED_MODULE_11__(data).then(function (canvas) {
                var imgData = canvas.toDataURL('image/png');
                _this.authService.setDataInCache(_this.authService.BASE_64, imgData);
            }).catch(function (e) {
                console.log(e);
            });
        }
    };
    //format contactNUmber
    ProfileComponent.prototype.formatPhoneNumber = function (phoneNum) {
        var cleaned = ('' + phoneNum).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            this.phoneNumber = '(' + match[1] + ') ' + match[2] + '-' + match[3];
        }
        else {
            this.phoneNumber = '';
        }
    };
    /* Edit Modal Code */
    ProfileComponent.prototype.openEditModal = function (editProfileModal) {
        this.modalRef = this.modalService.show(editProfileModal, Object.assign({}, { class: 'gray modal-lg' }));
    };
    ProfileComponent.prototype.editProfileModalClose = function (event) {
        this.modalRef.hide();
        if (event) {
            this.ngOnInit();
        }
        if (event && 'address' in event && event.address) {
            this.userAttributes.address = Object.assign(this.userAttributes.address, event.address);
        }
        if (event && 'picture' in event && event.picture) {
            this.userAttributes.picture = event.picture;
            this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
            console.log("In Edit Profile modal close lno 564");
            this.headerEvent.emit(this.userAttributes.picture);
        }
    };
    ProfileComponent.prototype.employerSection = function () {
        this.employerSectionDiv = false;
    };
    ProfileComponent.prototype.profileSection = function () {
        this.employerSectionDiv = true;
        // We are reading employers back from session since it might have got updated
        this.userAttributes.employers = this.authService.getDataFromCache(this.authService.USER_PROFILE).employers;
    };
    ProfileComponent.prototype.setRadioBtnValue = function (preValue, index) {
        try {
            for (var _a = __values(this.userAttributes.employers), _b = _a.next(); !_b.done; _b = _a.next()) {
                var e = _b.value;
                if ('preferred' in e && e.preferred) {
                    e.preferred = false;
                }
            }
        }
        catch (e_11_1) { e_11 = { error: e_11_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_11) throw e_11.error; }
        }
        if (preValue) {
            this.userAttributes.employers[index].preferred = true;
        }
        else {
            this.userAttributes.employers[index].preferred = false;
        }
        var e_11, _c;
    };
    ProfileComponent.prototype.setEmployerDetails = function () {
        var _this = this;
        var data = {};
        var testPrefered = false;
        if (this.userAttributes.employers) {
            try {
                for (var _a = __values(this.userAttributes.employers), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var index = _b.value;
                    if (!index.preferred) {
                        if (!testPrefered) {
                            testPrefered = false;
                        }
                    }
                    else {
                        testPrefered = true;
                    }
                    if (index.present) {
                        index.present = true;
                    }
                    else {
                        index.present = false;
                    }
                    if (index.preferred) {
                        index.preferred = true;
                    }
                    else {
                        index.preferred = false;
                    }
                    if (index.from_duration_in_date) {
                        index.from_duration = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](index.from_duration_in_date).format(this.DATE_FORMAT);
                        delete index.from_duration_in_date;
                    }
                    else {
                        delete index.from_duration_in_date;
                        delete index.from_duration;
                    }
                    if (index.to_duration_in_date) {
                        index.to_duration = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](index.to_duration_in_date).format(this.DATE_FORMAT);
                        delete index.to_duration_in_date;
                    }
                    else {
                        delete index.to_duration_in_date;
                        delete index.to_duration;
                    }
                    delete index.user_id;
                    delete index.years;
                    delete index.months;
                    delete index.created_at;
                }
            }
            catch (e_12_1) { e_12 = { error: e_12_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_12) throw e_12.error; }
            }
            data.employers = JSON.parse(JSON.stringify(this.userAttributes.employers));
        }
        try {
            for (var _d = __values(data.employers), _e = _d.next(); !_e.done; _e = _d.next()) {
                var index = _e.value;
                if (!index.address) {
                    delete index.address;
                }
                if (!index.phone_number) {
                    delete index.phone_number;
                }
                if (!index.email) {
                    delete index.email;
                }
                if (!index.contact_person) {
                    delete index.contact_person;
                }
                if (!index.title) {
                    delete index.title;
                }
                delete index.from_duration_in_date;
                delete index.to_duration_in_date;
                delete index.income;
                delete index.seeTransaction;
                delete index.dateDuration;
                if (!index.from_duration) {
                    delete index.from_duration;
                }
                if (!index.to_duration) {
                    delete index.to_duration;
                }
            }
        }
        catch (e_13_1) { e_13 = { error: e_13_1 }; }
        finally {
            try {
                if (_e && !_e.done && (_f = _d.return)) _f.call(_d);
            }
            finally { if (e_13) throw e_13.error; }
        }
        var path = "/users/" + this.authResultInfo.idTokenPayload.sub;
        if (testPrefered) {
            this.spinnerService.show();
            this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(function (res) {
                _this.translate.get('PC_EMPL_UPDATE_SUCCES')
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
                var response = res.json();
                console.log('Response#: ', response);
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.userAttributes);
                _this.employerSectionDiv = true;
                _this.ngOnInit();
                _this.spinnerService.hide();
            }, function (err) {
                var errorVar = err.json();
                console.log('Error: ', errorVar["message"]);
                _this.translate.get('DASH_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + ": " + errorVar["message"]);
                });
                _this.spinnerService.hide();
            });
        }
        else {
            this.translate.get('PC_SELECET_ATLEAST_ONE')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
            this.ngOnInit();
            this.spinnerService.hide();
        }
        var e_12, _c, e_13, _f;
    };
    ProfileComponent.prototype.transactionHandler = function (transaction) {
        if (transaction && transaction.length > 0) {
            var initialState = { transactions: transaction };
            this.modalRef = this.modalService.show(_transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_9__["TransactionsModalComponent"], Object.assign({}, { class: 'gray modal-lg', initialState: initialState }));
        }
    };
    // This function is used to create score graph on profile component
    ProfileComponent.prototype.createNexusScoreGraph = function () {
        var _this = this;
        // First get transactions for last 12 months
        // Make get transactions call
        var start_date = moment__WEBPACK_IMPORTED_MODULE_7__["utc"]().add(-12, 'months').startOf('month');
        var end_date = moment__WEBPACK_IMPORTED_MODULE_7__["utc"]().add(-1, 'months').endOf('month');
        var path = '/users/transactions';
        var qp = [];
        if (start_date) {
            qp.push('start_date=' + start_date.format(this.DATE_FORMAT));
        }
        if (end_date) {
            qp.push('end_date=' + end_date.format(this.DATE_FORMAT));
        }
        this.loadingScoreGraph = true;
        var token = this.report ? this.report.token : this.authService.getAccessTokenAndID().accessToken;
        this.backend.get(path, token)
            .subscribe(function (data) {
            var allTransactions = data.json();
            if (!_common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].isNullOrEmpty(allTransactions, 'transactions')) {
                var dates = { start_date: start_date.format(_this.DATE_FORMAT), end_date: end_date.format(_this.DATE_FORMAT) };
                // income calculation
                var filterData = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].filterIncomeTransactions(allTransactions.transactions);
                var incomeData = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].groupGraphDataByFormat(filterData, dates, 'amount', "MMMM-YY");
                console.log('createNexusScoreGraph - chartData', incomeData);
                // Spending calculation
                var spendDataArr = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].filterSpendingTransactions(allTransactions.transactions);
                var cloneSpendDataArr = _this.genericCloneMethod(spendDataArr);
                var spendingData = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].groupGraphDataByFormat(cloneSpendDataArr, dates, 'amount', "MMMM-YY");
                console.log('createNexusScoreGraph - spendingData', spendingData);
                return _this.backend.postHandlerForMeter(_this.createDataForScoreAPI(incomeData, spendingData, start_date, end_date))
                    .subscribe(function (res) {
                    if ('_body' in res && res['_body']) {
                        //Need to check it in production. If this condition is working.
                        try {
                            var bodyData = JSON.parse(res['_body']);
                            if (!_common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].isNullOrEmpty(bodyData, 'TippingPoints')) {
                                _this.createScoreGraph(bodyData.TippingPoints);
                            }
                            else {
                                _this.createScoreGraph({ tt: 0, tm: 0, tb: 0, err: 'err' });
                            }
                        }
                        catch (err) {
                            _this.createScoreGraph({ tt: 0, tm: 0, tb: 0, err: 'err' });
                        }
                    }
                    _this.loadingScoreGraph = false;
                });
            }
        }, function (err) {
            console.log('createNexusScoreGraph - error', err);
            _this.loadingScoreGraph = false;
        });
    };
    // this function is used to create api data
    // generate json data for meter, which will shown in profile
    // when post call successfull, then emit response in profile.ts
    // then show that response data in meter as per requiremnet.
    ProfileComponent.prototype.createDataForScoreAPI = function (income, spending, start_date, end_date) {
        var data = {
            "factors": {
                "top": "1.01",
                "middle": "0.98",
                "bottom": "0.94"
            },
            "listMonths": []
        };
        for (var l = end_date; l.isSameOrAfter(start_date); l.subtract(1, 'month')) {
            var apiFormatedDate = l.format("MMMM-YY");
            var monthData = {
                Month: apiFormatedDate,
                Income: 0,
                SpendingTotal: 0
            };
            if (!_common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].isNullOrEmpty(income, apiFormatedDate)) {
                monthData.Income = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].roundTo2Digit(income[apiFormatedDate].amount);
            }
            if (!_common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].isNullOrEmpty(spending, apiFormatedDate)) {
                monthData.SpendingTotal = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].roundTo2Digit(spending[apiFormatedDate].amount);
            }
            data.listMonths.push(monthData);
        }
        return data;
    };
    //Net Income over time
    ProfileComponent.prototype.createTotalOverTimeGraph = function (dates, transactions) {
        var avgData = [];
        var incomeData = [];
        var spendData = [];
        // income calculation
        var filterData = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].filterIncomeTransactions(transactions);
        var chartData = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].groupGraphDataByFormat(filterData, dates, 'amount', null);
        console.log('chartData', chartData);
        // Spending calculation
        var spendDataArr = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].filterSpendingTransactions(transactions);
        var cloneSpendDataArr = this.genericCloneMethod(spendDataArr);
        var spendingData = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].groupGraphDataByFormat(cloneSpendDataArr, dates, 'amount', null);
        console.log('spendingData', spendingData);
        // averageData calculation
        incomeData = Object.values(chartData).map(function (a) { return a.amount; });
        spendData = Object.values(spendingData).map(function (a) { return a.amount * -1; });
        for (var i = 0; i < incomeData.length; i++) {
            avgData.push(incomeData[i] + spendData[i]);
        }
        // net total income and its avg
        var sumOfAmount = avgData.reduce(function (a, b) { return a + b; });
        this.graphs.net_income_over_time.totalAmount = sumOfAmount;
        this.graphs.net_income_over_time.avgAmount = sumOfAmount / avgData.length;
        // add income and spend transaction in one variable
        var incomeTransArr = Object.values(chartData).map(function (t) { return t.transactionArr; });
        var spendingTransArr = Object.values(spendingData).map(function (t) { return t.transactionArr; });
        var avgTransactionArr = [];
        for (var i = 0; i < incomeTransArr.length; i++) {
            var incomeKey = null;
            var spendKey = null;
            incomeKey = incomeTransArr[i];
            spendKey = spendingTransArr[i];
            avgTransactionArr.push(incomeKey.concat(spendKey));
        }
        this.graphs.net_income_over_time.labels.length = 0;
        (_a = this.graphs.net_income_over_time.labels).push.apply(_a, __spread(Object.keys(chartData)));
        this.graphs.net_income_over_time.data = [
            {
                data: avgData,
                label: "" + this.G_Net_Income,
                type: "line",
                borderColor: "#717171",
                fill: false,
                lineTension: 0,
                pointRadius: 4,
                pointBackgroundColor: "#717171",
                transactions: avgTransactionArr
            },
            {
                data: incomeData,
                label: "" + this.G_Income,
                transactions: Object.values(chartData).map(function (t) { return t.transactionArr; })
            },
            {
                data: spendData,
                label: "" + this.G_Spending,
                transactions: Object.values(spendingData).map(function (t) { return t.transactionArr; })
            }
        ];
        this.graphs.net_income_over_time.load = true;
        this.spinnerService.hide();
        var _a;
    };
    // Debt distribution
    ProfileComponent.prototype.createDebtDistributionGraph = function (accountArray) {
        var deposTypeAccArr = accountArray.filter(function (t) { return t.type == 'depository' && t.balances.current <= 0; });
        var creditTypeAccArr = accountArray.filter(function (t) { return t.type == 'credit' && t.balances.current >= 0; });
        var loanTypeAccArr = accountArray.filter(function (t) { return t.type == 'loan' && t.balances.current >= 0; });
        var mergedArray = __spread(deposTypeAccArr, creditTypeAccArr, loanTypeAccArr);
        var debtDistribution = {};
        var total = 0;
        var category;
        mergedArray.forEach(function (t, index) {
            if (t.official_name) {
                category = t.official_name + ' deleting ' + t.account_id;
                total += Math.abs(t.balances.current);
                //add all data from mergedArray
                debtDistribution[category] = {
                    amount: Math.abs(t.balances.current),
                    transactions: [t]
                };
            }
        });
        console.log('debtDistribution', debtDistribution);
        this.graphs.current_debt_distribution.barOptions.elements.center.text = this.currency.transform(total, 'USD', true, '1.0-0');
        this.graphs.current_debt_distribution.labels.length = 0;
        (_a = this.graphs.current_debt_distribution.labels).push.apply(_a, __spread(Object.keys(debtDistribution)));
        this.graphs.current_debt_distribution.data = [{
                data: Object.values(debtDistribution).map(function (t) { return t.amount; }),
                transactions: Object.values(debtDistribution).map(function (t) { return t.transactions; })
            }];
        this.graphs.current_debt_distribution.load = true;
        var _a;
    };
    // set employer address
    ProfileComponent.prototype.addAddressModal = function (addressModalTemplate, employerValue, index) {
        this.employerDetail = {
            employer: JSON.parse(JSON.stringify(employerValue)),
            index: index
        };
        this.openEditModal(addressModalTemplate);
    };
    ProfileComponent.prototype.addressModalClose = function (event) {
        if (event) {
            if ('employer' in event && 'index' in event) {
                if ('address' in event.employer && event.employer.address) {
                    this.userAttributes.employers[event.index].address = event.employer.address;
                }
                if ('email' in event.employer || event.employer.email) {
                    this.userAttributes.employers[event.index].email = event.employer.email;
                }
                if ('phone_number' in event.employer || event.employer.phone_number) {
                    this.userAttributes.employers[event.index].phone_number = event.employer.phone_number;
                }
                if ('contact_person' in event.employer || event.employer.contact_person) {
                    this.userAttributes.employers[event.index].contact_person = event.employer.contact_person;
                }
                if ('title' in event.employer || event.employer.title) {
                    this.userAttributes.employers[event.index].title = event.employer.title;
                }
            }
            this.modalRef.hide();
            this.setEmployerDetails();
        }
        else {
            this.modalRef.hide();
        }
    };
    ProfileComponent.prototype.addAccountHandler = function (refrenceName) {
        this.addAccountHandlerEmit.emit(refrenceName);
    };
    ProfileComponent.prototype.propertyHandler = function (PropertyName) {
        this.propertyHandlerEmit.emit(PropertyName);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('nexusscore'),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "nexusscore", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("reloadScreen"),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "reloadScreen", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("tranxData"),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "tranxData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("properties"),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "properties", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("report"),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "report", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("scoreGraphObj"),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "scoreGraphObj", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("updatedReport"),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "updatedReport", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("employerSectionValue"),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "employerSectionValue", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "headerEvent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "addAccountHandlerEmit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProfileComponent.prototype, "propertyHandlerEmit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('employerBackButton'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProfileComponent.prototype, "employerBackButton", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ProfileComponent.prototype, "onResize", null);
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/profile/profile.component.css"), __webpack_require__(/*! ../global/global.css */ "./src/app/global/global.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"],
            angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["BsModalService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_6__["NotifierService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateService"],
            _common_currencyformatter_pipe__WEBPACK_IMPORTED_MODULE_10__["LooseCurrencyPipe"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/qr-generate-modal/qr-generate-modal.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/qr-generate-modal/qr-generate-modal.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".qr-center{\n    display:flex;\n    justify-content: center;\n    align-items:center;\n}"

/***/ }),

/***/ "./src/app/qr-generate-modal/qr-generate-modal.component.html":
/*!********************************************************************!*\
  !*** ./src/app/qr-generate-modal/qr-generate-modal.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"border border-dark\">\n  <div class=\"bg-dark text-white font-weight-bold p-3\"><h5>{{\"QRGM_DIV_SHARE_REPORT\" | translate}} <i class=\"fa fa-close float-right cursor-pointer\" (click)=\"closeModal()\"></i></h5></div>\n  <div class=\"row\">\n    <div class=\"col-sm-12 col-md-6\">\n      <div class=\"p-5 qr-center\">\n        <img [src]=\"qrCodeData.qr_code_url\" alt=\"\">\n      </div>\n    </div>\n    <div class=\"col-sm-12 col-md-6 col-lg-6 border-left border-dark\">\n      <div class=\"p-5\">\n        <div class=\"font-weight-bold text-center \"><h5>{{\"QRGM_H5_SCAN_QR_CODE\" | translate}}</h5><div>\n          <div class=\"mt-3\">\n            <form [formGroup]=\"emailValidate\" class=\"pl-line\">\n                <input type=\"email\" class=\"form-control\" id=\"email\" [(ngModel)]=\"emailValue\"\n                  placeholder=\"email@example.com\" formControlName=\"emailId\" />\n            <button class=\"font-weight-bold ml-3 pl-2 pr-2 text-white bg-success border-0 btn btn-success btn-sm mt-2\" (click)=\"sendEmail()\" [disabled]=\"emailValidate.invalid\">{{\"QRGM_BUTTON_SEND\" | translate}}</button>\n            </form>\n          </div>\n          <p class=\"mt-3 text-center cursor-pointer text-primary\" (click)=\"goToReport()\"><u>{{\"QRGM_P_VIEW_REPORT\" | translate}}</u></p>\n        </div>\n        </div>\n      </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/qr-generate-modal/qr-generate-modal.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/qr-generate-modal/qr-generate-modal.component.ts ***!
  \******************************************************************/
/*! exports provided: QrGenerateModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrGenerateModalComponent", function() { return QrGenerateModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var QrGenerateModalComponent = /** @class */ (function () {
    function QrGenerateModalComponent(spinnerService, backend, authService, notifierService, translate) {
        this.spinnerService = spinnerService;
        this.backend = backend;
        this.authService = authService;
        this.notifierService = notifierService;
        this.translate = translate;
        this.childEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.notifier = notifierService;
    }
    QrGenerateModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.generateReportId = this.qrCodeData.id;
        this.authResultInfo = this.authService.getAccessTokenAndID();
        this.translate.get('QR_TIME_START')
            .subscribe(function (value) {
            _this.notifier.notify('success', value);
        });
        this.emailValidate = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            emailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")
            ]))
        });
    };
    QrGenerateModalComponent.prototype.closeModal = function () {
        this.childEvent.emit();
    };
    QrGenerateModalComponent.prototype.sendEmail = function () {
        var _this = this;
        this.spinnerService.show();
        var base64Data = this.authService.getDataFromCache(this.authService.BASE_64);
        console.log('base64Data', base64Data);
        var data = {
            email: this.emailValue,
            snapshot: base64Data
        };
        var path = "/reports/" + this.generateReportId + "/send";
        this.backend.post(data, path, this.authResultInfo.accessToken).subscribe(function (res) {
            _this.spinnerService.hide();
            console.log('Response: ', res);
            _this.translate.get('QR_SUCCESS_SENT')
                .subscribe(function (value) {
                _this.notifier.notify('success', value);
            });
        }, function (err) {
            _this.spinnerService.hide();
            var errorVar = err.json();
            console.log('Error: ', errorVar["message"]);
            _this.translate.get('DASH_ERROR')
                .subscribe(function (value) {
                _this.notifier.notify('error', value + ": " + errorVar["message"]);
            });
        });
    };
    QrGenerateModalComponent.prototype.goToReport = function () {
        window.open("/report/" + this.generateReportId, "_blank"); // go to the Report page
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], QrGenerateModalComponent.prototype, "childEvent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("qrCodeData"),
        __metadata("design:type", Object)
    ], QrGenerateModalComponent.prototype, "qrCodeData", void 0);
    QrGenerateModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-qr-generate-modal',
            template: __webpack_require__(/*! ./qr-generate-modal.component.html */ "./src/app/qr-generate-modal/qr-generate-modal.component.html"),
            styles: [__webpack_require__(/*! ./qr-generate-modal.component.css */ "./src/app/qr-generate-modal/qr-generate-modal.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_3__["BackendService"],
            _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_4__["NotifierService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"]])
    ], QrGenerateModalComponent);
    return QrGenerateModalComponent;
}());



/***/ }),

/***/ "./src/app/realtime/realtime.service.ts":
/*!**********************************************!*\
  !*** ./src/app/realtime/realtime.service.ts ***!
  \**********************************************/
/*! exports provided: RealTimeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RealTimeService", function() { return RealTimeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _websocket_websocket_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../websocket/websocket.service */ "./src/app/websocket/websocket.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RealTimeService = /** @class */ (function () {
    function RealTimeService(wsService, authService) {
        this.authService = authService;
        // Add auth to chat URL
        var authResultInfo = this.authService.getAccessTokenAndID();
        this.messages = wsService.connect(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].wsapiurl + '?auth=' + authResultInfo.accessToken).map(function (response) {
            return JSON.parse(response.data);
        });
    }
    RealTimeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_websocket_websocket_service__WEBPACK_IMPORTED_MODULE_1__["WebsocketService"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], RealTimeService);
    return RealTimeService;
}());



/***/ }),

/***/ "./src/app/report-sidebar/report-sidebar.component.css":
/*!*************************************************************!*\
  !*** ./src/app/report-sidebar/report-sidebar.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "li[aria-expanded=true] .fa.fa-caret-down{\n\tdisplay:none;\n}\n.custom-green {\n    color: rgb(0, 194, 86);\n  }\n.custom-red {\n    color: rgb(234, 104, 109);\n  }\n.collapsed .fa.fa-caret-up{\n\tdisplay:none;\n}\n.list-group a, span{\n color: white;\n /*border: 0px !important;\n background: #454589;*/\n}\n.ml-4{\n  margin-left: 4% !important;\n}\n.ml-10{\n  margin-left: 10%;\n}\n.main-menu-style{\n  font-size: 16px !important;\n}\n.menu-style{\n  font-size: 14px !important;\n}\n.sub-menu-style{\n  font-size: 15px !important;\n}\n/* .nav-side-menu ul .active{\n  background-color: #676796 !important;\n}\n.nav-side-menu ul :hover{\n   background-color: #676796 !important;\n } */\n.nav-side-menu {\n  overflow: auto;\n  font-size: 12px;\n  font-weight: 400;\n  background-color: #2e353d;\n  position: fixed;\n  top: 0px;\n  height: 100%;\n  color: #e1ffff;\n}\n.nav-side-menu .brand {\n  background-color: #23282e;\n  line-height: 50px;\n  display: block;\n  text-align: center;\n  font-size: 14px;\n}\n.nav-side-menu .toggle-btn {\n  display: none;\n}\n.nav-side-menu ul,\n.nav-side-menu li {\n  list-style: none;\n  padding: 0px;\n  margin: 0px;\n  line-height: 35px;\n  cursor: pointer;\n  /*\n    .collapsed{\n       .arrow:before{\n                 content: \"\\f053\";\n                 display: inline-block;\n                 padding-left:10px;\n                 padding-right: 10px;\n                 vertical-align: middle;\n                 float:right;\n            }\n     }\n*/\n}\n.nav-side-menu ul :not(collapsed) .arrow:before,\n.nav-side-menu li :not(collapsed) .arrow:before {\n  content: \"\\f078\";\n  display: inline-block;\n  padding-left: 10px;\n  padding-right: 10px;\n  vertical-align: middle;\n}\n.nav-side-menu ul .active,\n.nav-side-menu li .active {\n  border-left: 3px solid #d19b3d;\n  background-color: #4f5b69;\n}\n.nav-side-menu ul .sub-menu li.active,\n.nav-side-menu li .sub-menu li.active {\n  color: #d19b3d;\n}\n.nav-side-menu ul .sub-menu li.active a,\n.nav-side-menu li .sub-menu li.active a {\n  color: #d19b3d;\n}\n.nav-side-menu ul .sub-menu li,\n.nav-side-menu li .sub-menu li {\n  background-color: #181c20;\n  border: none;\n  line-height: 28px;\n  border-bottom: 1px solid #23282e;\n  margin-left: 0px;\n}\n.nav-side-menu ul .sub-menu li:hover,\n.nav-side-menu li .sub-menu li:hover {\n  background-color: #020203;\n}\n.nav-side-menu ul .sub-menu li:before,\n/*.nav-side-menu li .sub-menu li:before {\n  content: \"\\f105\";\n  display: inline-block;\n  padding-left: 20px;\n  padding-right: 10px;\n  vertical-align: middle;\n}*/\n.nav-side-menu li {\n  padding-left: 0px;\n  border-left: 3px solid #2e353d;\n  border-bottom: 1px solid #23282e;\n}\n.nav-side-menu li a {\n  text-decoration: none;\n  color: #e1ffff;\n}\n.nav-side-menu li a i {\n  padding-left: 10px;\n  width: 20px;\n  padding-right: 20px;\n}\n.nav-side-menu li:hover {\n  border-left: 3px solid #d19b3d;\n  background-color: #4f5b69;\n  transition: all 1s ease;\n}\n@media (max-width: 767px) {\n  .nav-side-menu {\n    position: relative;\n    width: 100%;\n    margin-bottom: 10px;\n  }\n  .nav-side-menu .toggle-btn {\n    display: block;\n    cursor: pointer;\n    position: absolute;\n    right: 12px;\n    top: 3px;\n    z-index: 10 !important;\n    padding: 3px;\n    background-color: #ffffff;\n    color: #000;\n    width: 40px;\n    text-align: center;\n  }\n  .brand {\n    text-align: left !important;\n    font-size: 22px;\n    padding-left: 20px;\n    line-height: 50px !important;\n  }\n}\n@media (min-width: 767px) {\n  .nav-side-menu .menu-list .menu-content {\n    display: block;\n  }\n}\nbody {\n  margin: 0px;\n  padding: 0px;\n}\n.nav-side-menu ul .sub-menu ul .sub-line li.active,\n.nav-side-menu li .sub-menu li .sub-line li.active {\n  color: #d19b3d;\n}\n.nav-side-menu ul .sub-menu li .sub-line li.active a,\n.nav-side-menu li .sub-menu li .sub-line li.active a {\n  color: #d19b3d;\n}\n.nav-side-menu ul .sub-menu li .sub-line li,\n.nav-side-menu li .sub-menu li .sub-line li {\n  background-color: #181c20;\n  border: none;\n  line-height: 28px;\n  border-bottom: 1px solid #23282e;\n  margin-left: 0px;\n}\n.nav-side-menu ul .sub-menu li .sub-line li:hover,\n.nav-side-menu li .sub-menu li .sub-line li:hover {\n  background-color: #020203;\n}\n.nav-side-menu ul .sub-menu li .sub-line li:before,\n.nav-side-menu li .sub-menu li .sub-line li:before {\n  content: \"\\f105\";\n  display: inline-block;\n  padding-left: 100px;\n  padding-right: 10px;\n  vertical-align: middle;\n}\n.nav-side-menu .sub-menu li {\n  padding-left: 20px;\n  border-left: 3px solid #2e353d;\n  border-bottom: 1px solid #23282e;\n}\n.nav-side-menu .sub-menu li a {\n  text-decoration: none;\n  color: #e1ffff;\n}\n.sub-menu li a i {\n  padding-left: 10px;\n  width: 20px;\n  padding-right: 20px;\n}\n.nav-side-menu li .sub-menu li:hover {\n  border-left: 3px solid #d19b3d;\n  background-color: #4f5b69;\n  transition: all 1s ease;\n}\n@media (max-width: 767px) {\n .nav-side-menu .sub-menu {\n    position: relative;\n    width: 100%;\n    margin-bottom: 10px;\n  }\n  .sub-line ul .sub-press li.active,\n  .sub-line li .sub-press li.active {\n    color: #d19b3d;\n  }\n\n  .sub-line ul .sub-press li.active a,\n  .sub-line li .sub-press li.active a {\n    color: #d19b3d;\n  }\n  .sub-line ul .sub-press li,\n  .sub-line li .sub-press li {\n    background-color: #181c20;\n    border: none;\n    line-height: 28px;\n    border-bottom: 1px solid #23282e;\n    margin-left: 0px;\n  }\n  .sub-line ul .sub-press li:hover,\n  .sub-line li .sub-press li:hover {\n    background-color: #020203;\n  }\n  .sub-line ul .sub-press li:before,\n  .sub-line li .sub-press li:before {\n    content: \"\\f105\";\n    display: inline-block;\n    padding-left: 50px;\n    padding-right: 10px;\n    vertical-align: middle;\n  }\n\n  .sub-line li {\n    padding-left: 20px;\n    border-left: 3px solid #2e353d;\n    border-bottom: 1px solid #23282e;\n  }\n  .sub-line li a {\n    text-decoration: none;\n    color: #e1ffff;\n  }\n  .sub-line li a i {\n    padding-left: 50px;\n    width: 20px;\n    padding-right: 20px;\n  }\n  .sub-line li:hover {\n    border-left: 3px solid #d19b3d;\n    background-color: #4f5b69;\n    transition: all 1s ease;\n  }\n}\n@media (max-width: 767px) {\n  .sub-line {\n    position: relative;\n    width: 100%;\n    margin-bottom: 10px;\n  }\n}\n/*custom css*/\n.custom-nav-side{\n  top: auto !important;\n  position: unset !important;\n}\n.custom-sidebar{\n    background: rgb(86, 86, 86);\n    color: white;\n}\n.brand.sidebar-bg{\n  visibility:hidden;\n}\n.right-align{\n  float:right;\n  margin-right:6px;\n}\n/* Start Add Property Modal CSS */\n.property-modal-footer{\n  padding:10px !important;\n  justify-content: normal !important;\n  color: #ffffff;\n  background: #333333;\n  border:1px solid;\n  font-size:14px;\n}\n.common-font{\n  font-size: 0.9rem;\n}\n.w-100{\n  width:100%;\n}\n.cancel{\n  float:right;\n}\n.back{\n  float:left;\n}\n.back:hover, .cancel:hover{\n  cursor:pointer;\n}\n.icon-size{\n  font-size:2.2rem;\n}\n.plus-icon{\n  font-size:1.2rem;\n}\n.hover-color{\n  line-height:1.5rem;\n  border:1px solid #999999;\n}\n.hover-color:hover .fa {\n  color:#008198;\n}\n.hover-color:hover{\n  border:1px solid #666666;\n}\n.hover-color .visible-plus{\n  visibility:hidden;\n}\n.hover-color:hover .visible-plus{\n  visibility:visible;\n}\n.mt-btn{\n  margin-top:6rem;\n}\n.mb-btn{\n  margin-bottom:4rem;\n}\n.set-padding{\n  padding-left:5px;\n  padding-right:5px;\n}\n@media screen and (max-width: 575px){\n  .set-padding{\n    margin-top:0.25rem;\n    padding-left:25px;\n    padding-right:25px;\n  }\n  .mt-btn{\n    margin-top:2rem;\n  }\n  .mb-btn{\n    margin-bottom:1rem;\n  }\n  .top-sp {\n    margin-top: 10px;\n  }\n  .top-mp{\n    margin-top:1px;\n  }\n}\n@media screen and (max-width:350px){\n  .property-modal-footer{\n    font-size:12px;\n  }\n  .padding-r{\n    padding-right:2px;\n  }\n  .padding-l{\n    padding-left:2px;\n  }\n}\n@media screen and (max-width:480px){\n  .padding-r{\n    padding-right:4px;\n  }\n  .padding-l{\n    padding-left:4px;\n  }\n  .btn-Overview{\n    margin-right: 0 !important;\n    margin-bottom: 16px !important;\n  }\n}\n/* End Add Property Modal CSS */\n"

/***/ }),

/***/ "./src/app/report-sidebar/report-sidebar.component.html":
/*!**************************************************************!*\
  !*** ./src/app/report-sidebar/report-sidebar.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"nav-side-menu custom-nav-side custom-sidebar\">\n    <i class=\"fa fa-bars fa-2x toggle-btn\" data-toggle=\"collapse\" data-target=\"#menu-content\"></i>\n  \n    <div class=\"menu-list\">\n  \n      <ul id=\"menu-content\" class=\"menu-content collapse out\">\n        <!-- <li data-toggle=\"collapse\" data-target=\"#allAccounts\" class=\"collapsed active\">\n          <a class=\"main-menu-style\"><i class=\"fa fa-caret-down\"></i><i class=\"fa fa-caret-up\"></i>All Accounts\n            <span class=\"custom-float-right\">\n            <i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"></i>\n          </span>\n          </a>\n        </li>\n        <ul class=\"sub-menu collapse\" id=\"allAccounts\">\n          <li *ngFor=\"let accountsByInst of allAccounts\">\n            <span class=\"menu-collapsed ml-4 menu-style\">{{accountsByInst.inst_name}}</span>\n            <p class=\"menu-collapsed ml-10 menu-style\" *ngFor=\"let accounts of accountsByInst.accounts\">{{accounts.subtype}} ****{{accounts.mask}} <span class=\"right-align\">${{accounts.balances.current}}</span></p>\n          </li>\n        </ul> -->\n  \n        <!--Bank Accounts -->\n        <li data-toggle=\"collapse\" data-target=\"#products\" class=\"collapsed active\">\n          <a class=\"main-menu-style\"><i class=\"fa fa-caret-down\"></i><i class=\"fa fa-caret-up\"></i>{{\"RSB_A_CASH\" | translate}}\n            <span class=\"custom-float-right\">\n            <span>${{totalBal.cash}}</span>\n            <i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"></i>\n          </span>\n          </a>\n        </li>\n        <ul class=\"sub-menu collapse\" id=\"products\">\n          <li *ngFor=\"let accountsByInst of allAccounts; let parentIndex = index\">\n            <span class=\"menu-collapsed ml-4 menu-style\">{{accountsByInst.inst_name}}</span>\n            <p class=\"menu-collapsed ml-10 menu-style\" *ngFor=\"let depository of accountsByInst.accounts; let childIndex = index\">\n              <span *ngIf=\"depository.type == 'depository'\">\n                <span *ngIf=\"!depository.showFullName\" (mouseover)=\"showFullAccName(parentIndex,childIndex)\">{{depository.name.length > 20 ? depository.name.substring(0,15)+\"...\" : depository.name}}</span>\n                <span *ngIf=\"depository.showFullName\" (mouseout)=\"hideFullAccName(parentIndex,childIndex)\">{{depository.name}}</span>  \n                <span class=\"right-align custom-green\">${{depository.balances.current}}</span>\n              </span>\n            </p>\n            <!--<p class=\"menu-collapsed ml-10 menu-style\">Savings ****9800</p>-->\n          </li>\n        </ul>\n       \n        <!-- Credit Cards -->\n        <li data-toggle=\"collapse\" data-target=\"#service\" class=\"collapsed\">\n          <a class=\"main-menu-style\"><i class=\"fa fa-caret-down \"></i><i class=\"fa fa-caret-up\"></i>{{\"RSB_SPAN_CREDIT CARDS\" | translate}}<span>\n            <span class=\"custom-float-right\">\n              <span>${{totalBal.credit}}</span>\n              <i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"></i>\n            </span>\n          </span></a>\n        </li>\n        <ul class=\"sub-menu collapse\" id=\"service\">\n          <li *ngFor=\"let accountsByInst of allAccounts; let parentIndex = index\">\n            <span class=\"menu-collapsed menu-style ml-4\">{{accountsByInst.inst_name}}</span>\n            <p class=\"menu-style ml-10\" *ngFor=\"let credit of accountsByInst.accounts; let childIndex = index\">\n              <span *ngIf=\"credit.type == 'credit'\">\n                <span *ngIf=\"!credit.showFullName\" (mouseover)=\"showFullAccName(parentIndex,childIndex)\">{{credit.name.length > 20 ? credit.name.substring(0,15)+\"...\" : credit.name}}</span>\n                <span *ngIf=\"credit.showFullName\" (mouseout)=\"hideFullAccName(parentIndex,childIndex)\">{{credit.name}}</span> \n                <span class=\"right-align custom-red\">${{credit.balances.current}}</span>\n              </span>\n            </p>\n          </li>\n        </ul>\n  \n        <li data-toggle=\"collapse\" data-target=\"#loans\" class=\"collapsed\">\n          <a class=\"main-menu-style\"><i class=\"fa fa-caret-down\"></i><i class=\"fa fa-caret-up\"></i>{{\"RSB_A_LOANS\" | translate}}\n            <span class=\"custom-float-right\">\n              <span>$0</span>\n            <i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"></i>\n          </span>\n          </a>\n        </li>\n        <ul class=\"sub-menu collapse\" id=\"loans\">\n          <li>\n            <span class=\"menu-collapsed menu-style ml-4\">{{\"RSB_SPAN_COMING SOON\" | translate}}</span>\n          </li>\n        </ul>\n      </ul>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/report-sidebar/report-sidebar.component.ts":
/*!************************************************************!*\
  !*** ./src/app/report-sidebar/report-sidebar.component.ts ***!
  \************************************************************/
/*! exports provided: ReportSidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportSidebarComponent", function() { return ReportSidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};

var ReportSidebarComponent = /** @class */ (function () {
    function ReportSidebarComponent() {
        this.totalBal = {};
        this.allAccounts = [];
        this.realEstateCurrentDiv = "selectPropertyType";
    }
    ReportSidebarComponent.prototype.ngOnInit = function () {
        console.log('inside sidebar', this.allAccounts);
        this.totalBal = { cash: 0, credit: 0, property: 0 };
        try {
            //allAccounts balance
            for (var _a = __values(this.allAccounts), _b = _a.next(); !_b.done; _b = _a.next()) {
                var acc = _b.value;
                try {
                    for (var _c = __values(acc.accounts), _d = _c.next(); !_d.done; _d = _c.next()) {
                        var i = _d.value;
                        // depository type balance
                        if (i.type == "depository") {
                            this.totalBal.cash += i.balances.current;
                        }
                        // credit type balance
                        if (i.type == "credit") {
                            this.totalBal.credit += i.balances.current;
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_d && !_d.done && (_e = _c.return)) _e.call(_c);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_f = _a.return)) _f.call(_a);
            }
            finally { if (e_2) throw e_2.error; }
        }
        var e_2, _f, e_1, _e;
    };
    ReportSidebarComponent.prototype.showFullAccName = function (parentIndex, childIndex) {
        this.allAccounts[parentIndex].accounts[childIndex].showFullName = true;
    };
    ReportSidebarComponent.prototype.hideFullAccName = function (parentIndex, childIndex) {
        delete this.allAccounts[parentIndex].accounts[childIndex].showFullName;
    };
    ReportSidebarComponent.prototype.getKeys = function (map) {
        return Array.from(Object.keys(map));
    };
    ReportSidebarComponent.prototype.changeModalDiv = function (divName) {
        this.realEstateCurrentDiv = divName;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("accounts"),
        __metadata("design:type", Object)
    ], ReportSidebarComponent.prototype, "allAccounts", void 0);
    ReportSidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-report-sidebar',
            template: __webpack_require__(/*! ./report-sidebar.component.html */ "./src/app/report-sidebar/report-sidebar.component.html"),
            styles: [__webpack_require__(/*! ./report-sidebar.component.css */ "./src/app/report-sidebar/report-sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ReportSidebarComponent);
    return ReportSidebarComponent;
}());



/***/ }),

/***/ "./src/app/report-transaction/report-transaction.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/report-transaction/report-transaction.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table td{\n    border-top:1px solid #999999;\n    border-left:1px solid #999999;\n    white-space: nowrap;\n  }\n  .table tr:last-child td{\n    border-bottom:1px solid #999999;\n  }\n  .table tr td:last-child{\n    border-right:1px solid #999999;\n  }\n  .table-hover tbody tr:hover {\n    background-color: #E1E1E1;\n  }\n  .dateSort{\n    float: right;\n    width: 23px;\n    height: 26px;\n    cursor: pointer;\n    background: none;\n    border: none;\n  }\n  .d-width{\n    min-width: 106px;\n  }\n  .table td{\n    font-size: 15px;\n    padding: 0 0 0 3px !important\n  }\n  .trans-secondary{\n    color:rgb(68,68,68) !important;\n  }"

/***/ }),

/***/ "./src/app/report-transaction/report-transaction.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/report-transaction/report-transaction.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row mt-3\">\n    <div class=\"col-sm-12 col-12 col-md-12 col-lg-12 pl-4 pr-4\">\n      <h4>{{\"RT_H4_ALL_CASH_AND_CREDIT_ACCOUNT_TRANSACTIONS\" | translate}}</h4>\n    </div>\n  </div>\n  <div class=\"row mt-4 d-none\">\n    <div class=\"col-sm-1 col-md-1 col-lg-1 mt-2\"></div>\n    <div class=\"col-6 col-sm-4 col-md-3 col-lg-3 mt-2\">\n      <select class=\"form-control cursor-pointer\" name=\"sellist1\" (change)=\"filterForTransactions($event)\"\n              [(ngModel)]=\"selectedAccountFilter\">\n        <option value=\"all\">{{\"RT_OPTIONS_ALL ACCOUNTS\" | translate}}</option>\n        <ng-container *ngFor=\"let account of accounts\">\n          <option *ngIf=\"account.official_name && (account.status == 'ACTIVE')\" [ngValue]=\"account\"  [selected]=\"account.account_id==this.selectedAccId\">\n            {{account.inst_name}} {{account.official_name}}\n          </option>\n        </ng-container>\n      </select>\n    </div>\n    <div class=\"col-6 col-sm-4 col-md-3 col-lg-3 mt-2\">\n      <select class=\"form-control cursor-pointer\" id=\"sel2\" name=\"sellist2\" (change)=\"filterForTransactions($event)\"\n              [(ngModel)]=\"selectedDateFilter\">\n        <option *ngFor=\"let index of dateFilterArray\" [ngValue]=\"index.value\">{{index.key}}</option>\n      </select>\n    </div>\n    <div class=\"col-sm-3 col-md-3 col-lg-3 mt-2 mb-1 d-none\">\n      <select class=\"form-control custom-select\" id=\"sel3\" name=\"sellist3\" (change)=\"setDatePrefrence($event)\"\n              [(ngModel)]=\"datePrefrence\">\n        <option value=\"us\">MM-DD-YYYY</option>\n        <option value=\"uk\">DD-MM-YYYY</option>\n      </select>\n    </div>\n  </div>\n  <div *ngIf=\"(transactions && transactions.length > 0); else noDataBlock\">\n    <table class=\"table table-responsive-sm table-responsive-md table-responsive-lg table-hover mt-4\">\n      <tbody>\n      <tr class=\"bg-dark text-light\">\n        <td>\n          {{\"RT_TD_DATE\" | translate}}\n          <button class=\"dateSort\" (click) = \"orderByDate()\"><i class=\" fa fa-sort text-light\"></i></button>\n        </td>\n        <td style=\"width:225px\">{{\"RT_TD_ACCOUNT_NAME\" | translate}}</td>\n        <td style=\"width:375px\">{{\"RT_TD_DESCRIPTION\" | translate}}</td>\n        <td style=\"width:375px\">{{\"RT_TD_CATEGORY\" | translate}}</td>\n        <td>{{\"RT_TD_AMOUNT\" | translate}}</td>\n      </tr>\n      <tr *ngFor=\"let trans of transactions | orderBydate : orderByField\">\n        <td class=\"d-width\">{{trans.date}}</td>\n        <td>\n          <span placement=\"top\" popover=\"{{trans.officialName}}\" triggers=\"mouseenter:mouseleave\">\n            {{trans.officialName ? (trans.officialName.length > 25 ? trans.officialName.substring(0, 25)+\"...\" : trans.officialName) : \"updating soon...\"}}\n          </span>\n        </td>\n        <td>\n          <span placement=\"top\" popover=\"{{trans.name}}\" triggers=\"mouseenter:mouseleave\">{{trans.name.length > 35 ? trans.name.substring(0, 35)+\"...\" : trans.name}}</span>\n        </td>\n        <td>\n          <span placement=\"top\" popover=\"{{trans.category}}\" triggers=\"mouseenter:mouseleave\">{{trans.category.length > 35 ? trans.category.substring(0, 35)+\"...\": trans.category}}</span>\n        </td>\n        <td [ngClass]=\"trans.amount > 0 ? 'text-success' : 'trans-secondary'\">{{trans.amount > 0 ? (trans.amount | currency) : \"&#x2212;\" + ((trans.amount * -1) | currency)}}</td>\n      </tr>\n      </tbody>\n    </table>\n  </div>\n  \n  <ng-template #noDataBlock>\n    <div class=\"mt-4 mb-4 ml-2 blue text-center\">\n      {{\"RT_DIV_NO_BALANCE_TO_SHOW\" | translate}}\n    </div>\n  </ng-template>\n</div>\n"

/***/ }),

/***/ "./src/app/report-transaction/report-transaction.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/report-transaction/report-transaction.component.ts ***!
  \********************************************************************/
/*! exports provided: ReportTransactionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportTransactionComponent", function() { return ReportTransactionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __read = (undefined && undefined.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (undefined && undefined.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};






var ReportTransactionComponent = /** @class */ (function () {
    function ReportTransactionComponent(authService, spinnerService, backend, translate) {
        this.authService = authService;
        this.spinnerService = spinnerService;
        this.backend = backend;
        this.translate = translate;
        this.transactions = [];
        this.DATE_FORMAT = 'YYYY-MM-DD';
        this.selectedAccountFilter = 'all';
        this.selectedDateFilter = '3months';
        this.orderByField = '-date';
        this.datePrefrence = null;
        this.dateFilterArray = [
            {
                key: 'G_LAST_7_DAYS',
                value: '7days'
            },
            {
                key: 'G_LAST_14_DAYS',
                value: '14days'
            },
            {
                key: 'G_THIS_MONTH ',
                value: 'thismonth'
            },
            {
                key: 'G_LAST_3_MONTHS',
                value: '3months'
            }
        ];
        this.accounts = [];
        this.report = [];
    }
    ReportTransactionComponent.prototype.ngOnInit = function () {
        // Get transactions also
        var start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(this.report.trans_interval - 1, 'months').format(this.DATE_FORMAT);
        var end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
        this.populateTransactions(start_date, end_date, null);
        if (this.report.trans_interval === 6) {
            this.dateFilterArray.push({
                key: 'Last 6 months',
                value: '6months'
            });
            this.selectedDateFilter = null;
            this.selectedDateFilter = '6months';
        }
        else if (this.report.trans_interval === 12) {
            this.dateFilterArray.push({
                key: 'Last 6 months',
                value: '6months'
            }, {
                key: 'Last 12 months',
                value: '12months'
            });
            this.selectedDateFilter = null;
            this.selectedDateFilter = '12months';
        }
    };
    ReportTransactionComponent.prototype.populateTransactions = function (start_date, end_date, accounts) {
        var _this = this;
        this.spinnerService.show();
        console.log('populateTransactions Data', accounts);
        // Make get transactions call
        var path = '/users/transactions';
        var qp = [];
        if (start_date) {
            qp.push('start_date=' + start_date);
        }
        if (end_date) {
            qp.push('end_date=' + end_date);
        }
        if (accounts) {
            qp.push('account_ids=' + accounts);
        }
        if (qp && qp.length > 0) {
            path += '?' + qp.join('&');
        }
        console.log(path);
        this.backend.get(path, this.report.token)
            .subscribe(function (data) {
            var allTransactions = data.json();
            console.log('report-trans', allTransactions);
            var clonedTransaction = [];
            try {
                for (var _a = __values(allTransactions.transactions), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var obj = _b.value;
                    var b = null;
                    b = Object.assign({}, obj);
                    clonedTransaction.push(b);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
            // change the account value; if type depository
            if (clonedTransaction) {
                try {
                    for (var clonedTransaction_1 = __values(clonedTransaction), clonedTransaction_1_1 = clonedTransaction_1.next(); !clonedTransaction_1_1.done; clonedTransaction_1_1 = clonedTransaction_1.next()) {
                        var t = clonedTransaction_1_1.value;
                        if (t.account_type == 'depository' || t.account_type == 'credit') {
                            if (t.amount < 0) {
                                t.amount = t.amount * -1;
                            }
                            else {
                                t.amount = t.amount * -1;
                            }
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (clonedTransaction_1_1 && !clonedTransaction_1_1.done && (_d = clonedTransaction_1.return)) _d.call(clonedTransaction_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            _this.transactions = clonedTransaction;
            _this.spinnerService.hide();
            var e_1, _c, e_2, _d;
        });
    };
    ReportTransactionComponent.prototype.getstartenddate = function () {
        var start_date = '';
        var end_date = '';
        var passVar = this.selectedDateFilter;
        switch (passVar) {
            case '7days':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(6, 'days').format(this.DATE_FORMAT);
                break;
            case '12months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(11, 'months').startOf('month').format(this.DATE_FORMAT);
                break;
            case '14days':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(13, 'days').format(this.DATE_FORMAT);
                break;
            case 'thismonth':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().startOf('month').format(this.DATE_FORMAT);
                break;
            case '3months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(2, 'months').startOf('month').format(this.DATE_FORMAT);
                break;
            case '6months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(5, 'months').startOf('month').format(this.DATE_FORMAT);
                break;
            case 'thisyear':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().startOf('year').format(this.DATE_FORMAT);
                break;
            case 'lastyear':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract('1', 'year').endOf('year').format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract('1', 'year').startOf('year').format(this.DATE_FORMAT);
                break;
        }
        return { start_date: start_date, end_date: end_date };
    };
    ReportTransactionComponent.prototype.filterForTransactions = function (event) {
        this.spinnerService.show();
        this.transactions = [];
        var dates = this.getstartenddate();
        console.log("dates", dates);
        var selectedAccountsFilter = this.selectedAccountFilter;
        console.log('accounts filter', selectedAccountsFilter);
        var accountsFilter = null;
        if (selectedAccountsFilter == 'all') {
            accountsFilter = null;
        }
        else {
            accountsFilter = selectedAccountsFilter.account_id;
        }
        this.populateTransactions(dates.start_date, dates.end_date, accountsFilter);
    };
    ReportTransactionComponent.prototype.orderByDate = function () {
        if (this.orderByField && this.orderByField === '-date') {
            this.orderByField = 'date';
        }
        else {
            this.orderByField = '-date';
        }
    };
    ;
    //datePrefrence method
    ReportTransactionComponent.prototype.setDatePrefrence = function (e) {
        var _this = this;
        console.log('setDatePrefrence', this.datePrefrence);
        this.spinnerService.show();
        var data = {};
        if (this.datePrefrence) {
            data.preferences = this.datePrefrence;
        }
        var path = "/users/" + this.authService.getAccessTokenAndID().idTokenPayload.sub;
        this.backend.put(data, path, this.authService.getAccessTokenAndID().accessToken)
            .subscribe(function (res) {
            console.log('preferencesRes', res);
            var clonedTrans = __spread(_this.transactions);
            if (res.status == 204) {
                _this.transactions = [];
                try {
                    for (var clonedTrans_1 = __values(clonedTrans), clonedTrans_1_1 = clonedTrans_1.next(); !clonedTrans_1_1.done; clonedTrans_1_1 = clonedTrans_1.next()) {
                        var t = clonedTrans_1_1.value;
                        if (_this.datePrefrence == 'uk') {
                            t.date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"](t.date, 'MM-DD-YYYY').format('DD-MM-YYYY');
                        }
                        else {
                            t.date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"](t.date, 'DD-MM-YYYY').format('MM-DD-YYYY');
                        }
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (clonedTrans_1_1 && !clonedTrans_1_1.done && (_a = clonedTrans_1.return)) _a.call(clonedTrans_1);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
            }
            //update transactions
            _this.transactions = __spread(_this.transactions, clonedTrans);
            _this.spinnerService.hide();
            var e_3, _a;
        }, function (err) {
            console.log('preferenceError', err);
            _this.spinnerService.hide();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("accounts"),
        __metadata("design:type", Object)
    ], ReportTransactionComponent.prototype, "accounts", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("report"),
        __metadata("design:type", Object)
    ], ReportTransactionComponent.prototype, "report", void 0);
    ReportTransactionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-report-transaction',
            template: __webpack_require__(/*! ./report-transaction.component.html */ "./src/app/report-transaction/report-transaction.component.html"),
            styles: [__webpack_require__(/*! ./report-transaction.component.css */ "./src/app/report-transaction/report-transaction.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"], _backend_backend_service__WEBPACK_IMPORTED_MODULE_3__["BackendService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"]])
    ], ReportTransactionComponent);
    return ReportTransactionComponent;
}());



/***/ }),

/***/ "./src/app/sidebar/sidebar.component.css":
/*!***********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "li[aria-expanded=true] .fa.fa-caret-right{\n\tdisplay:none;\n}\n.collapsed .fa.fa-caret-down{\n\tdisplay:none;\n}\n.green{\n color: #46a81b !important;\n}\n.black{\n  color:black;\n}\n.div-flex{\n  display:flex;\n}\n.property-flex{\n  flex:1;\n}\n.text-right{\n  text-align:right;\n}\n.ml-10{\n  margin-left: 5%;\n}\n.main-menu-style{\n  font-size: 16px !important;\n}\n.menu-style{\n  font-size: 14px !important;\n}\n.nav-side-menu {\n  overflow: auto;\n  font-size: 12px;\n  font-weight: 400;\n  background-color: #2e353d;\n  position: fixed;\n  top: 0px;\n  /* height: 100%; */\n  color: #e1ffff;\n}\n.nav-side-menu .brand {\n  display: block;\n  font-size: 14px;\n  color: #333333;\n}\n.brand.sidebar-bg{\n  visibility: hidden;\n  line-height: 0px;\n}\n.add-account{\n  display: block;\n  font-size: 14px;\n  color: #333333;\n  padding: 12px 15px 5px 10px;\n  background-color:#DCDCDC;\n  height:43px;\n  width:25%;\n  z-index:99;\n  position: fixed;\n}\n.menu-list{\n  margin-top:46px;\n}\n.nav-side-menu .toggle-btn {\n  display: none;\n}\n.nav-side-menu ul,\n.nav-side-menu li {\n  list-style: none;\n  padding: 0px;\n  margin: 0px;\n}\n.nav-side-menu ul.menu-content > li {\n  background-color: #cccdcf;\n  padding: 7px 0px 7px 0px;\n}\n.nav-side-menu ul .sub-menu li,\n.nav-side-menu li .sub-menu li {\n  background-color: #dcdcdc;\n  border: none;\n  margin-left: 0px;\n}\n.nav-side-menu li {\n  padding-left: 0px;\n  border-top: 1px solid #23282e;\n}\n.nav-side-menu li a {\n  text-decoration: none;\n  color: #333333;\n}\n.nav-side-menu li a i.fa {\n  padding-left: 10px;\n}\n.nav-side-menu li:hover {\n  transition: all 1s ease;\n}\nbody {\n  margin: 0px;\n  padding: 0px;\n}\n.nav-side-menu .sub-menu li {\n  padding: 2px 0px 0px 18px;\n  border-left: 3px solid #2e353d;\n  border-bottom: 1px solid #23282e;\n}\n.left-margin{\n  margin-left:-6px;\n}\n.nav-side-menu .sub-menu li a {\n  text-decoration: none;\n  color: #e1ffff;\n}\n.sub-menu li a i {\n  padding-left: 10px;\n  width: 20px;\n  padding-right: 20px;\n}\n.nav-side-menu li .sub-menu li:hover {\n  border-left: 3px solid #d19b3d;\n  background-color: #4f5b69;\n  transition: all 1s ease;\n}\n@media (max-width: 767px) {\n .nav-side-menu .sub-menu {\n    position: relative;\n    width: 100%;\n  }\n  .nav-side-menu  .sub-menu .toggle-btn {\n    display: block;\n    cursor: pointer;\n    position: absolute;\n    right: 10px;\n    top: 10px;\n    z-index: 10 !important;\n    padding: 3px;\n    background-color: #ffffff;\n    color: #000;\n    width: 40px;\n    text-align: center;\n  }\n\n  .nav-side-menu  .toggle-btn{\n    right: 0px;\n    top: 61px;\n    height: 45px;\n   }\n  .sub-line ul .sub-press li,\n  .sub-line li .sub-press li {\n    background-color: #181c20;\n    border: none;\n    border-bottom: 1px solid #23282e;\n    margin-left: 0px;\n  }\n  .sub-line ul .sub-press li:hover,\n  .sub-line li .sub-press li:hover {\n    background-color: #020203;\n  }\n  .sub-line ul .sub-press li:before,\n  .sub-line li .sub-press li:before {\n    content: \"\\f105\";\n    display: inline-block;\n    padding-left: 50px;\n    padding-right: 10px;\n    vertical-align: middle;\n  }\n\n  .sub-line li {\n    padding-left: 20px;\n    border-left: 3px solid #2e353d;\n    border-bottom: 1px solid #23282e;\n  }\n  .sub-line li a {\n    text-decoration: none;\n    color: #e1ffff;\n  }\n  .sub-line li a i {\n    padding-left: 50px;\n    width: 20px;\n    padding-right: 20px;\n  }\n  .sub-line li:hover {\n    border-left: 3px solid #d19b3d;\n    background-color: #4f5b69;\n    transition: all 1s ease;\n  }\n\n  .sub-line {\n    position: relative;\n    width: 100%;\n    margin-bottom: 10px;\n  }\n  .sub-line .toggle-btn {\n    display: block;\n    cursor: pointer;\n    position: absolute;\n    right: 10px;\n    top: 10px;\n    z-index: 10 !important;\n    padding: 3px;\n    background-color: #ffffff;\n    color: #000;\n    width: 40px;\n    text-align: center;\n  }\n  .ml-10{\n    margin-left: 2%;\n  }\n}\n/*custom css*/\n.custom-nav-side{\n  top: auto !important;\n  position: unset !important;\n}\n.sidebar-bg{\n  background: #dcdcdc;\n  margin-top:31px;\n}\n.right-align{\n  float:right;\n  margin-right:6px;\n}\n.accounts-details{\n  margin-bottom: 0.5rem;\n}\n.accounts-details td{\n  padding-top: 5px;\n  padding-bottom: 5px;\n}\n.accounts-details tr td:first-child {\n  padding-left:22px;\n  white-space: nowrap;\n}\n.accounts-details tr td:last-child {\n  float: right;\n  padding-right: 9px;\n}\n.accounts-details tr:hover{\n  background: #cccdcf;\n}\n.sub-menu{\n  color:#333333;\n}\n.right-amount{\n  text-align: right;\n  padding-left: 9px;\n}\ndiv.overflow{\n  white-space: nowrap; \n  overflow: hidden;\n  text-overflow:ellipsis;\n}\n.dropdown-menu.dropdown-position {\n  /* position: relative !important; */\n  will-change: unset !important;\n  -webkit-transform: unset !important;\n          transform: unset !important;\n}\n@media only screen and (max-width: 767px) {\n  .sidebar-bg{\n    background: unset;\n    margin-top:0px;\n  }\n  .add-account{\n    width:100%;\n    top:106px;\n  }\n  .margin-sidebar{\n    margin-top:53px !important;\n  }\n  .text-wrap{\n    overflow: auto !important;\n    white-space: nowrap !important;\n  }\n  .fixed-sidebar{\n    position:fixed;\n    width:100%;\n    z-index:999;\n  }\n}\n.top-0{\n  top: unset!important;\n}\n@media (max-width: 767px) {\n  .nav-side-menu {\n    position: relative;\n    width: 100%;\n    margin-bottom: 10px;\n  }\n  .nav-side-menu .toggle-btn {\n    display: block;\n    cursor: pointer;\n    position: fixed;\n    right: 0px;\n    top: 61px;\n    z-index: 10 !important;\n    padding: 3px;\n    background-color: #ffffff;\n    color: #000;\n    width: 40px;\n    text-align: center;\n    align-items: center;\n    justify-content: center;\n    display:flex;\n  }\n  .brand {\n    text-align: left !important;\n    font-size: 22px;\n    padding-left: 20px;\n    line-height: 50px !important;\n  }\n  .media-left{\n    left: -150px !important;\n  }\n}\n@media (min-width: 768px) {\n  .nav-side-menu .menu-list .menu-content {\n    display: block;\n  }\n  div.overflow{\n    width: 70px;\n  }\n}\n@media (min-width: 955px) {\n  div.overflow{\n    width: 94px;\n  }\n}\n@media (min-width: 1090px) {\n  div.overflow{\n    width: 120px;\n  }\n}\n@media (min-width: 1216px) {\n  div.overflow{\n    width: 160px;\n  }\n}\n@media only screen and (min-width: 767px) and (max-width: 998px){\n .font-sidebr {\n      font-size:13px !important;\n  }\n}\n@media only screen and (min-width: 601px) and (max-width: 767px){\n  .toggle-top{\n    top:63px;\n    position: fixed;\n    right: 0px;\n  }\n}\n@media only screen and (max-width: 700px){\n  .toggle-top{\n    top:61px;\n    right:-3px;\n  }\n}\n.hide-scroll{\n    overflow-x: hidden;\n  }\n@media only screen and (max-width: 575px) and (min-width: 320px){\n    .add-account{\n      top:161px;\n    }\n    .menu-list{\n      margin-top:101px;\n    }\n\n  }\n\n"

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.html":
/*!************************************************!*\
  !*** ./src/app/sidebar/sidebar.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"nav-side-menu custom-nav-side sidebar-bg\">\n  <div class=\"brand sidebar-bg\">{{ \"SB_DIV_PRESS_CURING_CONTROL\" | translate}}</div>\n  <i class=\"fa fa-bars fa-2x  toggle-btn toggle-top\" data-toggle=\"collapse\" data-target=\"#menu-content\"></i>\n\n  <div class=\"add-account\">{{\"SB_DIV_ACCOUNT\" | translate}} \n    <span class=\"custom-float-right cursor-pointer font-weight-bold dropdown\">        \n      <i class=\"fa fa-cog\" id=\"dropdownMenuButton\" data-target=\"dropdownnotifyButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"></i>\n      <div id=\"dropdownnotifyButton\" class=\"dropdown-menu top-0 dropdown-position ml-2 media-left\" aria-labelledby=\"dropdownMenuButton\">\n        <a class=\"dropdown-item cursor-pointer pl-1 pr-1\" (click)=\"multipleHandler('callingAddAccountBtn')\">{{\"SB_A_ADD_ACCOUNTS\" | translate}}</a>\n        <a class=\"dropdown-item cursor-pointer pl-1 pr-1\" (click)=\"multipleHandler('manageAccountSection')\">{{\"SB_A_MANAGE_ACCOUNTS\"| translate}}</a>\n      </div>\n    </span>  \n  </div>\n\n  <div class=\"menu-list hide-scroll\">\n    <ul id=\"menu-content\" class=\"menu-content collapse out margin-sidebar fixed-sidebar\">\n      <!--Bank Accounts -->\n      <li data-toggle=\"collapse\" data-target=\"#products\" class=\"collapsed cursor-pointer col-md-12 div-flex text-wrap\">\n        <a class=\"main-menu-style col-sm-7 col-md-5 col-lg-7 col-xl-8 property-flex font-sidebr\">\n          <i class=\"fa fa-caret-right\"></i><i class=\"fa fa-caret-down\"></i>\n          <i class=\"fa fa-money fa-lg mr-2 pl-2 font-icon\" aria-hidden=\"true\"></i>{{\"SB_A_CASH\" | translate}}\n        </a>\n        <span class=\"custom-float-right green pr-2 col-sm-5 col-md-7 col-lg-5 col-xl-4 text-right font-icon\">\n          {{totalBal.cash | currency}}\n        </span>\n      </li>\n      <ul class=\"sub-menu collapse\" id=\"products\">\n        <li *ngFor=\"let accountsByInst of allAccounts; let parentIndex = index\">\n          <span *ngIf=\"accountsByInst.testDepos\">\n            <span class=\"left-margin\">\n              <img *ngIf=\"(accountsByInst.sync_status)\" src=\"{{statusToIconMapping[accountsByInst.sync_status]}}\" alt=\"\" width=\"22\" height=\"22\" />\n              <img *ngIf=\"!(accountsByInst.sync_status)\" src=\"{{statusToIconMapping['inprogress']}}\" alt=\"\" width=\"22\" height=\"22\" />\n            </span>\n            <span class=\"menu-collapsed ml-1 menu-style\">\n              <u>{{accountsByInst.inst_name}}</u>\n            </span>\n          </span>\n          <div class=\"table-responsive\">\n            <table class=\"table table-borderless accounts-details\">\n              <tbody>\n                <tr class=\"cursor-pointer\" *ngFor=\"let depository of accountsByInst.accounts; let childIndex = index\">\n                  <span *ngIf=\"depository.type == 'depository'\">\n                    <span (click)=\"emitAccount(depository.account_id)\" *ngIf=\"depository.status == 'ACTIVE'\">\n                      <td *ngIf=\"!depository.matchOfficialName\">\n                        <div class=\"overflow\" *ngIf=\"depository.official_name\" placement=\"right\" popover=\"{{depository.official_name}}\" triggers=\"mouseenter:mouseleave\">\n                          {{depository.official_name}}\n                        </div>\n                      </td>\n                      <td *ngIf=\"depository.matchOfficialName\">\n                        <div class=\"overflow\" *ngIf=\"depository.name\" placement=\"right\" popover=\"{{depository.name}}\" triggers=\"mouseenter:mouseleave\">\n                          {{depository.name}}\n                        </div>\n                      </td>\n                      <td class=\"green d-inline-block\">{{depository.balances.current | currency}}</td>\n                    </span>\n                  </span>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </li>\n      </ul>\n    \n      <!-- Credit Cards -->\n      <li data-toggle=\"collapse\" data-target=\"#service\" class=\"collapsed cursor-pointer col-md-12 div-flex text-wrap\">\n        <a class=\"main-menu-style col-sm-7 col-md-5 col-lg-7 col-xl-8 property-flex font-sidebr\">\n          <i class=\"fa fa-caret-right\"></i><i class=\"fa fa-caret-down\"></i>\n          <i class=\"fa fa-credit-card fa-lg mr-2 pl-2 font-icon\" aria-hidden=\"true\"></i>{{\"SB_A_CREDIT_CARDS\" | translate}}\n        </a>\n        <span class=\"custom-float-right pr-2 col-sm-5 col-md-7 col-lg-5 col-xl-4 black text-right font-icon\">{{totalBal.credit | currency}}</span>\n      </li>\n      <ul class=\"sub-menu collapse\" id=\"service\">\n        <li *ngFor=\"let accountsByInst of allAccounts; let parentIndex = index\">\n          <span *ngIf=\"accountsByInst.testCredit\">\n            <span class=\"left-margin\">\n              <img *ngIf=\"(accountsByInst.sync_status)\" src=\"{{statusToIconMapping[accountsByInst.sync_status]}}\" alt=\"\" width=\"22\" height=\"22\" />\n              <img *ngIf=\"!(accountsByInst.sync_status)\" src=\"{{statusToIconMapping['inprogress']}}\" alt=\"\" width=\"22\" height=\"22\" />\n            </span>\n            <span class=\"menu-collapsed menu-style ml-1\">\n              <u>{{accountsByInst.inst_name}}</u>\n            </span>\n         </span>\n          <div class=\"table-responsive\">\n            <table class=\"table table-borderless accounts-details\">\n              <tbody>\n                <tr class=\"cursor-pointer\" *ngFor=\"let credit of accountsByInst.accounts; let childIndex = index\">\n                  <span *ngIf=\"credit.type == 'credit'\">\n                    <span (click)=\"emitAccount(credit.account_id)\" *ngIf=\"credit.status == 'ACTIVE'\">\n                      <td>\n                        <div class=\"overflow\" *ngIf=\"credit.official_name\" placement=\"right\" popover=\"{{credit.official_name}}\" triggers=\"mouseenter:mouseleave\">\n                          {{credit.official_name}}\n                        </div>\n                      </td>\n                      <td class=\"d-inline-block\">\n                        {{credit.balances.current | currency}}\n                      </td>\n                    </span>\n                  </span>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </li>\n      </ul>\n\n      <!-- Investments -->\n      <li data-toggle=\"collapse\" data-target=\"#investments\" class=\"collapsed cursor-pointer col-md-12 div-flex text-wrap\">\n        <a class=\"main-menu-style col-sm-5 col-md-5 col-lg-7 col-xl-8 property-flex font-sidebr\">\n          <i class=\"fa fa-caret-right\"></i><i class=\"fa fa-caret-down\"></i>\n          <i class=\"fas fa-hand-holding-usd fa-lg mr-2 pl-2\" aria-hidden=\"true\"></i>{{\"SB_A_INVESTMENTS\" | translate}}\n        </a>\n        <span class=\"custom-float-right pr-2 col-sm-7 col-md-7 col-lg-5 col-xl-4 black text-right font-icon\">\n          {{totalBal.investments | currency}}\n        </span>\n      </li>\n      <ul class=\"sub-menu collapse\" id=\"investments\">\n        <li class=\"mb-2 d-none\" (click)=\"openAddPropertyModal(AddPropertyTemplate, 'INV_MODAL')\">\n          <span class=\"menu-collapsed menu-style ml-3 cursor-pointer\">+{{\"SB_SPAN_ADD_INVESTMENT\" | translate}}</span>\n        </li>\n        <li *ngFor=\"let accountsByInst of allAccounts; let parentIndex = index\">\n          <span *ngIf=\"accountsByInst.testInvestments\">\n            <span class=\"left-margin\">\n              <img *ngIf=\"(accountsByInst.sync_status)\" src=\"{{statusToIconMapping[accountsByInst.sync_status]}}\" alt=\"\" width=\"22\" height=\"22\" />\n              <img *ngIf=\"!(accountsByInst.sync_status)\" src=\"{{statusToIconMapping['inprogress']}}\" alt=\"\" width=\"22\" height=\"22\" />\n            </span>\n            <span class=\"menu-collapsed menu-style ml-1\">\n              <u>{{accountsByInst.inst_name}}</u>\n            </span>\n          </span>\n          <div class=\"table-responsive\">\n            <table class=\"table table-borderless accounts-details\">\n              <tbody>\n                <tr class=\"cursor-pointer\" *ngFor=\"let invest of accountsByInst.accounts; let childIndex = index\">\n                  <span *ngIf=\"invest.type == 'brokerage'\">\n                    <span (click)=\"emitAccount(invest.account_id)\" *ngIf=\"invest.status == 'ACTIVE'\">\n                      <td>\n                        <div class=\"overflow\" *ngIf=\"invest.official_name\" placement=\"right\" popover=\"{{invest.official_name}}\" triggers=\"mouseenter:mouseleave\">\n                          {{invest.official_name}}\n                        </div>\n                      </td>\n                      <td class=\"d-inline-block\">\n                        {{invest.balances.current | currency}}\n                      </td>\n                    </span>\n                  </span>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </li>\n      </ul>\n\n      <!-- Loans -->\n      <li data-toggle=\"collapse\" data-target=\"#loans\" class=\"collapsed cursor-pointer col-md-12 div-flex text-wrap\">\n        <a class=\"main-menu-style col-sm-7 col-md-5 col-lg-7 col-xl-8 property-flex font-sidebr\">\n          <i class=\"fa fa-caret-right\"></i><i class=\"fa fa-caret-down\"></i>\n          <i class=\"fa fa-usd fa-lg mr-2 pl-2\" aria-hidden=\"true\"></i>{{\"SB_A_LOANS\" | translate}}\n        </a>\n        <span class=\"custom-float-right pr-2 col-sm-5 col-md-7 col-lg-5 col-xl-4 black text-right font-icon\">\n          {{totalBal.loan | currency}}\n      </span>\n      </li>\n      <ul class=\"sub-menu collapse\" id=\"loans\">\n           <li class=\"mb-2 d-none\" (click)=\"openAddPropertyModal(AddPropertyTemplate, 'LOAN_MODAL')\">\n              <span class=\"menu-collapsed menu-style ml-3 cursor-pointer\">+{{\"SB_SPAN_ADD_LOAN\" | translate}}</span>\n            </li> \n        <span *ngIf=\"loanAccountNotExist\">\n          <li *ngFor=\"let accountsByInst of allAccounts; let parentIndex = index\">\n              <span *ngIf=\"accountsByInst.testCredit\">\n                <span class=\"left-margin\">\n                  <img *ngIf=\"(accountsByInst.sync_status)\" src=\"{{statusToIconMapping[accountsByInst.sync_status]}}\" alt=\"\" width=\"22\" height=\"22\" />\n                  <img *ngIf=\"!(accountsByInst.sync_status)\" src=\"{{statusToIconMapping['inprogress']}}\" alt=\"\" width=\"22\" height=\"22\" />\n                </span>\n                <span class=\"menu-collapsed menu-style ml-1\">\n                  <u>{{accountsByInst.inst_name}}</u>\n                </span>\n              </span>\n              <div class=\"table-responsive\">\n                <table class=\"table table-borderless accounts-details\">\n                  <tbody>\n                    <tr class=\"cursor-pointer\" *ngFor=\"let loan of accountsByInst.accounts; let childIndex = index\">\n                       <span *ngIf=\"loan.type == 'loan'\"> \n                        <span (click)=\"emitAccount(loan.account_id)\" *ngIf=\"loan.status == 'ACTIVE'\">\n                          <td>\n                            <div class=\"overflow\" *ngIf=\"loan.official_name\" placement=\"right\" popover=\"{{loan.official_name}}\" triggers=\"mouseenter:mouseleave\">\n                              {{loan.official_name}}\n                            </div>\n                          </td>\n                          <td class=\"d-inline-block\">\n                            {{loan.balances.current | currency}}\n                          </td>\n                        </span>\n                       </span>\n                      <span *ngIf=\"!loan.type == 'loan'\">\n\n                      </span>\n                    </tr>\n                  </tbody>\n                </table>\n              </div>\n          </li>\n        </span>\n        <!-- <span *ngIf=\"!loanAccountNotExist\">\n          <div class=\"text-center\">{{\"SB_DIV_COMING_SOON\" | translate}}</div>\n        </span> -->\n      </ul>\n\n      <!-- Property -->\n      <li data-toggle=\"collapse\" data-target=\"#property\" class=\"collapsed cursor-pointer col-md-12 div-flex text-wrap\">\n        <a class=\"main-menu-style col-sm-7 col-md-5 col-lg-7 col-xl-8 property-flex font-sidebr\">\n          <i class=\"fa fa-caret-right\"></i><i class=\"fa fa-caret-down\"></i>\n          <i class=\"fa fa-building fa-lg mr-2 pl-2\" aria-hidden=\"true\"></i>{{\"SB_A_PROPERTY\" | translate}}\n        </a>\n        <span class=\"custom-float-right green pr-2 col-sm-5 col-md-7 col-lg-5 col-xl-4 text-right font-icon\">\n          {{totalBal.property | currency}}\n        </span>\n      </li>\n      <ul class=\"sub-menu collapse\" id=\"property\">\n         <li #invokeProperty (click)=\"openAddPropertyModal(AddPropertyTemplate, 'PROP_MODAL')\">\n          <span class=\"menu-collapsed menu-style ml-3 cursor-pointer\"> + {{\"SB_SPAN_ADD_A_HOUSE_OR_CAR\" | translate}}</span>\n        </li>\n        <li *ngFor=\"let property of allProperty\">\n          <div class=\"ml-10 menu-style\"><u>{{property.name}}</u> \n            <span class=\"float-right green pr-2\">{{property.estimated_value | currency}}</span>\n          </div>\n          <span class=\"ml-10\">{{property.type}} <div class=\"ns-text-overflow-ellipsis\" *ngIf=\"property.subtype\">({{property.subtype}})</div></span>\n        </li>\n      </ul>\n    </ul>\n  </div>\n</div>\n\n<!--Add Property modal-->\n<ng-template #AddPropertyTemplate>\n <span *ngIf=\"modalFlag.prop\">\n    <app-add-property-modal (childEvent)=\"addPropertyModalClose()\" (propertiesChild)=\"modalCloseAfterGet($event)\"></app-add-property-modal>\n </span>\n <span *ngIf=\"modalFlag.loan\">\n    <app-add-loan-modal></app-add-loan-modal>\n </span>\n <span *ngIf=\"modalFlag.investment\">\n    <app-add-investment-modal></app-add-investment-modal>\n </span>\n</ng-template>\n<!--End Edit modal-->\n"

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.ts ***!
  \**********************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-web-storage */ "./node_modules/angular-web-storage/fesm5/angular-web-storage.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};






var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(session, modalService, notifierService, spinnerService, authService) {
        this.session = session;
        this.modalService = modalService;
        this.notifierService = notifierService;
        this.spinnerService = spinnerService;
        this.authService = authService;
        this.accEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.propertyDataEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.multipleHandlerEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.booleanValueForSpinner = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.accounts = {};
        this.totalBal = {};
        this.allProperty = [];
        this.modalFlag = { prop: false, loan: false, investment: false };
        this.statusToIconMapping = {
            completed: '../assets/success.png',
            failed: '../assets/close-icon.png',
            inprogress: '../assets/loading-icon.gif'
        };
        this.notifier = notifierService;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        this.user = this.authService.getUserProfile();
        this.allAccounts = [];
        this.allAccounts = this.user.accounts;
        this.allProperty = this.user.properties;
        this.totalBal = { cash: 0, credit: 0, property: 0, loan: 0, investments: 0 };
        console.log('updated');
        if (this.allAccounts && this.allAccounts.length > 0) {
            try {
                for (var _a = __values(this.allAccounts), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var acc = _b.value;
                    var checkPoint = void 0;
                    if (!(acc.sync_status)) {
                        checkPoint = true;
                    }
                    else {
                        checkPoint = false;
                    }
                    this.booleanValueForSpinner.emit(checkPoint);
                    // check official name duplicate or not
                    if ('accounts' in acc && acc.accounts) {
                        for (var i = 0; i < acc.accounts.length; i++) {
                            if (acc.accounts[i].type == "depository") {
                                for (var j = 0; j < acc.accounts.length; j++) {
                                    if (acc.accounts[j].type == "depository") {
                                        if (!(i === j)) {
                                            if (!(acc.accounts[i].official_name === acc.accounts[j].official_name)) {
                                                if (!(acc.accounts[i].matchOfficialName)) {
                                                    acc.accounts[i].matchOfficialName = false;
                                                }
                                            }
                                            else {
                                                acc.accounts[i].matchOfficialName = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        try {
                            //allAccounts balance
                            for (var _c = __values(acc.accounts), _d = _c.next(); !_d.done; _d = _c.next()) {
                                var i = _d.value;
                                // depository type balance
                                if (i.type == "depository" && (i.status == 'ACTIVE')) {
                                    this.totalBal.cash += i.balances.current;
                                    acc.testDepos = true;
                                }
                                // credit type balance
                                if (i.type == "credit" && (i.status == 'ACTIVE')) {
                                    if ("current" in i.balances || i.balances.current == 0) {
                                        this.totalBal.credit += i.balances.current;
                                        acc.testCredit = true;
                                    }
                                }
                                // investments type balance
                                if (i.type == "brokerage" && (i.status == 'ACTIVE')) {
                                    if ("current" in i.balances || i.balances.current == 0) {
                                        this.totalBal.investments += i.balances.current;
                                        acc.testInvestments = true;
                                    }
                                }
                                // loan type balance
                                if (i.type == "loan" && (i.status == 'ACTIVE')) {
                                    if ('official_name' in i && !(i.official_name) && 'inst_name' in i && i.inst_name) {
                                        i.official_name = i.inst_name;
                                    }
                                    if ("current" in i.balances || i.balances.current == 0) {
                                        this.totalBal.loan += i.balances.current;
                                        acc.testloan = true;
                                        this.loanAccountNotExist = true;
                                    }
                                }
                                else {
                                    this.loanAccountNotExist = false;
                                }
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (_d && !_d.done && (_e = _c.return)) _e.call(_c);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_f = _a.return)) _f.call(_a);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        // properties balance
        if (this.allProperty && this.allProperty.length > 0) {
            try {
                for (var _g = __values(this.allProperty), _h = _g.next(); !_h.done; _h = _g.next()) {
                    var i = _h.value;
                    this.totalBal.property += i.estimated_value;
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (_h && !_h.done && (_j = _g.return)) _j.call(_g);
                }
                finally { if (e_3) throw e_3.error; }
            }
        }
        var e_2, _f, e_1, _e, e_3, _j;
    };
    SidebarComponent.prototype.ngOnChanges = function (changes) {
        console.log('changes', changes);
        if (('reloadScreen' in changes && changes.reloadScreen != null) || ('sidebarReload' in changes && changes.sidebarReload != null)) {
            this.ngOnInit();
        }
        if ('callPropertyButton' in changes && changes.callPropertyButton && 'currentValue' in changes.callPropertyButton && changes.callPropertyButton.currentValue) {
            this.invokeProperty.nativeElement.click();
        }
    };
    SidebarComponent.prototype.emitAccount = function (accId) {
        this.accEmit.emit(accId);
    };
    SidebarComponent.prototype.openAddPropertyModal = function (addPropertyModal, event) {
        var ngbModalOptions = {
            class: "gray modal-lg"
        };
        if (event == 'PROP_MODAL') {
            this.modalFlag.prop = true;
            this.modalFlag.investment = false;
            this.modalFlag.loan = false;
            ngbModalOptions['backdrop'] = 'static';
            ngbModalOptions['keyboard'] = false;
        }
        if (event == 'INV_MODAL') {
            this.modalFlag.investment = true;
            this.modalFlag.prop = false;
            this.modalFlag.loan = false;
        }
        if (event == 'LOAN_MODAL') {
            this.modalFlag.loan = true;
            this.modalFlag.prop = false;
            this.modalFlag.investment = false;
        }
        this.modalRef = this.modalService.show(addPropertyModal, ngbModalOptions);
    };
    SidebarComponent.prototype.addPropertyModalClose = function () {
        this.modalRef.hide();
    };
    SidebarComponent.prototype.modalCloseAfterGet = function (event) {
        this.modalRef.hide();
        if (event) {
            this.allProperty = event;
            this.propertyDataEmit.emit(event);
            this.ngOnInit();
        }
    };
    SidebarComponent.prototype.getKeys = function (map) {
        return Array.from(Object.keys(map));
    };
    SidebarComponent.prototype.multipleHandler = function (refrenceName) {
        this.multipleHandlerEmit.emit(refrenceName);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("reloadScreen"),
        __metadata("design:type", Object)
    ], SidebarComponent.prototype, "reloadScreen", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("sidebarReload"),
        __metadata("design:type", Object)
    ], SidebarComponent.prototype, "sidebarReload", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("callPropertyButton"),
        __metadata("design:type", Object)
    ], SidebarComponent.prototype, "callPropertyButton", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SidebarComponent.prototype, "accEmit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SidebarComponent.prototype, "propertyDataEmit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SidebarComponent.prototype, "multipleHandlerEmit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SidebarComponent.prototype, "booleanValueForSpinner", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('invokeProperty'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SidebarComponent.prototype, "invokeProperty", void 0);
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.css */ "./src/app/sidebar/sidebar.component.css"), __webpack_require__(/*! ../global/global.css */ "./src/app/global/global.css")]
        }),
        __metadata("design:paramtypes", [angular_web_storage__WEBPACK_IMPORTED_MODULE_1__["SessionStorageService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__["BsModalService"],
            angular_notifier__WEBPACK_IMPORTED_MODULE_3__["NotifierService"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"],
            _auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/signuppage/signuppage.component.css":
/*!*****************************************************!*\
  !*** ./src/app/signuppage/signuppage.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/signuppage/signuppage.component.html":
/*!******************************************************!*\
  !*** ./src/app/signuppage/signuppage.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"nav justify-content-end\">\n  <li class=\"nav-item pr-5 mt-2 mb-2\">\n    <button class=\"btn btn-success\">{{\"SP_BUTTON_LOGIN /SIGN IN\" | translate}}</button>\n  </li>\n</ul>\n"

/***/ }),

/***/ "./src/app/signuppage/signuppage.component.ts":
/*!****************************************************!*\
  !*** ./src/app/signuppage/signuppage.component.ts ***!
  \****************************************************/
/*! exports provided: SignuppageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignuppageComponent", function() { return SignuppageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SignuppageComponent = /** @class */ (function () {
    function SignuppageComponent() {
    }
    SignuppageComponent.prototype.ngOnInit = function () {
    };
    SignuppageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signuppage',
            template: __webpack_require__(/*! ./signuppage.component.html */ "./src/app/signuppage/signuppage.component.html"),
            styles: [__webpack_require__(/*! ./signuppage.component.css */ "./src/app/signuppage/signuppage.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SignuppageComponent);
    return SignuppageComponent;
}());



/***/ }),

/***/ "./src/app/transactions-modal/transactions-modal.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/transactions-modal/transactions-modal.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table td{\n    border-top:1px solid #999999;\n    border-left:1px solid #999999;\n    padding: 0.3rem;\n    white-space: nowrap;\n  }\n  .table tr:last-child td{\n    border-bottom:1px solid #999999;\n  }\n  .table tr td:last-child{\n    border-right:1px solid #999999;\n  }\n  .table-striped tbody tr:first-child {\n    background-color: #E1E1E1 !important;\n  }\n  .table-striped tbody tr:nth-child(odd) {\n    background-color: #F7F7F7;\n  }\n  .table-striped tbody tr:nth-child(even) {\n    background-color: #ffffff;\n  }\n  .table-hover.table-striped tbody tr:hover {\n    background-color: #E1E1E1;\n  }\n  .table.table-hover .scroll-tbl{\n    overflow-y: auto;\n    max-height: 360px;\n    display: block;\n  }\n  .checkbox-top{\n    top:0px;\n  }\n  .form-group{\n    margin-bottom:0rem;\n  }\n  .dateSort{\n    float: right;\n    width: 23px;\n    height: 26px;\n    cursor: pointer;\n    background: none;\n    border: none;\n  }\n  "

/***/ }),

/***/ "./src/app/transactions-modal/transactions-modal.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/transactions-modal/transactions-modal.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\" *ngIf=\"transactions && transactions.length > 0\">\n  <h4 class=\"modal-title ml-3\">{{\"TM_H4_MATCHING_TRANSACTIONS\" | translate}}</h4>\n  <button type=\"button\" class=\"close\" (click)=\"modalRef.hide()\">&times;</button>\n</div>\n\n<div class=\"modal-body mt-0 pt-0\" *ngIf=\"transactions && transactions.length > 0\">\n  <div class=\"container\">\n    <table class=\"table table-striped table-responsive-sm table-responsive-md table-responsive-lg table-hover mt-4\">\n      <tbody class=\"scroll-tbl\">\n      <tr>\n        <td class=\"d-none\">\n          <div class=\"form-group form-check mb-0\">\n            <label class=\"form-check-label\">\n              <input class=\"form-check-input checkbox-top\" type=\"checkbox\">\n            </label>\n          </div>\n        </td>\n        <td style=\"min-width:100px\">{{\"TM_TD_DATE\" | translate}} <button class=\"dateSort\" (click)=\"orderByDate()\"><i class=\" fa fa-sort\"></i></button></td>\n        <td style=\"min-width:240px\">{{\"TM_TD_DESCRIPTION\" | translate}}</td>\n        <td style=\"min-width:240px\">{{\"TM_TD_CATEGORY\" | translate}}</td>\n        <td style=\"min-width:115px\">{{\"TM_TD_AMOUNT\" | translate}}</td>\n      </tr>\n      <tr class=\"bg-dark d-none\">\n        <td>\n          <div class=\"form-group form-check mb-0\">\n            <label class=\"form-check-label\">\n              <input class=\"form-check-input checkbox-top\" type=\"checkbox\">\n            </label>\n          </div>\n        </td>\n        <td class=\"pt-1 pr-1 pb-1 pl-1\"><input type=\"text\" [ngModel]= \"transactions[0].date | date: 'MMM dd'\" class=\"form-control\" id=\"date\"></td>\n        <td class=\"pt-1 pr-1 pb-1 pl-1\"><input type=\"text\" [ngModel]= \"transactions[0].name\" class=\"form-control\" id=\"desc\"></td>\n        <td class=\"pt-1 pr-1 pb-1 pl-1\"><input type=\"text\" [ngModel]= \"transactions[0].category\" class=\"form-control\" id=\"category\"></td>\n        <td class=\"text-right text-white\">$0.00</td>\n      </tr>\n      <tr *ngFor=\"let trans of transactions | orderBydate : orderByField\">\n        <td class=\"d-none\">\n          <div class=\"form-group form-check\">\n            <label class=\"form-check-label\">\n              <input class=\"form-check-input checkbox-top\" type=\"checkbox\">\n            </label>\n          </div>\n        </td>\n        <td>{{trans.date | date: 'MMM dd yyyy'}}</td>\n        <td>\n          <span placement=\"top\" popover=\"{{trans.name}}\" triggers=\"mouseenter:mouseleave\">\n            {{trans.name.length > 22 ? trans.name.substring(0, 22)+\"...\" : trans.name}}\n          </span>\n        </td>\n        <td>\n          <span placement=\"top\" popover=\"{{trans.category}}\" triggers=\"mouseenter:mouseleave\">\n            {{trans.category.length > 22 ? trans.category.substring(0, 22)+\"...\" : trans.category}}\n          </span>\n        </td>\n        <td class=\"text-right\" [ngClass]=\"trans.amount < 0 ? 'text-success' : 'trans-secondary'\">\n          {{(trans.amount || trans.amount == 0) ? ((trans.amount) > 0 ? ('&#x2212;'+ ((trans.amount) | currency )) : (trans.amount * -1) | currency): trans.estimated_value | currency}}\n        </td>\n      </tr>\n      </tbody>\n    </table>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/transactions-modal/transactions-modal.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/transactions-modal/transactions-modal.component.ts ***!
  \********************************************************************/
/*! exports provided: TransactionsModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsModalComponent", function() { return TransactionsModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TransactionsModalComponent = /** @class */ (function () {
    function TransactionsModalComponent(modalRef) {
        this.modalRef = modalRef;
        this.transactions = [];
        this.orderByField = '-date';
    }
    TransactionsModalComponent.prototype.ngOnInit = function () {
        console.log('inside TransactionsModalComponent');
        console.log('transactionsModal', this.transactions);
    };
    TransactionsModalComponent.prototype.orderByDate = function () {
        if (this.orderByField && this.orderByField === '-date') {
            this.orderByField = 'date';
        }
        else {
            this.orderByField = '-date';
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], TransactionsModalComponent.prototype, "transactions", void 0);
    TransactionsModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-transactions-modal',
            template: __webpack_require__(/*! ./transactions-modal.component.html */ "./src/app/transactions-modal/transactions-modal.component.html"),
            styles: [__webpack_require__(/*! ./transactions-modal.component.css */ "./src/app/transactions-modal/transactions-modal.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalRef"]])
    ], TransactionsModalComponent);
    return TransactionsModalComponent;
}());



/***/ }),

/***/ "./src/app/transactions/transactions.component.css":
/*!*********************************************************!*\
  !*** ./src/app/transactions/transactions.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table td{\n    border-top:1px solid #999999;\n    border-left:1px solid #999999;\n    white-space: nowrap;\n  }\n  .table tr:last-child td{\n    border-bottom:1px solid #999999;\n    padding: 6px 3px 6px 3px !important;\n  }\n  .table tr td:last-child{\n    border-right:1px solid #999999;\n  }\n  .table-hover.table-striped tbody tr:hover {\n    background-color: #E1E1E1;\n  }\n  .inactive-msg{\n    color:blue;\n    position: absolute;\n    top: 50%;\n    left:50%;\n    text-align: center;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n  }\n  .blue{\n    color:blue;\n    position: absolute;\n    top: 60%;\n    left:50%;\n    text-align: center;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n  }\n  .table-padding{\n    padding-right:30px;\n  }\n  .table-width{\n    width: 98%;\n    margin-bottom: 1rem;\n    background-color: transparent;\n  }\n  .dateSort{\n    float: right;\n    width: 23px;\n    height: 26px;\n    cursor: pointer;\n    background: none;\n    border: none;\n  }\n  .parent-date{\n    width:88px;\n  }\n  .amount-data{\n    width:100px;\n  }\n  .d-width{\n    min-width: 97px;\n  }\n  .prefrences{\n    height: 50px;\n    margin: 20px;\n    line-height: 50px;\n    padding:50px\n  }\n  pagination-controls{\n    text-align: center;\n  }\n  .search-text span{\n    position: absolute;\n    z-index: 99;\n    margin: 6px;\n  }\n  .search-text input{\n    padding-left: 26px;\n    border-radius: .25rem;\n  }\n  .table td{\n    font-size: 15px;\n    padding: 0 3px 0 3px !important\n  }\n  .trans-secondary{\n    color:rgb(68,68,68) !important;\n  }\n\n"

/***/ }),

/***/ "./src/app/transactions/transactions.component.html":
/*!**********************************************************!*\
  !*** ./src/app/transactions/transactions.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"accounts.length > 0\">\n<div *ngIf=\"(!firstTimeWarning); else anotherOne\">\n  <div class=\"row\">\n    <div class=\"col-12\">\n      <h6>{{\"T_H4_ALL_CASH_CREDIT_ACCOUNTS\" | translate}}</h6>\n    </div>\n    <div class=\"row col-md-12\">\n      <div class=\"col-md-3 col-6 col-sm-4 col-lg-3\">{{\"T_DIV_FROM\" | translate}}</div>\n      <div class=\"col-md-3 col-6 col-sm-4 col-lg-3\">{{\"T_DIV_DURING\" | translate}}</div>\n    </div>\n    <div class=\"row col-md-12\">\n      <!-- <div class=\"col-sm-1 col-md-1 col-lg-1 mt-2 mb-1\"></div> -->\n      <div class=\"col-3 col-sm-3 col-md-3 col-lg-3 mb-1\">\n        <select class=\"form-control custom-select\" name=\"sellist1\" (change)=\"filterForTransactions($event,false)\"\n          [(ngModel)]=\"selectedAccountFilter\">\n          <option value=\"all\">{{\"T_OPTION_ALL_ACCOUNTS\" | translate}}</option>\n          <ng-container *ngFor=\"let account of accounts\">\n            <option *ngIf=\"(account.official_name || account.inst_name) && (account.status == 'ACTIVE')\"\n              [ngValue]=\"account\" [selected]=\"account.account_id == selectedAccId\">\n              {{account.inst_name}} {{account.official_name}}\n            </option>\n          </ng-container>\n        </select>\n      </div>\n      <div class=\"col-3 col-sm-3 col-md-3 col-lg-3 mb-1\">\n        <select class=\"form-control custom-select\" id=\"sel2\" name=\"sellist2\"\n          (change)=\"filterForTransactions($event,false)\" [(ngModel)]=\"selectedDateFilter\">\n          <option value=\"default\">{{\"T_OPTIONS_ALL\" | translate}}</option>\n          <option value=\"12months\">{{\"T_OPTIONS_LAST_12_MONTHS\" | translate}}</option>\n          <option value=\"7days\">{{\"T_OPTIONS_LAST_7_DAYS\" | translate}}</option>\n          <option value=\"14days\">{{\"T_OPTIONS_LAST_14_DAYS\" | translate}}</option>\n          <option value=\"thismonth\">{{ \"T_OPTIONS_THIS_MONTHS\" | translate}}</option>\n          <option value=\"3months\">{{\"T_OPTIONS_LAST_3_MONTHS\" | translate}}</option>\n          <option value=\"6months\">{{\"T_OPTIONS_LAST_6_MONTHS\" | translate}}</option>\n          <option value=\"thisyear\">{{\"T_OPTIONS_THIS_YEAR\" | translate}}</option>\n          <option value=\"lastyear\">{{\"T_OPTIONS_LAST_YEAR\" | translate}}</option>\n        </select>\n      </div>\n      <div class=\"col-sm-3 col-md-3 col-lg-3 mt-2 mb-1 d-none\">\n        <select class=\"form-control custom-select\" id=\"sel3\" name=\"sellist3\" (change)=\"setDatePrefrence($event)\"\n          [(ngModel)]=\"datePrefrence\">\n          <option value=\"us\">MM-DD-YYYY</option>\n          <option value=\"uk\">DD-MM-YYYY</option>\n        </select>\n      </div>\n\n      <div class=\"col-3 col-sm-3 col-md-3 col-lg-3 mb-1 pr-1\">\n        <div class=\"input-group search-text\">\n          <span>\n            <i class=\"fa fa-search text-secondary\" aria-hidden=\"true\"></i>\n          </span>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"searchText\">\n        </div>\n      </div>\n    </div>\n\n    <div *ngIf=\"(!showNoTransactionsWarning); else noDataBlock\" class=\"col-md-12 table-padding\">\n        <div class=\"table-responsive table-hover\">\n          <table class=\"table table-bordered\">\n            <thead>\n                <tr class=\"bg-dark text-light\">\n                  <td class=\"parent-date\">\n                    {{\"RT_TD_DATE\" | translate}}\n                    <button class=\"dateSort\" (click)=\"orderByDate()\"><i class=\"fa fa-sort text-light\"></i></button>\n                  </td>\n                  <td>{{\"RT_TD_ACCOUNT_NAME\" | translate}}</td>\n                  <td>{{\"RT_TD_DESCRIPTION\" | translate}}</td>\n                  <td>{{\"RT_TD_CATEGORY\" | translate}}</td>\n                  <td>{{\"RT_TD_AMOUNT\" | translate}}</td>\n                </tr>\n            </thead>\n            <tbody *ngIf=\"(transactions | searchFilter:searchText) as result\">\n              <tr *ngFor=\"let trans of result | orderBydate : orderByField | paginate: { itemsPerPage: 50, currentPage: page }; let ind = index\">\n                <td class=\"d-width\">{{trans.date | date: 'MMM dd yyyy'}}</td>\n                <td>\n                  <span placement=\"top\" popover=\"{{trans.officialName}}\" triggers=\"mouseenter:mouseleave\">\n                    {{trans.officialName ? (trans.officialName.length > 25 ? trans.officialName.substring(0, 25)+\"...\" : trans.officialName) : \"updating soon...\"}}\n                  </span>\n                </td>\n                <td>\n                  <span placement=\"top\" popover=\"{{trans.name}}\"\n                    triggers=\"mouseenter:mouseleave\">{{trans.name.length > 20 ? trans.name.substring(0, 20)+\"...\" : trans.name}}</span>\n                </td>\n                <td>\n                  <span placement=\"top\" popover=\"{{trans.category}}\"\n                    triggers=\"mouseenter:mouseleave\">{{trans.category.length > 32 ? trans.category.substring(0, 32)+\"...\": trans.category}}</span>\n                </td>\n                <td [ngClass]=\"trans.amount > 0 ? 'text-success' : 'trans-secondary'\">\n                  {{trans.amount > 0 ? (trans.amount | currency) : \"&#x2212;\" + ((trans.amount * -1) | currency)}}</td>\n              </tr>\n              <tr *ngIf=\"result.length == 0\">\n                <td colspan=\"5\" class=\"text-center text-primary\">No record found</td>\n              </tr>\n              <tr *ngIf=\"result.length > 0\">\n                <td colspan=\"5\">\n                  <pagination-controls (pageChange)=\"page = $event\" *ngIf=\"!(firstTimeWarning || showNoTransactionsWarning)\">\n                    </pagination-controls>\n                </td>\n              </tr>\n            </tbody>\n        </table>\n    </div>\n  </div>\n      <!-- <div *ngIf=\"(!showNoTransactionsWarning); else noDataBlock\" class=\"container\">\n      <div class=\"table-responsive\">\n        <table class=\"table table-bordered\">\n          <thead class=\"thead-dark\">\n            <tr class=\"bg-dark text-light\">\n              <th class=\"parent-date\">\n                {{\"RT_TD_DATE\" | translate}}\n                <button class=\"dateSort\" (click)=\"orderByDate()\"><i class=\"fa fa-sort text-light\"></i></button>\n              </th>\n              <th>{{\"RT_TD_ACCOUNT_NAME\" | translate}}</th>\n              <th>{{\"RT_TD_DESCRIPTION\" | translate}}</th>\n              <th>{{\"RT_TD_CATEGORY\" | translate}}</th>\n              <th>{{\"RT_TD_AMOUNT\" | translate}}</th>\n            </tr>\n          </thead>\n        <tbody>\n          <tr\n            *ngFor=\"let trans of transactions  | searchFilter : searchText | orderBydate : orderByField | paginate: { itemsPerPage: 50, currentPage: page }; let ind = index\">\n\n            <td class=\"d-width\">{{trans.date | date: 'MMM dd yyyy'}}</td>\n            <td>\n              <span placement=\"top\" popover=\"{{trans.officialName}}\" triggers=\"mouseenter:mouseleave\">\n                {{trans.officialName ? (trans.officialName.length > 25 ? trans.officialName.substring(0, 25)+\"...\" : trans.officialName) : \"updating soon...\"}}\n              </span>\n            </td>\n            <td>\n              <span placement=\"top\" popover=\"{{trans.name}}\"\n                triggers=\"mouseenter:mouseleave\">{{trans.name.length > 20 ? trans.name.substring(0, 20)+\"...\" : trans.name}}</span>\n            </td>\n            <td>\n              <span placement=\"top\" popover=\"{{trans.category}}\"\n                triggers=\"mouseenter:mouseleave\">{{trans.category.length > 32 ? trans.category.substring(0, 32)+\"...\": trans.category}}</span>\n            </td>\n            <td [ngClass]=\"trans.amount > 0 ? 'text-success' : 'trans-secondary'\">\n              {{trans.amount > 0 ? (trans.amount | currency) : \"&#x2212;\" + ((trans.amount * -1) | currency)}}</td>\n          </tr>\n\n        </tbody>\n        </table>\n\n      </div>\n    </div> -->\n      <ng-template #noDataBlock>\n          <div class=\"blue text-center\">\n            {{\"T_DIV_NO_TRANSACTIONS_EXIST\" | translate}}\n          </div>\n      </ng-template>\n    </div>\n  </div>\n\n  <ng-template #anotherOne>\n      <div class=\"inactive-msg text-center\">\n        {{\"T_DIV_FETCHING_TRANSACTIONS\" | translate}}\n      </div>\n  </ng-template>\n</div>\n\n<div class=\"row\" *ngIf=\"accounts.length == 0\">\n  <div class=\"col-12\">\n    <div class=\"bg-transaction-image bg-common\"></div>\n    <div class=\"bg-text\">\n      <p>Link your account to view the widget</p>\n      <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add Account</button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/transactions/transactions.component.ts":
/*!********************************************************!*\
  !*** ./src/app/transactions/transactions.component.ts ***!
  \********************************************************/
/*! exports provided: TransactionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsComponent", function() { return TransactionsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __read = (undefined && undefined.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (undefined && undefined.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};





var TransactionsComponent = /** @class */ (function () {
    function TransactionsComponent(authService, spinnerService, backend) {
        this.authService = authService;
        this.spinnerService = spinnerService;
        this.backend = backend;
        this.transactions = [];
        this.accounts = [];
        this.DATE_FORMAT = 'YYYY-MM-DD';
        this.selectedAccountFilter = 'all';
        this.selectedDateFilter = 'default';
        this.orderByField = '-date';
        this.datePrefrence = null;
        this.showNoTransactionsWarning = false;
        this.firstTimeWarning = false;
        this.page = 1;
        this.searchText = "";
        this.searchValue = "";
        this.canParseAccounts = true;
        this.addAccountHandlerEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    TransactionsComponent.prototype.ngOnChanges = function (changes) {
        if (changes.accountId && changes.accountId.currentValue) {
            this.selectedAccId = changes.accountId.currentValue;
            this.selectedDateFilter = 'default';
            var start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().add('-7', 'year').format(this.DATE_FORMAT);
            var end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
            // trying to match acc id with our caputured accId 
            if (this.canParseAccounts) {
                try {
                    for (var _a = __values(this.allAccounts), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var acc = _b.value;
                        if ('accounts' in acc && acc.accounts) {
                            try {
                                for (var _c = __values(acc.accounts), _d = _c.next(); !_d.done; _d = _c.next()) {
                                    var account = _d.value;
                                    if (account.account_id == changes.accountId.currentValue) {
                                        return this.populateTransactions(start_date, end_date, changes.accountId.currentValue, false);
                                    }
                                }
                            }
                            catch (e_1_1) { e_1 = { error: e_1_1 }; }
                            finally {
                                try {
                                    if (_d && !_d.done && (_e = _c.return)) _e.call(_c);
                                }
                                finally { if (e_1) throw e_1.error; }
                            }
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_f = _a.return)) _f.call(_a);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
        }
        // reload
        if (changes.reloadScreen.currentValue != changes.reloadScreen.previousValue) {
            this.firstTimeWarning = false;
            this.ngOnInit();
        }
        if ('controlData' in changes && changes.controlData.previousValue != null) {
            this.firstTimeWarning = true;
        }
        var e_2, _f, e_1, _e;
    };
    TransactionsComponent.prototype.ngOnInit = function () {
        this.user = this.authService.getUserProfile();
        this.allAccounts = [];
        this.allAccounts = this.user.accounts;
        var messageError = Object.create(null);
        if (this.allAccounts.length == 1 && typeof this.allAccounts[0] === 'string') {
            messageError = JSON.parse(this.allAccounts[0]);
            if (messageError.message == "ITEM_LOGIN_REQUIRED") {
                this.canParseAccounts = false;
                alert("Accounts can't be fetched, retry changing credentials");
            }
        }
        if (this.allAccounts && this.allAccounts.length > 0) {
            this.accounts = [];
            try {
                for (var _a = __values(this.allAccounts), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var us = _b.value;
                    if ('accounts' in us && us.accounts) {
                        try {
                            for (var _c = __values(us.accounts), _d = _c.next(); !_d.done; _d = _c.next()) {
                                var account = _d.value;
                                account.inst_name = us.inst_name;
                                this.accounts.push(account);
                            }
                        }
                        catch (e_3_1) { e_3 = { error: e_3_1 }; }
                        finally {
                            try {
                                if (_d && !_d.done && (_e = _c.return)) _e.call(_c);
                            }
                            finally { if (e_3) throw e_3.error; }
                        }
                    }
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_f = _a.return)) _f.call(_a);
                }
                finally { if (e_4) throw e_4.error; }
            }
            // Get transactions also
            var start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().add('-7', 'year').format(this.DATE_FORMAT);
            var end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
            return this.populateTransactions(start_date, end_date, null, true);
        }
        var e_4, _f, e_3, _e;
    };
    TransactionsComponent.prototype.populateTransactions = function (start_date, end_date, accounts, firstload) {
        var _this = this;
        this.spinnerService.show();
        console.log('populateTransactions Data', accounts);
        // Make get transactions call
        var path = '/users/transactions';
        var qp = [];
        if (start_date) {
            qp.push('start_date=' + start_date);
        }
        if (end_date) {
            qp.push('end_date=' + end_date);
        }
        if (accounts) {
            qp.push('account_ids=' + accounts);
        }
        if (qp && qp.length > 0) {
            path += '?' + qp.join('&');
        }
        console.log(path);
        this.backend.get(path, this.authService.getAccessTokenAndID().accessToken)
            .subscribe(function (data) {
            var allTransactions = null;
            allTransactions = data.json();
            _this.spinnerService.show();
            _this.showNoTransactionsWarning = false;
            _this.firstTimeWarning = false;
            console.log('transssallTransactions', allTransactions);
            // don't delete this code, it will be use in future.........!!
            //changing date format according to userLoggedIn
            // if(allTransactions.transactions && allTransactions.transactions.length > 0){}
            // for(let t of allTransactions.transactions){
            //   if(this.authService.getUserProfile().preferences == 'uk'){
            //     this.datePrefrence = 'uk'
            //     t.date = moment.utc(t.date).format('DD-MM-YYYY');
            //   }else{
            //     this.datePrefrence = 'us'
            //     t.date = moment.utc(t.date).format('MM-DD-YYYY');
            //   }
            // }
            var clonedTransaction = [];
            try {
                for (var _a = __values(allTransactions.transactions), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var obj = _b.value;
                    var b = null;
                    b = Object.assign({}, obj);
                    clonedTransaction.push(b);
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_5) throw e_5.error; }
            }
            try {
                for (var clonedTransaction_1 = __values(clonedTransaction), clonedTransaction_1_1 = clonedTransaction_1.next(); !clonedTransaction_1_1.done; clonedTransaction_1_1 = clonedTransaction_1.next()) {
                    var t = clonedTransaction_1_1.value;
                    if (t.account_type == 'depository' || t.account_type == 'credit' || t.account_type == 'brokerage') {
                        if (t.amount < 0) {
                            t.amount = (t.amount * -1).toFixed(2);
                        }
                        else {
                            t.amount = (t.amount * -1).toFixed(2);
                        }
                    }
                }
            }
            catch (e_6_1) { e_6 = { error: e_6_1 }; }
            finally {
                try {
                    if (clonedTransaction_1_1 && !clonedTransaction_1_1.done && (_d = clonedTransaction_1.return)) _d.call(clonedTransaction_1);
                }
                finally { if (e_6) throw e_6.error; }
            }
            _this.transactions = clonedTransaction;
            if (firstload && !(_this.transactions && _this.transactions.length > 0)) {
                _this.firstTimeWarning = true;
            }
            if (!firstload && !(_this.transactions && _this.transactions.length > 0)) {
                _this.showNoTransactionsWarning = true;
            }
            _this.spinnerService.hide();
            var e_5, _c, e_6, _d;
        }, function (err) {
            console.log('got error', err.json());
            _this.spinnerService.hide();
        });
    };
    TransactionsComponent.prototype.getstartenddate = function () {
        var start_date = '';
        var end_date = '';
        var passVar = this.selectedDateFilter;
        switch (passVar) {
            case '7days':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(6, 'days').format(this.DATE_FORMAT);
                break;
            case '12months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(11, 'months').startOf('month').format(this.DATE_FORMAT);
                break;
            case '14days':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(13, 'days').format(this.DATE_FORMAT);
                break;
            case 'thismonth':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().startOf('month').format(this.DATE_FORMAT);
                break;
            case '3months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(2, 'months').format(this.DATE_FORMAT);
                break;
            case '6months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract(5, 'months').format(this.DATE_FORMAT);
                break;
            case 'thisyear':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().startOf('year').format(this.DATE_FORMAT);
                break;
            case 'lastyear':
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract('1', 'year').endOf('year').format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract('1', 'year').startOf('year').format(this.DATE_FORMAT);
                break;
            default:
                end_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"]().subtract('7', 'year').startOf('year').format(this.DATE_FORMAT);
                break;
        }
        return { start_date: start_date, end_date: end_date };
    };
    TransactionsComponent.prototype.filterForTransactions = function (event, load) {
        this.spinnerService.show();
        this.transactions = [];
        var dates = this.getstartenddate();
        console.log("dates", dates);
        var selectedAccountsFilter = this.selectedAccountFilter;
        console.log('accounts filter', selectedAccountsFilter);
        var accountsFilter = null;
        if (selectedAccountsFilter == 'all') {
            accountsFilter = null;
        }
        else {
            accountsFilter = selectedAccountsFilter.account_id;
        }
        this.populateTransactions(dates.start_date, dates.end_date, accountsFilter, load);
    };
    TransactionsComponent.prototype.orderByDate = function () {
        if (this.orderByField && this.orderByField === '-date') {
            this.orderByField = 'date';
        }
        else {
            this.orderByField = '-date';
        }
    };
    //datePrefrence method
    TransactionsComponent.prototype.setDatePrefrence = function (e) {
        var _this = this;
        console.log('setDatePrefrence', this.datePrefrence);
        this.spinnerService.show();
        var data = {};
        if (this.datePrefrence) {
            data.preferences = this.datePrefrence;
        }
        var path = "/users/" + this.authService.getAccessTokenAndID().idTokenPayload.sub;
        this.backend.put(data, path, this.authService.getAccessTokenAndID().accessToken)
            .subscribe(function (res) {
            console.log('preferencesRes', res);
            var clonedTrans = __spread(_this.transactions);
            if (res.status == 204) {
                _this.transactions = [];
                try {
                    for (var clonedTrans_1 = __values(clonedTrans), clonedTrans_1_1 = clonedTrans_1.next(); !clonedTrans_1_1.done; clonedTrans_1_1 = clonedTrans_1.next()) {
                        var t = clonedTrans_1_1.value;
                        if (_this.datePrefrence == 'uk') {
                            t.date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"](t.date, 'MM-DD-YYYY').format('DD-MM-YYYY');
                        }
                        else {
                            t.date = moment__WEBPACK_IMPORTED_MODULE_4__["utc"](t.date, 'DD-MM-YYYY').format('MM-DD-YYYY');
                        }
                    }
                }
                catch (e_7_1) { e_7 = { error: e_7_1 }; }
                finally {
                    try {
                        if (clonedTrans_1_1 && !clonedTrans_1_1.done && (_a = clonedTrans_1.return)) _a.call(clonedTrans_1);
                    }
                    finally { if (e_7) throw e_7.error; }
                }
            }
            //update transactions
            _this.transactions = __spread(_this.transactions, clonedTrans);
            _this.spinnerService.hide();
            var e_7, _a;
        }, function (err) {
            console.log('preferenceError', err);
            _this.spinnerService.hide();
        });
    };
    TransactionsComponent.prototype.addAccountHandler = function (refrenceName) {
        this.addAccountHandlerEmit.emit(refrenceName);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("reloadScreen"),
        __metadata("design:type", Object)
    ], TransactionsComponent.prototype, "reloadScreen", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("accountId"),
        __metadata("design:type", Object)
    ], TransactionsComponent.prototype, "accountId", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("controlData"),
        __metadata("design:type", Object)
    ], TransactionsComponent.prototype, "controlData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], TransactionsComponent.prototype, "addAccountHandlerEmit", void 0);
    TransactionsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-transactions',
            template: __webpack_require__(/*! ./transactions.component.html */ "./src/app/transactions/transactions.component.html"),
            styles: [__webpack_require__(/*! ./transactions.component.css */ "./src/app/transactions/transactions.component.css")],
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"], _backend_backend_service__WEBPACK_IMPORTED_MODULE_3__["BackendService"]])
    ], TransactionsComponent);
    return TransactionsComponent;
}());



/***/ }),

/***/ "./src/app/trends/trends.component.css":
/*!*********************************************!*\
  !*** ./src/app/trends/trends.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".custom-select{\n  width: 66% !important;\n  border-radius: 0px !important;\n  height: 32px !important;\n  font-size: 14px;\n}\n/* .abc{\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n} */\n/*end toggle css*/\n.canvas-div{\n  border: 1px solid black;\n  padding: 15px;\n}\n.f-weight{\n  font-weight: bold;\n}\n.blue{\n  color:blue;\n  text-align: center;\n}\n.chart-container {\n  position: relative;\n  margin: auto;\n  height: 70vh;\n}\n.button-values{\n  width: 126px;\n  padding: 20px 0px 20px 0px;\n  margin: 15px 2px 0px 5px;\n  border: 1px solid #000000;\n  border-radius: 4px;\n  font-size: 9px !important;\n}\n.active-btn {\n  background-color: #46a81b;\n  font-weight:bold ;\n  color:#ffffff;\n}\n.graph-width{\n  width:88%;\n  margin:0 auto ;\n}\n.set-overview{\n  position: absolute;\n  top: 58%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n.link-overview{\n  border-bottom: 2px solid #F5BF13;\n  color: #F5BF13 !important;\n  cursor: pointer !important;\n}\n@media only screen and (max-width: 600px) {\n   .button-values {\n     width:100% ;\n     height:30px;\n     margin:5px !important;\n     padding:0px ;\n   }\n   .width-line {\n     width:80%;\n     margin:0 auto;\n   }\n   .graph-width{\n     width:unset;\n   }\n }\n@media only screen and (min-width: 600px) and (max-width: 800px) {\n   .button-values {\n     width:74px ;\n     font-size: 60%;\n     padding: 0px;\n     height: 60px;\n   }\n   .width-line {\n     margin: 0 auto ;\n   }\n   .graph-width{\n     width:100%;\n   }\n }\n@media only screen and (min-width: 800px) and (max-width: 1050px) {\n   .button-values {\n     width:100px ;\n     font-size:80%;\n     padding: 0px;\n     height: 70px;\n   }\n   .graph-width{\n     width:100%;\n   }\n }\n/* background images */\n.bg-text {\n  color: #000000;\n  font-weight: bold;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  text-align: center;\n}\n.bg-text .dash-hide{\n  display: none;\n}\n.bg-common {\n  /* Add the blur effect */\n  filter: blur(7px);\n  -webkit-filter: blur(7px);\n\n  /* Center and scale the image nicely */\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.bg-graphs-image {\n  /* The image used */\n  background-image: url(\"/../assets/graphs.png\");\n  height: 510px; \n}\n"

/***/ }),

/***/ "./src/app/trends/trends.component.html":
/*!**********************************************!*\
  !*** ./src/app/trends/trends.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"abc\">\n<div class=\"\">\n  <ul class=\"nav report-entry\">\n    <li class=\"width-line\">\n      <button class=\"button-values cursor-pointer\" [ngClass]=\"{'active-btn':graphValue == 'income_over_time'}\" (click)=\"showGraph('income_over_time')\">{{\"TR_BUTTON_INCOME_OVER_TIME\" | translate}}</button>\n    </li>\n    <li class=\"width-line\">\n      <button class=\"button-values cursor-pointer\" [ngClass]=\"{'active-btn':graphValue == 'spending_over_time'}\" (click)=\"showGraph('spending_over_time')\">{{\"TR_BUTTON_SPENDING_OVER_TIME\" | translate}}</button>\n    </li>\n    <li class=\"width-line\">\n      <button class=\"button-values cursor-pointer\" [ngClass]=\"{'active-btn':graphValue == 'spending_by_category'}\" (click)=\"showGraph('spending_by_category')\">{{\"TR_BUTTON_SPENDING_BY_CATEGORY\" | translate}}</button>\n    </li>\n    <li class=\"width-line\">\n      <button class=\"button-values cursor-pointer\" [ngClass]=\"{'active-btn':graphValue == 'net_income_over_time'}\" (click)=\"showGraph('net_income_over_time')\">{{\"TR_BUTTON_NET_INCOME\" | translate}}</button>\n    </li>\n    <li class=\"width-line\">\n      <button class=\"button-values cursor-pointer\" [ngClass]=\"{'active-btn':graphValue == 'assets_over_time'}\" (click)=\"showGraph('assets_over_time')\">{{\"TR_BUTTON_ASSETS\" | translate}}</button>\n    </li>\n    <li class=\"width-line\">\n      <button class=\"button-values cursor-pointer\" [ngClass]=\"{'active-btn':graphValue == 'debts_over_time'}\" (click)=\"showGraph('debts_over_time')\">{{\"TR_BUTTON_DEBTS\" | translate}}</button>\n    </li>\n    <li class=\"width-line\">\n      <button class=\"button-values cursor-pointer\" [ngClass]=\"{'active-btn':graphValue == 'networth_over_time'}\" (click)=\"showGraph('networth_over_time')\">{{\"TR_BUTTON_NET_WORTH\" | translate}}</button>\n    </li>\n  </ul>\n</div>\n\n<div class=\"parent-property\">\n  <div class=\"row\" *ngIf=\"accountsForBlur.length == 0\">\n    <div class=\"col-12 mt-4\">\n      <div class=\"bg-graphs-image bg-common\"></div>\n      <div class=\"bg-text\">\n        <p class=\"report-hide\">Link your account to view the widget</p>\n        <p class=\"dash-hide\">User does not have any active account</p>\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addAccountHandler('callingAddAccountBtn')\">Add Account</button>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"accountsForBlur.length > 0\">\n    <div *ngIf=\"(!showNoTransactionsWarning); else elseBlockTransaction\">\n      <div *ngFor=\"let id of getKeys(graphs)\" class= \"mt-3 container\">\n        <div class=\"container canvas-div\" *ngIf=\"id == graphValue\">\n        <p class=\"f-weight\">{{graphs[id].translationKey | translate}}</p>\n        <div class=\"row\">\n          <div class=\"hide-in-report col-md-3 col-6 col-sm-4 col-lg-3\" [hidden]=\"!graphs[id].showFromOption\">\n            <span>{{\"TR_SPAN_FROM\" | translate}}</span>\n          </div>\n          <div class=\"hide-in-report col-md-3 col-6 col-sm-4 col-lg-3\">\n            <span>{{\"TR_SPAN_DURING\" | translate}}</span>\n          </div>\n\n          <div class=\" margin-in-trend col-md-3 col-6 col-sm-4 col-lg-3\">\n            <span>{{graphs[id].totalAmountNameKey | translate}}</span>\n          </div>\n          <div class=\"col-md-3 border-info d-inline\">\n            <span>{{graphs[id].avgAmountNameKey | translate}}</span>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\" hide-in-report col-6 col-sm-4 col-md-3 col-lg-3 mb-1\" [hidden]=\"!graphs[id].showFromOption\">\n            <select class=\"form-control custom-select\" name=\"sellist1\" (change)=\"filterForTransactions($event,id)\"\n                    [(ngModel)]=\"graphs[id].accountsFilter\">\n              <option value=\"all\">{{\"TR_OPTION_ALL_ACCOUNTS\" | translate}}</option>\n              <ng-container *ngFor=\"let account of accounts\">\n                  <option *ngIf=\"(account.official_name || account.inst_name) && (account.status == 'ACTIVE')\" [ngValue]=\"account\">\n                    {{account.inst_name}} {{account.official_name}}\n                  </option>\n                </ng-container>\n            </select>\n          </div>\n          <div class=\" hide-in-report col-6 col-sm-4 col-md-3 col-lg-3 mb-1\">\n            <select class=\"form-control custom-select cursor-pointer\"  name=\"sellist2\" (change)=\"filterForTransactions($event,id)\"\n                    [(ngModel)]=\"graphs[id].datesFilter\">\n              <option *ngFor=\"let index of dateFilterArray\" [ngValue]=\"index.value\">{{index.translateKeyValue | translate}}</option>\n            </select>\n          </div>\n          <div class=\"margin-in-trend col-md-3 col-6 col-sm-4 col-lg-3\">\n            <span class=\"h4\">{{graphs[id].totalAmount | currency}}</span>\n          </div>\n          <div class=\"col-md-3 border-info d-inline\">\n            <span class=\"h4\">{{graphs[id].avgAmount | currency}}</span>\n          </div>\n        </div>\n        <div *ngIf=\"graphs[id].load\" class=chart-container>\n          <canvas baseChart #baseChart=\"base-chart\" [datasets]=\"graphs[id].data\" [labels]=\"graphs[id].labels\" [options]=\"graphs[id].barOptions\"\n                  [chartType]=\"graphs[id].type\" [colors]=\"graphs[id].colors\" (chartClick)=\"onChartClick($event)\"\n                  ></canvas>\n          <span class=\"set-overview\" *ngIf=\"graphs[id].type === 'doughnut' && graphs[id].level>0\">\n            <span class=\"pt-2 link-overview cursor-pointer h4\" (click)=\"drillDownPieChart(0, 'RESET_PIE')\" >\n              {{\"TR_SPAN_OVERVIEW\" | translate}}\n            </span>\n            <span class=\"h4\" *ngFor=\"let label of graphs[id].labelHierachy; let i = index\">\n              > {{label}}\n            </span>\n          </span>\n        </div>\n        <div *ngIf=\"!graphs[id].load\">\n          <img src=\"../../assets/threeDotLoad.gif\" alt=\"\" height=\"60\">\n        </div>\n      </div>\n      </div>\n    </div>\n\n    <ng-template #elseBlockTransaction>\n      <div class=\"mt-4 mb-4 ml-2 blue\"><br>\n        {{\"T_DIV_FETCHING_TRANSACTIONS\" | translate}}\n      </div>\n    </ng-template>\n  </div>\n</div>\n<app-transactions-modal></app-transactions-modal>\n"

/***/ }),

/***/ "./src/app/trends/trends.component.ts":
/*!********************************************!*\
  !*** ./src/app/trends/trends.component.ts ***!
  \********************************************/
/*! exports provided: TrendsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrendsComponent", function() { return TrendsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var chart_piecelabel_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! chart.piecelabel.js */ "./node_modules/chart.piecelabel.js/src/Chart.PieceLabel.js");
/* harmony import */ var chart_piecelabel_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(chart_piecelabel_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../transactions-modal/transactions-modal.component */ "./src/app/transactions-modal/transactions-modal.component.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _common_currencyformatter_pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common/currencyformatter.pipe */ "./src/app/common/currencyformatter.pipe.ts");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../common/utils */ "./src/app/common/utils.ts");
/* harmony import */ var _common_dateOrdering_pipe__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../common/dateOrdering.pipe */ "./src/app/common/dateOrdering.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __read = (undefined && undefined.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (undefined && undefined.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};














var TrendsComponent = /** @class */ (function () {
    function TrendsComponent(authService, modalService, spinnerService, backend, currency, notifierService, translate, dateOrderPipe) {
        this.authService = authService;
        this.modalService = modalService;
        this.spinnerService = spinnerService;
        this.backend = backend;
        this.currency = currency;
        this.notifierService = notifierService;
        this.translate = translate;
        this.dateOrderPipe = dateOrderPipe;
        this.transactions = [];
        this.balanceArr = [];
        this.showNoTransactionsWarning = false;
        this.graphValue = 'income_over_time';
        this.dateFilterArray = [];
        this.DATE_FORMAT = 'YYYY-MM-DD';
        this.report = null;
        this.tranxDataEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.addAccountHandlerEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.accounts = [];
        this.accountsForBlur = [];
        this.notifier = notifierService;
        var G_TRANSACTIONS;
        translate.get('G_TRANSACTIONS').subscribe(function (value) { return G_TRANSACTIONS = value; });
        this.graphs = {
            "income_over_time": {
                name: 'G_INCOME_OVER_TIME',
                translationKey: "G_INCOME_OVER_TIME",
                totalAmountNameKey: 'G_TOTAL_INCOME_FOR_PERIOD',
                avgAmountNameKey: 'G_AVERAGE_INCOME',
                load: false,
                showFromOption: true,
                accountsFilter: 'all',
                datesFilter: '12months',
                barOptions: {
                    maintainAspectRatio: false,
                    scaleShowHorizontalLines: true,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 60 //set that fits the best
                        }
                    },
                    tooltips: {
                        bodyFontSize: 24,
                        bodySpacing: 4,
                        xPadding: 16,
                        yPadding: 10,
                        xAlign: "center",
                        yAlign: 'top',
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            label: function (tooltipItem, data) {
                                return '$' + tooltipItem.yLabel.toLocaleString();
                            },
                            footer: function (tooltipItem, data) {
                                var index = tooltipItem[0].index;
                                var t = data.datasets[0].transactions[index].length;
                                return t + ("" + G_TRANSACTIONS);
                            }
                        }
                    },
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length) {
                                e.target.style.cursor = 'pointer';
                            }
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    autoSkip: false,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) < 0 || parseInt(value) >= 0) {
                                            return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                        }
                                    }
                                }
                            }]
                    }
                },
                colors: [
                    {
                        backgroundColor: 'rgb(0, 194, 86)'
                    }
                ],
                labels: [],
                type: 'bar',
                data: [],
                totalAmount: 0,
                avgAmount: 0,
            },
            "spending_over_time": {
                name: 'Spending Over Time',
                translationKey: "G_SPENDING_OVER_TIME",
                totalAmountName: 'Total Spending for period',
                totalAmountNameKey: 'G_TOTAL_SPENDING_FOR_PERIOD',
                avgAmountName: 'Average Spending',
                avgAmountNameKey: 'G_AVERAGE_SPENDING',
                load: false,
                showFromOption: true,
                accountsFilter: 'all',
                accountsFilterkey: 'G_ALL',
                datesFilter: '12months',
                datesFilterKey: 'G_12MONTHS',
                barOptions: {
                    maintainAspectRatio: false,
                    scaleShowHorizontalLines: true,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 60 //set that fits the best
                        }
                    },
                    tooltips: {
                        bodyFontSize: 24,
                        bodySpacing: 4,
                        xPadding: 16,
                        yPadding: 10,
                        xAlign: "center",
                        yAlign: 'top',
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            label: function (tooltipItem, data) {
                                return '$' + tooltipItem.yLabel.toLocaleString();
                            },
                            footer: function (tooltipItem, data) {
                                var index = tooltipItem[0].index;
                                var t = data.datasets[0].transactions[index].length;
                                return t + ("" + G_TRANSACTIONS);
                            }
                        }
                    },
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length)
                                e.target.style.cursor = 'pointer';
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    autoSkip: false,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) < 0 || parseInt(value) >= 0) {
                                            return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                        }
                                    }
                                }
                            }]
                    }
                },
                colors: [
                    {
                        backgroundColor: 'rgb(255, 107, 113)'
                    }
                ],
                labels: [],
                type: 'bar',
                data: [],
                totalAmount: 0,
                avgAmount: 0,
            },
            "net_income_over_time": {
                name: 'Net Income Over Time',
                translationKey: "G_NET_INCOME_OVER_TIME",
                totalAmountName: 'Total Net income for period',
                totalAmountNameKey: 'G_TOTAL_NET_INCOME_FOR_PERIOD',
                avgAmountName: 'Average net income',
                avgAmountNameKey: 'G_AVERAGE_NET_INCOME',
                load: false,
                showFromOption: false,
                accountsFilter: 'all',
                accountsFilterkey: 'G_ALL',
                datesFilter: '12months',
                datesFilterKey: 'G_12MONTHS',
                barOptions: {
                    maintainAspectRatio: false,
                    scaleShowHorizontalLines: true,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 70 //set that fits the best
                        }
                    },
                    scales: {
                        xAxes: [{
                                stacked: true
                            }],
                        yAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true,
                                    autoSkip: false,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) < 0 || parseInt(value) >= 0) {
                                            return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                        }
                                    }
                                }
                            }]
                    },
                    tooltips: {
                        titleFontSize: 20,
                        bodyFontSize: 18,
                        bodySpacing: 4,
                        xPadding: 16,
                        yPadding: 6,
                        xAlign: "center",
                        yAlign: "top",
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            if (('dataPoints' in tooltip && tooltip.dataPoints && tooltip.dataPoints[0].datasetIndex)) {
                                tooltip.opacity = 1;
                            }
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            beforeTitle: function (tooltipItem, data) {
                                if (data.datasets[tooltipItem[0].datasetIndex].label) {
                                    var title = data.datasets[tooltipItem[0].datasetIndex].label;
                                    return title;
                                }
                            },
                            title: function () { return null; },
                            beforeLabel: function (tooltipItem, data) {
                                var modifiedLabels = data.labels[tooltipItem.index];
                                return modifiedLabels;
                            },
                            label: function (tooltipItem, data) {
                                if (tooltipItem && tooltipItem.datasetIndex >= 0) {
                                    return '$' + tooltipItem.yLabel.toLocaleString();
                                }
                                return '';
                            },
                            footer: function (tooltipItem, data) {
                                var index = tooltipItem[0].index;
                                if (data.datasets[tooltipItem[0].datasetIndex].transactions[index] && data.datasets[tooltipItem[0].datasetIndex].transactions[index].length > 0) {
                                    var t = data.datasets[tooltipItem[0].datasetIndex].transactions[index].length;
                                    return t + ("" + G_TRANSACTIONS);
                                }
                            }
                        }
                    },
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length)
                                e.target.style.cursor = 'pointer';
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                },
                colors: [
                    {
                        backgroundColor: '#717171'
                    },
                    {
                        backgroundColor: 'rgb(0, 194, 86)'
                    },
                    {
                        backgroundColor: 'rgb(255, 107, 113)'
                    }
                ],
                labels: [],
                type: 'bar',
                data: [],
                totalAmount: 0,
                avgAmount: 0,
            },
            "debts_over_time": {
                name: 'Debts Over Time',
                translationKey: "G_DEBTS_OVER_TIME",
                totalAmountName: 'Lowest Debt',
                totalAmountNameKey: 'G_LOWEST_DEBT',
                avgAmountName: 'Highest debt',
                avgAmountNameKey: 'G_HIGHEST_DEBT',
                load: false,
                showFromOption: true,
                accountsFilter: 'all',
                accountsFilterKey: 'G_ALL',
                datesFilter: '12months',
                datesFilterKey: 'G_12MONTHS',
                barOptions: {
                    maintainAspectRatio: false,
                    scaleShowHorizontalLines: true,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 60 //set that fits the best
                        }
                    },
                    tooltips: {
                        enabled: true,
                        bodyFontSize: 24,
                        bodySpacing: 4,
                        xPadding: 16,
                        yPadding: 10,
                        xAlign: "center",
                        yAlign: 'top',
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            label: function (tooltipItem, data) {
                                return '$' + tooltipItem.yLabel.toLocaleString();
                            },
                            footer: function (tooltipItem, data) {
                                var index = tooltipItem[0].index;
                                var t = data.datasets[0].transactions[index].length;
                                // return t + " Transactions";
                            }
                        }
                    },
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length)
                                e.target.style.cursor = 'default';
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    autoSkip: false,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) < 0 || parseInt(value) >= 0) {
                                            return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                        }
                                    }
                                }
                            }]
                    }
                },
                colors: [
                    {
                        backgroundColor: 'rgb(255, 107, 113)'
                    }
                ],
                labels: [],
                type: 'bar',
                data: [],
                totalAmount: 0,
                avgAmount: 0,
            },
            "assets_over_time": {
                name: 'Assets Over Time',
                translationKey: "G_ASSETS_OVER_TIME",
                totalAmountName: 'Highest Assets for period',
                totalAmountNameKey: 'G_CURRENT_ASSETS_FOR_PERIOD',
                avgAmountName: 'Average assets',
                avgAmountNameKey: 'G_AVERAGE_ASSETS',
                load: false,
                showFromOption: true,
                accountsFilter: 'all',
                accountsFilterKey: 'G_ALL',
                datesFilter: '12months',
                datesFilterKey: 'G_12MONTHS',
                barOptions: {
                    maintainAspectRatio: false,
                    scaleShowHorizontalLines: true,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 60 //set that fits the best
                        }
                    },
                    tooltips: {
                        enabled: true,
                        bodyFontSize: 24,
                        bodySpacing: 4,
                        xPadding: 16,
                        yPadding: 10,
                        xAlign: "center",
                        yAlign: 'top',
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            beforeTitle: function (tooltipItem, data) {
                                if (data.datasets[tooltipItem[0].datasetIndex].label) {
                                    var title = data.datasets[tooltipItem[0].datasetIndex].label;
                                    return title;
                                }
                            },
                            title: function () { return null; },
                            beforeLabel: function (tooltipItem, data) {
                                var modifiedLabels = data.labels[tooltipItem.index];
                                return modifiedLabels;
                            },
                            label: function (tooltipItem, data) {
                                return '$' + tooltipItem.yLabel.toLocaleString();
                            },
                            footer: function (tooltipItem, data) {
                                var index = tooltipItem[0].index;
                                var t = data.datasets[0].transactions[index].length;
                                // return t + `${G_TRANSACTIONS}`;
                            }
                        }
                    },
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length)
                                e.target.style.cursor = 'default';
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                    scales: { xAxes: [{
                                stacked: true
                            }],
                        yAxes: [{
                                stacked: true,
                                ticks: {
                                    autoSkip: false,
                                    min: 0,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) < 0 || parseInt(value) >= 0) {
                                            return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                        }
                                    }
                                }
                            }]
                    }
                },
                colors: [
                    {
                        backgroundColor: 'rgb(0, 194, 86)'
                    },
                    {
                        backgroundColor: 'orange'
                    }
                ],
                labels: [],
                type: 'bar',
                data: [],
                totalAmount: 0,
                avgAmount: 0,
            },
            "networth_over_time": {
                name: 'NetWorth Over Time',
                translationKey: "G_NETWORTH_OVER_TIME",
                totalAmountName: 'Lowest Net Worth',
                totalAmountNameKey: 'G_LOWEST_NET_WORTH',
                avgAmountName: 'Highest Net Worth',
                avgAmountNameKey: 'G_HIGHEST_NET_WORTH',
                load: false,
                showFromOption: false,
                accountsFilter: 'all',
                accountsFilterKey: 'G_ALL',
                datesFilter: '12months',
                datesFilterKey: 'G_12MONTHS',
                barOptions: {
                    maintainAspectRatio: false,
                    scaleShowHorizontalLines: true,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 60 //set that fits the best
                        }
                    },
                    tooltips: {
                        enabled: true,
                        bodyFontSize: 24,
                        bodySpacing: 4,
                        xPadding: 16,
                        yPadding: 10,
                        xAlign: "center",
                        yAlign: 'top',
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            beforeTitle: function (tooltipItem, data) {
                                if (data.datasets[tooltipItem[0].datasetIndex].label) {
                                    var title = data.datasets[tooltipItem[0].datasetIndex].label;
                                    return title;
                                }
                            },
                            title: function () { return null; },
                            beforeLabel: function (tooltipItem, data) {
                                var modifiedLabels = data.labels[tooltipItem.index];
                                return modifiedLabels;
                            },
                            label: function (tooltipItem, data) {
                                if (tooltipItem && tooltipItem.datasetIndex >= 0) {
                                    return '$' + tooltipItem.yLabel.toLocaleString();
                                }
                                return '';
                            },
                            footer: function (tooltipItem, data) {
                                var index = tooltipItem[0].index;
                                if (data.datasets[tooltipItem[0].datasetIndex].transactions[index] && data.datasets[tooltipItem[0].datasetIndex].transactions[index].length > 0) {
                                    var t = data.datasets[tooltipItem[0].datasetIndex].transactions[index].length;
                                    // return t + `${G_TRANSACTIONS}`;
                                }
                            }
                        }
                    },
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length)
                                e.target.style.cursor = 'default';
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                    scales: {
                        xAxes: [{
                                stacked: true
                            }],
                        yAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true,
                                    autoSkip: false,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) < 0 || parseInt(value) >= 0) {
                                            return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                                        }
                                    }
                                }
                            }]
                    },
                },
                colors: [
                    {
                        backgroundColor: '#717171'
                    },
                    {
                        backgroundColor: 'rgb(0, 194, 86)'
                    },
                    {
                        backgroundColor: 'rgb(255, 107, 113)'
                    },
                    {
                        backgroundColor: 'orange'
                    }
                ],
                labels: [],
                type: 'bar',
                data: [],
                totalAmount: 0,
                avgAmount: 0,
            },
            "spending_by_category": {
                name: 'Spending By Category for past 12 months',
                translationKey: "G_SPENDING_BY_CATEGORY",
                load: false,
                showFromOption: true,
                accountsFilter: 'all',
                accountsFilterKey: 'G_ALL',
                datesFilter: '12months',
                datesFilterKey: 'G_12MONTHS',
                barOptions: {
                    maintainAspectRatio: false,
                    responsive: true,
                    layout: {
                        padding: {
                            bottom: 30 //set that fits the best
                        }
                    },
                    tooltips: {
                        titleFontSize: 20,
                        bodySpacing: 4,
                        xPadding: 12,
                        yPadding: 10,
                        xAlign: "center",
                        yAlign: 'top',
                        mode: 'label',
                        position: 'cursor',
                        intersect: true,
                        custom: function (tooltip) {
                            if (!tooltip)
                                return;
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            title: function (tooltipItem, data) {
                                var tooltipIndex = tooltipItem.index ? tooltipItem.index : tooltipItem[0].index;
                                var amountData = data.datasets[0].data[tooltipIndex];
                                return '$' + ((amountData).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                            },
                            label: function (tooltipItem, data) {
                                return "";
                            },
                            footer: function (tooltipItem, data) {
                                var index = tooltipItem[0].index;
                                var t = data.datasets[1].data[index].length;
                                return t + ("" + G_TRANSACTIONS);
                            }
                        }
                    },
                    cutoutPercentage: 70,
                    hover: {
                        onHover: function (e) {
                            var point = this.getElementAtEvent(e);
                            if (point.length) {
                                e.target.style.cursor = 'pointer';
                            }
                            else
                                e.target.style.cursor = 'default';
                        }
                    },
                    legend: {
                        display: false,
                        labels: {
                            fontColor: '#000',
                            fontSize: 16
                        },
                        position: 'right'
                    },
                    pieceLabel: {
                        segment: true,
                        position: 'outside',
                        overlap: false,
                        fontColor: '#000',
                        fontSize: 12,
                        textMargin: 12,
                        outsidePadding: 0,
                        render: function (args) {
                            return args.label;
                        }
                    },
                    elements: {
                        center: {
                            text: '$',
                            color: '#36A2EB',
                            fontStyle: 'Helvetica',
                            sidePadding: 15 //Default 20 (as a percentage)
                        }
                    },
                    onResize: function (chart, size) {
                        if (size.width > 350) {
                            chart.options.legend.position = 'right';
                            chart.options.legend.fontSize = 16;
                            chart.options.pieceLabel.textMargin = 12;
                        }
                        else if (size.width < 350) {
                            chart.options.legend.position = 'top';
                            chart.options.legend.fontSize = 6;
                            chart.options.pieceLabel.textMargin = 8;
                            chart.height = 300;
                        }
                    }
                },
                colors: [
                    {
                        backgroundColor: [],
                        borderWidth: 1
                    },
                ],
                labels: [],
                type: 'doughnut',
                data: [],
                transactions: [],
                level: 0,
                labelHierachy: []
            }
        };
    }
    TrendsComponent.prototype.drillDownPieChart = function (index, type) {
        var _this = this;
        if (type == 'RESET_PIE') {
            this.graphs.spending_by_category.level = 0;
            var allFirstTranx = this.graphs.spending_by_category.transactions[this.graphs.spending_by_category.level];
            var selectedPieTranx = [];
            for (var i = 0; i < allFirstTranx.length; i++) {
                selectedPieTranx = selectedPieTranx.concat(allFirstTranx[i]);
            }
            this.createPieChartOverTimeGraph(selectedPieTranx, 0);
        }
        else {
            if (this.graphs.spending_by_category.level < 1) {
                var selectedPieTranx = this.graphs.spending_by_category.transactions[this.graphs.spending_by_category.level][index];
                // collection of category
                var collectionOfCatog = selectedPieTranx.map(function (t) { return t.category; });
                // find repeated category
                var collectRepeatedCatog = __spread(new Set(collectionOfCatog));
                // if only one category repeating in all transaction, then show modal.
                if (collectRepeatedCatog.length == 1) {
                    var initialState = { transactions: selectedPieTranx };
                    this.modalRef = this.modalService.show(_transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_7__["TransactionsModalComponent"], Object.assign({}, { class: 'gray modal-lg', initialState: initialState }));
                    return;
                }
                // trying to find how many string in transaction's category and then find which category have more string
                var catg = void 0;
                var count = 0;
                try {
                    for (var selectedPieTranx_1 = __values(selectedPieTranx), selectedPieTranx_1_1 = selectedPieTranx_1.next(); !selectedPieTranx_1_1.done; selectedPieTranx_1_1 = selectedPieTranx_1.next()) {
                        var t = selectedPieTranx_1_1.value;
                        catg = t.category.split(',');
                        if (catg.length > count) {
                            count = catg.length;
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (selectedPieTranx_1_1 && !selectedPieTranx_1_1.done && (_a = selectedPieTranx_1.return)) _a.call(selectedPieTranx_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                //if finalCatg have only 2 string and our lavel is 1; then i reset level with 0;
                if ((count - this.graphs.spending_by_category.level) == 1) {
                    this.graphs.spending_by_category.level = 0;
                    var allFirstTranx = this.graphs.spending_by_category.transactions[this.graphs.spending_by_category.level];
                    var selectedPieTranx_2 = [];
                    for (var i = 0; i < allFirstTranx.length; i++) {
                        selectedPieTranx_2 = selectedPieTranx_2.concat(allFirstTranx[i]);
                    }
                    this.createPieChartOverTimeGraph(selectedPieTranx_2, 0);
                }
                else {
                    this.graphs.spending_by_category.labelHierachy.push(this.graphs.spending_by_category.labels[index]);
                    this.createPieChartOverTimeGraph(selectedPieTranx, this.graphs.spending_by_category.level + 1);
                }
            }
            else {
                // showing transaction modal
                var allFirstTranx = this.graphs.spending_by_category.transactions[this.graphs.spending_by_category.level];
                var initialState = { transactions: allFirstTranx[index] };
                if ('transactions' in initialState && initialState.transactions && initialState.transactions.length > 0) {
                    this.modalRef = this.modalService.show(_transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_7__["TransactionsModalComponent"], Object.assign({}, { class: 'gray modal-lg', initialState: initialState }));
                }
                else {
                    this.translate.get('G_NO_TRANS')
                        .subscribe(function (value) {
                        _this.notifier.notify('warning', value);
                    });
                }
            }
        }
        var e_1, _a;
    };
    TrendsComponent.prototype.onChartClick = function (event) {
        var _this = this;
        console.log('onChartClick', event);
        if (event && event.active
            && event.active.length > 0
            && (event.active[0]._chart.config.type === 'doughnut' || event.active[0]._chart.config.type === 'bar')
            && event.active[0]._chart.config.data
            && event.active[0]._chart.config.data.datasets
            && event.active[0]._chart.config.data.datasets.length > 0) {
            var stopClickEvent = ["DEBTS", "BALANCE", "NETWORTH", "DEUDAS", "BALANCE", "VALOR NETO"];
            if (event.active[0]._chart.config.type === 'bar') {
                if (!(stopClickEvent.includes(event.active[0]._model.datasetLabel ||
                    event.active[0]._chart.config.data.datasets[2].label))) {
                    var chart = event.active[0]._chart;
                    var activePoints = chart.getElementAtEvent(event.event);
                    var clickedElementIndex = activePoints[0]._datasetIndex;
                    var initialState = { transactions: event.active[clickedElementIndex]._chart.config.data.datasets[clickedElementIndex].transactions[event.active[clickedElementIndex]._index] };
                    if ('transactions' in initialState && initialState.transactions && initialState.transactions.length > 0) {
                        this.modalRef = this.modalService.show(_transactions_modal_transactions_modal_component__WEBPACK_IMPORTED_MODULE_7__["TransactionsModalComponent"], Object.assign({}, { class: 'gray modal-lg', initialState: initialState }));
                    }
                    else {
                        this.translate.get('G_NO_TRANS')
                            .subscribe(function (value) {
                            _this.notifier.notify('warning', value);
                        });
                    }
                }
            }
            else {
                this.drillDownPieChart(event.active[0]._index, null);
            }
        }
    };
    ;
    TrendsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.authService.getUserProfile();
        console.log('fromTends-user', this.user);
        console.log('trend-report', this.report);
        this.allAccounts = [];
        if (this.report) {
            this.allAccounts = this.report.accounts;
        }
        else if (this.user) {
            this.allAccounts = this.user.accounts;
        }
        this.allProperty = this.user.properties;
        console.log('this.allaccounts', this.user);
        console.log('this.allProperty', this.allProperty);
        this.translate.get('G_SPENDING').subscribe(function (value) { return _this.G_Spending = value; });
        this.translate.get('G_INCOME').subscribe(function (value) { return _this.G_Income = value; });
        this.translate.get('G_NET_INCOME').subscribe(function (value) { return _this.G_Net_Income = value; });
        this.translate.get('G_BALANCE').subscribe(function (value) { return _this.G_Assets = value; });
        this.translate.get('G_DEBTS').subscribe(function (value) { return _this.G_Debts = value; });
        this.translate.get('G_NET_WORTH').subscribe(function (value) { return _this.G_Net_Worth = value; });
        this.translate.get('MA_PROPERTY').subscribe(function (value) { return _this.G_PROPERTY = value; });
        var start_date = null;
        var end_date = null;
        this.dateFilterArray = [];
        this.dateFilterArray.push({
            key: 'Last 7 days',
            translateKeyValue: "G_LAST_7_DAYS",
            value: '7days',
        }, {
            key: 'Last 14 days',
            translateKeyValue: "G_LAST_14_DAYS",
            value: '14days',
        }, {
            key: 'This month',
            translateKeyValue: "G_THIS_MONTH",
            value: 'thismonth',
        }, {
            key: 'Last 3 months',
            translateKeyValue: "G_LAST_3_MONTHS",
            value: '3months',
        });
        if (this.report) {
            console.log('Inside report trend component:', this.report);
            if (this.report.trans_interval === 6) {
                this.dateFilterArray.push({
                    key: 'Last 6 months',
                    translateKeyValue: "G_LAST_6_MONTHS",
                    value: '6months',
                });
            }
            else if (this.report.trans_interval === 12) {
                this.dateFilterArray.push({
                    key: 'Last 6 months',
                    translateKeyValue: "G_LAST_6_MONTHS",
                    value: '6months',
                }, {
                    key: 'Last 6 months',
                    translateKeyValue: "G_LAST_12_MONTHS",
                    value: '12months',
                });
            }
            try {
                // Set default value
                for (var _a = __values(this.getKeys(this.graphs)), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var graph = _b.value;
                    this.graphs[graph].datesFilter = this.dateFilterArray[this.dateFilterArray.length - 1].value;
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_2) throw e_2.error; }
            }
            start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().subtract(11, this.report.trans_duration).format(this.DATE_FORMAT);
            end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
        }
        else {
            this.dateFilterArray.push({
                key: 'Last 6 months',
                translateKeyValue: "G_LAST_6_MONTHS",
                value: '6months'
            }, {
                key: 'Last 6 months',
                translateKeyValue: "G_LAST_12_MONTHS",
                value: '12months'
            }, {
                key: 'This year',
                translateKeyValue: "G_THIS_YEAR",
                value: 'thisyear'
            }, {
                key: 'Last year',
                translateKeyValue: "G_LAST_YEAR",
                value: 'lastyear'
            });
            if (this.allAccounts && this.allAccounts.length > 0) {
                this.accounts = [];
                try {
                    for (var _d = __values(this.allAccounts), _e = _d.next(); !_e.done; _e = _d.next()) {
                        var us = _e.value;
                        if ('accounts' in us && us.accounts) {
                            try {
                                for (var _f = __values(us.accounts), _g = _f.next(); !_g.done; _g = _f.next()) {
                                    var account = _g.value;
                                    account.inst_name = us.inst_name;
                                    this.accounts.push(account);
                                }
                            }
                            catch (e_3_1) { e_3 = { error: e_3_1 }; }
                            finally {
                                try {
                                    if (_g && !_g.done && (_h = _f.return)) _h.call(_f);
                                }
                                finally { if (e_3) throw e_3.error; }
                            }
                        }
                    }
                }
                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                finally {
                    try {
                        if (_e && !_e.done && (_j = _d.return)) _j.call(_d);
                    }
                    finally { if (e_4) throw e_4.error; }
                }
            }
            start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().add(-11, 'months').startOf('month').format(this.DATE_FORMAT);
            end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
        }
        //get accounts
        if (this.allAccounts && this.allAccounts.length > 0) {
            this.accountsForBlur = [];
            try {
                for (var _k = __values(this.allAccounts), _l = _k.next(); !_l.done; _l = _k.next()) {
                    var us = _l.value;
                    if ('accounts' in us && us.accounts) {
                        try {
                            for (var _m = __values(us.accounts), _o = _m.next(); !_o.done; _o = _m.next()) {
                                var account = _o.value;
                                this.accountsForBlur.push(account);
                            }
                        }
                        catch (e_5_1) { e_5 = { error: e_5_1 }; }
                        finally {
                            try {
                                if (_o && !_o.done && (_p = _m.return)) _p.call(_m);
                            }
                            finally { if (e_5) throw e_5.error; }
                        }
                    }
                }
            }
            catch (e_6_1) { e_6 = { error: e_6_1 }; }
            finally {
                try {
                    if (_l && !_l.done && (_q = _k.return)) _q.call(_k);
                }
                finally { if (e_6) throw e_6.error; }
            }
        }
        // Get transactions also
        //  this.spinnerService.show();
        return this.populateTransactions(start_date, end_date, null, 'all');
        var e_2, _c, e_4, _j, e_3, _h, e_6, _q, e_5, _p;
    };
    TrendsComponent.prototype.ngOnChanges = function (changes) {
        if ('reloadScreen' in changes && changes.reloadScreen.previousValue != null) {
            this.showNoTransactionsWarning = false;
            this.ngOnInit();
        }
        if ('controlData' in changes && changes.controlData.previousValue != null) {
            this.showNoTransactionsWarning = true;
        }
        if ('updatedReport' in changes && 'currentValue' in changes.updatedReport && changes.updatedReport.currentValue) {
            this.report = null;
            this.report = this.updatedReport;
            this.ngOnInit();
        }
        if ('propertyAttribute' in changes && changes.propertyAttribute && 'currentValue' in changes.propertyAttribute && changes.propertyAttribute.currentValue) {
            this.ngOnInit();
        }
        if ('sidebarReload' in changes && changes.sidebarReload != null) {
            this.ngOnInit();
        }
    };
    // dynamic color generator
    TrendsComponent.prototype.dynamicColors = function () {
        var r = Math.floor((Math.random() * 255) - 30);
        var g = Math.ceil((Math.random() * 255) + 30);
        var b = Math.round((Math.random() * 255));
        return "rgba(" + r + "," + g + "," + b + "," + 0.8 + ")";
    };
    ;
    TrendsComponent.prototype.getColor = function () {
        if (this.graphs.spending_by_category.data && this.graphs.spending_by_category.data.length > 0) {
            this.graphs.spending_by_category.colors[0].backgroundColor.length = 0;
            for (var i in this.graphs.spending_by_category.data[0].data) {
                var returnColor = this.dynamicColors();
                this.graphs.spending_by_category.colors[0].backgroundColor.push(returnColor);
            }
            console.log('sampleColor', this.graphs.spending_by_category.colors);
            console.log('pie-color', this.graphs.spending_by_category.colors[0].backgroundColor);
        }
    };
    // deep cloning 
    TrendsComponent.prototype.genericCloneMethod = function (arrayData) {
        var arrr = [];
        try {
            for (var arrayData_1 = __values(arrayData), arrayData_1_1 = arrayData_1.next(); !arrayData_1_1.done; arrayData_1_1 = arrayData_1.next()) {
                var obj = arrayData_1_1.value;
                var b = null;
                b = Object.assign({}, obj);
                arrr.push(b);
            }
        }
        catch (e_7_1) { e_7 = { error: e_7_1 }; }
        finally {
            try {
                if (arrayData_1_1 && !arrayData_1_1.done && (_a = arrayData_1.return)) _a.call(arrayData_1);
            }
            finally { if (e_7) throw e_7.error; }
        }
        return arrr;
        var e_7, _a;
    };
    TrendsComponent.prototype.getKeys = function (map) {
        return Array.from(Object.keys(map));
    };
    //spending over time
    TrendsComponent.prototype.createSpendingOverTimeGraph = function (dates, transactions) {
        var SpendFilterData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].filterSpendingTransactions(transactions);
        var spendingData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].groupGraphDataByFormat(SpendFilterData, dates, 'amount', null);
        var amountArr = Object.values(spendingData).map(function (t) { return Math.abs(t.amount); });
        var sumOfAmount = amountArr.reduce(function (a, b) { return a + b; });
        this.graphs.spending_over_time.totalAmount = sumOfAmount;
        this.graphs.spending_over_time.avgAmount = sumOfAmount / amountArr.length;
        this.graphs.spending_over_time.labels.length = 0;
        (_a = this.graphs.spending_over_time.labels).push.apply(_a, __spread(Object.keys(spendingData)));
        this.graphs.spending_over_time.data = [{
                data: Object.values(spendingData).map(function (t) { return Math.abs(t.amount); }),
                label: "" + this.G_Spending,
                transactions: Object.values(spendingData).map(function (t) { return t.transactionArr; })
            }];
        this.graphs.spending_over_time.load = true;
        var _a;
    };
    // income over time
    TrendsComponent.prototype.createIncomeOverTimeGraph = function (dates, transactions) {
        var incomeDataArr = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].filterIncomeTransactions(transactions);
        var chartData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].groupGraphDataByFormat(incomeDataArr, dates, 'amount', null);
        // sum of chartdata amount
        var amountArr = Object.values(chartData).map(function (t) { return Math.abs(t.amount); });
        var sumOfAmount = amountArr.reduce(function (a, b) { return a + b; });
        this.graphs.income_over_time.totalAmount = sumOfAmount;
        this.graphs.income_over_time.avgAmount = sumOfAmount / amountArr.length;
        this.graphs.income_over_time.labels.length = 0;
        (_a = this.graphs.income_over_time.labels).push.apply(_a, __spread(Object.keys(chartData)));
        this.graphs.income_over_time.data = [{
                data: Object.values(chartData).map(function (t) { return Math.abs(t.amount); }),
                label: "" + this.G_Income,
                transactions: Object.values(chartData).map(function (t) { return t.transactionArr; })
            }];
        this.graphs.income_over_time.load = true;
        this.spinnerService.hide();
        var _a;
    };
    ;
    //Net Income over time
    TrendsComponent.prototype.createTotalOverTimeGraph = function (dates, transactions) {
        var avgData = [];
        var incomeData = [];
        var spendData = [];
        // income calculation
        var filterData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].filterIncomeTransactions(transactions);
        var chartData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].groupGraphDataByFormat(filterData, dates, 'amount', null);
        // Spending calculation
        var spendDataArr = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].filterSpendingTransactions(transactions);
        var cloneSpendDataArr = this.genericCloneMethod(spendDataArr);
        var spendingData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].groupGraphDataByFormat(cloneSpendDataArr, dates, 'amount', null);
        // averageData calculation
        incomeData = Object.values(chartData).map(function (a) { return a.amount; });
        spendData = Object.values(spendingData).map(function (a) { return a.amount * -1; });
        for (var i = 0; i < incomeData.length; i++) {
            avgData.push(incomeData[i] + spendData[i]);
        }
        // net total income and its avg
        var sumOfAmount = avgData.reduce(function (a, b) { return a + b; });
        this.graphs.net_income_over_time.totalAmount = sumOfAmount;
        this.graphs.net_income_over_time.avgAmount = sumOfAmount / avgData.length;
        // add income and spend transaction in one variable
        var incomeTransArr = Object.values(chartData).map(function (t) { return t.transactionArr; });
        var spendingTransArr = Object.values(spendingData).map(function (t) { return t.transactionArr; });
        var avgTransactionArr = [];
        for (var i = 0; i < incomeTransArr.length; i++) {
            var incomeKey = null;
            var spendKey = null;
            incomeKey = incomeTransArr[i];
            spendKey = spendingTransArr[i];
            avgTransactionArr.push(incomeKey.concat(spendKey));
        }
        this.graphs.net_income_over_time.labels.length = 0;
        (_a = this.graphs.net_income_over_time.labels).push.apply(_a, __spread(Object.keys(chartData)));
        this.graphs.net_income_over_time.data = [
            {
                data: avgData,
                label: "" + this.G_Net_Income,
                type: "line",
                borderColor: "#717171",
                fill: false,
                lineTension: 0,
                pointRadius: 4,
                pointBackgroundColor: "#717171",
                transactions: avgTransactionArr
            },
            {
                data: incomeData,
                label: "" + this.G_Income,
                transactions: Object.values(chartData).map(function (t) { return t.transactionArr; })
            },
            {
                data: spendData,
                label: "" + this.G_Spending,
                transactions: Object.values(spendingData).map(function (t) { return t.transactionArr; })
            }
        ];
        this.graphs.net_income_over_time.load = true;
        this.spinnerService.hide();
        var _a;
    };
    // this function is used to create snapshot of balance based on dateformat
    TrendsComponent.prototype.createBalanceSnapshotByDateFormat = function (dates, filteredBalances) {
        var start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.start_date);
        var end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.end_date);
        var dateFormat = 'MMM YYYY'; // Default to monthy view
        var interval = 1;
        var duration = 'month';
        if ((end_date.diff(start_date, 'months') == 0) || (end_date.diff(start_date, 'days') < 15)) {
            dateFormat = 'MM-DD-YYYY';
            duration = 'days';
        }
        else {
            end_date = end_date.endOf('month');
        }
        // First group balances by account id. Since it might happen that one account has balance for the
        // need date and other does not
        var balancesByAccount = {};
        try {
            for (var filteredBalances_1 = __values(filteredBalances), filteredBalances_1_1 = filteredBalances_1.next(); !filteredBalances_1_1.done; filteredBalances_1_1 = filteredBalances_1.next()) {
                var balance = filteredBalances_1_1.value;
                if (!(balance.account_id in balancesByAccount)) {
                    balancesByAccount[balance.account_id] = [];
                }
                balancesByAccount[balance.account_id].push(balance);
            }
        }
        catch (e_8_1) { e_8 = { error: e_8_1 }; }
        finally {
            try {
                if (filteredBalances_1_1 && !filteredBalances_1_1.done && (_a = filteredBalances_1.return)) _a.call(filteredBalances_1);
            }
            finally { if (e_8) throw e_8.error; }
        }
        console.log('balancesByAccount', balancesByAccount);
        var groupedFilteredBalances = {};
        try {
            for (var _b = __values(Object.keys(balancesByAccount)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var account = _c.value;
                var filteredAccountBalances = balancesByAccount[account];
                var notAvailableBalanceMapping = {};
                var groupedFilteredAccountBalances = {};
                var loop_start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.start_date);
                var _loop_1 = function (l) {
                    // for each account 
                    var formatedDate = l.format(dateFormat);
                    var filteredBalancesByFormat = filteredAccountBalances.filter(function (t) { return t.date && moment__WEBPACK_IMPORTED_MODULE_2__["utc"](t.date).format(dateFormat) === formatedDate; });
                    console.log('filteredBalancesByFormat', formatedDate, filteredBalancesByFormat);
                    // If duration is month only keep those balances of last day of month/last date available balance in that month
                    if (duration === 'month') {
                        // Now sort it by date
                        if (filteredBalancesByFormat && filteredBalancesByFormat.length > 0) {
                            filteredBalancesByFormat = this_1.dateOrderPipe.transform(filteredBalancesByFormat, '-date');
                            // Get date of last available date and get all transactions of that date
                            // Since we can have multiple account balances for that date
                            var dateToConsider_1 = filteredBalancesByFormat[0].date;
                            groupedFilteredAccountBalances[formatedDate] = filteredBalancesByFormat.filter(function (t) { return t.date === dateToConsider_1; });
                        }
                    }
                    else {
                        groupedFilteredAccountBalances[formatedDate] = filteredBalancesByFormat;
                        if (!filteredBalancesByFormat || filteredBalancesByFormat.length === 0) {
                            // Get date of last available date and get all transactions of that date
                            // Since we can have multiple account balances for that date
                            var filteredBalancesByFormat_1 = this_1.dateOrderPipe.transform(filteredAccountBalances, '-date');
                            // Get date of last available date and get all transactions of that date
                            // Since we can have multiple account balances for that date
                            notAvailableBalanceMapping[formatedDate] = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](filteredBalancesByFormat_1[0].date).format(dateFormat);
                        }
                    }
                };
                var this_1 = this;
                for (var l = loop_start_date; l.isSameOrBefore(end_date); l.add(interval, duration)) {
                    _loop_1(l);
                }
                // Check if any date/month transaction is not present that consider last available
                if (Object.keys(notAvailableBalanceMapping).length > 0) {
                    console.log('notAvailableBalanceMapping', notAvailableBalanceMapping);
                    try {
                        for (var _d = __values(Object.keys(notAvailableBalanceMapping)), _e = _d.next(); !_e.done; _e = _d.next()) {
                            var durationKey = _e.value;
                            groupedFilteredAccountBalances[durationKey] = groupedFilteredAccountBalances[notAvailableBalanceMapping[durationKey]];
                        }
                    }
                    catch (e_9_1) { e_9 = { error: e_9_1 }; }
                    finally {
                        try {
                            if (_e && !_e.done && (_f = _d.return)) _f.call(_d);
                        }
                        finally { if (e_9) throw e_9.error; }
                    }
                }
                console.log('groupedFilteredAccountBalances', groupedFilteredAccountBalances);
                try {
                    for (var _g = __values(Object.keys(groupedFilteredAccountBalances)), _h = _g.next(); !_h.done; _h = _g.next()) {
                        var gfab = _h.value;
                        if (!(gfab in groupedFilteredBalances)) {
                            groupedFilteredBalances[gfab] = [];
                        }
                        (_j = groupedFilteredBalances[gfab]).push.apply(_j, __spread(groupedFilteredAccountBalances[gfab]));
                    }
                }
                catch (e_10_1) { e_10 = { error: e_10_1 }; }
                finally {
                    try {
                        if (_h && !_h.done && (_k = _g.return)) _k.call(_g);
                    }
                    finally { if (e_10) throw e_10.error; }
                }
            }
        }
        catch (e_11_1) { e_11 = { error: e_11_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_l = _b.return)) _l.call(_b);
            }
            finally { if (e_11) throw e_11.error; }
        }
        return groupedFilteredBalances;
        var e_8, _a, e_11, _l, e_9, _f, e_10, _k, _j;
    };
    //Debts over time
    TrendsComponent.prototype.createDebtOverTimeGraph = function (dates, balanceArr) {
        var start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.start_date);
        var end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.end_date);
        // for spending transaction
        var filteredDepoTrans = balanceArr.filter(function (t) { return ((t.balance < 0 && t.account_type == "depository") || (t.balance > 0 && t.account_type == "credit")) && t.date && (moment__WEBPACK_IMPORTED_MODULE_2__["utc"](t.date).isBetween(start_date, end_date, null, '[]')); });
        var balanceMapping = this.createBalanceSnapshotByDateFormat(dates, filteredDepoTrans);
        console.log('balanceMapping:', balanceMapping);
        var debtsData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].createEmptyGraphDataByDuration(dates);
        if (balanceMapping && Object.keys(balanceMapping).length > 0) {
            try {
                for (var _a = __values(Object.keys(debtsData)), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var key = _b.value;
                    if (key in balanceMapping && balanceMapping[key]) {
                        debtsData[key].transactionArr = balanceMapping[key];
                        debtsData[key].amount = balanceMapping[key].reduce(function (sum, t) {
                            return sum + (Math.round(Math.abs(t.balance) * 100) / 100);
                        }, 0);
                    }
                }
            }
            catch (e_12_1) { e_12 = { error: e_12_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_12) throw e_12.error; }
            }
        }
        // Debts total income and its avg
        var amountArr = Object.values(debtsData).map(function (t) { return Math.abs(t.amount); });
        var maxAmount = Math.max.apply(Math, __spread(amountArr));
        var minAmount = Math.min.apply(null, amountArr.filter(Boolean));
        minAmount = minAmount == Infinity ? 0 : minAmount;
        this.graphs.debts_over_time.totalAmount = minAmount;
        this.graphs.debts_over_time.avgAmount = maxAmount;
        this.graphs.debts_over_time.labels.length = 0;
        (_d = this.graphs.debts_over_time.labels).push.apply(_d, __spread(Object.keys(debtsData)));
        this.graphs.debts_over_time.data = [
            {
                data: Object.values(debtsData).map(function (t) { return t.amount; }),
                label: "" + this.G_Debts,
                transactions: Object.values(debtsData).map(function (t) { return t.transactionArr; })
            }
        ];
        this.graphs.debts_over_time.load = true;
        this.spinnerService.hide();
        var e_12, _c, _d;
    };
    // This function is used to calculate properties data based on duration
    TrendsComponent.prototype.calculatePropertiesData = function (dates, allProperties) {
        var graphData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].createEmptyGraphDataByDuration(dates);
        var start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.start_date);
        var dateAndDuration = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].calculateDuration(dates);
        try {
            // loop all properties
            for (var allProperties_1 = __values(allProperties), allProperties_1_1 = allProperties_1.next(); !allProperties_1_1.done; allProperties_1_1 = allProperties_1.next()) {
                var property = allProperties_1_1.value;
                // Get when propery was created
                var created_at = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](property.created_at);
                // We need to check if this property was created after the start date of the dates
                if (created_at.isSameOrAfter(start_date)) {
                    // Add this property to all the formats
                    var propertyAdded = false;
                    try {
                        for (var _a = __values(Object.keys(graphData)), _b = _a.next(); !_b.done; _b = _a.next()) {
                            var time = _b.value;
                            // Once property is added we want to keep it adding for rest of all the dates
                            if (time === created_at.format(dateAndDuration.dateFormat) || propertyAdded) {
                                graphData[time].amount += property.estimated_value;
                                graphData[time].transactionArr.push(property);
                                propertyAdded = true;
                            }
                        }
                    }
                    catch (e_13_1) { e_13 = { error: e_13_1 }; }
                    finally {
                        try {
                            if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                        }
                        finally { if (e_13) throw e_13.error; }
                    }
                }
            }
        }
        catch (e_14_1) { e_14 = { error: e_14_1 }; }
        finally {
            try {
                if (allProperties_1_1 && !allProperties_1_1.done && (_d = allProperties_1.return)) _d.call(allProperties_1);
            }
            finally { if (e_14) throw e_14.error; }
        }
        return graphData;
        var e_14, _d, e_13, _c;
    };
    // This function is used to calculate asset data based on balances
    // We take the balance present on that day/month as part of calculation
    TrendsComponent.prototype.calculateAssetsData = function (dates, balanceArr) {
        var start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.start_date);
        var end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.end_date);
        var filteredBalances = balanceArr.filter(function (t) { return t.account_type == "depository" && t.balance > 0 && t.date && (moment__WEBPACK_IMPORTED_MODULE_2__["utc"](t.date).isBetween(start_date, end_date, null, '[]')); });
        console.log('filteredBalances', filteredBalances);
        var balanceMapping = this.createBalanceSnapshotByDateFormat(dates, filteredBalances);
        console.log('balanceMapping:', balanceMapping);
        var assetsData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].createEmptyGraphDataByDuration(dates);
        if (balanceMapping && Object.keys(balanceMapping).length > 0) {
            try {
                for (var _a = __values(Object.keys(assetsData)), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var key = _b.value;
                    if (key in balanceMapping && balanceMapping[key]) {
                        assetsData[key].transactionArr = balanceMapping[key];
                        assetsData[key].amount = balanceMapping[key].reduce(function (sum, t) {
                            return sum + (Math.round(t.balance * 100) / 100);
                        }, 0);
                    }
                }
            }
            catch (e_15_1) { e_15 = { error: e_15_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_15) throw e_15.error; }
            }
        }
        console.log('AssestData', assetsData);
        return assetsData;
        var e_15, _c;
    };
    //Assets Over Time
    TrendsComponent.prototype.createAssetsOverTimeGraph = function (dates, balanceArr, allProperties) {
        var assetsData = this.calculateAssetsData(dates, balanceArr);
        console.log('AssestData', assetsData);
        var propertiesData = this.calculatePropertiesData(dates, allProperties);
        console.log('propertiesData', propertiesData);
        // Assets total income and its avg
        var amountArr = Object.values(assetsData).map(function (t) { return Math.abs(t.amount); });
        var propArr = Object.values(propertiesData).map(function (t) { return t.amount; });
        var merdedData = [];
        for (var i = 0; i < amountArr.length; i++) {
            merdedData.push(amountArr[i] + propArr[i]);
        }
        var sumOfAmount = merdedData.reduce(function (a, b) { return a + b; });
        var maxAmount = Math.max.apply(Math, __spread(merdedData));
        this.graphs.assets_over_time.totalAmount = maxAmount;
        this.graphs.assets_over_time.avgAmount = sumOfAmount / amountArr.length;
        console.log('assets-amountArr', amountArr);
        // this.graphs.income_over_time.data[0].transaction.length= 0;
        this.graphs.assets_over_time.labels.length = 0;
        (_a = this.graphs.assets_over_time.labels).push.apply(_a, __spread(Object.keys(assetsData)));
        this.graphs.assets_over_time.data = [{
                data: Object.values(assetsData).map(function (t) { return t.amount; }),
                label: "" + this.G_Assets,
                transactions: Object.values(assetsData).map(function (t) { return t.transactionArr; })
            },
            {
                data: Object.values(propertiesData).map(function (t) { return t.amount; }),
                label: "" + this.G_PROPERTY,
                transactions: Object.values(propertiesData).map(function (t) { return t.transactionArr; })
            }
        ];
        this.graphs.assets_over_time.load = true;
        this.spinnerService.hide();
        var _a;
    };
    // This function is used to merge assets data and properties data
    TrendsComponent.prototype.mergeAssetAndProperties = function (assetsData, propertiesData) {
        var mergedData = assetsData;
        if (propertiesData) {
            try {
                for (var _a = __values(Object.keys(propertiesData)), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var timeKey = _b.value;
                    if (!_common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].isNullOrEmpty(assetsData, timeKey)) {
                        assetsData[timeKey].amount = Math.abs(assetsData[timeKey].amount) + propertiesData[timeKey].amount;
                        (_c = assetsData[timeKey].transactionArr).push.apply(_c, __spread(propertiesData[timeKey].transactionArr));
                    }
                }
            }
            catch (e_16_1) { e_16 = { error: e_16_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_d = _a.return)) _d.call(_a);
                }
                finally { if (e_16) throw e_16.error; }
            }
        }
        return mergedData;
        var e_16, _d, _c;
    };
    //NetWorth Over Time
    TrendsComponent.prototype.createNetWorthOverTimeGraph = function (dates, balanceArr, allProperties) {
        var start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.start_date);
        var end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](dates.end_date);
        var assetsData = this.calculateAssetsData(dates, balanceArr);
        var propertiesData = this.calculatePropertiesData(dates, allProperties);
        console.log('propertiesData', propertiesData);
        assetsData = this.mergeAssetAndProperties(assetsData, propertiesData);
        var netWorthData = [];
        var assetsArray = [];
        var debtArray = [];
        //  Net Worth over time=>  the assets over time minus the debts over time (debts ove time is the like assets but for NEGATIVE values, meaning, you owe money to someone )bank, credit card, mortgage, loan)
        var filteredDebtsTransactions = balanceArr.filter(function (t) { return (t.balance < 0 && t.account_type == "depository" || t.balance > 0 && t.account_type == "credit") && t.date && (moment__WEBPACK_IMPORTED_MODULE_2__["utc"](t.date).isBetween(start_date, end_date, null, '[]')); });
        var balanceMapping = this.createBalanceSnapshotByDateFormat(dates, filteredDebtsTransactions);
        console.log('balanceMapping:', balanceMapping);
        var debtsData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].createEmptyGraphDataByDuration(dates);
        if (balanceMapping && Object.keys(balanceMapping).length > 0) {
            try {
                for (var _a = __values(Object.keys(debtsData)), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var key = _b.value;
                    if (key in balanceMapping && balanceMapping[key]) {
                        debtsData[key].transactionArr = balanceMapping[key];
                        debtsData[key].amount = balanceMapping[key].reduce(function (sum, t) {
                            return sum + (Math.round(Math.abs(t.balance) * 100) / 100);
                        }, 0);
                    }
                }
            }
            catch (e_17_1) { e_17 = { error: e_17_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_17) throw e_17.error; }
            }
        }
        // averageData calculation
        assetsArray = Object.values(assetsData).map(function (a) { return a.amount; });
        debtArray = Object.values(debtsData).map(function (a) { return Math.abs(a.amount) * -1; });
        for (var i = 0; i < assetsArray.length; i++) {
            netWorthData.push(assetsArray[i] + debtArray[i]);
        }
        // netWorthTransactionArr
        var assetsTransArr = Object.values(assetsData).map(function (t) { return t.transactionArr; });
        var debtTransArr = Object.values(debtsData).map(function (t) { return t.transactionArr; });
        var netWorthTransactionArr = [];
        for (var i = 0; i < assetsTransArr.length; i++) {
            var incomeKey = null;
            var spendKey = null;
            incomeKey = assetsTransArr[i];
            spendKey = debtTransArr[i];
            netWorthTransactionArr.push(incomeKey.concat(spendKey));
        }
        // networth min and max value  
        this.graphs.networth_over_time.totalAmount = Math.min.apply(Math, __spread(netWorthData));
        this.graphs.networth_over_time.avgAmount = Math.max.apply(Math, __spread(netWorthData));
        this.graphs.networth_over_time.labels.length = 0;
        (_d = this.graphs.networth_over_time.labels).push.apply(_d, __spread(Object.keys(assetsData)));
        this.graphs.networth_over_time.data = [
            {
                data: netWorthData,
                label: "" + this.G_Net_Worth,
                // label : 'T_NET_WORTH
                type: "line",
                borderColor: "#717171",
                fill: false,
                lineTension: 0,
                pointRadius: 4,
                pointBackgroundColor: "#717171",
                transactions: netWorthTransactionArr
            },
            {
                data: assetsArray,
                label: "" + this.G_Assets,
                // label : 'T_ASSETS
                transactions: Object.values(assetsData).map(function (t) { return t.transactionArr; })
            },
            {
                data: debtArray,
                label: "" + this.G_Debts,
                // label : 'T_DEBT
                transactions: Object.values(debtsData).map(function (t) { return t.transactionArr; })
            }
        ];
        this.graphs.networth_over_time.load = true;
        this.spinnerService.hide();
        var e_17, _c, _d;
    };
    // pie chart data over time
    TrendsComponent.prototype.createPieChartOverTimeGraph = function (transactions, level) {
        var PiechartData = {};
        var total = 0;
        var SpendFilterData = _common_utils__WEBPACK_IMPORTED_MODULE_12__["default"].filterSpendingTransactions(transactions);
        var category;
        try {
            for (var SpendFilterData_1 = __values(SpendFilterData), SpendFilterData_1_1 = SpendFilterData_1.next(); !SpendFilterData_1_1.done; SpendFilterData_1_1 = SpendFilterData_1.next()) {
                var t = SpendFilterData_1_1.value;
                total += Math.abs(t.amount);
                if (t.category) {
                    // This is done to get first level category
                    var catg = t.category.split(',');
                    if (catg.length >= level) {
                        catg.length == level ? category = catg[0] : category = catg[level];
                    }
                    if (category in PiechartData) {
                        PiechartData[category].amount += Math.abs(t.amount);
                        PiechartData[category].transactions.push(t);
                    }
                    else {
                        PiechartData[category] = {
                            amount: Math.abs(t.amount),
                            transactions: [t]
                        };
                    }
                }
            }
        }
        catch (e_18_1) { e_18 = { error: e_18_1 }; }
        finally {
            try {
                if (SpendFilterData_1_1 && !SpendFilterData_1_1.done && (_a = SpendFilterData_1.return)) _a.call(SpendFilterData_1);
            }
            finally { if (e_18) throw e_18.error; }
        }
        try {
            // calculate percentage
            for (var _b = __values(Object.keys(PiechartData)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var category_1 = _c.value;
                var percentage = (PiechartData[category_1].amount * 100) / total;
                // console.log(percentage);
                if (percentage < 5) {
                    // Club into others category
                    if (!('Others' in PiechartData)) {
                        PiechartData.Others = {
                            amount: 0,
                            transactions: []
                        };
                    }
                    PiechartData.Others.amount += PiechartData[category_1].amount;
                    (_d = PiechartData.Others.transactions).push.apply(_d, __spread(PiechartData[category_1].transactions));
                    delete PiechartData[category_1];
                }
            }
        }
        catch (e_19_1) { e_19 = { error: e_19_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_e = _b.return)) _e.call(_b);
            }
            finally { if (e_19) throw e_19.error; }
        }
        this.graphs.spending_by_category.barOptions.elements.center.text = this.currency.transform(total, 'USD', true, '1.0-0');
        this.graphs.spending_by_category.labels.length = 0;
        (_f = this.graphs.spending_by_category.labels).push.apply(_f, __spread(Object.keys(PiechartData)));
        if (level === 0) {
            this.graphs.spending_by_category.labelHierachy = [];
            this.graphs.spending_by_category.transactions = [];
        }
        this.graphs.spending_by_category.transactions.push(Object.values(PiechartData).map(function (t) { return t.transactions; }));
        this.graphs.spending_by_category.level = level;
        // We are keeping in dataset at index 1
        this.graphs.spending_by_category.data = [{
                data: Object.values(PiechartData).map(function (t) { return (t.amount); }),
                transactions: Object.values(PiechartData).map(function (t) { return t.transactions; })
            }, {
                data: Object.values(PiechartData).map(function (t) { return t.transactions; })
            }];
        this.getColor();
        this.graphs.spending_by_category.load = true;
        var e_18, _a, e_19, _e, _d, _f;
    };
    ;
    TrendsComponent.prototype.getstartenddate = function (graph) {
        var start_date = '';
        var end_date = '';
        var passVar = this.graphs[graph].datesFilter;
        switch (passVar) {
            case '7days':
                end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().subtract(6, 'days').format(this.DATE_FORMAT);
                break;
            case '12months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().subtract(11, 'months').startOf('month').format(this.DATE_FORMAT);
                break;
            case '14days':
                end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().subtract(13, 'days').format(this.DATE_FORMAT);
                break;
            case 'thismonth':
                end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().startOf('month').format(this.DATE_FORMAT);
                break;
            case '3months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().subtract(2, 'months').format(this.DATE_FORMAT);
                break;
            case '6months':
                end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().subtract(5, 'months').format(this.DATE_FORMAT);
                break;
            case 'thisyear':
                end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().startOf('year').format(this.DATE_FORMAT);
                break;
            case 'lastyear':
                end_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().subtract('1', 'year').endOf('year').format(this.DATE_FORMAT);
                start_date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"]().subtract('1', 'year').startOf('year').format(this.DATE_FORMAT);
                break;
        }
        return { start_date: start_date, end_date: end_date };
    };
    TrendsComponent.prototype.populateTransactions = function (start_date, end_date, accounts, graph) {
        var _this = this;
        console.log('trends-startDate', start_date);
        console.log('trends-end_date', end_date);
        console.log('populateTransactions Data', accounts);
        // Make get transactions call
        var path = '/users/transactions';
        var qp = [];
        if (start_date) {
            qp.push('start_date=' + start_date);
        }
        if (end_date) {
            qp.push('end_date=' + end_date);
        }
        if (accounts) {
            qp.push('account_ids=' + accounts);
        }
        if (qp && qp.length > 0) {
            path += '?' + qp.join('&');
        }
        console.log(path);
        var token = '';
        if (this.report) {
            token = this.report.token;
        }
        else {
            token = this.authService.getAccessTokenAndID().accessToken;
        }
        this.backend.get(path, token)
            .subscribe(function (data) {
            var allTransactions = data.json();
            console.log('allTransactionDetails', allTransactions);
            _this.transactions = [];
            _this.transactions = allTransactions.transactions;
            console.log('trends-showNoTransactionsWarning', _this.showNoTransactionsWarning);
            if (graph == 'all') {
                _this.tranxDataEmit.emit(allTransactions);
            }
            try {
                // transform the date into date format
                for (var _a = __values(allTransactions.balances), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var t = _b.value;
                    t.date = moment__WEBPACK_IMPORTED_MODULE_2__["utc"](t.date).format('YYYY-MM-DD');
                }
            }
            catch (e_20_1) { e_20 = { error: e_20_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_20) throw e_20.error; }
            }
            _this.balanceArr = allTransactions.balances;
            if (graph == 'all' && !(_this.transactions && _this.transactions.length > 0 || _this.balanceArr && _this.balanceArr.length > 0)) {
                _this.showNoTransactionsWarning = true;
                console.log('trends-showNoTransactionsWarning', _this.showNoTransactionsWarning);
            }
            _this.spinnerService.hide();
            var dates = { start_date: start_date, end_date: end_date };
            console.log("dates", dates);
            var allProperty = [];
            if (_this.report) {
                allProperty = _this.report.properties;
            }
            else {
                allProperty = _this.allProperty;
            }
            //Clone transactions data
            var clonedTransaction = _this.genericCloneMethod(_this.transactions);
            var clonedProperties = _this.genericCloneMethod(allProperty);
            var clonedBalanceArr = _this.genericCloneMethod(_this.balanceArr);
            //  this.renderGraph(dates);
            if (graph == 'all' || graph == 'income_over_time') {
                _this.createIncomeOverTimeGraph(dates, clonedTransaction);
            }
            if (graph == 'all' || graph == 'spending_over_time') {
                _this.createSpendingOverTimeGraph(dates, clonedTransaction);
            }
            if (graph == 'all' || graph == 'net_income_over_time') {
                _this.createTotalOverTimeGraph(dates, clonedTransaction);
            }
            if (graph == 'all' || graph == 'debts_over_time') {
                _this.createDebtOverTimeGraph(dates, clonedBalanceArr);
            }
            if (graph == 'all' || graph == 'assets_over_time') {
                _this.createAssetsOverTimeGraph(dates, clonedBalanceArr, clonedProperties);
            }
            if (graph == 'all' || graph == 'networth_over_time') {
                _this.createNetWorthOverTimeGraph(dates, clonedBalanceArr, clonedProperties);
            }
            if (graph == 'all' || graph == 'spending_by_category') {
                _this.createPieChartOverTimeGraph(clonedTransaction, 0);
            }
            var e_20, _c;
        });
    };
    TrendsComponent.prototype.filterForTransactions = function (event, graph) {
        this.graphs[graph].load = false;
        this.transactions = [];
        this.balanceArr = [];
        var dates = this.getstartenddate(graph);
        console.log("dates", dates);
        var selectedAccountsFilter = this.graphs[graph].accountsFilter;
        console.log('accounts filter', selectedAccountsFilter);
        var accountsFilter = null;
        if (selectedAccountsFilter == 'all') {
            accountsFilter = null;
        }
        else {
            accountsFilter = selectedAccountsFilter.account_id;
        }
        this.populateTransactions(dates.start_date, dates.end_date, accountsFilter, graph);
    };
    TrendsComponent.prototype.showGraph = function (item) {
        if (item) {
            this.graphValue = item;
        }
    };
    TrendsComponent.prototype.addAccountHandler = function (refrenceName) {
        this.addAccountHandlerEmit.emit(refrenceName);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('transactionModal'),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "transactionModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("baseChart"),
        __metadata("design:type", ng2_charts__WEBPACK_IMPORTED_MODULE_8__["BaseChartDirective"])
    ], TrendsComponent.prototype, "chart", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("report"),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "report", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("propertyAttribute"),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "propertyAttribute", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("reloadScreen"),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "reloadScreen", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("controlData"),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "controlData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("updatedReport"),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "updatedReport", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("sidebarReload"),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "sidebarReload", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "tranxDataEmit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], TrendsComponent.prototype, "addAccountHandlerEmit", void 0);
    TrendsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-trends',
            template: __webpack_require__(/*! ./trends.component.html */ "./src/app/trends/trends.component.html"),
            styles: [__webpack_require__(/*! ./trends.component.css */ "./src/app/trends/trends.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__["BsModalService"], ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_5__["BackendService"], _common_currencyformatter_pipe__WEBPACK_IMPORTED_MODULE_9__["LooseCurrencyPipe"], angular_notifier__WEBPACK_IMPORTED_MODULE_10__["NotifierService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_11__["TranslateService"], _common_dateOrdering_pipe__WEBPACK_IMPORTED_MODULE_13__["DateOrdering"]])
    ], TrendsComponent);
    return TrendsComponent;
}());



/***/ }),

/***/ "./src/app/verify-loan-data/verify-loan-data.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/verify-loan-data/verify-loan-data.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".nexusLogo-size{\n    width:30%;\n    vertical-align: sub;\n}\n.tick-size{\n    height:40px;\n    color:#94e53d !important;\n}\n.nexusLogo-size1{\n    width:22%;\n    height:50px;\n    \n}\n.m-15{\n    margin: 15px;\n}\n.check-font{\n    font-size:24px;\n}\n.continue-button{\n    width:30%;\n    height:70px;\n    font-size:26px;\n    background:forestgreen;\n    color:white;\n    font-weight:800;\n}\n.margin15{\n    margin:15px;\n}\n.display-tick{\n    display:inline-block;\n    line-height:4;\n}\n.display-text{\n    display:inline-block;\n    word-break:break-word;\n}\n.font-color{\n    color:#5a5b5a;\n    font-size:13px;\n}\n.radio-size{\n    border: 0px;\n    width: 50%;\n    height: 2em;\n    vertical-align:middle;\n}\n.next-btn{\n    height: 40px;\n    width: 12%;\n    color: white;\n    background: forestgreen;\n}\n.skip-btn{\n    height: 40px;\n    width: 26%;\n    color: white;\n    background: forestgreen;\n}\n.marginb0{\n    margin-bottom:0px !important;\n}\n.wrapper{\n    width: 20px;\n    height: 22px;\n    border-radius: 50%;\n    border: 3px solid green;\n    overflow: hidden;\n  }\n.one{\n    display: inline-block;\n    background-color: green;\n    height: 80px; width:30px;\n  \n  }\n.two{\n    display: inline-block;\n    /* background-color: white; */\n    background-color:green;\n    height: 100px; width:50px;\n  }\n.greyCircle1{\n    width: 20px;\n    height: 22px;\n    border-radius: 50%;\n    border: 3px solid grey;\n    overflow: hidden;\n  }\n.one1{\n    display: inline-block;\n    background-color: green;\n    height: 80px;\n\n  \n  }\n.two1{\n    display: inline-block;\n    background-color: white;\n    height: 100px; width:50px;\n  }\n#circle {\n    border-radius: 100%;\n    height:20px;\n    width: 3%;\n    background: green;\n    border: 3px solid green;\n}\n#p1 {\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    background-color: white;\n    width: 49%;\n    height: 100%;\n}\n#greenCircle1 {\n    border-radius: 100%;\n    height: 21px;\n    width: 3%;\n    background: green;\n    border: 3px solid green;\n}\n#p12 {\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    background-color: white;\n    width: 49%;\n    height: 100%;\n}\n\n\n"

/***/ }),

/***/ "./src/app/verify-loan-data/verify-loan-data.component.html":
/*!******************************************************************!*\
  !*** ./src/app/verify-loan-data/verify-loan-data.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-body\">\n  <div class=\"container\" *ngIf=\"displayCurrentDiv == 'welcomeDiv'\">\n    <div class=\"row  ns-font-34 ns-justify-text\">\n      <div>Hi Carlos!</div>\n    </div>\n    <div class=\"row  ns-font-34 ns-text-center\">\n      <div>Welcome to <img src=\"assets/nexus-log.png\" alt=\"\" class=\"nexusLogo-size carlos-font\" /> </div>\n    </div>\n    <div class=\"\">\n      <div class=\"row mt-3 ns-justify-text\">\n        <div class=\"col-md-1 display-tick\" for=\"check1\">\n          <img src=\"assets/greenTick.png\" alt=\"\" class=\"mr-2 tick-size\" /></div>\n        <div class=\"col-md-10 display-text ns-font-23\">Nexus is working with Nissan to get you approved for a new car\n          with just\n          few clicks</div>\n      </div>\n\n      <div class=\"row mt-3 ns-justify-text\">\n        <div class=\"col-md-1 display-tick\" for=\"check2\">\n          <img src=\"assets/greenTick.png\" alt=\"\" class=\"mr-2 tick-size\" /></div>\n        <div class=\" col-md-10 display-text ns-font-23\">We will do our best to get you into a new car while not\n          increasing your\n          monthly payments</div>\n      </div>\n\n      <div class=\"row mt-3 ns-justify-text\">\n        <div class=\"col-md-1 display-tick\">\n          <img src=\"assets/greenTick.png\" alt=\"\" class=\"mr-2 tick-size\" />\n        </div>\n        <div class=\"col-md-10 display-text ns-font-23\"> Nissan will never have access to your personal information\n          without your\n          permission\n        </div>\n      </div>\n\n      <div class=\"row margin15 mt-4 ns-justify-text ns-font-13\">\n        <div class=\"font-last\">Nexus is a secured online personal financial platform, click 'Continue' and verify your\n          information in the next screens</div>\n      </div>\n    </div>\n    <div class=\"row  mt-5 ns-justify-text\">\n      <button type=\"button\" class=\"btn btn-lg continue-button\" (click)=\"changeModalDiv('userDetailsDiv')\">\n        <i class=\"fa fa-lock mr-3\" aria-hidden=\"true\"></i> Continue\n      </button>\n    </div>\n  </div>\n\n  <!-- User details form -->\n  <div class=\"container\" *ngIf=\"displayCurrentDiv == 'userDetailsDiv'\">\n    <div class=\"modal-header\">\n      <div class=\"\"><img src=\"assets/nexus-log.png\" alt=\"\" class=\"nexusLogo-size1\" /></div>\n      <span class=\"pr-1\"><img src=\"../../assets/Nissan-logo.png\" style=\"height:60px ;\"></span>\n    </div>\n\n    <div class=\"modal-body\">\n       <div class=\"row\">\n              <div id=\"greenCircle1\">\n                  <div id=\"p12\"></div>\n              </div>\n        <div class=\"col-md-4\">Personal Information</div>      \n          <div class=\"greyCircle1\">\n              <div class=\"one1\"></div>\n              <div class=\"two1\"></div>\n              </div>\n        <div class=\"col-md-4\"> Employment</div>\n      </div>\n\n      <div class=\"mt-4\">Please verify your information below</div>\n\n      <div class=\"row mt-4\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"form-group col-md-5 font-color marginb0\">\n          <label for=\"usr \"> First Name</label>\n        </div>\n        <div class=\"form-group col-md-5 font-color marginb0\">\n          <label for=\"usr\"> Last Name</label>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-2 ns-font-13 ns-font-weight-bold\">Name</div>\n        <div class=\"form-group col-md-5 marginb0\">\n          <input type=\"text\" class=\"form-control\" name=\"firstName\" (keypress)=\"alphabetOnly($event)\"\n            [(ngModel)]=\"userAttributes.first_name\" maxlength=\"30\">\n        </div>\n        <div class=\"form-group col-md-5 marginb0\">\n          <input type=\"text\" class=\"form-control\" name=\"lastName\" (keypress)=\"alphabetOnly($event)\"\n            [(ngModel)]=\"userAttributes.last_name\" maxlength=\"30\">\n        </div>\n      </div>\n      <div class=\"row mt-3\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"form-group col-md-5 font-color marginb0\">\n          <label for=\"usr\">Date of Birth</label>\n          <div class=\"input-group\">\n            <input type=\"text\" name=\"DOB\" [(ngModel)]=\"userAttributes.dobInDate\" class=\"form-control\"\n              [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" [maxDate]=\"maxDate\" placement=\"bottom\" #dp=\"bsDatepicker\"\n              bsDatepicker>\n            <div class=\"input-group-append\">\n              <span class=\"input-group-text bg-none\" (click)=\"dp.show()\" [attr.aria-expanded]=\"dp.isOpen\"><i\n                  class=\"fa fa-calendar\"></i></span>\n            </div>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"row mt-4\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"form-group col-md-5 font-color marginb0\">\n          <label for=\"usr\">Home Phone Number</label>\n        </div>\n        <div class=\"form-group col-md-5 font-color marginb0\">\n          <label for=\"usr\">Cell Phone Number</label>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-2 ns-font-13 ns-font-weight-bold\">Contact</div>\n        <div class=\"form-group col-md-5 marginb0\">\n          <input type=\"text\" class=\"form-control\" name=\"homePhone\" (keypress)=\"numberOnly($event)\"\n            (ngModelChange)=\"userAttributes.home_phone = $event\" maxlength=\"14\"\n            [ngModel]=\"userAttributes.home_phone | phoneNumber\">\n        </div>\n        <div class=\"form-group col-md-5 marginb0\">\n          <input type=\"text\" class=\"form-control\" name=\"phoneNumbers\" (keypress)=\"numberOnly($event)\"\n            (ngModelChange)=\"userAttributes.phone_numbers = $event\" maxlength=\"14\"\n            [ngModel]=\"userAttributes.phone_numbers | phoneNumber\">\n        </div>\n      </div>\n      <div class=\"row mt-3\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"form-group col-md-10 font-color marginb0\">\n          <label for=\"usr\">Email</label>\n          <input type=\"text\" class=\"form-control\" name=\"email\" [(ngModel)]=\"userAttributes.emails\">\n        </div>\n      </div>\n\n      <div class=\"row mt-4\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"form-group col-md-10 font-color marginb0\">\n          <label for=\"usr\">Address1</label>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-2 ns-font-13 ns-font-weight-bold\">Address</div>\n        <div class=\"form-group col-md-10 marginb0\">\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userAttributes.address.street\" name=\"street\"\n            maxlength=\"45\">\n        </div>\n\n      </div>\n      <div class=\"row mt-3\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"form-group col-md-10 font-color marginb0\">\n          <label for=\"usr\">Address2</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userAttributes.address.street2\" name=\"street2\"\n            maxlength=\"45\">\n        </div>\n      </div>\n      <div class=\"row mt-3\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"form-group col-md-5 font-color marginb0\">\n          <label for=\"usr\">City</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userAttributes.address.city\" name=\"city\" maxlength=\"20\">\n        </div>\n        <div class=\"form-group col-md-5 font-color marginb0\">\n          <label for=\"usr\">State</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userAttributes.address.state\" name=\"state\"\n            maxlength=\"20\">\n        </div>\n      </div>\n      <div class=\"row mt-3\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"form-group col-md-5 font-color marginb0\">\n          <label for=\"usr\">Zip</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userAttributes.address.zip\" name=\"zipCode\"\n            maxlength=\"10\">\n        </div>\n      </div>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"next-btn cursor-pointer\"\n        (click)=\"updateUserProfile('employerDetailsDiv')\">Next</button>\n    </div>\n  </div>\n\n  <!-- Employer details form -->\n  <div class=\"container\" *ngIf=\"displayCurrentDiv == 'employerDetailsDiv'\">\n    <div class=\"modal-header\">\n      <div class=\"\"><img src=\"assets/nexus-log.png\" alt=\"\" class=\"nexusLogo-size1\" /></div>\n      <span class=\"pr-1\"><img src=\"../../assets/Nissan-logo.png\" style=\"height:60px ;\"></span>\n    </div>\n\n    <div class=\"modal-body\">\n      <div class=\"row\">\n          <div class=\"wrapper\">\n              <div class=\"two\"></div>\n              <div class=\"one\"></div>\n              </div>\n        <div class=\"col-md-4\">Personal Information</div>\n        <div id=\"circle\">\n            <div id=\"p1\"></div>\n        </div>\n        <div class=\"col-md-4\">Employment\n        </div>\n      </div>\n      <div class=\"mt-4\">Please verify your information below</div>\n\n      <div *ngFor=\"let empDetails of userAttributes.employers; let i = index\">\n        <div>\n          <div class=\"row mt-4\">\n            <div class=\"col-md-4 ns-font-13 ns-font-weight-bold\">Current Employer</div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Employer Name </label>\n              <input type=\"text\" class=\"form-control\" name=\"empName\" [(ngModel)]=\"empDetails.employer_name\">\n            </div>\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\"> Employer Phone Number</label>\n              <input type=\"text\" class=\"form-control\" name=\"empNumber\" [ngModel]=\"empDetails.phone_number | phoneNumber\"\n                (keypress)=\"numberOnly($event)\" (ngModelChange)=\"empDetails.phone_number = $event\" maxlength=\"14\">\n            </div>\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Self Employed?</label><br>\n              <label class=\"radio-inline col-md-3\">\n                <input type=\"radio\" name=\"optradio\" class=\"radio-size\" [(ngModel)]=\"empDetails.self_employed\"\n                  [value]=\"1\"> YES\n              </label>\n              <label class=\"radio-inline col-md-3\">\n                <input type=\"radio\" name=\"optradio\" class=\"radio-size\" [(ngModel)]=\"empDetails.self_employed\"\n                  [value]=\"0\"> NO\n              </label>\n            </div>\n          </div>\n          <div class=\"row mt-1\">\n            <div class=\"col-md-4 ns-font-13 ns-font-weight-bold\">Current Employer Address</div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Address1</label>\n              <input type=\"text\" class=\"form-control\" name=\"address1\" [(ngModel)]=\"empDetails.address.street1\">\n            </div>\n\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Address2</label>\n              <input type=\"text\" class=\"form-control\" name=\"address2\" [(ngModel)]=\"empDetails.address.street2\">\n            </div>\n\n            <div class=\"col-md-4\"></div>\n            <div class=\"col-md-8\">\n              <div class=\"row\">\n                <div class=\"form-group col-md-6 font-color\">\n                  <label for=\"usr\">City</label>\n                  <input type=\"text\" class=\"form-control\" name=\"city\" [(ngModel)]=\"empDetails.address.city\">\n                </div>\n                <div class=\"form-group col-md-6 font-color\">\n                  <label for=\"usr\">State</label>\n                  <input type=\"text\" class=\"form-control\" name=\"state\" [(ngModel)]=\"empDetails.address.state\">\n                </div>\n              </div>\n            </div>\n\n            <div class=\"col-md-4\"></div>\n            <div class=\"col-md-8\">\n              <div class=\"row\">\n                <div class=\"form-group col-md-6 font-color\">\n                  <label for=\"usr\">Zip</label>\n                  <input type=\"text\" class=\"form-control\" name=\"zipCode\" [(ngModel)]=\"empDetails.address.zipCode\">\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"row mt-1\">\n            <div class=\"col-md-4 ns-font-13 ns-font-weight-bold\">Employment</div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Occupation</label>\n              <input type=\"text\" class=\"form-control\" name=\"occupation\" [(ngModel)]=\"empDetails.occupation\">\n            </div>\n\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-4 font-color\">\n              <label for=\"usr\">Employment Start date</label>\n              <input type=\"text\" class=\"form-control text-secondary\" [(ngModel)]=\"empDetails.from_duration_in_date\"\n                [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" [maxDate]=\"maxDate\" placement=\"top\" #dp=\"bsDatepicker\"\n                bsDatepicker (bsValueChange)=\"calculateDuration($event, i)\">\n            </div>\n            <div class=\"col-md-4 pt-4\">\n              <span class=\"text-success\" *ngIf=\"empDetails.dateDuration\">{{empDetails.dateDuration}}</span>\n            </div>\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-4 font-color\">\n              <label for=\"usr\">Income Per Month</label>\n              <input type=\"text\" class=\"form-control\" name=\"incomePerMonth\" [(ngModel)]=\"empDetails.income\">\n            </div>\n            <div class=\"col-md-4\"></div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"next-btn cursor-pointer\" (click)=\"setEmployerDetails('previousEmployerDiv')\">Next</button>\n    </div>\n  </div>\n\n  <!-- Previous employer details form -->\n  <div class=\"container\" *ngIf=\"displayCurrentDiv == 'previousEmployerDiv'\">\n    <div class=\"modal-header\">\n      <div class=\"\"><img src=\"assets/nexus-log.png\" alt=\"\" class=\"nexusLogo-size1\" /></div>\n      <span class=\"pr-1\"><img src=\"../../assets/Nissan-logo.png\" style=\"height:60px ;\"></span>\n    </div>\n\n    <div class=\"modal-body\">\n      <div class=\"row\">\n        <div class=\"col-md-4\"> <i class=\"far fa-circle mr-2\"></i>Personal Information</div>\n        <div class=\"col-md-4\"><i class=\"far fa-circle mr-2\"></i>Employment\n        </div>\n      </div>\n      <div class=\"mt-4\">Please verify your information below</div>\n\n          <div class=\"row mt-4\">\n            <div class=\"col-md-4 ns-font-13 ns-font-weight-bold\">Previous Employer</div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Employer Name </label>\n              <input type=\"text\" class=\"form-control\" name=\"empName\" [(ngModel)]=\"previousEmployerData.employer_name\">\n            </div>\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\"> Employer Phone Number</label>\n              <input type=\"text\" class=\"form-control\" name=\"empNumber\" [ngModel]=\"previousEmployerData.phone_number | phoneNumber\"\n                (keypress)=\"numberOnly($event)\" (ngModelChange)=\"previousEmployerData.phone_number = $event\" maxlength=\"14\">\n            </div>\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Self Employed?</label><br>\n              <label class=\"radio-inline col-md-3\">\n                <input type=\"radio\" name=\"optradio\" class=\"radio-size\" [(ngModel)]=\"previousEmployerData.self_employed\"\n                  [value]=\"1\"> YES\n              </label>\n              <label class=\"radio-inline col-md-3\">\n                <input type=\"radio\" name=\"optradio\" class=\"radio-size\" [(ngModel)]=\"previousEmployerData.self_employed\"\n                  [value]=\"0\"> NO\n              </label>\n            </div>\n          </div>\n          <div class=\"row mt-1\">\n            <div class=\"col-md-4 ns-font-13 ns-font-weight-bold\">Previous Employer Address</div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Address1</label>\n              <input type=\"text\" class=\"form-control\" name=\"address1\" [(ngModel)]=\"previousEmployerData.address.street1\">\n            </div>\n\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Address2</label>\n              <input type=\"text\" class=\"form-control\" name=\"address2\" [(ngModel)]=\"previousEmployerData.address.street2\">\n            </div>\n\n            <div class=\"col-md-4\"></div>\n            <div class=\"col-md-8\">\n              <div class=\"row\">\n                <div class=\"form-group col-md-6 font-color\">\n                  <label for=\"usr\">City</label>\n                  <input type=\"text\" class=\"form-control\" name=\"city\" [(ngModel)]=\"previousEmployerData.address.city\">\n                </div>\n                <div class=\"form-group col-md-6 font-color\">\n                  <label for=\"usr\">State</label>\n                  <input type=\"text\" class=\"form-control\" name=\"state\" [(ngModel)]=\"previousEmployerData.address.state\">\n                </div>\n              </div>\n            </div>\n\n            <div class=\"col-md-4\"></div>\n            <div class=\"col-md-8\">\n              <div class=\"row\">\n                <div class=\"form-group col-md-6 font-color\">\n                  <label for=\"usr\">Zip</label>\n                  <input type=\"text\" class=\"form-control\" name=\"zipCode\" [(ngModel)]=\"previousEmployerData.address.zipCode\">\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"row mt-1\">\n            <div class=\"col-md-4 ns-font-13 ns-font-weight-bold\">Employment</div>\n            <div class=\"form-group col-md-8 font-color\">\n              <label for=\"usr\">Occupation</label>\n              <input type=\"text\" class=\"form-control\" name=\"occupation\" [(ngModel)]=\"previousEmployerData.occupation\">\n            </div>\n\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-4 font-color\">\n              <label for=\"usr\">Employment Start date</label>\n              <input type=\"text\" class=\"form-control text-secondary\" [(ngModel)]=\"previousEmployerData.from_duration_in_date\"\n                [bsConfig]=\"{ dateInputFormat: 'MM/DD/YYYY' }\" [maxDate]=\"maxDate\" placement=\"top\" #dp=\"bsDatepicker\"\n                bsDatepicker (bsValueChange)=\"calculateDuration($event, 'previousEmployer')\">\n            </div>\n            <div class=\"col-md-4 pt-4\">\n              <span class=\"text-success\" *ngIf=\"previousEmployerData.dateDuration\">{{previousEmployerData.dateDuration}}</span>\n            </div>\n            <div class=\"col-md-4\"></div>\n            <div class=\"form-group col-md-4 font-color\">\n              <label for=\"usr\">Income Per Month</label>\n              <input type=\"text\" class=\"form-control\" name=\"incomePerMonth\" [(ngModel)]=\"previousEmployerData.income\">\n            </div>\n            <div class=\"col-md-4\"></div>\n          </div>\n\n        </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"next-btn cursor-pointer\" (click)=\"setPreviousEmployerDetails(previousEmployerData)\">Submit</button>\n      <button type=\"button\" class=\"skip-btn bg-danger cursor-pointer\" (click)=\"closeModal()\">No previous employer</button>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/verify-loan-data/verify-loan-data.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/verify-loan-data/verify-loan-data.component.ts ***!
  \****************************************************************/
/*! exports provided: VerifyLoanDataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyLoanDataComponent", function() { return VerifyLoanDataComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend/backend.service */ "./src/app/backend/backend.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common/utils */ "./src/app/common/utils.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (undefined && undefined.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};









var VerifyLoanDataComponent = /** @class */ (function () {
    function VerifyLoanDataComponent(notifierService, authService, backend, modalService, spinnerService, translate) {
        this.notifierService = notifierService;
        this.authService = authService;
        this.backend = backend;
        this.modalService = modalService;
        this.spinnerService = spinnerService;
        this.translate = translate;
        this.verifyChildEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.displayCurrentDiv = 'welcomeDiv';
        this.DATE_FORMAT = 'YYYY-MM-DD';
        this.previousEmployer = 0;
        this.previousEmployerData = {
            'preferred': 0,
            'present': 0,
            'from_duration_in_date': null,
            'dateDuration': 0,
            'address': {
                street1: null,
                city: null,
                street2: null,
                state: null,
                zip: null
            },
            'to_duration': null,
            'from_duration': null,
            'email': null,
            'phone_number': null,
            'contact_person': null,
            'title': null,
            'occupation': null,
            'income': null,
            'self_employed': null,
            'employer_name': null
        };
        this.notifier = notifierService;
        // this is default langugage
        translate.setDefaultLang('en');
        // this language is to set by your prefered browser language
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang);
    }
    VerifyLoanDataComponent.prototype.ngOnInit = function () {
        // Clone this
        this.userAttributes = _common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].cloneJSON(this.authService.getUserProfile());
        if (this.userAttributes.dob) {
            this.userAttributes.dobInDate = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](this.userAttributes.dob).toDate();
        }
        else {
            this.userAttributes.dobInDate = null;
        }
        if (!this.userAttributes.address) {
            this.userAttributes.address = {
                street: null,
                city: null,
                street2: null,
                country: null,
                state: null,
                zip: null
            };
        }
        if ('employers' in this.userAttributes && this.userAttributes.employers && this.userAttributes.employers.length > 0) {
            try {
                for (var _a = __values(this.userAttributes.employers), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var emp = _b.value;
                    if (emp.from_duration) {
                        emp.from_duration_in_date = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](emp.from_duration).toDate();
                    }
                    else {
                        emp.from_duration_in_date = null;
                    }
                    if ('self_employed' in emp && emp.self_employed) {
                        emp.self_employed = true;
                    }
                    else {
                        emp.self_employed = false;
                    }
                    if (!emp.address) {
                        emp.address = {
                            street1: null,
                            city: null,
                            street2: null,
                            state: null,
                            zip: null
                        };
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        this.authResultInfo = this.authService.getAccessTokenAndID();
        var e_1, _c;
    };
    VerifyLoanDataComponent.prototype.changeModalDiv = function (divName) {
        this.displayCurrentDiv = divName;
    };
    /* Update profile Code */
    VerifyLoanDataComponent.prototype.updateUserProfile = function (divName) {
        var _this = this;
        var data = {};
        var requiredField = true;
        if (!_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(this.userAttributes, 'first_name')) {
            data.first_name = this.userAttributes.first_name;
        }
        else {
            requiredField = false;
        }
        if ('last_name' in this.userAttributes) {
            data.last_name = this.userAttributes.last_name;
            if (data.last_name == '') {
                data.last_name = null;
            }
        }
        if (!_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(this.userAttributes, 'dobInDate')) {
            data.dob = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](this.userAttributes.dobInDate).format(this.DATE_FORMAT);
            this.userAttributes.dob = data.dob;
        }
        if (!_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(this.userAttributes, 'address')) {
            data.address = JSON.parse(JSON.stringify(this.userAttributes.address));
        }
        if (!_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(this.userAttributes, 'emails')) {
            data.emails = this.userAttributes.emails;
        }
        if (!_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(this.userAttributes, 'phone_numbers')) {
            data.phone_numbers = this.userAttributes.phone_numbers;
        }
        if (!_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(this.userAttributes, 'home_phone')) {
            data.home_phone = this.userAttributes.home_phone;
        }
        console.log("Data : ", data);
        this.spinnerService.show();
        var path = "/users/" + this.authResultInfo.idTokenPayload.sub;
        if (requiredField) {
            this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(function (res) {
                _this.spinnerService.hide();
                // translate error message
                _this.translate.get('EPM_UPDATE_PROFILE')
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
                _this.displayCurrentDiv = divName;
                var response = res.json();
                console.log('Response: ', response);
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.userAttributes);
            }, function (err) {
                _this.spinnerService.hide();
                var errorVar = err.json();
                console.log('Error: ', errorVar["message"]);
                // translate error message
                _this.translate.get('EPM_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + errorVar["message"]);
                });
            });
        }
        else {
            this.spinnerService.hide();
            // translate error message
            this.translate.get('EPM_FIRSTNAME_REQUIRED')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
        }
    };
    VerifyLoanDataComponent.prototype.setEmployerDetails = function (divName) {
        var _this = this;
        var data = {
            'account_verified': 1,
            'update_account_status': true
        };
        this.userAttributes.account_verified = 1;
        var requiredField = true;
        if ('employers' in this.userAttributes && this.userAttributes.employers) {
            try {
                for (var _a = __values(this.userAttributes.employers), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var index = _b.value;
                    if (_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(index, 'employer_name')) {
                        requiredField = false;
                    }
                    if (index.from_duration_in_date) {
                        index.from_duration = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](index.from_duration_in_date).format(this.DATE_FORMAT);
                    }
                    else {
                        delete index.from_duration_in_date;
                        delete index.from_duration;
                    }
                    delete index.user_id;
                    delete index.years;
                    delete index.months;
                    delete index.created_at;
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_2) throw e_2.error; }
            }
            data.employers = JSON.parse(JSON.stringify(this.userAttributes.employers));
        }
        try {
            for (var _d = __values(data.employers), _e = _d.next(); !_e.done; _e = _d.next()) {
                var index = _e.value;
                if (!index.address) {
                    delete index.address;
                }
                if (!index.phone_number) {
                    delete index.phone_number;
                }
                if (!index.email) {
                    delete index.email;
                }
                if (!index.contact_person) {
                    delete index.contact_person;
                }
                if (!index.title) {
                    delete index.title;
                }
                delete index.from_duration_in_date;
                delete index.to_duration_in_date;
                delete index.seeTransaction;
                delete index.dateDuration;
                if (!index.from_duration) {
                    delete index.from_duration;
                }
                if (!index.to_duration) {
                    delete index.to_duration;
                }
                if (!index.income) {
                    delete index.income;
                }
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (_e && !_e.done && (_f = _d.return)) _f.call(_d);
            }
            finally { if (e_3) throw e_3.error; }
        }
        var path = "/users/" + this.authResultInfo.idTokenPayload.sub;
        if (requiredField) {
            this.spinnerService.show();
            this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(function (res) {
                _this.spinnerService.hide();
                _this.translate.get('PC_EMPL_UPDATE_SUCCES')
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
                var response = res.json();
                console.log('Response#: ', response);
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.userAttributes);
                if (divName == 'previousEmployerDiv' && _this.previousEmployer < 24) {
                    _this.displayCurrentDiv = divName;
                }
                else {
                    _this.closeModal('reload');
                }
            }, function (err) {
                _this.spinnerService.hide();
                var errorVar = err.json();
                console.log('Error: ', errorVar["message"]);
                _this.translate.get('DASH_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + ": " + errorVar["message"]);
                });
            });
        }
        else {
            this.spinnerService.hide();
            this.translate.get('PC_SELECET_ATLEAST_ONE')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
        }
        var e_2, _c, e_3, _f;
    };
    VerifyLoanDataComponent.prototype.closeModal = function (event) {
        this.verifyChildEvent.emit(event);
    };
    // restrict alpha character and allow only number
    VerifyLoanDataComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    // restrict special character 
    VerifyLoanDataComponent.prototype.alphabetOnly = function (event) {
        var regex = new RegExp("^[a-zA-Z ]*$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    };
    VerifyLoanDataComponent.prototype.calculateDuration = function (event, index) {
        var now = moment__WEBPACK_IMPORTED_MODULE_7__["utc"]();
        var fromDuration = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](event);
        var year = now.diff(fromDuration, 'year');
        var months = now.diff(fromDuration, 'months');
        var days = now.diff(fromDuration, 'days');
        var dateDiffrence = _common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].diffYearMonthes(year, months, days);
        if (index == 'previousEmployer') {
            this.previousEmployerData.dateDuration = dateDiffrence;
        }
        else {
            this.userAttributes.employers[index].dateDuration = dateDiffrence;
        }
        this.previousEmployer = months;
    };
    VerifyLoanDataComponent.prototype.setPreviousEmployerDetails = function (preData) {
        var _this = this;
        var data = {
            'employers': []
        };
        var requiredField = true;
        if (_common_utils__WEBPACK_IMPORTED_MODULE_8__["default"].isNullOrEmpty(preData, 'employer_name')) {
            requiredField = false;
        }
        if (preData.from_duration_in_date) {
            preData.from_duration = moment__WEBPACK_IMPORTED_MODULE_7__["utc"](preData.from_duration_in_date).format(this.DATE_FORMAT);
        }
        this.userAttributes.employers.push(preData);
        var emp = [];
        emp.push(preData);
        data.employers = JSON.parse(JSON.stringify(emp));
        try {
            for (var _a = __values(data.employers), _b = _a.next(); !_b.done; _b = _a.next()) {
                var index = _b.value;
                delete index.from_duration_in_date;
                delete index.to_duration_in_date;
                delete index.dateDuration;
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_4) throw e_4.error; }
        }
        if (requiredField) {
            this.spinnerService.show();
            this.backend.post(data, '/users/employers', this.authResultInfo.accessToken).subscribe(function (res) {
                _this.spinnerService.hide();
                _this.translate.get('PC_EMPL_UPDATE_SUCCES')
                    .subscribe(function (value) {
                    _this.notifier.notify('success', value);
                });
                var response = res.json();
                console.log('Response#: ', response);
                _this.authService.setDataInCache(_this.authService.USER_PROFILE, _this.userAttributes);
                _this.closeModal('reload');
            }, function (err) {
                _this.spinnerService.hide();
                var errorVar = err.json();
                console.log('Error: ', errorVar["message"]);
                _this.translate.get('DASH_ERROR')
                    .subscribe(function (value) {
                    _this.notifier.notify('error', value + ": " + errorVar["message"]);
                });
            });
        }
        else {
            this.spinnerService.hide();
            this.translate.get('PC_SELECET_ATLEAST_ONE')
                .subscribe(function (value) {
                _this.notifier.notify('error', value);
            });
        }
        var e_4, _c;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], VerifyLoanDataComponent.prototype, "verifyChildEvent", void 0);
    VerifyLoanDataComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-verify-loan-data',
            template: __webpack_require__(/*! ./verify-loan-data.component.html */ "./src/app/verify-loan-data/verify-loan-data.component.html"),
            styles: [__webpack_require__(/*! ./verify-loan-data.component.css */ "./src/app/verify-loan-data/verify-loan-data.component.css"), __webpack_require__(/*! ../global/global.css */ "./src/app/global/global.css")]
        }),
        __metadata("design:paramtypes", [angular_notifier__WEBPACK_IMPORTED_MODULE_3__["NotifierService"],
            _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _backend_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalService"],
            ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"]])
    ], VerifyLoanDataComponent);
    return VerifyLoanDataComponent;
}());



/***/ }),

/***/ "./src/app/websocket/websocket.service.ts":
/*!************************************************!*\
  !*** ./src/app/websocket/websocket.service.ts ***!
  \************************************************/
/*! exports provided: WebsocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebsocketService", function() { return WebsocketService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WebsocketService = /** @class */ (function () {
    function WebsocketService() {
    }
    WebsocketService.prototype.connect = function (url) {
        if (!this.subject) {
            this.subject = this.create(url);
            console.log("Successfully connected: " + url);
        }
        return this.subject;
    };
    WebsocketService.prototype.create = function (url) {
        var ws = new WebSocket(url);
        var observable = rxjs_Rx__WEBPACK_IMPORTED_MODULE_1__["Observable"].create(function (obs) {
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);
            return ws.close.bind(ws);
        });
        var observer = {
            next: function (data) {
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(JSON.stringify(data));
                }
            }
        };
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_1__["Subject"].create(observer, observable);
    };
    WebsocketService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], WebsocketService);
    return WebsocketService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    auth: {
        clientID: '7q6hiRHKiZlGvgqGGtEHpt8DTWHNWtTH',
        domain: 'aclaro.auth0.com',
        audience: 'https://localhost:4200',
        redirect: 'http://localhost:4200/',
        scope: 'openid profile email'
    },
    placid: {
        apiid: '58b5868dead2e3931a909a866b60ca',
        environment: "sandbox",
        productArray: ['transactions', 'assets']
    },
    apibaseurl: 'https://api-dev.nexusscore.com',
    nexusscoremeterurl: 'https://aclaronexusscore.azurewebsites.net/CheckScore',
    wsapiurl: 'wss://vv6govd0m0.execute-api.us-east-1.amazonaws.com/dev'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/deepak/Desktop/CodeBlock/nexus/www/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map