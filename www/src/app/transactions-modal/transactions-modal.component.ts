import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-transactions-modal',
  templateUrl: './transactions-modal.component.html',
  styleUrls: ['./transactions-modal.component.css']
})
export class TransactionsModalComponent implements OnInit {

  @Input() transactions: any= [];
  orderByField = '-date';
  constructor( public modalRef : BsModalRef) { }

  ngOnInit() {
    console.log('inside TransactionsModalComponent');
    console.log('transactionsModal', this.transactions);
  }

  orderByDate(){
    if(this.orderByField && this.orderByField === '-date'){
      this.orderByField = 'date';
    }else{
      this.orderByField = '-date';
    }
  }

}
