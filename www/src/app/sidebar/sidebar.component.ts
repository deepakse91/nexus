import { Component, TemplateRef, OnChanges, OnInit, Input, EventEmitter, Output, SimpleChanges,ViewChild, ElementRef } from '@angular/core';
import { SessionStorageService } from 'angular-web-storage';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NotifierService } from 'angular-notifier';
import { AuthService } from '../auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css','../global/global.css']
})
export class SidebarComponent implements OnInit, OnChanges {
  @Input("reloadScreen") reloadScreen: any;
  @Input("sidebarReload") sidebarReload: any;
  @Input("callPropertyButton") callPropertyButton: any;
  @Output() accEmit = new EventEmitter<any>();
  @Output() propertyDataEmit = new EventEmitter();
  @Output() multipleHandlerEmit = new EventEmitter<any>();
  @Output() booleanValueForSpinner = new EventEmitter<any>();
  accounts: any = {};
  private readonly notifier: NotifierService;
  modalRef: BsModalRef;
  totalBal: any = {};
  user: any;
  allAccounts: any;
  allProperty: any = [];
  loanAccountNotExist: boolean;
  modalFlag:any = {prop:false, loan:false, investment:false}

  statusToIconMapping: object = {
    completed: '../assets/success.png',
    failed: '../assets/close-icon.png',
    inprogress: '../assets/loading-icon.gif'
  };
  //invoke the openAddPropertyModal() method
  @ViewChild('invokeProperty') invokeProperty: ElementRef;

  constructor(private session: SessionStorageService,
    private modalService: BsModalService,
    private notifierService: NotifierService,
    private spinnerService: NgxSpinnerService,
    private authService: AuthService) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.user = this.authService.getUserProfile();
    this.allAccounts = [];
    this.allAccounts = this.user.accounts;
    this.allProperty = this.user.properties;
    this.totalBal = { cash: 0, credit: 0, property: 0, loan: 0, investments: 0 };
    console.log('updated');

    if (this.allAccounts && this.allAccounts.length > 0) {
      for (let acc of this.allAccounts) {
        let checkPoint;
        if(!(acc.sync_status)){
          checkPoint = true;
        }else {
          checkPoint = false;
        }
        
        this.booleanValueForSpinner.emit(checkPoint);

        // check official name duplicate or not
        if ('accounts' in acc && acc.accounts) {
          for (let i = 0; i < acc.accounts.length; i++) {
            if (acc.accounts[i].type == "depository") {
              for (let j = 0; j < acc.accounts.length; j++) {
                if (acc.accounts[j].type == "depository") {
                  if (!(i === j)) {
                    if (!(acc.accounts[i].official_name === acc.accounts[j].official_name)) {
                      if (!(acc.accounts[i].matchOfficialName)) {
                        acc.accounts[i].matchOfficialName = false;
                      }
                    } else {
                      acc.accounts[i].matchOfficialName = true;
                    }
                  }
                }
              }
            }
          }

          //allAccounts balance
          for (let i of acc.accounts) {
            // depository type balance
            if (i.type == "depository" && (i.status == 'ACTIVE')) {
              this.totalBal.cash += i.balances.current;
              acc.testDepos = true;
            }
            // credit type balance
            if (i.type == "credit" && (i.status == 'ACTIVE')) {
              if ("current" in i.balances || i.balances.current == 0) {
                this.totalBal.credit += i.balances.current;
                acc.testCredit = true;
              }
            }
            // investments type balance
            if (i.type == "brokerage" && (i.status == 'ACTIVE')) {
              if ("current" in i.balances || i.balances.current == 0) {
                this.totalBal.investments += i.balances.current;
                acc.testInvestments = true;
              }
            }
            // loan type balance
            if (i.type == "loan" && (i.status == 'ACTIVE')) {
              if('official_name' in i && !(i.official_name) && 'inst_name' in i && i.inst_name){
                i.official_name = i.inst_name;
              }
              
              if ("current" in i.balances || i.balances.current == 0) {
                this.totalBal.loan += i.balances.current;
                acc.testloan = true;
                this.loanAccountNotExist = true;
              }
            } else {
              this.loanAccountNotExist = false;
            }
          }
        }
      }
    }

    // properties balance
    if (this.allProperty && this.allProperty.length > 0) {
      for (let i of this.allProperty) {
        this.totalBal.property += i.estimated_value;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
    if (('reloadScreen' in changes && changes.reloadScreen != null) || ('sidebarReload' in changes && changes.sidebarReload != null)) {
       this.ngOnInit();
    }

    if ('callPropertyButton' in changes && changes.callPropertyButton && 'currentValue' in changes.callPropertyButton && changes.callPropertyButton.currentValue) {
      this.invokeProperty.nativeElement.click()
    }
  }

  emitAccount(accId) {
    this.accEmit.emit(accId);
  }

  openAddPropertyModal(addPropertyModal: TemplateRef<any>, event) {

    let ngbModalOptions: any = {
      class: "gray modal-lg"
    };

     if(event == 'PROP_MODAL'){
        this.modalFlag.prop = true;
        this.modalFlag.investment = false;
        this.modalFlag.loan = false;
        ngbModalOptions['backdrop'] = 'static';
        ngbModalOptions['keyboard'] = false;
     }
     if(event == 'INV_MODAL'){
      this.modalFlag.investment = true;
      this.modalFlag.prop = false;
      this.modalFlag.loan = false;
     }
     if(event == 'LOAN_MODAL'){
      this.modalFlag.loan = true;
      this.modalFlag.prop = false;
      this.modalFlag.investment = false;
     }
    
    this.modalRef = this.modalService.show(
      addPropertyModal,
      ngbModalOptions
    );
  }

  addPropertyModalClose() {
    this.modalRef.hide();
  }

  modalCloseAfterGet(event) {
    this.modalRef.hide();
    if(event){
      this.allProperty = event;
      this.propertyDataEmit.emit(event);
      this.ngOnInit();
    }
  }

  getKeys(map) {
    return Array.from(Object.keys(map));
  }

  
  multipleHandler(refrenceName) {
    this.multipleHandlerEmit.emit(refrenceName);
  }
}
