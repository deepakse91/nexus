import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import Utils from '../common/utils';
import * as moment from 'moment';

@Component({
  selector: 'app-nisaan',
  templateUrl: './nisaan.component.html',
  styleUrls: ['./nisaan.component.css']
})
export class NisaanComponent implements OnInit {
  @Input("applicationJson") applicationJson: any;
  @Input("invokePdf") invokePdf: any;
  user:any;
  changeText:boolean = true;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if('applicationJson' in changes && 'currentValue' in changes.applicationJson && changes.applicationJson.currentValue){
      this.user = changes.applicationJson.currentValue;
      if(!Utils.isNullOrEmpty(this.user, 'familyName')){
        this.user.name = this.user.name + ' ' + this.user.familyName;
      }
      if(!('birthDate' in this.user && this.user.birthDate)){
        this.user.birthDate = "";
      }
      if ('address' in this.user && this.user.address && 'street2' in this.user.address && this.user.address.street2) {
        this.user.address.streetAddress = this.user.address.streetAddress + ', ' + this.user.address.street2;
      }

      // for previous address
      this.user['previous addresses'][0].fullAddress = "";
      if(!Utils.isNullOrEmpty(this.user, 'previous addresses') && this.user['previous addresses'].length > 0){
        this.user['previous addresses'][0].fullAddress = Utils.constructAddress(this.user['previous addresses'][0]);
      }

      // for current employer
      this.user.worksFor.empAddress = "";
      if('worksFor' in this.user && 'address' in this.user.worksFor && Object.keys(this.user.worksFor.address).length > 0){
        this.user.worksFor.empAddress = Utils.constructAddress(this.user.worksFor.address);
      }

      // for self employed
      if(!Utils.isNullOrEmpty(this.user.worksFor, 'self_employed')){
        if(this.user.worksFor.self_employed){
          this.user.worksFor.self_employed = 1;
          this.user.worksFor.self_NoEmployed = 1;
        } else {
          this.user.worksFor.self_employed = 0;
          this.user.worksFor.self_NoEmployed = 0;
        }
      }

      // for months and years
      if(!Utils.isNullOrEmpty(this.user.worksFor, 'from')){
        let presentEmployer = moment.utc();
        let fromDuration = moment.utc(this.user.worksFor.from);
        let months = presentEmployer.diff(fromDuration, 'months');
        this.user.worksFor.year = presentEmployer.diff(fromDuration, 'year');
        this.user.worksFor.months = months - (this.user.worksFor.year * 12);
      }

      // for previous employer
      this.user.preEmployer = {
        name:'',
        phone_number:''
      };
      if('worksFor' in this.user && 'setDuration' in this.user.worksFor && this.user.worksFor.setDuration > 2){
        if(this.user.workedAt && this.user.workedAt.length > 0){
          let workedAtByDuration = this.user.workedAt.sort(function (a, b) { return (a.setDuration - b.setDuration); });
          if(workedAtByDuration && workedAtByDuration.length > 0){
            this.user.preEmployer.name = workedAtByDuration[0].name + ', ' + Utils.constructAddress(workedAtByDuration[0].address);
            this.user.preEmployer.phone_number = workedAtByDuration[0].phone_number;
          }
        }
      } 
      console.log('User', this.user);
    }

    if('invokePdf' in changes && 'currentValue' in changes.invokePdf && changes.invokePdf.currentValue && typeof changes.invokePdf.currentValue == 'string'){
      this.printPdf();
    }
  }

  // for print 
  printPdf() {
    window.print();
  }

}
