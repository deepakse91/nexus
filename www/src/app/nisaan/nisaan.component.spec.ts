import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NisaanComponent } from './nisaan.component';

describe('NisaanComponent', () => {
  let component: NisaanComponent;
  let fixture: ComponentFixture<NisaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NisaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NisaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
