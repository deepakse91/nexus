import { Component, OnInit, Input} from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {NgxSpinnerService} from "ngx-spinner";
import {BackendService} from "../backend/backend.service";
import * as moment from "moment";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-report-transaction',
  templateUrl: './report-transaction.component.html',
  styleUrls: ['./report-transaction.component.css']
})
export class ReportTransactionComponent implements OnInit {

  transactions: any = [];
  DATE_FORMAT: string = 'YYYY-MM-DD';
  selectedAccountFilter: any = 'all';
  selectedDateFilter: string = '3months';
  orderByField = '-date';
  datePrefrence:string = null;
  dateFilterArray:any = [
    {
      key:'G_LAST_7_DAYS',
      value:'7days'
    },
    {
      key:'G_LAST_14_DAYS',
      value:'14days'
    },
    {
      key:'G_THIS_MONTH ',
      value:'thismonth'
    },
    {
      key:'G_LAST_3_MONTHS',
      value:'3months'
    }
  ];
  @Input("accounts") accounts: any=[];
  @Input("report") report: any=[];


  constructor(private authService: AuthService,private spinnerService: NgxSpinnerService,private backend: BackendService, public translate: TranslateService) { }

  ngOnInit() {   

      // Get transactions also
      let start_date = moment.utc().subtract(this.report.trans_interval - 1,'months').format(this.DATE_FORMAT);
      let end_date = moment.utc().format(this.DATE_FORMAT);
      this.populateTransactions(start_date, end_date, null);
      
      if(this.report.trans_interval === 6){
        this.dateFilterArray.push({
          key:'Last 6 months',
          value:'6months'
        });
        this.selectedDateFilter = null;
        this.selectedDateFilter = '6months';
  
      }else if(this.report.trans_interval === 12){
        this.dateFilterArray.push(
          {
            key:'Last 6 months',
            value:'6months'
          },
          {
            key:'Last 12 months',
            value:'12months'
          }
        );
        this.selectedDateFilter = null;
        this.selectedDateFilter = '12months';
      }
    }
  
    

  populateTransactions(start_date, end_date, accounts) {
    this.spinnerService.show();
    console.log('populateTransactions Data', accounts);

    // Make get transactions call
    let path = '/users/transactions';
    let qp = [];
    if(start_date){
      qp.push('start_date='+start_date);
    }
    if(end_date){
      qp.push('end_date='+end_date);
    }
    if(accounts){
      qp.push('account_ids='+accounts);
    }

    if(qp && qp.length>0) {
      path +='?'+qp.join('&');
    }
    console.log(path);
    this.backend.get(path,this.report.token)
      .subscribe(data => {
        let allTransactions = data.json();
        console.log('report-trans', allTransactions);


        let clonedTransaction = [];
            for (let obj of allTransactions.transactions) {
              let b = null;
              b = Object.assign({},obj);
              clonedTransaction.push(b);
            }

        // change the account value; if type depository
        if(clonedTransaction){
          for(let t of clonedTransaction){
            if(t.account_type == 'depository' || t.account_type == 'credit'){
              if(t.amount < 0){
                t.amount = t.amount * -1
              }else{
               t.amount = t.amount * -1
              }
            }
          }
        }

        this.transactions = clonedTransaction;
        this.spinnerService.hide();
      });
  }

  getstartenddate() {
    let start_date = '';
    let end_date = '';
    let passVar = this.selectedDateFilter;
    switch (passVar) {
      case '7days':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(6, 'days').format(this.DATE_FORMAT);
        break;
      case '12months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(11, 'months').startOf('month').format(this.DATE_FORMAT);
        break;
      case '14days':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(13, 'days').format(this.DATE_FORMAT);
        break;
      case 'thismonth':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().startOf('month').format(this.DATE_FORMAT);
        break;
      case '3months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(2, 'months').startOf('month').format(this.DATE_FORMAT);
        break;
      case '6months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(5, 'months').startOf('month').format(this.DATE_FORMAT);
        break;
      case 'thisyear':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().startOf('year').format(this.DATE_FORMAT);
        break;
      case 'lastyear':
        end_date = moment.utc().subtract('1','year').endOf('year').format(this.DATE_FORMAT);
        start_date = moment.utc().subtract('1','year').startOf('year').format(this.DATE_FORMAT);
        break;
    }
    return {start_date: start_date, end_date: end_date};
  }

  filterForTransactions(event) {
    this.spinnerService.show();
    this.transactions = [];
    let dates: any = this.getstartenddate();
    console.log("dates", dates);

    let selectedAccountsFilter = this.selectedAccountFilter;
    console.log('accounts filter',selectedAccountsFilter);
    let accountsFilter = null;
    if(selectedAccountsFilter == 'all'){
      accountsFilter = null;
    }else {
      accountsFilter = selectedAccountsFilter.account_id;
    }
    this.populateTransactions(dates.start_date, dates.end_date, accountsFilter);
  }

  orderByDate(){
    if(this.orderByField && this.orderByField === '-date'){
      this.orderByField = 'date';
    }else{
      this.orderByField = '-date';
    }
  };

  //datePrefrence method
  setDatePrefrence(e){
    console.log('setDatePrefrence',this.datePrefrence);
    this.spinnerService.show();
    let data:any = {}
    if(this.datePrefrence){
     data.preferences = this.datePrefrence;
    }
    let path = "/users/" + this.authService.getAccessTokenAndID().idTokenPayload.sub;
     this.backend.put(data, path, this.authService.getAccessTokenAndID().accessToken)
     .subscribe(res => {
       console.log('preferencesRes', res);
       let clonedTrans = [...this.transactions];
        if(res.status == 204){
          this.transactions = [];
           for(let t of clonedTrans){
             if(this.datePrefrence == 'uk'){
               t.date = moment.utc(t.date, 'MM-DD-YYYY').format('DD-MM-YYYY')            
             }
             else{
               t.date = moment.utc(t.date, 'DD-MM-YYYY').format('MM-DD-YYYY')
             }
           }
        }
        //update transactions
        this.transactions = [...this.transactions,...clonedTrans];
         this.spinnerService.hide();
       },
       err => {
         console.log('preferenceError', err)
         this.spinnerService.hide();
       });
   }
}
