import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { ActivatedRoute } from "@angular/router";
import { NotifierService } from 'angular-notifier';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private readonly notifier: NotifierService;
  routeHash: string = null;

  constructor(private authService: AuthService, private route: ActivatedRoute, private notifierService: NotifierService) {
    this.notifier = notifierService;
    this.routeHash = route.snapshot.fragment;
    // console.log('fragment',this.routeHash);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(response => {
      console.log('response', response);
      if ('success' in response && response.success && 'message' in response) {
          this.notifier.notify('success', response.message);
      }
    });
    this.authService.showLogin();
  }
}
