import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {BackendService} from '../backend/backend.service';
import {AuthService} from "../auth/auth.service";
import {SessionStorageService} from "angular-web-storage";
import {NgxSpinnerService} from 'ngx-spinner';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-add-address-modal',
  templateUrl: './add-address-modal.component.html',
  styleUrls: ['./add-address-modal.component.css']
})
export class AddAddressModalComponent implements OnInit {
  @Input("employerDetail") employerDetail: any;
  @Output() closeAddressmodal = new EventEmitter();
  private readonly notifier: NotifierService;

  constructor(private authService: AuthService,
              private backend: BackendService,
              public session: SessionStorageService,
              private spinnerService: NgxSpinnerService,
              private notifierService: NotifierService) 
              {
                this.notifier = notifierService;
              }

  ngOnInit() {
    if(!(this.employerDetail.employer.address)){
      this.employerDetail.employer.address = {
        street1:null,
        street2:null,
        country:null,
        state:null,
        city:null,
        zipCode:null
      };
    }
  }

  modalClose(value){
    this.closeAddressmodal.emit(value);
  }

  addEmployerAddress(){
    this.modalClose(this.employerDetail);
  }

  // restrict characters and allow only number
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
