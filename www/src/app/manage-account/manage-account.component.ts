import { Component, OnInit, Input, OnChanges, AfterViewInit, SimpleChange, SimpleChanges, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { SessionStorageService } from 'angular-web-storage';
import { BackendService } from "../backend/backend.service";
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PlaidLinkHandler } from 'ngx-plaid-link/lib/ngx-plaid-link-handler';
import { NgxPlaidLinkService } from "ngx-plaid-link";
import { NotifierService } from 'angular-notifier';
import { environment } from './../../environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-manage-account',
  templateUrl: './manage-account.component.html',
  styleUrls: ['./manage-account.component.css'],
  // providers:[WebsocketService,RealTimeService]
})
export class ManageAccountComponent implements OnInit, AfterViewInit, OnChanges {
  @Input("addAnotherAcc") addAccByCongratePage: any;
  @Input("pubToken") publicToken: any;
  @Input("propertyAttribute") propertyAttribute: any;
  @Input("callTheAddAccount") callTheAddAccount: any;
  @Input("hasRealTimeData") hasRealTimeData: any;
  @Input("logoUpdated") logoUpdated: any;
  @Output() syncChildEvent = new EventEmitter();
  @Output() congratePageScreen = new EventEmitter();
  @Output() controlData = new EventEmitter();
  @Output() propertyHandlerEmit = new EventEmitter<any>();
  @Output() employerDivHide = new EventEmitter();
  propertyVariable: any = { value: null, index: null };
  private readonly notifier: NotifierService;
  private plaidLinkHandler: PlaidLinkHandler;
  CONGRATS_PAGE: string = '/congratpage';
  user: any;
  userToken: any;
  dynamicText: string = null;
  userAttributes: any;

  @ViewChild('invokeAddAccount') invokeAddAccount: ElementRef;
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('propertBtnClose') propertBtnClose: ElementRef;

  constructor(private session: SessionStorageService,
    private backend: BackendService,
    private authService: AuthService,
    public router: Router,
    private plaidLinkService: NgxPlaidLinkService,
    private spinnerService: NgxSpinnerService,
    public translate: TranslateService,
    private notifierService: NotifierService) {
    this.notifier = notifierService;


    // this is default langugage
    translate.setDefaultLang('en');

    // this language is to set by your prefered browser language
    let browserLang = translate.getBrowserLang();
    translate.use(browserLang);
  }

  ngOnInit() {
    this.userAttributes = this.authService.getUserProfile();
    this.userToken = this.authService.getDataFromCache('tokens');
    if (!this.userToken) {
      this.router.navigate(['/']);
    }
    this.user = this.authService.getUserProfile();
    
    this.controllActiveInactiveAccount(this.user.accounts);

    if ('properties' in this.user && this.user.properties && this.user.properties.length > 0) {
      for (let pro of this.user.properties) {
        if ('subtype' in pro && pro.subtype) {
          pro.subtypeToUpdate = pro.subtype;
        } else if ('name' in pro && pro.name) {
          pro.nameToUpdate = pro.name;
        }
      }
    }
    console.log('manage-accounts', this.user.accounts)

    if ('accounts' in this.user && this.user.accounts) {
      for (let acc of this.user.accounts) {
        if ('accounts' in acc && acc.accounts) {
          acc.subAccountsLength = acc.accounts.filter(e => (e.type == 'depository' || e.type == 'credit' || e.type == 'brokerage' || e.type == 'loan'));
        }
      }
    }
  }

  /* start Plaid */
  ngOnChanges(changes: SimpleChanges) {
    console.log('manage-changes', changes);
    if ('publicToken' in changes && changes.publicToken) {
      let getPublicToken: SimpleChange = changes.publicToken;
      console.log('Public Token ', getPublicToken.currentValue);
      if (getPublicToken.currentValue) {
        this.openPlaidWidget(getPublicToken.currentValue, false);
      }
    }

    if ('propertyAttribute' in changes && changes.propertyAttribute && 'currentValue' in changes.propertyAttribute && changes.propertyAttribute.currentValue) {
      this.ngOnInit()
    }

    if ('callTheAddAccount' in changes && changes.callTheAddAccount && 'currentValue' in changes.callTheAddAccount && (typeof (changes.callTheAddAccount.currentValue) == 'string')) {
      this.invokeAddAccount.nativeElement.click()
    }

    if ('addAccByCongratePage' in changes && changes.addAccByCongratePage && 'currentValue' in changes.addAccByCongratePage && changes.addAccByCongratePage.currentValue) {
      this.invokeAddAccount.nativeElement.click();
    }

    if ('hasRealTimeData' in changes && changes.hasRealTimeData && 'currentValue' in changes.hasRealTimeData && changes.hasRealTimeData.currentValue) {
      console.log('changes.hasRealTimeData.currentValue', changes.hasRealTimeData.currentValue);
    }

    if('logoUpdated' in changes && changes.logoUpdated && 'currentValue' in changes.logoUpdated && changes.logoUpdated.currentValue != changes.logoUpdated.previousValue) {
      this.ngOnInit();
    }

  }

  openPlaidWidget(public_token, varForNewAccount) {
    if (varForNewAccount) {
      public_token = null;
    }
    let params: any = {
      apiVersion: "v2",
      env: environment.placid.environment,
      product: environment.placid.productArray,
      key: environment.placid.apiid,
      webhook: environment.apibaseurl + '/updatewehbook',
      onSuccess: (token, metadata) => this.onSuccess(token, metadata, public_token),
      onExit: (error, metadata) => this.onExit(error, metadata, public_token)
    };
    if (public_token) {
      params.token = typeof public_token == "object" ? public_token.public_token : public_token;
    }

    this.plaidLinkService.createPlaid(params)
      .then((handler: PlaidLinkHandler) => {
        this.plaidLinkHandler = handler;
        if (!this.user.accounts || this.user.accounts.length == 0) {
          this.open();
        }
        if (public_token || varForNewAccount) {
          this.open();
        }
      });
  }

  ngAfterViewInit() {
    this.openPlaidWidget(null, false);
  }

  onExit(error, metadata, public_token) {
    console.log("We exited:", error);
    console.log("We got metadata:", metadata);
    if (!this.user.accounts || this.user.accounts.length == 0) {
      this.ngAfterViewInit()
    }
    if (public_token) {
      console.log('Inside public token success');
      this.syncChildEvent.emit();
    }
    this.employerDivHide.emit();
  }

  onSuccess(token, metadata, public_token) {
    console.log("We got metadata:", metadata);
    if (public_token) {
      console.log('Inside public token success');
      this.syncChildEvent.emit();
    } else {
      this.onPlaidSuccess({ token, metadata });
    }
  }

  addNewAccount() {
    this.openPlaidWidget(null, true);
  }

  open() {
    this.spinnerService.hide();
    this.plaidLinkHandler.open();
    return;
  }

  exit() {
    this.plaidLinkHandler.exit();
  }

  onPlaidSuccess(event) {
    console.log('onPlaidSuccess', event);
    this.spinnerService.show();
    let post_data = {
      "public_token": event.token || event.public_token,
      "inst_id": event.metadata.institution.institution_id,
      "inst_name": event.metadata.institution.name
    };
    let path = "/accounts";
    this.backend.post(post_data, path, this.userToken.accessToken)
      .subscribe(data => {
        let resAccount = data.json();
        console.log('successfully post your account', resAccount);

        // Need to update session also and then navigate
        this.user = this.authService.getUserProfile();
        if (!this.user.accounts) {
          this.user.accounts = [];
        }
        this.user.accounts.push(data.json());
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.user);
        // this.router.navigate([this.CONGRATS_PAGE]);
        this.congratePageScreen.emit(resAccount);
        this.spinnerService.hide();
      },
        error => {
          this.spinnerService.hide();
          let errMsg = JSON.parse(error._body);
          if ('message' in errMsg && errMsg.message && errMsg.message == 'ITEM_LOGIN_REQUIRED') {
            // translate error message
            this.translate.get('MA_WIDGET')
              .subscribe((value) => {
                this.notifier.notify('warning', value);
              })

          } else if ('message' in errMsg && errMsg.message == "Account is already added!") {
            this.translate.get('MA_ACCOUNT_ALREADY_ADDED')
              .subscribe((value) => {
                this.notifier.notify('error', value);
              })
          }
          else {
            this.translate.get('MA_ERROR')
              .subscribe((value) => {
                this.notifier.notify('error', value);
              })
          }

          // Check if 0 accounts then open plaid again
          if(!this.user.accounts || this.user.accounts.length === 0){
                  
            // This interval is added because without it notification was closing too fast
            setTimeout(()=>{    //<<<---    using ()=> syntax
                  this.open();
            }, 3000);
          }
        });
  }
  /* end Plaid */

  // collect the failed account
  showSyncStatus(msg) {
    if (msg && msg.id) {
      this.spinnerService.show();
      let path = "/accounts/" + msg.id + "/public_token";
      this.backend.get(path, this.userToken.accessToken)
        .subscribe(res => {
          let publicToken = res.json();
          this.openPlaidWidget(publicToken, false);
        },
          error => {
            let errorVar = error.json();
            this.translate.get('MA_ERRORMSG')
              .subscribe((value) => {
                this.notifier.notify('error', value);
              })
            this.spinnerService.hide();
          });
    }
  };

  showAccount(accounts, index) {
    if (accounts[index].showDetails) {
      accounts[index].showDetails = false;
    } else {
      accounts[index].showDetails = true;
    }
  }

  // controll div through active and inactive option
  controllActiveInactiveAccount(accounts){
    let mappedData;
        let statusCollection = [];
        for(let acc of accounts){
          mappedData = acc.accounts.map(item => item.status);
          statusCollection.push(mappedData);
        }
        statusCollection = [].concat(...statusCollection);

        if(!(statusCollection.includes('ACTIVE'))){
          this.controlData.emit('notactive');
        }else{
          this.congratePageScreen.emit('reload');
        }
  }

  setStatusOfAccount(accountId, accountObj) {
    let data: any = {};
    if ('official_name' in accountObj && accountObj.official_name) {
      data.officialName = accountObj.official_name;
    }
    if ('status' in accountObj && accountObj.status) {
      data.status = accountObj.status;
    }

    let path = "/accounts/" + accountId + "/subAccount/" + accountObj.account_id;
    if ('officialName' in data && data.officialName) {
      this.spinnerService.show();
      this.backend.put(data, path, this.userToken.accessToken).subscribe(res => {
        delete accountObj.showDetails;

        // update account
        let userDetail = this.authService.getDataFromCache(this.authService.USER_PROFILE);
        for(let account of userDetail.accounts){
          for(let acc of account.accounts){
            if(acc.account_id == accountObj.account_id){
              if ('official_name' in accountObj && accountObj.official_name) {
                acc.official_name = accountObj.official_name;
              }
              if ('status' in accountObj && accountObj.status) {
                acc.status = accountObj.status;
              }
            }
          }
        }

        this.authService.setDataInCache(this.authService.USER_PROFILE, userDetail);
        this.controllActiveInactiveAccount(userDetail.accounts);
        // this.ngOnInit();
        this.spinnerService.hide();
        this.translate.get("MA_UPDATE_ACCOUNT")
          .subscribe((value) => {
            this.notifier.notify('success', value);
          })

      },
        err => {
          let errorVar = err.json();
          this.translate.get('DASH_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + ": " + errorVar["message"]);
            })
          this.spinnerService.hide();
        });
    } else {
      this.translate.get('MA_RQR_FIELD_MISSING')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
    }
  }

  //edit account method
  editAccounts(accountId) {
    this.spinnerService.show();
    let path = "/accounts/" + accountId + "/public_token";
    this.backend.get(path, this.userToken.accessToken)
      .subscribe(res => {
        this.spinnerService.hide();
        let responce = res.json();
        if (responce && 'public_token' in responce && responce.public_token) {
          this.openPlaidWidget(responce.public_token, false);
        }
      },
        error => {
          let errorVar = error.json();
          this.translate.get('DASH_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + ": " + errorVar["message"]);
            })
          this.spinnerService.hide();
        });
  }

  // deleteAll Generic method
  deleteAll(stringValue) {
    if (stringValue == "All_Property") {
      this.translate.get("MA_PROPERTIES").subscribe(value => this.dynamicText = value);
    }
  }

  callDeleteAll(stringValue) {
    let parsedPropties;
    this.translate.get("MA_PROPERTIES").subscribe(value => parsedPropties = value);
    if (stringValue == parsedPropties) {
      this.deleteAllProperty();
    }
  }

  //delete account method
  deleteAccounts(accountId, index) {
    if (accountId) {
      let path = '/accounts/' + accountId;
      this.spinnerService.show();
      this.backend.delete(path, this.userToken.accessToken).subscribe(res => {
        this.translate.get('MA_ACC_DELETED')
          .subscribe((value) => {
            this.notifier.notify('success', value);
          })
        this.spinnerService.hide();
        this.propertBtnClose.nativeElement.click();

        // Read the data from cache again then remove the account and set
        // This is done because in cache other component might have updated data like profile name etc
        let userDataFromCache = this.authService.getDataFromCache(this.authService.USER_PROFILE);
        userDataFromCache.accounts.splice(index, 1);
        this.authService.setDataInCache(this.authService.USER_PROFILE, userDataFromCache);

        this.congratePageScreen.emit('sidereload');
        this.ngOnInit();
      },
        (err) => {
          this.spinnerService.hide();
          let errorVar = err.json();
          this.translate.get('DASH_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + ": " + errorVar["message"]);
            })
        });
    } else {
      this.translate.get('MA_RQR_FIELD_MISSING')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
    }
  }

  updatePropertyName(item, index) {
    let data: any = {};
    if ('subtype' in item && item.subtype) {
      data.subtype = item.subtype;
      this.user.properties[index].subtype = item.subtype;
    } else if ('name' in item && item.name) {
      data.name = item.name;
      this.user.properties[index].name = item.name;
    }
    let path = "/properties/" + item.id;
    if (('name' in data && data.name) || ('subtype' in data && data.subtype)) {
      this.spinnerService.show();
      this.backend.put(data, path, this.userToken.accessToken).subscribe(res => {
        delete item.showDetails;
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.user);
        this.congratePageScreen.emit('sidereload');
        this.ngOnInit();
        this.spinnerService.hide();
        this.translate.get('MA_PROPERTY_ADDED')
          .subscribe((value) => {
            this.notifier.notify('success', value);
          })
      },
        err => {
          let errorVar = err.json();
          this.translate.get('DASH_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + ": " + errorVar["message"]);
            })
          this.spinnerService.hide();
        });
    } else {
      this.translate.get('MA_RQR_FIELD_MISSING')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
    }
  }

  // delete propety method
  deleteIndividual(propertyValue, index, event) {
    console.log(event);
    let parsedAcc;
    let parsedProp;
    this.translate.get("MA_ACCOUNT").subscribe(value => parsedAcc = value);
    this.translate.get("MA_PROPERTY").subscribe(value => parsedProp = value)

    if (event == parsedAcc) {
      this.deleteAccounts(propertyValue, index);
    }
    if (event == parsedProp) {
      if (propertyValue.id) {
        let path = "/properties/" + propertyValue.id;
        this.spinnerService.show();
        this.backend.delete(path, this.userToken.accessToken).subscribe(res => {
          this.spinnerService.hide();
          this.translate.get('MA_PROPERTY_DELETED')
            .subscribe((value) => {
              this.notifier.notify('success', value);
            })

          // Read the data from cache again then remove the account and set
          // This is done because in cache other component might have updated data like profile name etc
          let userDataFromCache = this.authService.getDataFromCache(this.authService.USER_PROFILE);
          userDataFromCache.properties.splice(index, 1);
          this.authService.setDataInCache(this.authService.USER_PROFILE, userDataFromCache);

          this.propertBtnClose.nativeElement.click();
          this.congratePageScreen.emit('sidereload');
          this.ngOnInit();
        },
          err => {
            this.spinnerService.hide();
            let errorVar = err.json();
            this.translate.get('DASH_ERROR')
              .subscribe((value) => {
                this.notifier.notify('error', value + ": " + errorVar["message"]);
              })
          });
      } else {
        this.translate.get('MA_RQR_FIELD_MISSING')
          .subscribe((value) => {
            this.notifier.notify('error', value);
          })
      }
    }
  }

  passDltValue(value, index, event) {
    if (event == 'PROPERTY') {
      this.propertyVariable.value = null;
      this.propertyVariable.index = null;
      this.dynamicText = null;
      // this.dynamicText = 'property'
      this.translate.get('MA_PROPERTY').subscribe(value => this.dynamicText = value);
      this.propertyVariable.value = value;
      this.propertyVariable.index = index;
    }
    if (event == 'ACCOUNTS') {
      this.propertyVariable.value = null;
      this.propertyVariable.index = null;
      this.dynamicText = null;
      //this.dynamicText = 'account' 
      this.translate.get('MA_ACCOUNT').subscribe(value => this.dynamicText = value);
      this.propertyVariable.value = value;
      this.propertyVariable.index = index;
    }
  }

  deleteAllProperty() {
    let path = "/properties/" + -1;
    this.spinnerService.show()
    this.backend.delete(path, this.userToken.accessToken).subscribe(res => {
      this.translate.get('MA_PROPERTIES_DELETED')
        .subscribe((value) => {
          this.notifier.notify('success', value);
        })
      this.closeBtn.nativeElement.click();
      
      // Read the data from cache again then remove the account and set
        // This is done because in cache other component might have updated data like profile name etc
      let userDataFromCache = this.authService.getDataFromCache(this.authService.USER_PROFILE);
      userDataFromCache.properties.splice(0, this.user.properties.length);
      this.authService.setDataInCache(this.authService.USER_PROFILE, userDataFromCache);
      
      this.congratePageScreen.emit('sidereload');
      this.ngOnInit();
      this.spinnerService.hide();
    },
      err => {
        let errorVar = err.json();
        this.translate.get('DASH_ERROR')
          .subscribe((value) => {
            this.notifier.notify('error', value + ": " + errorVar["message"]);
          })
        this.closeBtn.nativeElement.click()
        this.spinnerService.hide();
      });
  }

  propertyHandler(PropertyName) {
    this.propertyHandlerEmit.emit(PropertyName);
  }

}

