import { Component, OnInit, TemplateRef, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {NotifierService} from 'angular-notifier';
import {BackendService} from '../backend/backend.service';
import {AuthService} from "../auth/auth.service";
import { TranslateService } from '@ngx-translate/core';
import { mergeMap } from "rxjs/operators";
import Utils from '../common/utils';

@Component({
  selector: 'app-add-property-modal',
  templateUrl: './add-property-modal.component.html',
  styleUrls: ['./add-property-modal.component.css','../global/global.css']
})
export class AddPropertyModalComponent implements OnInit {
  @Output() childEvent = new EventEmitter();
  @Output() propertiesChild = new EventEmitter();
  private readonly notifier: NotifierService;
  modalRef: BsModalRef;
  realEstateCurrentDiv="selectPropertyType";
  authResultInfo:any;
  realEstateData:any = {
    name:"Primary Residence",
    objAddress:{}
  };
  vehicleData:any = {
    name:"Automobile",
    year:"year",
    brand:"brand",
    model:"model",
    trim:"trim"
  };
  cashOrDebtData:any = {
    name:"Debt"
  };
  otherData:any = {
    name:"Artwork"
  };
  userAttributes:any;
  propertiesUpdated: boolean = false;
  
  constructor(private authService: AuthService,
              private backend: BackendService,
              private modalService: BsModalService,
              private spinnerService: NgxSpinnerService,
              private notifierService: NotifierService,
              public translate: TranslateService)
              {
                this.notifier = notifierService;
                // this is default langugage
                translate.setDefaultLang('en');

                // this language is to set by your prefered browser language
                let browserLang = translate.getBrowserLang();
                translate.use(browserLang);
              }

  ngOnInit() {
    this.userAttributes = this.authService.getUserProfile();
    this.authResultInfo = this.authService.getAccessTokenAndID();
  }

  closeModal(){
    // Check if properties where updated
    let event = null;
    if(this.propertiesUpdated){
      event = this.userAttributes.properties;
    }
    this.propertiesChild.emit(event);
  }
 
  openAddPropertyModal(addPropertyModal: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      addPropertyModal,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  changeModalDiv(divName){
    this.realEstateCurrentDiv = divName;
    if(divName=='realEstateI'){
      this.realEstateData.type = 'Real Estate';
    } else if(divName=='VehicleI'){
      this.vehicleData.type = 'Vehicle';
    } else if(divName=='cashOrDebtI'){
      this.cashOrDebtData.type = 'Cash or Debt';
    } else if(divName=='otherI'){
      this.otherData.type = 'Other';
    } else if(divName=='selectPropertyType'){
      this.resetPropertiesValue();
    }
  }

  resetPropertiesValue(){
    this.realEstateData.address = null;
    this.realEstateData.city = null;
    this.realEstateData.state = null;
    this.realEstateData.country = null;
    this.realEstateData.zip = null;
    this.realEstateData.subtype=null;
    this.realEstateData.estimated_value=null;
    this.realEstateData.name="Primary Residence";
    this.vehicleData.subtype=null;
    this.vehicleData.estimated_value=null;
    this.vehicleData.name="Automobile";
    this.vehicleData.year="year";
    this.vehicleData.brand="brand";
    this.vehicleData.model="model";
    this.vehicleData.trim="trim";
    this.otherData.subtype=null;
    this.otherData.estimated_value=null;
    this.otherData.name="ArtWork";
    this.cashOrDebtData.subtype=null;
    this.cashOrDebtData.estimated_value=null;
    this.cashOrDebtData.name="Debt";
  }

  backDiv(){
    switch(this.realEstateCurrentDiv){
      case 'realEstateI':
        this.realEstateCurrentDiv = "selectPropertyType";
        break;
      case 'realEstateII':
        this.realEstateCurrentDiv = "realEstateI";
        break;
      case 'addAddressDiv':
        this.realEstateCurrentDiv = "realEstateI"
        break;
      case 'realEstateIII':
        this.realEstateCurrentDiv = "addAddressDiv";
        break;
      case 'VehicleI':
        this.realEstateCurrentDiv = "selectPropertyType";
        break;
      case 'VehicleII':
        this.realEstateCurrentDiv = "VehicleI";
        break;
      case 'VehicleIII':
        this.realEstateCurrentDiv = "VehicleII";
        break;
      case 'cashOrDebtI':
        this.realEstateCurrentDiv = "selectPropertyType";
        break;
      case 'cashOrDebtII':
        this.realEstateCurrentDiv = "cashOrDebtI";
        break;
      case 'otherI':
        this.realEstateCurrentDiv = "selectPropertyType";
        break;
      case 'otherII':
        this.realEstateCurrentDiv = "otherI";
        break;
      default:
        this.realEstateCurrentDiv = "selectPropertyType";
        break;
    }
    return this.realEstateCurrentDiv;
  }

  addressIntegration(divName, addressData){
    if(divName){
      this.realEstateCurrentDiv = divName;
    }
    let path = "";
    if(addressData.zip){
      path = "/properties/realestate?city=" + addressData.city + "&state=" + addressData.state + "&zip=&address=" + addressData.zip + " " + addressData.address;
    } else {
      path = "/properties/realestate?city=" + addressData.city + "&state=" + addressData.state + "&zip=&address=" + addressData.address;
    }

    this.backend.get(path, this.authResultInfo.accessToken)
      .subscribe(data => {
        let resAddress = data.json();
        if(resAddress && resAddress.length > 0){
          if('address' in resAddress[0] && resAddress[0].address && resAddress[0].address.length > 0){
            for(let add of resAddress[0].address){
              if(add.street[0] && add.street.length > 0){
                this.realEstateData.objAddress.street = add.street[0];
                this.realEstateData.subtype = add.street[0];
              }
              if(add.city[0] && add.city.length > 0){
                this.realEstateData.objAddress.city = add.city[0];
              }
              if(add.state[0] && add.state.length > 0){
                this.realEstateData.objAddress.state = add.state[0];
              }
              if(add.zipcode[0] && add.zipcode.length > 0){
                this.realEstateData.objAddress.zipcode = add.zipcode[0];
              }
              
            }

            this.realEstateData.objAddress.zpid = resAddress[0].zpid[0];
          }
          if('zestimate' in resAddress[0] && resAddress[0].zestimate && resAddress[0].zestimate.length > 0){
            for(let val of resAddress[0].zestimate){
              let values = Object.values(val.amount[0]);
              this.realEstateData.estimated_value = values[0];
            }
          }
          console.log("Real Estate", this.realEstateData);
          // translate error message
          this.translate.get('APM_ADDRESS_SUCCESS')
          .subscribe((value)=>{
            this.notifier.notify('success', value);
          })
          
          this.realEstateCurrentDiv = 'addAddressDiv';
        } else {
          this.notifier.notify('error', "Address not found!");
          this.realEstateCurrentDiv = 'realEstateI';
        }
      },
      error => {
        console.log(error);
        let errorVar = error.json();
        // translate error message
        this.translate.get('APM_ERROR')
          .subscribe((value) => {
            this.notifier.notify('error', value + errorVar["message"]);
          })
      });
  }

  updatePropertiesInCache(properties){
    this.userAttributes =  this.authService.getUserProfile();
    this.userAttributes.properties = properties;
    this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
    this.propertiesUpdated = true;
  }

  // This method is called on adding a cash/debt
  validateAndAddProperty(divName, propertiesData){
    // First validate
    let estimated_value = parseInt(propertiesData.estimated_value);

    if(!Utils.isNullOrEmpty(propertiesData,'subtype') && propertiesData.subtype.length > 200){
      this.notifier.notify('error', "Short name cannot be greater than 200 characters");
      return;
    }
    // Negative values not allowed
    if(estimated_value < 0){
      this.notifier.notify('error', "Amount or value cannot be negative.");
      return;
    }

    // If validation passes then call add properties
    this.addProperties(divName,propertiesData);
  }

  addProperties(divName, propertiesData){
    this.spinnerService.show();
    let data:any = {
      type: propertiesData.type,
      name: propertiesData.name,
      estimated_value: parseInt(propertiesData.estimated_value),
      subtype: "",
      other_detail:{}
    };

    if(propertiesData.objAddress){
      data.other_detail = {
        address: propertiesData.objAddress.street,
        city: propertiesData.objAddress.city,
        state: propertiesData.objAddress.state,
        zipid: propertiesData.objAddress.zipcode,
        zpid: propertiesData.objAddress.zpid
      };
    }

    if(propertiesData.country){
      data.other_detail.country = propertiesData.country;
    }
    if(propertiesData.year){
      data.other_detail.year= propertiesData.year;
    }
    if(propertiesData.subtype){
      data.subtype= propertiesData.subtype;
    }
    console.log("Data : ", data);

    let token = this.authResultInfo.accessToken;
    this.backend.post(data, '/properties', token)
    .pipe(mergeMap(data => {
      console.log('response from post properties',data);
      return this.backend.get('/properties', token);
    }))
    .subscribe(data => {
        let propertyRes = data.json();
        console.log('Properties: ', propertyRes);
        this.updatePropertiesInCache(propertyRes.properties);
        
        // translate error message
        this.translate.get('APM_ADD_SUCCESS')
        .subscribe((value)=>{
          this.notifier.notify('success', value);
        }) 
        this.realEstateCurrentDiv = divName;
        this.spinnerService.hide();
      },
      err => {
        let errorVar = err.json();
        console.log('Error: ', errorVar["message"]);
              // translate error message
        this.translate.get('APM_ERROR')
          .subscribe((value) => {
            this.notifier.notify('error', value + errorVar["message"]);
          })
        this.spinnerService.hide();
      });
  }

}
