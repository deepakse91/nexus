import { Component, OnInit, TemplateRef, ViewChild, ElementRef, ChangeDetectorRef, HostListener } from '@angular/core';
import { BackendService } from '../backend/backend.service';
import { AuthService } from "../auth/auth.service";
import { SessionStorageService } from "angular-web-storage";
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as moment from 'moment'
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxPlaidLinkService } from "ngx-plaid-link";
import { RealTimeService } from "../realtime/realtime.service";
import { WebsocketService } from "../websocket/websocket.service";
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import Utils from '../common/utils';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [WebsocketService, RealTimeService]
})
export class DashboardComponent implements OnInit {
  modalRef: BsModalRef;
  authResultInfo: any;
  tab: string = "trends";
  selectedHours: string = '30min';
  selectedBackMonths: string = '3';
  shareTrends = false;
  shareTransactions = false;
  userAttributes: any;
  private readonly notifier: NotifierService;
  checkSyncStatus: any = null;
  hideProgressbar: any = {};
  maxValue: number = 100;
  failedAcc: any = [];
  failedAccDetails = false;
  progressbarPercent: any;
  publicToken: any = {};
  emitAccId: any;
  tranxData: any;
  qrCodeModalData: any;
  propertiesData: any;
  showScreen = {
    congrateScreen: false,
    nameSelectionScreen: false,
    reloadScreen: false,
    controlData:false,
    logoUpdated: false
  };
  sidebarReload : boolean = false;
  user: any;
  accountsLength: any;
  firstStar = true;
  secondStar = true;
  thirdStar = true;
  fourthStar = true;
  fifthStar = true;
  addAnotherAcc = false;
  userDetail: any;
  callAddAccount: any;
  callPropertyButton: any;
  employerSectionHideValue: any = false;
  checkPoint: boolean = false;
  hasRealTimeData: any;
  accounts: any = [];
  
  userActivity;
  warningActivity;
  userInactive: Subject<any> = new Subject();
  warningMsg: Subject<any> = new Subject();

  @ViewChild('transactionButton') transactionButton: ElementRef;
  @ViewChild('nameSelection') nameSelection: any;
  @ViewChild('manageAccount') manageAccount: ElementRef;
  @ViewChild('capturedProfile') capturedProfile: ElementRef;
  @ViewChild('verifyDetailsModal') verifyDetailsModal: any;

  constructor(private authService: AuthService,
    private backend: BackendService,
    public session: SessionStorageService,
    private spinnerService: NgxSpinnerService,
    private modalService: BsModalService,
    public router: Router,
    private notifierService: NotifierService,
    private eleRef: ElementRef,
    private plaidLinkService: NgxPlaidLinkService,
    private cdr: ChangeDetectorRef,
    public translate: TranslateService,
    private realTimeService: RealTimeService) {
      this.notifier = notifierService;
      this.setTimeout();

      this.userInactive.subscribe(() =>{
        console.log('user has been inactive for 10 min');
        this.authService.getUserLogout();
      });

      this.warningMsg.subscribe(() =>{
        this.notifier.notify('warning', "Session will expire after 60 Seconds, please move cursor!!");
      });

      // this is default langugage
      translate.setDefaultLang('en');

      // this language is to set by your prefered browser language
      let browserLang = translate.getBrowserLang();
      translate.use(browserLang);

      realTimeService.messages.subscribe((msg: any) => {
        console.log("Response from websocket: " + JSON.stringify(msg));
        if (msg && 'action' in msg && msg.action === 'sync') {
          this.hasRealTimeData = msg.id;
          this.showSyncStatus(msg);
        } else if (msg && 'action' in msg && msg.action === 'addemployer') {
          this.userAttributes.employers = Object.assign(this.userAttributes.employers, msg.emp);
          this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        } else if (msg && 'action' in msg && msg.action === 'updateUserIdentity') {
          this.userAttributes.address = msg.userIdentity.address;
          if ((this.userAttributes.picture) === 'NA') {
            let firstAndLastName = [];
            firstAndLastName = (msg.userIdentity.name).split(' ');
            if (firstAndLastName.length >= 2) {
              this.userAttributes.picture = ((firstAndLastName[0])[0]) + ((firstAndLastName[firstAndLastName.length - 1])[0]);
            } else {
              this.userAttributes.picture = ((msg.userIdentity.name)[0]);
            }

            this.updateUserPicture();
          }
          this.userAttributes.phone_numbers = msg.userIdentity.phone_number;
          this.userAttributes.emails = msg.userIdentity.email;
          this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        } else if (msg && 'action' in msg && msg.action === 'logoUpdated') { 
          this.appendLogo(msg);
        } else if (msg && 'action' in msg && msg.action === 'balance_sync') { 
          this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
        }
      });
    }

    appendLogo(msg) {
      let account  = this.userAttributes.accounts.filter(acc => acc.id === msg.id);
      let otherAccounts = this.userAttributes.accounts.filter(acc => acc.id !== msg.id);
      if (account && account.length > 0) {
        account[0].logo = msg.logo;
        account[0].primaryColor = msg.primaryColor;
        otherAccounts.push(account[0]);
        this.userAttributes.accounts = otherAccounts;
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);

        // Change message to update accounts
        this.showScreen.logoUpdated = !this.showScreen.logoUpdated;
      }
    }

    setTimeout() {
      // logout completed 10 min
      this.userActivity = setTimeout(() => this.userInactive.next(undefined), 10 * 60000);
      // show warning modal on 9 min completed
      this.warningActivity = setTimeout(() => this.warningMsg.next(undefined), 9 * 60000);
    }
  
    @HostListener('window:mousemove')
    @HostListener('window:click')
    @HostListener('window:keydown')    
    
    refreshUserState() {
      // clear timeout
      clearTimeout(this.userActivity);
      clearTimeout(this.warningActivity);
      // again call the timeout method
      this.setTimeout();
    }           

  ngOnInit() {
    this.userAttributes = null;
    this.userAttributes = this.authService.getUserProfile();
    this.authResultInfo = this.authService.getAccessTokenAndID();
    
    if(!Utils.isNullOrEmpty(this.userAttributes, 'account_verified')){
      if(this.userAttributes.account_verified == 0){
        for (let acc of this.userAttributes.accounts) {
          if ('accounts' in acc && acc.accounts) {
            for (let i of acc.accounts) {
              if (('mask' in i && i.mask == null) && ('type' in i && i.type == 'loan')) {
                this.openQRCodeModal(this.verifyDetailsModal);
              }
            }
          }
        }
      }
    }

    this.accounts = [];
    for (let acc of this.userAttributes.accounts) {
      if ('accounts' in acc && acc.accounts) {
        for (let i of acc.accounts) {
          if ((i.type == "credit" && i.status == 'ACTIVE') || i.type == "depository" && i.status == 'ACTIVE') {
            this.accounts.push(i);
          }
        }
      }
    }
  }

  getHitProfile(event){
    if(event == "hitprofile"){
      console.log(' this.capturedProfile.nativeElement',  this.capturedProfile);
       this.capturedProfile.nativeElement.click();
    }
  }

  receivedvalueForSpinner(event) {
    // if(event){
    //   this.checkPoint = event;
    //   this.cdr.detectChanges();
    //   timer(15000).subscribe(val =>  this.checkPoint = false);
    // }
  }

  receivedAccId(e: any) {
    this.emitAccId = e;
    this.transactionButton.nativeElement.click();
  }

  updateUserPicture() {
    this.spinnerService.show();
    let path = "/users/" + this.authResultInfo.idTokenPayload.sub;
    const data = {
      picture: this.userAttributes.picture
    }
    this.backend.put(data, path, this.authService.getAccessTokenAndID()).subscribe(res => {
      let response = res.json();
      console.log('Response: ', response);
      this.spinnerService.hide();
    },
      err => {
        let errorVar = err.json();
        console.log('Error: ', errorVar["message"]);
        this.spinnerService.hide();
      });
  }
  receivedTranxData(e: any) {
    this.tranxData = e;
  }

  receivedPropertyData(e: any) {
    this.propertiesData = e;
  }

  showSyncStatus(msg) {
    // Get the account 
    if (msg) {
      let fa = this.userAttributes.accounts.filter(a => a.id == msg.id);
      if (fa && fa.length > 0 && fa[0].sync_status != msg.status) {
        fa[0].sync_status = msg.status;

        if (msg.status === 'failed' && msg.last_sync_retry >= 3) {
          this.failedAcc.push(fa[0]);
        }

        // Update user_profile
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        
        if (this.failedAcc.length > 0) {
          if (this.hideProgressbar.display == 'none') {
            this.hideProgressbar.display = 'block';
          }
          this.progressbarPercent = (this.failedAcc.length / this.userAttributes.accounts.length) * 100;
        }
        console.log("failedAcc ", this.failedAcc);
        this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
      }
    }
  }

  showTab(tabs: string) {
    this.tab = tabs;
  }

  /* Edit Modal Code */
  openQRCodeModal(editProfileModal: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      editProfileModal,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  editProfileModalClose() {
    this.modalRef.hide();
  }

  changedShareTrends() {
    if (this.shareTrends) {
      this.shareTrends = true;
    } else {
      this.shareTrends = false;
    }
  }

  changedShareTransactions() {
    if (this.shareTransactions) {
      this.shareTransactions = true;
    } else {
      this.shareTransactions = false;
    }
  }

  shareDateCalculation(value) {
    let dateVal;
    switch (value) {
      case '30min':
        dateVal = moment.utc().add(30, 'minutes');
        return '30min';
      case '1hour':
        dateVal = moment.utc().add(1, 'hours');
        return '1hour';
      case '8hours':
        dateVal = moment.utc().add(8, 'hours');
        return '8hours';
      case 'endofday':
        dateVal = moment.utc().endOf('day');
        return 'endofday';
      case '7days':
        dateVal = moment.utc().add(7, 'days');
        return '7days';
      case '14days':
        dateVal = moment.utc().add(14, 'days');
        return '14days';
      case '30days':
        dateVal = moment.utc().add(30, 'days');
        return '30days';
    }
  }

  generateReport(value) {
    this.spinnerService.show();
    let data = {
      validTill: this.shareDateCalculation(this.selectedHours),
      shareTrends: this.shareTrends,
      shareTrans: this.shareTransactions,
      trans: {
        interval: parseInt(this.selectedBackMonths),
        duration: 'months'
      }
    };

    this.backend.post(data, "/reports", this.authResultInfo.accessToken).subscribe(res => {
      this.qrCodeModalData = res.json();
      this.openQRCodeModal(value);
      this.spinnerService.hide();
    },
      err => {
        this.spinnerService.hide();
        let errorVar = err.json();
        console.log('Error: ', errorVar["message"]);
        // translate error message
        this.translate.get('DASH_ERROR')
          .subscribe((value) => {
            this.notifier.notify('error', value + errorVar["message"]);
          })
      });
  }

  closeProgressbar() {
    this.hideProgressbar = {
      display: 'none'
    };
  }

  headerEvent(e: any) {
    this.user = this.authService.getUserProfile();
  }

  getPublicToken(accountId) {
    this.spinnerService.show();
    let path = "/accounts/" + accountId + "/public_token";
    this.backend.get(path, this.authResultInfo.accessToken)
      .subscribe(res => {
        this.publicToken = res.json();
        this.spinnerService.hide();
      },
        error => {
          let errorVar = error.json();
          // translate error message
          this.translate.get('DASH_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + errorVar["message"]);
            })
          this.spinnerService.hide();
        });
  }

  callSyncStatusMethod() {
    this.hideProgressbar = {
      display: 'none'
    };
    this.failedAccDetails = false;
    //this.showSyncStatus();
  }

  showCongrateScreenSection(event) {
    console.log('Got reload event',event);
    if (event === 'reload') {
      this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
    } else if(event == 'sidereload'){
      this.sidebarReload = !this.sidebarReload;
    } else {
      this.congrateScreenOnLoad();
      this.showScreen.congrateScreen = true;
      this.receivedAccountData(event);
    }
  }

  gotControlData(event){
    if(event == 'notactive'){
      this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
      this.showScreen.controlData = !this.showScreen.controlData;
    }

    if(event == 'active'){
      this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
    }
  }

  congrateScreenOnLoad() {
    this.user = this.authService.getUserProfile();
    let tokenID = this.authService.getAccessTokenAndID();
    this.accountsLength = this.user.accounts.length;
    if (this.accountsLength === 1) {
      let path = "/users/" + tokenID.idTokenPayload.sub;
      this.spinnerService.show();
      this.backend.get(path, tokenID.accessToken)
        .subscribe(data => {
          this.user = Object.assign(this.user, data.json());
          this.authService.setDataInCache(this.authService.USER_PROFILE, this.user);
          this.spinnerService.hide();
        },
          error => {
            this.router.navigate(['/']);
            let errorVar = error.json();
             // translate error message
            this.translate.get('DASH_ERROR')
              .subscribe((value) => {
                this.notifier.notify('error', value + errorVar["message"]);
              })
            this.spinnerService.hide();
          });
    }

    switch (this.accountsLength) {
      case 1: {
        this.firstStar = false;
        this.secondStar = true;
        this.thirdStar = true;
        this.fourthStar = true;
        this.fifthStar = true;
        this.accountsLength = this.accountsLength + 'st';
        break;
      }
      case 2: {
        this.firstStar = false;
        this.secondStar = false;
        this.thirdStar = true;
        this.fourthStar = true;
        this.fifthStar = true;
        this.accountsLength = this.accountsLength + 'nd';
        break;
      }
      case 3: {
        this.firstStar = false;
        this.secondStar = false;
        this.thirdStar = false;
        this.fourthStar = true;
        this.fifthStar = true;
        this.accountsLength = this.accountsLength + 'rd';
        break;
      }
      case 4: {
        this.firstStar = false;
        this.secondStar = false;
        this.thirdStar = false;
        this.fourthStar = false;
        this.fifthStar = true;
        this.accountsLength = this.accountsLength + 'th';
        break;
      }
      case 5: {
        this.firstStar = false;
        this.secondStar = false;
        this.thirdStar = false;
        this.fourthStar = false;
        this.fifthStar = false;
        this.accountsLength = this.accountsLength + 'th';
        break;
      }
      default: {
        this.firstStar = false;
        this.secondStar = false;
        this.thirdStar = false;
        this.fourthStar = false;
        this.fifthStar = false;
        this.accountsLength = this.accountsLength + 'th';
        break;
      }
    }
    this.spinnerService.hide();
  }

  goToDashboard() {
    // i just add this number for change callAddAccount variable value, to prevent recall plaid calling
    this.callAddAccount = 5;

    this.showScreen.congrateScreen = false;
    this.addAnotherAcc = false;
  }

  addAnotherAccount() {
    this.showScreen.congrateScreen = false;
    this.addAnotherAcc = true;
  }

  receivedMultipleHandler(referenceName) {
    if (referenceName) {
      if (referenceName == 'manageAccountSection') {
        this.manageAccount.nativeElement.click();
      } else {
        // exchange the data to make clickable and prevent passing the same value
        this.callAddAccount == 'callingAddAccount' ? this.callAddAccount = 'swapVariable' : this.callAddAccount = 'callingAddAccount';
      }
    }
  }

  receivedpropertyHandler(propertyName) {
    if (propertyName) {
      this.callPropertyButton == 'callingAddProperty' ? this.callPropertyButton = 'swapManageV' : this.callPropertyButton = 'callingAddProperty';
    }
  }

  openModal(nameS: TemplateRef<any>) {
    // prevent closing when user click outside of modal
    let ngbModalOptions: any = {
      backdrop: 'static',
      keyboard: false
    };

    this.modalRef = this.modalService.show(nameS, ngbModalOptions);
  }

  // UPDATE THE NAME
  updatedName(namePassed) {
    console.log('selectedName', namePassed.value.gender);
    if (namePassed.value.gender === 'H') {
      return;
    }
    let data: any = {};
    if ('gender' in namePassed.value && namePassed.value.gender) {
      data.first_name = namePassed.value.gender;
      let splitted = [];
      splitted = (namePassed.value.gender).split(' ');
      try {
        if(splitted.length > 0){
          data.first_name = splitted[0];
          data.last_name = namePassed.value.gender.replace(data.first_name,'').trim();
          this.userAttributes.last_name = data.last_name;
        }
        if (splitted.length >= 2) {
          data.picture = splitted[0][0] + splitted[splitted.length - 1][0]
        } else {
          data.picture = splitted[0][0];
        }
        this.userAttributes.picture = data.picture;
      } catch (e) {
        console.log("Error logged", e);
      }
      if (this.userAttributes.first_name) {
        this.userAttributes.first_name = data.first_name;
      }
    } else {
      data.first_name = this.userAttributes.first_name;
    }

    let path = "/users/" + this.authResultInfo.idTokenPayload.sub;
    this.spinnerService.show()

    this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(res => {
      this.spinnerService.hide()
      // translate error message
      this.translate.get('DASH_UPDATE_SUCCESS')
        .subscribe((value) => {
          this.notifier.notify('success', value);
        })
      this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
      this.modalRef.hide();
      this.showScreen.nameSelectionScreen = false;
      this.showScreen.congrateScreen = true;
    },
      err => {
        let errorVar = err.json();
        console.log('Error: ', errorVar["message"]);
        this.spinnerService.hide()
        
        // translate error message
        this.translate.get('DASH_ERROR')
          .subscribe((value) => {
            this.notifier.notify('error', value + errorVar["message"]);
          })
        this.modalRef.hide();
      });
  }

  // when new user signup and add account; that time user gets multiple name
  receivedAccountData(event) {
    console.log('accountData', event);
    this.ngOnInit();
    this.userDetail = event;
    if (this.userDetail) {
      if (('names' in this.userDetail && this.userDetail.names.length > 0)
        && ('notify_for_names_selection' in this.userDetail && this.userDetail.notify_for_names_selection)) {
        this.showScreen.nameSelectionScreen = true;
        this.showScreen.congrateScreen = false;
        this.openModal(this.nameSelection);
        this.spinnerService.hide();
      } else {
        this.showScreen.nameSelectionScreen = false;
        this.showScreen.congrateScreen = true;
        this.spinnerService.hide();
      }
    }
  }

  // getScoreGraphData(e: any) {
  //   if ('TippingPoints' in e && e.TippingPoints) {
  //     this.scoreGraphDataFromTrend = e.TippingPoints;
  //   }else{
  //     if('err' in  e){
  //       delete e.err;
  //       this.scoreGraphDataFromTrend = e;
  //     }
  //   }
  // }

  // Employer section hide on click
  employerSectionHide() {
    this.employerSectionHideValue = !this.employerSectionHideValue;
  }

  // verify details modal close
  verifyDetailsModalClose(event:any) {
    if (event === 'reload') {
      this.modalRef.hide();
      this.showScreen.reloadScreen = !this.showScreen.reloadScreen;
      this.ngOnInit();
    }
  }

}
