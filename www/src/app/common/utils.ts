import { Moment } from "moment";
import * as moment from "moment";
// This the common utils classes for some common functions
export default class Utils {

    // This function is used to filter spending transactions and
    // return transactions which are valid for spending
    static filterSpendingTransactions(transactions: any) {
        // spending calculation
        let ignoreCatog = ['Transfer,Internal Account Transfer', 'Payment,Credit Card'];
        let spendTrans = transactions.filter(t => t.amount && t.amount > 0 && t.account_type == 'depository' && (!ignoreCatog.includes(t.category)));

        // credit card
        let ignoredCreditCategories = ["Transfer,Credit"];
        let creditCardTransactions = transactions.filter(t => t.account_type === 'credit' && (!(ignoredCreditCategories.includes(t.category)) || (ignoredCreditCategories.includes(t.category))) && t.amount > 0);
        // End of spending calculation
        return spendTrans.concat(creditCardTransactions);
    }

    // This function is used to group chart data by format
    static groupGraphDataByFormat(transactions,dates,key,dateFormat){
        let graphData = Utils.createEmptyGraphDataByDuration(dates);

        if(!dateFormat){
            dateFormat = Utils.calculateDuration(dates).dateFormat;
        }

        for (let t of transactions) {
            let tranxDate = moment.utc(t.date);
            let formatedDate = tranxDate.format(dateFormat);
            if (formatedDate in graphData) {
                graphData[formatedDate].amount += Math.round(Math.abs(t[key]) * 100) / 100;
                graphData[formatedDate].transactionArr.push(t);
            } else {
                graphData[formatedDate] = {};
                graphData[formatedDate].amount = Math.round(Math.abs(t[key]) * 100) / 100;
                graphData[formatedDate].transactionArr = [t];
            }
        }
        return graphData;
    }

    // This function is used to filter incoming transactions and
    // return transactions which are valid for income
    static filterIncomeTransactions(transactions: any) {
        let ignoredCategories = ["Transfer,Internal Account Transfer", "Transfer,Credit"];
        let filterData = [];
        if (transactions && transactions.length > 0) {
            for (let t of transactions) {
                if (t.category && t.account_type == 'depository' && t.amount < 0 && !(ignoredCategories.includes(t.category))) {
                    filterData.push(t);
                }
            }
        }
        return filterData;
    }

    // This method is used to validate if a key is present in a JSON
    static isNullOrEmpty(json, key) {
        if (json && key in json && json[key] != null) {
            if (typeof json[key] == 'string') {
                if (!(json[key] == '')) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    // This function is used to merge address
    static constructAddress(addressJSON) {
        let address = [];
        if (!Utils.isNullOrEmpty(addressJSON, 'streetAddress')) {
            address.push(addressJSON.streetAddress);
        }
        if (!Utils.isNullOrEmpty(addressJSON, 'addressLocality')) {
            address.push(addressJSON.addressLocality);
        }
        if (!Utils.isNullOrEmpty(addressJSON, 'addressRegion')) {
            address.push(addressJSON.addressRegion);
        }
        if (!Utils.isNullOrEmpty(addressJSON, 'postalCode')) {
            address.push(addressJSON.postalCode);
        }

        return address.join(', ');
    }

    static cloneJSON(json) {
        return JSON.parse(JSON.stringify(json));
    }

    static roundTo2Digit(number) {
        return (Math.round(number * 100) / 100);
    }


    static calculateDuration(dates) {
        let end_date: Moment = moment.utc(dates.end_date);
        let start_date: Moment = moment.utc(dates.start_date);
        let dateFormat = 'MMM YYYY'; // Default to monthy view
        let duration = 'month';
        if ((end_date.diff(start_date, 'months') == 0) || (end_date.diff(start_date, 'days') < 15)) {
            dateFormat = 'MM-DD-YYYY';
            duration = 'days';
        }
        return { dateFormat, duration };
    }

    // This function is used to create empty graph data
    static createEmptyGraphDataByDuration(dates) {
        let loop_start_date = moment.utc(dates.start_date);
        let end_date: Moment = moment.utc(dates.end_date);

        let dateAndDuration = Utils.calculateDuration(dates);
        let dateFormat = dateAndDuration.dateFormat; // Default to monthy view
        let interval: any = -1;
        let duration = dateAndDuration.duration;
        if (duration === 'month') {
            end_date = end_date.endOf('month');
        }

        let graphData = {};
        for (let l = loop_start_date; l.isSameOrBefore(end_date); l.subtract(interval, duration)) {
            let formatedDate = l.format(dateFormat);
            graphData[formatedDate] = { amount: 0, transactionArr: [] };
        }
        return graphData;
    }

    // This function is used to calculate diffrence in years and months
    static diffYearMonthes(year, months, days){
        if(year > 0){
            let remainingMonths = months - (year * 12);
            let formatstring;
            if(year <= 1){
                formatstring = year + ' year';
            } else {
                formatstring = year + ' years';
            }
            if(remainingMonths >= 0){
                formatstring += ' and ' + remainingMonths + ' months';
            }
            return formatstring;
        } else {
            return (months + ' months');
        }

    }
}
