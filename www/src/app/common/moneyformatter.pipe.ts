import {Pipe, PipeTransform} from '@angular/core';

@Pipe({ name: 'moneyformatter' })
export class MoneyFormatterPipe implements PipeTransform{
  SI_SYMBOL: any = ["", "k", "M", "G", "T", "P", "E"];

  transform(amount: number) {
    // what tier? (determines SI symbol)
    let tier = Math.log10(amount) / 3 | 0;

    // if zero, we don't need a suffix
    if(tier == 0) return amount;

    // get suffix and determine scale
    let suffix = this.SI_SYMBOL[tier];
    let scale = Math.pow(10, tier * 3);

    // scale the number
    let scaled = amount / scale;

    // format number and add suffix
    return scaled.toFixed(1) + suffix;
  }
}
