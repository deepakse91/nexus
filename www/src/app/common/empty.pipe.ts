import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'empty'
})
export class Empty implements PipeTransform {
    transform(value: any[], emptyText: string = 'NO ITEMS'): any {
        return value && value.length > 0? value : [{emptyMessage: emptyText}];
      }
}
