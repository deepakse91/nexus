import {Pipe, PipeTransform, Injectable} from '@angular/core';
import * as moment from "moment";

@Pipe({
    name: "orderBydate"
  })
  export class DateOrdering implements PipeTransform {
    transform(array: Array<any>, args: string): Array<any>{
        if (!args[0]) {
            return array;
        }
        let direction = args[0][0];
        let column = args.replace('-','');
        array.sort((a: any, b: any) => {
            let left = Number(moment.utc(a[column]));
            let right = Number(moment.utc(b[column]));
            //  if(moment.utc(a[column], 'DD-MM-YYYY').isValid() || (moment.utc(a[column], 'MM-DD-YYYY').isValid())){
            //      if(moment.utc(a[column], 'DD-MM-YYYY').isValid()){
            //          left = Number(moment.utc(a[column], 'DD-MM-YYYY'));
            //          right = Number(moment.utc(b[column], 'DD-MM-YYYY'));
            //      }else if(moment.utc(a[column], 'MM-DD-YYYY').isValid()){
            //         left = Number(moment.utc(a[column], 'MM-DD-YYYY'));
            //         right = Number(moment.utc(b[column], 'MM-DD-YYYY'));
            //      }
            //  }else{
            //      left = Number(moment.utc(a[column]));
            //      right = Number(moment.utc(b[column]));
            // }
            return (direction === "-") ? right - left : left - right;
        });
        return array;
    }
  }