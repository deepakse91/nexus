import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        let retArray = [];
        if(!items) return [];
        if(!searchText){
            return items;
        } else {
            for(let row of items){
                if(row.name.trim().toLowerCase().includes(searchText.trim().toLowerCase())) {
                    retArray.push(row);
                } 
                else if(row.category.trim().toLowerCase().includes(searchText.trim().toLowerCase())) {
                    retArray.push(row);
                }
                else if(row.officialName.trim().toLowerCase().includes(searchText.trim().toLowerCase())) {
                    retArray.push(row);
                }
                else if(row.amount) {
                    if(row.amount.toString().toLowerCase().includes(searchText.trim().toLowerCase())) {
                        retArray.push(row);
                    }
                }
            }

            return retArray;
        }
    }
}
