import {Pipe, PipeTransform} from '@angular/core';
import * as moment from "moment";
import { TranslateService } from '@ngx-translate/core';

@Pipe({ name: 'datediffformat' })
export class DateDiffFormatPipe implements PipeTransform{

  constructor(public translate: TranslateService){}

  transform(date: any) {
    // Start from days
    let now = moment.utc();
    let dateInMoment =moment.utc(date);

    let diffInDays = dateInMoment.diff(now,'days');
    let diffInHours = dateInMoment.diff(now,'hours');

    if(diffInDays>0){
      // Show upto 2 units
      let remainingHours = diffInHours - (diffInDays * 24);
      let formatstring;
      this.translate.get("DF_DAYS").subscribe((value)=>{
        formatstring = `${diffInDays} ${value}`
      })
      if(remainingHours > 0){
        this.translate.get("DF_HOURS").subscribe((value)=>{
          formatstring += ` ${remainingHours} ${value}`
        })
      }
      return formatstring;
    }

    // Check diff in hours
    let diffInMinutes = dateInMoment.diff(now,'minutes');
    if(diffInHours > 0){
      // Show upto 2 units
      let remaining = diffInMinutes - (diffInHours * 60);
      let formatstring;
      this.translate.get("DF_HOURS").subscribe((value)=>{
        formatstring = `${diffInHours} ${value}`
      })
      
      if(remaining > 0){
        this.translate.get("DF_MINUTES").subscribe((value)=>{
          formatstring += ` ${remaining} ${value}`
        })
      }
      return formatstring;
    }

    // Check diff in seconds
    let diffInSecs = dateInMoment.diff(now,'seconds');
    if(diffInMinutes > 0){
      // Show upto 2 units
      let formatstring; 
      let remaining = diffInSecs - (diffInMinutes * 60);
      this.translate.get("DF_MINUTES").subscribe((value)=>{
        formatstring = `${diffInMinutes} ${value}`
      })
      if(remaining > 0){
        this.translate.get("DF_SECONDS").subscribe((value)=>{
          formatstring += ` ${remaining} ${value}`
        })
      }
      return formatstring;
    }
    this.translate.get("DF_SECONDS").subscribe((value)=>{
      return `${diffInSecs} ${value}`;
    })
   
  }
}
