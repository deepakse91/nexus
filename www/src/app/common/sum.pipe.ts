import { Pipe, PipeTransform } from '@angular/core';
import Utils from './utils';
@Pipe({
    name: 'sum'
})
export class Sum implements PipeTransform {
    transform(items: any[], field: string,abs: boolean): any {
        if (!items) return 0;
        let sum = 0;
        for(let item of items){
            sum+=item[field];
        }
        if(abs){
            return Math.abs(sum);
        }
        return sum;
    }
}
