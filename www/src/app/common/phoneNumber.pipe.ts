import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
     name: 'phoneNumber'
    })
export class PhoneNumberPipe implements PipeTransform{

  transform(phoneNum: any) {
    let cleaned = ('' + phoneNum).replace(/\D/g, '');
    let match = cleaned.match(/^(\d{3})(\d{3})(\d{0,9})$/);

    return match ? '(' + match[1] + ') ' + match[2] + '-' + match[3] : '';
  }
}
