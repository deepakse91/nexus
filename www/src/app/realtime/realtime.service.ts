import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs/Rx";
import { WebsocketService } from "../websocket/websocket.service";
import {AuthService} from "../auth/auth.service";
import { environment } from "src/environments/environment";

export interface Message {
  name: string;
  content: string;
}

@Injectable()
export class RealTimeService {
  public messages: Subject<Message>;

  constructor(wsService: WebsocketService,private authService: AuthService) {
    // Add auth to chat URL
    let authResultInfo = this.authService.getAccessTokenAndID();
    this.messages = <Subject<Message>>wsService.connect(environment.wsapiurl+'?auth='+authResultInfo.accessToken).map(
      (response: MessageEvent): Message => {
        return JSON.parse(response.data);
      }
    );
  }
}