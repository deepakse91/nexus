import { Component, OnInit , Renderer2, ElementRef, ViewChild} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})

export class EmailVerificationComponent implements OnInit {
  @ViewChild('promptModal') modal;
  modalRef;
  constructor(public router: Router) { }
  statusMessageHeading = 'EV_VERIFIED';

  ngOnInit() {
    
  }


  
  resend(){
   this.router.navigate(['/']);
  }
}
