import { Component, OnInit, TemplateRef, Input, OnChanges, SimpleChanges, Output, EventEmitter, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BackendService } from '../backend/backend.service';
import { AuthService } from "../auth/auth.service";
import { SessionStorageService } from "angular-web-storage";
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NotifierService } from 'angular-notifier';
import * as moment from "moment";
import 'chart.piecelabel.js';
import { TransactionsModalComponent } from '../transactions-modal/transactions-modal.component';
import { LooseCurrencyPipe } from '../common/currencyformatter.pipe';
import * as html2canvas from 'html2canvas';
import { TranslateService } from '@ngx-translate/core';
import Utils from '../common/utils';
import { BaseChartDirective } from 'ng4-charts';

declare var Chart: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css', '../global/global.css']
})
export class ProfileComponent implements OnInit, OnChanges {
  modalRef: BsModalRef;
  userAttributes: any;
  phoneNumber: any;
  employerSectionDiv = true;

  @ViewChild('nexusscore')
  nexusscore: any;

  @Input("reloadScreen") reloadScreen: any;
  @Input("tranxData") tranxData: any;
  @Input("properties") properties: any = [];
  @Input("report") report: any = null;
  @Input("scoreGraphObj") scoreGraphObj: any;
  @Input("updatedReport") updatedReport: any;
  @Input("employerSectionValue") employerSectionValue: any;
  @Output() headerEvent = new EventEmitter();
  @Output() addAccountHandlerEmit = new EventEmitter<any>();
  @Output() propertyHandlerEmit = new EventEmitter<any>();
  private readonly notifier: NotifierService;
  DATE_FORMAT: string = 'YYYY-MM-DD';
  authResultInfo: any;
  filteredCreditAcc: any = [];
  filteredDeposAcc: any = [];
  filteredLoanAcc: any = [];
  employersData = { totalTransArr: [] }
  totalSum = { creditSum: 0, limitSum: 0, depoSum: 0, propertSum: 0, loanSum: 0 };
  allProperty: any;
  otherEmployersData: any = {};
  employerDetail: any = {};
  allAccounts: any;
  G_Spending: any;
  G_Income: any;
  G_Net_Income: any;
  PC_OTHER: any;
  graphs: any;
  filteredAccForBlur: any = [];
  innerWidth: any;
  loadingScoreGraph: boolean = false;

  @ViewChild('employerBackButton') employerBackButton: ElementRef;

  // for score graph
  canvasWidth = 350;
  needleValue = 0;
  options = {
    hasNeedle: false,
    arcColors: ["rgb(238,90,69)", "rgb(241,128,72)", "rgb(247,182,69)", "rgb(79,149,120)"],
    arcDelimiters: [25, 50, 75],
    rangeLabel: []
  };
  graphTippingPoints: any = {};

  constructor(private authService: AuthService,
    private backend: BackendService,
    public session: SessionStorageService,
    private spinnerService: NgxSpinnerService,
    private modalService: BsModalService,
    private notifierService: NotifierService,
    public translate: TranslateService,
    private currency: LooseCurrencyPipe) {
    this.notifier = notifierService;

    let G_NO_DEBTS;
    let G_NO_DATA;

    translate.get('G_NO_DEBTS').subscribe(value => G_NO_DEBTS = value);
    translate.get('G_NO_DATA').subscribe(value => G_NO_DATA = value);

    let G_TRANSACTIONS;
    translate.get('G_TRANSACTIONS').subscribe(value => G_TRANSACTIONS = value);

    this.graphs = {
      "net_income_over_time": {
        name: 'Net Income Over Time',
        load: false,
        barOptions: {
          maintainAspectRatio: false,
          scaleShowHorizontalLines: true,
          responsive: true,
          layout: {
            padding: {
              bottom: 30  //set that fits the best
            }
          },
          scales: {
            xAxes: [{
              stacked: true
            }],
            yAxes: [{
              stacked: true,
              ticks: {
                beginAtZero: true,
                autoSkip: false,
                callback: function (value, index, values) {
                  if (parseInt(value) < 0 || parseInt(value) >= 0) {
                    return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  }
                }
              }
            }]
          },
          tooltips: {
            titleFontSize: 16,
            bodyFontSize: 14,
            bodySpacing: 4,
            xPadding: 16,
            yPadding: 4,
            xAlign: "center",
            yAlign: "top",
            custom: function (tooltip) {
              if (!tooltip) return;

              if (('dataPoints' in tooltip && tooltip.dataPoints && tooltip.dataPoints[0].datasetIndex)) {
                tooltip.opacity = 1;
              }
              // disable displaying the color box;
              tooltip.displayColors = false;
            },
            callbacks: {
              beforeTitle: function (tooltipItem, data) {
                if (data.datasets[tooltipItem[0].datasetIndex].label) {
                  let title = data.datasets[tooltipItem[0].datasetIndex].label;
                  return title;
                }
              },
              title: () => null,
              beforeLabel: function (tooltipItem, data) {
                let modifiedLabels = data.labels[tooltipItem.index];
                return modifiedLabels;
              },
              label: function (tooltipItem, data) {
                if (tooltipItem && tooltipItem.datasetIndex >= 0) {
                  return '$' + tooltipItem.yLabel.toLocaleString();
                }
                return '';
              },
              footer: function (tooltipItem, data) {
                let index = tooltipItem[0].index;
                if (data.datasets[tooltipItem[0].datasetIndex].transactions[index] && data.datasets[tooltipItem[0].datasetIndex].transactions[index].length > 0) {
                  let t = data.datasets[tooltipItem[0].datasetIndex].transactions[index].length;
                  return t + `${G_TRANSACTIONS}`;
                }
              }
            }
          },
          hover: {
            onHover: function (e) {
              let point = this.getElementAtEvent(e);
              if (point.length) e.target.style.cursor = 'pointer';
              else e.target.style.cursor = 'default';
            }
          },
        },
        colors: [
          {
            backgroundColor: '#717171'
          },
          {
            backgroundColor: 'rgb(0, 194, 86)'
          },
          {
            backgroundColor: 'rgb(255, 107, 113)'
          }
        ],
        labels: [],
        type: 'bar',
        data: []
      },
      "current_debt_distribution": {
        name: 'Spending By Catagory',
        load: false,
        barOptions: {
          maintainAspectRatio: false,
          scaleShowHorizontalLines: true,
          responsive: true,
          layout: {
            padding: {
              bottom: 30  //set that fits the best
            }
          },
          tooltips: {
            bodyFontSize: 18,
            bodySpacing: 4,
            xPadding: 12,
            yPadding: 14,
            xAlign: "center",
            yAlign: 'top',
            custom: function (tooltip) {
              if (!tooltip) return;
              if (('dataPoints' in tooltip && tooltip.dataPoints && tooltip.dataPoints[0].datasetIndex)) {
                tooltip.opacity = 1;
              }
              tooltip.displayColors = false;

              return tooltip;
            },
            callbacks: {
              beforeLabel: function (tooltipItem, data) {
                let modifiedLabels = data.labels[tooltipItem.index].split('deleting')[0];
                return modifiedLabels;
              },
              label: function (tooltipItem, data) {
                return '$' + data.datasets[0].data[tooltipItem.index].toLocaleString();
              }
            }
          },
          cutoutPercentage: 60,
          hover: {
            onHover: function (e) {
              let point = this.getElementAtEvent(e);
              if (point.length) {
                e.target.style.cursor = 'default';
              } else e.target.style.cursor = 'default';
            }
          },
          legend: {
            display: false,
            labels: {
              fontColor: '#000',
              fontSize: 12
            },
            position: 'bottom'
          },
          pieceLabel:
          {
            segment: true,
            position: 'outside',
            overlap: false,
            fontColor: '#000',
            fontSize: 12,
            textMargin: 5,
            outsidePadding: 10,
            render: function (args) {
              if (args.label) {
                let modifiedLabel = args.label.split('deleting')[0];
                let cuttedLabel = modifiedLabel ? (modifiedLabel.length > 12 ? modifiedLabel.substring(0, 10) + "..." : modifiedLabel) : ""
                return cuttedLabel;
              }
            }
          },
          elements: {
            center: {
              text: '$',
              color: '#36A2EB', //Default black
              fontStyle: 'Helvetica', //Default Arial
              sidePadding: 15 //Default 20 (as a percentage)
            }
          }
        },
        labels: [],
        type: 'pie',
        data: [],
      }
    };

    Chart.Tooltip.positioners.cursor = function (chartElements, coordinates) {
      return coordinates;
    };

    // for chart purpose
    Chart.pluginService.register({
      beforeDatasetsUpdate: function (chart) {
        // console.log('Inside after update');
        if (chart.data.datasets.length > 0) {
          let nodata = true;
          for (let dataset of chart.data.datasets) {
            if ('data' in dataset && dataset.data) {
              for (let d of dataset.data) {
                if (d != 0) {
                  nodata = false;
                }
              }
            }
          }
          if (nodata) {
            chart.data.datasets = [];
          }
        }
      },
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //console.log('chart.data.datasets',chart.data.datasets);
          //Get ctx from string
          let ctx = chart.chart.ctx;

          let total = 0;
          if (chart.data.datasets && chart.data.datasets.length > 0 && 'data' in chart.data.datasets[0]) {
            total = chart.data.datasets[0].data.reduce((a, b) => (a + b));
          }

          //Get options from the center object in options
          let centerConfig = chart.config.options.elements.center;
          let fontStyle = centerConfig.fontStyle || 'Arial';
          let txt = '$' + ((total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
          let color = '#000';
          let sidePadding = centerConfig.sidePadding || 20;
          let sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2);
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          let stringWidth = ctx.measureText(txt).width;
          let elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          let widthRatio = elementWidth / stringWidth;
          let newFontSize = Math.floor(30 * widthRatio);
          let elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          let fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          let centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          let centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          (total == 0) ? (fontSizeToUse = 15) : fontSizeToUse;
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          //Draw text in center
          ctx.fillText(txt, centerX, centerY)
        }
      },
      afterDraw: function (chart) {
        if (chart.data.datasets.length == 0) {

          // No data is present
          chart.data.datasets = [];
          let ctx = chart.chart.ctx;
          let width = chart.chart.width;
          let height = chart.chart.height;
          chart.clear();
          ctx.fillStyle = '#000';

          ctx.save();
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          ctx.font = "16px normal 'Helvetica Nueue'";
          chart.chart.config.type == 'pie' ? ctx.fillText(`${G_NO_DEBTS}`, width / 2, height / 2) : ctx.fillText(`${G_NO_DATA}`, width / 2, height / 2);
          ctx.restore();
        }
      }
    });
  }

  onChartClick(event) {
    if (event && event.active
      && event.active.length > 0
      && event.active[0]._chart.config.type === 'bar'
      && event.active[0]._chart.config.data
      && event.active[0]._chart.config.data.datasets
      && event.active[0]._chart.config.data.datasets.length > 0
    ) {
      if (event.active[0]._chart.config.type === 'bar') {
        const chart = event.active[0]._chart;
        const activePoints = chart.getElementAtEvent(event.event);
        const clickedElementIndex = activePoints[0]._datasetIndex;
        let initialState: any = { transactions: event.active[clickedElementIndex]._chart.config.data.datasets[clickedElementIndex].transactions[event.active[clickedElementIndex]._index] };
        if ('transactions' in initialState && initialState.transactions && initialState.transactions.length > 0) {
          this.modalRef = this.modalService.show(
            TransactionsModalComponent,
            Object.assign({}, { class: 'gray modal-lg', initialState }));
        } else {
          this.translate.get('G_NO_TRANS')
            .subscribe((value) => {
              this.notifier.notify('warning', value);
            })
        }
      }
    }
  };

  // deep cloning 
  genericCloneMethod(arrayData) {
    let arrr = [];
    if (arrayData && arrayData.length > 0) {
      for (let obj of arrayData) {
        let b = null;
        b = Object.assign({}, obj);
        arrr.push(b)
      }
    }
    return arrr;
  }

  ngOnInit() {
    this.userAttributes = null;
    this.userAttributes = this.authService.getUserProfile();
    this.allAccounts = [];
    this.allAccounts = this.userAttributes.accounts;
    this.authResultInfo = this.authService.getAccessTokenAndID();
    this.formatPhoneNumber(this.userAttributes.phone_numbers);
    this.allProperty = [];
    this.allProperty = this.authService.getUserProfile().properties;

    this.translate.get('G_SPENDING').subscribe(value => this.G_Spending = value);
    this.translate.get('G_INCOME').subscribe(value => this.G_Income = value);
    this.translate.get('G_NET_INCOME').subscribe(value => this.G_Net_Income = value);

    if ('employers' in this.userAttributes && this.userAttributes.employers) {
      for (let index of this.userAttributes.employers) {
        if (index.from_duration && index.to_duration) {
          let from = moment(index.from_duration);
          let to = moment(index.to_duration);
          let diffDate = to.diff(from, 'months');
          index.years = Math.floor(diffDate / 12);
          index.months = diffDate % 12;
        } else if (index.from_duration && index.present) {
          let from = moment(index.from_duration);
          let to = moment.utc();
          let diffDate = to.diff(from, 'months');
          index.years = Math.floor(diffDate / 12);
          index.months = diffDate % 12;
        } else {
          index.years = 0;
          index.months = 0;
        }

        if (index.from_duration) {
          index.from_duration_in_date = moment.utc(index.from_duration).toDate();
        } else {
          index.from_duration_in_date = null;
        }

        if (index.to_duration) {
          index.to_duration_in_date = moment.utc(index.to_duration).toDate();
        } else {
          index.to_duration_in_date = null;
        }

        if (index.present) {
          index.present = true;
        } else {
          index.present = false;
        }

        if ('preferred' in index && index.preferred) {
          index.preferred = true;
        } else {
          index.preferred = false;
        }
      }
      this.calculateOtherEmployers(null);
    }

    // credit and depos type acc calculation
    this.filteredCreditAcc = [];
    this.filteredDeposAcc = [];
    this.filteredLoanAcc = [];
    this.totalSum = { creditSum: 0, limitSum: 0, depoSum: 0, propertSum: 0, loanSum: 0 };
    this.filteredAccForBlur = [];

    for (let acc of this.allAccounts) {
      // check official name duplicate or not
      if ('accounts' in acc && acc.accounts) {
        for (let i = 0; i < acc.accounts.length; i++) {
          if (acc.accounts[i].type == "depository") {
            for (let j = 0; j < acc.accounts.length; j++) {
              if (acc.accounts[j].type == "depository") {
                if (!(i === j)) {
                  if (!(acc.accounts[i].official_name === acc.accounts[j].official_name)) {
                    if (!(acc.accounts[i].matchOfficialName)) {
                      acc.accounts[i].matchOfficialName = false;
                    }
                  } else {
                    acc.accounts[i].matchOfficialName = true;
                  }
                }
              }
            }
          } else if (acc.accounts[i].type == "credit" && acc.accounts[i].status == 'ACTIVE') {
            this.filteredCreditAcc.push(acc.accounts[i]);
            this.totalSum.creditSum += acc.accounts[i].balances.current;
            let limitAmount = acc.accounts[i].balances.limit ? (typeof (acc.accounts[i].balances.limit) == "string" ? parseInt(acc.accounts[i].balances.limit) : acc.accounts[i].balances.limit) : 0
            this.totalSum.limitSum += limitAmount;
          }
        }

        for (let i of acc.accounts) {

          if (i.type == "depository" && i.status == 'ACTIVE') {
            this.filteredDeposAcc.push(i)
            this.totalSum.depoSum += i.balances.current
          }
          if (i.type == "loan" && i.status == 'ACTIVE') {
            if ('official_name' in i && !(i.official_name) && 'inst_name' in i && i.inst_name) {
              i.official_name = i.inst_name;
            }
            this.filteredLoanAcc.push(i)
            this.totalSum.loanSum += i.balances.current
          }
          if (i.type == "loan" && i.status == 'ACTIVE' && i.mask == null) {
            this.filteredAccForBlur.push(i);
          }
        }
      }
    }

    // property clculation
    if (this.allProperty && this.allProperty.length > 0) {
      let totalPropertySum = this.allProperty.map(item => item.estimated_value)
      this.totalSum.propertSum = totalPropertySum.reduce((a, b) => a + b);
    }

    let arrayOfAccount = [...this.filteredCreditAcc, ...this.filteredDeposAcc, ...this.filteredLoanAcc];
    this.createDebtDistributionGraph(arrayOfAccount);
    this.createNexusScoreGraph();
    this.spinnerService.hide()
  }

  // This function is used to calculate other employers transactions
  private calculateOtherEmployers(transactions) {
    let employers = this.userAttributes.employers;
    if (transactions) {
      this.otherEmployersData = { employer_name: '', income: 0, seeTransaction: [] };

      this.translate.get('PC_OTHER').subscribe((value) => {
        this.otherEmployersData.employer_name = value;
      });
      let incomeTransactions = Utils.filterIncomeTransactions(transactions);
      for (let t of incomeTransactions) {
        // Let's start with employer not found with this income transaction
        let employerFound = false;
        for (let employer of employers) {
          if (t.name.toLowerCase().includes(employer.employer_name.toLowerCase())) {
            employer.income += Math.abs(t.amount);
            employer.income = Utils.roundTo2Digit(employer.income);
            if (!employer.seeTransaction) {
              employer.seeTransaction = [];
            }
            employer.seeTransaction.push(t);
            this.employersData.totalTransArr.push(t);
            employerFound = true;
          }
        }
        // Check if employer not found means it is from other source
        if (!employerFound) {
          this.otherEmployersData.income += Math.abs(t.amount);
          this.otherEmployersData.income = Utils.roundTo2Digit(this.otherEmployersData.income);
          this.otherEmployersData.seeTransaction.push(t);
          this.employersData.totalTransArr.push(t);
        }
      }
    }

    // set default employer
    // Check if preferred is set or not
    let employerWithPreferredSet = employers.filter(e => e.preferred);
    if (!(employerWithPreferredSet && employerWithPreferredSet.length > 0)) {
      // First try to get the preferred employees by income
      let employersByAmount = employers.filter(e => e.income).sort(function (a, b) { return (b.income - a.income); });
      if (employersByAmount && employersByAmount.length > 0 && 'income' in employersByAmount[0] && employersByAmount[0].income) {
        employersByAmount[0].preferred = true;
      } else {
        if ('employers' in this.userAttributes && this.userAttributes.employers.length > 0) {
          this.userAttributes.employers[0].preferred = true;
        }
      }
    }

    // Read data from cache
    let userAttributes = this.authService.getUserProfile();
    userAttributes.employers = employers;
    this.authService.setDataInCache(this.authService.USER_PROFILE, userAttributes);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('profile-changes', changes);
 
    // for properties
    if ('properties' in changes && 'currentValue' in changes.properties && changes.properties.currentValue && typeof (changes.properties.currentValue == 'Object') && changes.properties.currentValue.length > 0) {
      let receivedProperties = changes.properties.currentValue;
      this.allProperty = receivedProperties;

      if (this.allProperty && this.allProperty.length > 0) {
        let totalPropertySum = this.allProperty.map(item => item.estimated_value)
        this.totalSum.propertSum = totalPropertySum.reduce((a, b) => a + b);
      }
    }

    // update profile
    if ('updatedReport' in changes && 'currentValue' in changes.updatedReport && changes.updatedReport.currentValue) {
      this.spinnerService.show();
      this.userAttributes = null;
      this.userAttributes = this.updatedReport;
      this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
      this.ngOnInit();
    }

    // for transactions
    if ('tranxData' in changes && 'currentValue' in changes.tranxData && changes.tranxData.currentValue && changes.tranxData.currentValue.transactions.length > 0) {
      let transactionData = changes.tranxData.currentValue.transactions;

      let clonedForGraph = this.genericCloneMethod(transactionData);

      let end_date = moment.utc().format(this.DATE_FORMAT);
      let start_date = moment.utc().subtract(11, 'months').startOf('month').format(this.DATE_FORMAT);
      let dates: any = { start_date, end_date };
      this.createTotalOverTimeGraph(dates, clonedForGraph);

      if (this.userAttributes && this.userAttributes.employers && this.userAttributes.employers.length >= 0) {

        // First reset the income and transactions for each employers
        for (let employer of this.userAttributes.employers) {
          if ('income' in employer && employer.income) {
            employer.income = 0;
          }
          if ('seeTransaction' in employer && employer.seeTransaction) {
            employer.seeTransaction = [];
          }
        }

        this.employersData.totalTransArr = [];

        if (changes.tranxData.currentValue.transactions) {
          this.calculateOtherEmployers(changes.tranxData.currentValue.transactions);
        }

        
      }
    } else if ('tranxData' in changes && 'currentValue' in changes.tranxData && changes.tranxData.currentValue && changes.tranxData.currentValue.transactions.length == 0) {
      this.graphs.net_income_over_time.load = false;
      this.employersData.totalTransArr = [];
      this.userAttributes['showNoIncomeFound'] = true;
      this.calculateOtherEmployers(null);
      if (!Utils.isNullOrEmpty(this.userAttributes, 'employers') && this.userAttributes.employers.length > 0 && this.userAttributes.employers[0].title == 'csvEmployer') {
        this.userAttributes['showNoIncomeFound'] = false;
        this.userAttributes.employers[0].preferred = true;
      }
      this.otherEmployersData = {};
      this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
    }

    // reload page
    if ('reloadScreen' in changes && 'currentValue' in changes.reloadScreen && (changes.reloadScreen.currentValue != changes.reloadScreen.previousValue)) {
      this.ngOnInit();
    }

    if ('sidebarReload' in changes && changes.sidebarReload != null) {
      this.ngOnInit();
    }

    if ('employerSectionValue' in changes && 'currentValue' in changes.employerSectionValue && (changes.employerSectionValue.currentValue != changes.employerSectionValue.previousValue)) {
      this.employerBackButton.nativeElement.click();
    }
  }
  //widget
  @HostListener('window:resize') onResize() {
    this.canvasWidthFunction();
  }
  canvasWidthFunction() {
    this.innerWidth = window.innerWidth;
    if ((innerWidth > 480) && (innerWidth < 767)) {
      this.canvasWidth = 250;
      console.log('updatedcanvasWidth', this.canvasWidth);

    }
    if ((innerWidth < 480) && (innerWidth >= 320)) {
      this.canvasWidth = 200;
      console.log('updatedcanvasWidth', this.canvasWidth);

    }
    if (innerWidth > 768) {
      this.canvasWidth = 350;
    }
    if ((innerWidth > 992) && (innerWidth < 1200)) {
      this.canvasWidth = 250;
    }
  }

  createScoreGraph(item) {
    console.log('nexusScorePoint', item);
    if (!(Object.keys(item).length == 0)) {
      if (!('type' in item && item.type == 'report')) {
        if ('tt' in item && (item.tt || item.tt == 0)) {
          this.graphTippingPoints.tt = item.tt <= 0 ? 0 : (item.tt).toFixed(0);
        }
        if ('tm' in item && (item.tm || item.tm == 0)) {
          this.graphTippingPoints.tm = item.tm <= 0 ? 0 : (item.tm).toFixed(0);
        }
        if ('tb' in item && (item.tb || item.tb == 0)) {
          this.graphTippingPoints.tb = item.tb <= 0 ? 0 : (item.tb).toFixed(0);
        }
        this.capturedDoc();
        if (this.graphTippingPoints) {
          
          this.backend.post(this.graphTippingPoints, "/users/points", this.authResultInfo.accessToken).subscribe(res => {
            console.log("Tipping Points added!");
            
          },
            err => {
              let errorVar = err.json();
              console.log("Message from Server: " + errorVar["message"]);
            
            });
        } else {
          console.log("Tipping Points not available!");
        }
      } else {
        if ('tt' in item && (item.tt || item.tt == 0)) {
          this.graphTippingPoints.tt = item.tt < 0 ? 0 : item.tt;
        }
        if ('tm' in item && (item.tm || item.tm == 0)) {
          this.graphTippingPoints.tm = item.tm < 0 ? 0 : item.tm;
        }
        if ('tb' in item && (item.tb || item.tb == 0)) {
          this.graphTippingPoints.tb = item.tb < 0 ? 0 : item.tb;
        }
      }
    }
  }

  //nexusScore widget screenshot
  capturedDoc() {
    let data = document.getElementById('ss');
    if (data) {
      html2canvas(data).then(canvas => {

        let imgData = canvas.toDataURL('image/png');

        this.authService.setDataInCache(this.authService.BASE_64, imgData);
      }).catch(e => {
        console.log(e);
      });
    }
  }

  //format contactNUmber
  formatPhoneNumber(phoneNum) {
    let cleaned = ('' + phoneNum).replace(/\D/g, '')
    let match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
    if (match) {
      this.phoneNumber = '(' + match[1] + ') ' + match[2] + '-' + match[3];
    } else {
      this.phoneNumber = '';
    }
  }

  /* Edit Modal Code */
  openEditModal(editProfileModal: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      editProfileModal,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  }

  editProfileModalClose(event) {
    
    this.modalRef.hide();

    if(event){
      this.ngOnInit();
    }
    if (event && 'address' in event && event.address) {
      this.userAttributes.address = Object.assign(this.userAttributes.address, event.address);
    }

    if (event && 'picture' in event && event.picture) {
      this.userAttributes.picture = event.picture;
      this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
      console.log("In Edit Profile modal close lno 564");
      this.headerEvent.emit(this.userAttributes.picture);
    }
  }

  employerSection() {
    this.employerSectionDiv = false;
  }

  profileSection() {
    this.employerSectionDiv = true;

    // We are reading employers back from session since it might have got updated
    this.userAttributes.employers = this.authService.getDataFromCache(this.authService.USER_PROFILE).employers;
  }

  setRadioBtnValue(preValue, index) {
    for (let e of this.userAttributes.employers) {
      if ('preferred' in e && e.preferred) {
        e.preferred = false;
      }
    }

    if (preValue) {
      this.userAttributes.employers[index].preferred = true;
    } else {
      this.userAttributes.employers[index].preferred = false;
    }
  }

  setEmployerDetails() {
    let data: any = {};
    let testPrefered = false;
    if (this.userAttributes.employers) {
      for (let index of this.userAttributes.employers) {
        if (!index.preferred) {
          if (!testPrefered) {
            testPrefered = false;
          }
        } else {
          testPrefered = true;
        }

        if (index.present) {
          index.present = true;
        } else {
          index.present = false;
        }
        if (index.preferred) {
          index.preferred = true;
        } else {
          index.preferred = false;
        }

        if (index.from_duration_in_date) {
          index.from_duration = moment.utc(index.from_duration_in_date).format(this.DATE_FORMAT);
          delete index.from_duration_in_date;
        } else {
          delete index.from_duration_in_date;
          delete index.from_duration;
        }

        if (index.to_duration_in_date) {
          index.to_duration = moment.utc(index.to_duration_in_date).format(this.DATE_FORMAT);
          delete index.to_duration_in_date;
        } else {
          delete index.to_duration_in_date;
          delete index.to_duration;
        }
        delete index.user_id;
        delete index.years;
        delete index.months;
        delete index.created_at;
      }
      data.employers = JSON.parse(JSON.stringify(this.userAttributes.employers));
    }

    for (let index of data.employers) {
      if (!index.address) {
        delete index.address;
      }
      if (!index.phone_number) {
        delete index.phone_number;
      }
      if (!index.email) {
        delete index.email;
      }
      if (!index.contact_person) {
        delete index.contact_person;
      }
      if (!index.title) {
        delete index.title;
      }
      delete index.from_duration_in_date;
      delete index.to_duration_in_date;
      delete index.income;
      delete index.seeTransaction;
      delete index.dateDuration;
      if (!index.from_duration) {
        delete index.from_duration;
      }
      if (!index.to_duration) {
        delete index.to_duration;
      }
    }

    let path = "/users/" + this.authResultInfo.idTokenPayload.sub;
    if (testPrefered) {
      this.spinnerService.show();
      this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(res => {
        this.translate.get('PC_EMPL_UPDATE_SUCCES')
          .subscribe((value) => {
            this.notifier.notify('success', value);
          });
        let response = res.json();
        console.log('Response#: ', response);
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        this.employerSectionDiv = true;
        this.ngOnInit();
        this.spinnerService.hide();
      },
        err => {
          let errorVar = err.json();
          console.log('Error: ', errorVar["message"]);
          this.translate.get('DASH_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + ": " + errorVar["message"]);
            })
          this.spinnerService.hide();
        });
    } else {
      this.translate.get('PC_SELECET_ATLEAST_ONE')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
      this.ngOnInit();
      this.spinnerService.hide();
    }
  }

  transactionHandler(transaction) {
    if (transaction && transaction.length > 0) {

      let initialState: any = { transactions: transaction };
      this.modalRef = this.modalService.show(
        TransactionsModalComponent,
        Object.assign({}, { class: 'gray modal-lg', initialState }));
    }
  }

  // This function is used to create score graph on profile component
  createNexusScoreGraph(){
    // First get transactions for last 12 months
    // Make get transactions call
    let start_date = moment.utc().add(-12, 'months').startOf('month');
    let end_date = moment.utc().add(-1, 'months').endOf('month');
    
    let path = '/users/transactions';
    let qp = [];
    if (start_date) {
      qp.push('start_date=' + start_date.format(this.DATE_FORMAT));
    }
    if (end_date) {
      qp.push('end_date=' + end_date.format(this.DATE_FORMAT));
    }

    this.loadingScoreGraph = true;
    let token: string = this.report ? this.report.token:this.authService.getAccessTokenAndID().accessToken;
    this.backend.get(path, token)
      .subscribe(data => {
        let allTransactions = data.json();
        if(!Utils.isNullOrEmpty(allTransactions,'transactions')){
          let dates: any = { start_date: start_date.format(this.DATE_FORMAT), end_date: end_date.format(this.DATE_FORMAT) };
          // income calculation
          let filterData = Utils.filterIncomeTransactions(allTransactions.transactions);
          let incomeData = Utils.groupGraphDataByFormat(filterData, dates, 'amount',"MMMM-YY");
          console.log('createNexusScoreGraph - chartData', incomeData);

          // Spending calculation
          let spendDataArr = Utils.filterSpendingTransactions(allTransactions.transactions);
          let cloneSpendDataArr = this.genericCloneMethod(spendDataArr);
          let spendingData = Utils.groupGraphDataByFormat(cloneSpendDataArr, dates, 'amount',"MMMM-YY");
          console.log('createNexusScoreGraph - spendingData', spendingData);

          return this.backend.postHandlerForMeter(this.createDataForScoreAPI(incomeData,spendingData,start_date,end_date))
          .subscribe(res => {
            if ('_body' in res && res['_body']) {
              //Need to check it in production. If this condition is working.
              try {
                let bodyData = JSON.parse(res['_body']);
                if(!Utils.isNullOrEmpty(bodyData,'TippingPoints')){
                  this.createScoreGraph(bodyData.TippingPoints);
                }else{
                  this.createScoreGraph({tt:0,tm:0,tb:0,err:'err'});  
                }
              } catch (err) {
                this.createScoreGraph({tt:0,tm:0,tb:0,err:'err'});
              }
            }
            this.loadingScoreGraph = false;
          });
        }
      
      },err=>{
        console.log('createNexusScoreGraph - error',err);
        this.loadingScoreGraph = false;
      })
      
  }

  // this function is used to create api data
  // generate json data for meter, which will shown in profile
  // when post call successfull, then emit response in profile.ts
  // then show that response data in meter as per requiremnet.
  createDataForScoreAPI(income,spending,start_date,end_date){
    let data = {
      "factors": {
        "top": "1.01",
        "middle": "0.98",
        "bottom": "0.94"
      },
      "listMonths":[]
    }

    for (let l = end_date; l.isSameOrAfter(start_date); l.subtract(1, 'month')) {
        let apiFormatedDate = l.format("MMMM-YY");
        let monthData={
          Month: apiFormatedDate,
          Income: 0,
          SpendingTotal: 0
        }

        if(!Utils.isNullOrEmpty(income,apiFormatedDate)){
          monthData.Income = Utils.roundTo2Digit(income[apiFormatedDate].amount);
        }

        if(!Utils.isNullOrEmpty(spending,apiFormatedDate)){
          monthData.SpendingTotal = Utils.roundTo2Digit(spending[apiFormatedDate].amount);
        }
        data.listMonths.push(monthData);
    }
    return data;
  }

  //Net Income over time
  createTotalOverTimeGraph(dates, transactions) {

    let avgData = [];
    let incomeData: any = [];
    let spendData: any = [];

    // income calculation
    let filterData = Utils.filterIncomeTransactions(transactions);
    let chartData = Utils.groupGraphDataByFormat(filterData, dates, 'amount',null);
    console.log('chartData', chartData);

    // Spending calculation
    let spendDataArr = Utils.filterSpendingTransactions(transactions);
    let cloneSpendDataArr = this.genericCloneMethod(spendDataArr);
    let spendingData = Utils.groupGraphDataByFormat(cloneSpendDataArr, dates, 'amount',null);
    console.log('spendingData', spendingData);

    // averageData calculation
    incomeData = Object.values(chartData).map((a: any) => a.amount);
    spendData = Object.values(spendingData).map((a: any) => a.amount * -1);
    for (let i = 0; i < incomeData.length; i++) {
      avgData.push(incomeData[i] + spendData[i]);
    }

    // net total income and its avg
    let sumOfAmount = avgData.reduce((a, b) => a + b);
    this.graphs.net_income_over_time.totalAmount = sumOfAmount;
    this.graphs.net_income_over_time.avgAmount = sumOfAmount / avgData.length;

    // add income and spend transaction in one variable
    let incomeTransArr = Object.values(chartData).map((t: any) => t.transactionArr);
    let spendingTransArr = Object.values(spendingData).map((t: any) => t.transactionArr);
    let avgTransactionArr = []

    for (let i = 0; i < incomeTransArr.length; i++) {
      let incomeKey = null;
      let spendKey = null;

      incomeKey = incomeTransArr[i];
      spendKey = spendingTransArr[i];

      avgTransactionArr.push(incomeKey.concat(spendKey));
    }

    this.graphs.net_income_over_time.labels.length = 0;
    this.graphs.net_income_over_time.labels.push(...Object.keys(chartData));
    this.graphs.net_income_over_time.data = [
      {
        data: avgData,
        label: `${this.G_Net_Income}`,
        type: "line",
        borderColor: "#717171",
        fill: false,
        lineTension: 0,
        pointRadius: 4,
        pointBackgroundColor: "#717171",
        transactions: avgTransactionArr
      },
      {
        data: incomeData,
        label: `${this.G_Income}`,
        transactions: Object.values(chartData).map((t: any) => t.transactionArr)
      },
      {
        data: spendData,
        label: `${this.G_Spending}`,
        transactions: Object.values(spendingData).map((t: any) => t.transactionArr)
      }
    ];

    this.graphs.net_income_over_time.load = true;
    this.spinnerService.hide();
  }

  // Debt distribution
  createDebtDistributionGraph(accountArray) {

    let deposTypeAccArr = accountArray.filter(t => t.type == 'depository' && t.balances.current <= 0)
    let creditTypeAccArr = accountArray.filter(t => t.type == 'credit' && t.balances.current >= 0);
    let loanTypeAccArr = accountArray.filter(t => t.type == 'loan' && t.balances.current >= 0);

    let mergedArray = [...deposTypeAccArr, ...creditTypeAccArr, ...loanTypeAccArr];

    let debtDistribution: any = {};
    let total = 0;
    let category;

    mergedArray.forEach((t, index) => {
      if (t.official_name) {
        category = t.official_name + ' deleting ' + t.account_id;
        total += Math.abs(t.balances.current);

        //add all data from mergedArray
        debtDistribution[category] = {
          amount: Math.abs(t.balances.current),
          transactions: [t]
        };

      }
    })

    console.log('debtDistribution', debtDistribution)

    this.graphs.current_debt_distribution.barOptions.elements.center.text = this.currency.transform(total, 'USD', true, '1.0-0');
    this.graphs.current_debt_distribution.labels.length = 0;
    this.graphs.current_debt_distribution.labels.push(...Object.keys(debtDistribution));

    this.graphs.current_debt_distribution.data = [{
      data: Object.values(debtDistribution).map((t: any) => t.amount),
      transactions: Object.values(debtDistribution).map((t: any) => t.transactions)
    }]

    this.graphs.current_debt_distribution.load = true;
  }

  // set employer address
  addAddressModal(addressModalTemplate, employerValue, index) {
    this.employerDetail = {
      employer: JSON.parse(JSON.stringify(employerValue)),
      index: index
    }
    this.openEditModal(addressModalTemplate);
  }

  addressModalClose(event) {
    if (event) {
      if ('employer' in event && 'index' in event) {
        if ('address' in event.employer && event.employer.address) {
          this.userAttributes.employers[event.index].address = event.employer.address;
        }
        if ('email' in event.employer || event.employer.email) {
          this.userAttributes.employers[event.index].email = event.employer.email;
        }
        if ('phone_number' in event.employer || event.employer.phone_number) {
          this.userAttributes.employers[event.index].phone_number = event.employer.phone_number;
        }
        if ('contact_person' in event.employer || event.employer.contact_person) {
          this.userAttributes.employers[event.index].contact_person = event.employer.contact_person;
        }
        if ('title' in event.employer || event.employer.title) {
          this.userAttributes.employers[event.index].title = event.employer.title;
        }
      }
      this.modalRef.hide();
      this.setEmployerDetails();
    } else {
      this.modalRef.hide();
    }
  }

  addAccountHandler(refrenceName) {
    this.addAccountHandlerEmit.emit(refrenceName);
  }

  propertyHandler(PropertyName) {
    this.propertyHandlerEmit.emit(PropertyName);
  }
}
