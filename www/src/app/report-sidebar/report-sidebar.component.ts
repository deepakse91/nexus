import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-report-sidebar',
  templateUrl: './report-sidebar.component.html',
  styleUrls: ['./report-sidebar.component.css']
})
export class ReportSidebarComponent implements OnInit {

  totalBal: any = {};
  @Input("accounts") allAccounts: any = [];

  modalRef: BsModalRef;
  realEstateCurrentDiv = "selectPropertyType";

  constructor() { }

  ngOnInit() {
    console.log('inside sidebar', this.allAccounts);
    this.totalBal = { cash: 0, credit: 0, property: 0 };

    //allAccounts balance
    for (let acc of this.allAccounts) {
      for (let i of acc.accounts) {
        // depository type balance
        if (i.type == "depository") {
          this.totalBal.cash += i.balances.current
        }
        // credit type balance
        if (i.type == "credit") {
          this.totalBal.credit += i.balances.current
        }
      }
    }
  }

  showFullAccName(parentIndex, childIndex) {
    this.allAccounts[parentIndex].accounts[childIndex].showFullName = true;
  }

  hideFullAccName(parentIndex, childIndex) {
    delete this.allAccounts[parentIndex].accounts[childIndex].showFullName;
  }

  getKeys(map) {
    return Array.from(Object.keys(map));
  }

  changeModalDiv(divName) {
    this.realEstateCurrentDiv = divName;
  }


}
