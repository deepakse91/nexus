import { Component, OnInit, TemplateRef, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {environment} from './../../environments/environment';
import {BackendService} from '../backend/backend.service';
import {AuthService} from "../auth/auth.service";
import {SessionStorageService} from "angular-web-storage";
import {NgxSpinnerService} from 'ngx-spinner';
import {BsModalService} from 'ngx-bootstrap/modal';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as moment from 'moment';
import {NotifierService} from 'angular-notifier';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { TranslateService } from '@ngx-translate/core';
import Utils from '../common/utils';

@Component({
  selector: 'app-edit-profile-modal',
  templateUrl: './edit-profile-modal.component.html',
  styleUrls: ['./edit-profile-modal.component.css','../global/global.css']
})
export class EditProfileModalComponent implements OnInit {
  @Output() childEvent = new EventEmitter();
  private readonly notifier: NotifierService;
  modalRef: BsModalRef;
  userAttributes: any;
  authResultInfo: any;
  selectedFile: any;
  DATE_FORMAT: string = 'YYYY-MM-DD';
  buttonSave = false;
  clicked = false;
  maxDate: Date = new Date();
  //@Output() onChange: EventEmitter<File> = new EventEmitter<File>();

  constructor(private authService: AuthService,
              private backend: BackendService,
              public session: SessionStorageService,
              private spinnerService: NgxSpinnerService,
              private modalService: BsModalService,
              public translate: TranslateService,
              private notifierService: NotifierService) 
              {
                this.notifier = notifierService;

                
                  // this is default langugage
                  translate.setDefaultLang('en');
  
                  // this language is to set by your prefered browser language
                  let browserLang = translate.getBrowserLang();
                  translate.use(browserLang);
                

  }

  ngOnInit() {
    // Clone this
    this.userAttributes = Utils.cloneJSON(this.authService.getUserProfile());
    if (this.userAttributes.dob) {
      this.userAttributes.dobInDate = moment.utc(this.userAttributes.dob).toDate();
    } else {
      this.userAttributes.dobInDate = null;
    }
    if (!this.userAttributes.address) {
      this.userAttributes.address = {
        street: null,
        city: null,
        street2: null,
        country: null,
        state: null,
        zip: null
      }
    }
    this.authResultInfo = this.authService.getAccessTokenAndID();
  }

  closeModal(event) {
    this.childEvent.emit(event);
  }

  //imagePicker event
  updateSource($event: Event) {
    this.projectImage($event.target['files'][0]);
    this.clicked = true;
  }

  projectImage(file: File) {
    let reader = new FileReader;
    reader.onload = (e: any) => {
      console.log("Data string url Base64", e.target.result);
      this.userAttributes.picture = e.target.result;
    };
    reader.readAsDataURL(file);
  }

  /* Update profile Code */
  updateUserProfile() {
    this.spinnerService.show();
    let data: any = {};
    let requiredField = true;
    if (this.userAttributes.first_name) {
      data.first_name = this.userAttributes.first_name;
    } else {
      requiredField = false;
    }
    if ('last_name' in this.userAttributes) {
      data.last_name = this.userAttributes.last_name;
      if (data.last_name == '') {
        data.last_name = null;
      }

    }
    if (this.userAttributes.dobInDate) {
      data.dob = moment.utc(this.userAttributes.dobInDate).format(this.DATE_FORMAT);
      this.userAttributes.dob = data.dob;
    }
    if (this.userAttributes.address) {
      data.address = JSON.parse(JSON.stringify(this.userAttributes.address));
    }
    if (this.userAttributes.emails) {
      data.emails = this.userAttributes.emails;
    }
    if (this.userAttributes.phone_numbers) {
      data.phone_numbers = this.userAttributes.phone_numbers;
    }
    if (this.userAttributes.picture && this.clicked) {
      data.picture = this.userAttributes.picture;
    }
    console.log("Data : ", data);

    let path = "/users/" + this.authResultInfo.idTokenPayload.sub;
    if (requiredField) {
      this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(res => {

        // translate error message
        this.translate.get('EPM_UPDATE_PROFILE')
          .subscribe((value) => {
            this.notifier.notify('success', value);
          })
        let response = res.json();
        console.log('Response: ', response);
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        this.closeModal(this.userAttributes);
        this.spinnerService.hide();
      },
        err => {
          let errorVar = err.json();
          console.log('Error: ', errorVar["message"]);
          // translate error message
          this.translate.get('EPM_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + errorVar["message"]);
            })
          this.spinnerService.hide();
        });
    } else {
      // translate error message
      this.translate.get('EPM_FIRSTNAME_REQUIRED')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
      this.spinnerService.hide();
    }
  }


  verifyZipCode() {
    let re = /^[0-9]*[-\s]*[0-9]*$/;
    console.log("Zip code now", this.userAttributes.address.zip, re.test(this.userAttributes.address.zip));
    if (!re.test((this.userAttributes.address.zip).toString())) {
      // translate error message
      this.translate.get('EPM_VALID_ZIPCODE')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
      this.buttonSave = true;
    } else {
      this.buttonSave = false;
    }
  }

  emailValidateMethod() {
    let re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (!re.test(this.userAttributes.emails)) {
      if (this.userAttributes.emails == '') {
        this.buttonSave = false;
      } else {
        if (this.userAttributes.emails) {
          // translate error message
          this.translate.get('EPM_VALID_EMAIL')
            .subscribe((value) => {
              this.notifier.notify('error', value);
            })
          this.buttonSave = true;
        } else {
          this.buttonSave = false;
        }
      }
    } else {
      this.buttonSave = false;
    }
  }

  // check the phone number length 
  phoneLength() {
    if ('phone_numbers' in this.userAttributes && this.userAttributes.phone_numbers && this.userAttributes.phone_numbers.length < 14) {
      // translate error message
      this.translate.get('EPM_VALID_PHONENUMBER')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
      this.buttonSave = true;
    } else {
      this.buttonSave = false;
    }
  }

  // restrict alpha character and allow only number
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  alphabetOnly(event): boolean {
    let regex = new RegExp("^[a-zA-Z ]*$");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
      event.preventDefault();
      return false;
    }
  }

}
