import { Component, OnInit,Renderer2, HostListener, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { NotifierService } from 'angular-notifier';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'app';
  private readonly notifier: NotifierService;

  constructor(private renderer : Renderer2, private authService : AuthService, 
    private notifierService: NotifierService, public translate: TranslateService){
    this.notifier = notifierService;

     // this is default langugage
     translate.setDefaultLang('en');

     // this language is to set by your prefered browser language
     let browserLang = translate.getBrowserLang();
     translate.use(browserLang);
  }

  ngOnInit(){
    this.styleForMacOnly();
  }
 // style for mac
 styleForMacOnly = () => {
  if (navigator.userAgent.indexOf('Mac OS X') != -1) {
     this.renderer.addClass(document.body, 'mac');
  }
};
}
