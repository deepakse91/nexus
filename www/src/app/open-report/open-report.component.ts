import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { NotifierService } from "angular-notifier";
import { BackendService } from "../backend/backend.service";
import * as moment from 'moment';
import { of } from "rxjs";
import { mergeMap } from "rxjs/operators";
import { AuthService } from '../auth/auth.service';
import {  OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-open-report',
  templateUrl: './open-report.component.html',
  styleUrls: ['./open-report.component.css','../global/global.css']
})
export class OpenReportComponent implements OnInit {

  notifier: any;
  report: any = null;
  id: any;
  data: any;
  accounts: any = null;
  tranxData: any;
  reportData: any;
  applicationJson: any = {};
  showPdf:boolean = false;
  reportPage:boolean = true;
  scoreValue: any = {};
  subscription: any;
  authInfo:any;
  invokePdf:any;
  expiredDiv:boolean;
  loanType:any = {
    'name': ''
  };

  constructor(public router: Router, private route: ActivatedRoute, private spinnerService: NgxSpinnerService,
    private notifierService: NotifierService, private backend: BackendService, private authService: AuthService) {
    this.notifier = notifierService;

  }

  ngOnInit() {
    console.log("Got obj value", this.subscription);
    this.id = this.route.snapshot.paramMap.get("id");
    console.log('report', this.id);

    this.spinnerService.show();
    let path = "/reports/" + this.id;
    // Get report details
    this.backend.put(null , path, this.authService.getAccessTokenAndID())
      .pipe(mergeMap(data => {
        this.report = data.json();
        console.log('generateReport', this.report);
        this.report.valid_till = parseInt(this.report.valid_till); 
        console.log('ipdateGenerateReport', this.report);
        // this.spinnerService.hide();
        return this.backend.get('/accounts', this.report.token);
      }))
      .subscribe(accountsResponse => {
        console.log('Got accounts', accountsResponse);
        this.accounts = [];
        if (accountsResponse) {
          this.accounts = accountsResponse.json().accounts;
          console.log('this.accounts', this.accounts);

          let accountsObj = Object.create({});
          accountsObj['accounts'] = this.accounts;
          this.reportData = Object.assign(this.report, accountsObj);
        }
        this.reportPage= !this.reportPage;
        this.spinnerService.hide();
        this.backend.get('/users/points', this.report.token).subscribe((res: any) => {
          if(res){
            let points = res.json();
            this.scoreValue = points['data'];
            console.log('Tipping Points', this.scoreValue);
          }
        },
        error => {
          let errorMsg = error.json();
          this.notifier.notify('error', errorMsg.message);
        });
      },
        error => {
          console.log('error', error);
          this.reportPage = !this.reportPage;
          this.spinnerService.hide();
          let errorDetails = error.json();
          if(errorDetails.message == "Report has expired"){
            this.expiredDiv = true;
          }
          this.notifier.notify('error', errorDetails.message);
        });
  }

  receivedTranxData(e: any) {
    this.tranxData = e;
  }

  applicationFormJson(value) {
    this.showPdf = true;
    this.loanType.name = value;
    this.invokePdf = 10;

    this.applicationJson = {
      "@context": "http://schema.org",
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "",
        "addressRegion": "",
        "postalCode": "",
        "streetAddress": "",
        "from": "",
        "to": ""
      },
      "previous addresses": [
        {
          "@type": "PostalAddress",
          "addressLocality": "",
          "addressRegion": "",
          "postalCode": "",
          "streetAddress": "",
          "from": "",
          "to": ""
        }
      ],
      "name": "",
      "jobTitle": "",
      "image": "",
      "alternateName": "",
      "additionalName": "",
      "givenName": "",
      "familyName": "",
      "birthPlace": "",
      "birthDate": "",
      "email": [],
      "telephone": "",
      "mobilephone": "",
      "url": "",
      "sameAs": [],
      "worksFor": {
        "@type": "Organization",
        "name": "",
        "url": "",
        "from": "",
        "to": "",
        "annualSalary": "",
        "address": {
          "@type": "PostalAddress",
          "addressLocality": "",
          "postalCode": "",
          "streetAddress": "",
          "from": "",
          "to": "",
          "annualSalary": ""
        }
      },
      "workedAt": []
    };

    if (this.report && (typeof this.report === 'object')) {
      this.applicationJson.name = this.report.first_name;
      this.applicationJson.givenName = this.report.first_name;
      this.applicationJson.familyName = this.report.last_name;
      this.applicationJson.email.push(this.report.emails);
      this.applicationJson.telephone = this.report.phone_numbers;
      this.applicationJson.image = this.report.picture;
      if('dob' in this.report && this.report.dob){
        this.applicationJson.birthDate = moment.utc(this.report.dob).format('DD-MM-YYYY');
      } else {
        this.applicationJson.birthDate = "";
      }
    }
    if ('address' in this.report && this.report.address) {
      this.applicationJson.address.addressLocality = this.report.address.city;
      this.applicationJson.address.addressRegion = this.report.address.state;
      this.applicationJson.address.postalCode = this.report.address.zip;
      this.applicationJson.address.streetAddress = this.report.address.street;
      this.applicationJson.address.street2 = this.report.address.street2;
    }
    if('employers' in this.report && this.report.employers.length > 0){
      let employerWithPreferredSet = this.report.employers.filter(e => e.preferred);
      if (!(employerWithPreferredSet && employerWithPreferredSet.length > 0)) {
        let employersByAmount = this.report.employers.sort(function (a, b) { return (b.income - a.income); }); 
        if (employersByAmount && employersByAmount.length > 0 && 'income' in employersByAmount[0] && employersByAmount[0].income) {
          employersByAmount[0].preferred = true;
        } else {
          this.report.employers[0].preferred = true;
        }
      }

      for (let employer of this.report.employers) {
        if ('preferred' in employer && employer.preferred) {
          let data: any = {
            'address': {}
          };
          data.name = employer.employer_name;
          data.from = employer.from_duration;
          data.to = employer.to_duration;
          data.annualSalary = employer.income;
          data.preferred = employer.preferred;
          data.occupation = employer.occupation;
          data.self_employed = employer.self_employed;
          if ('address' in employer && employer.address && employer.address != null) {
            data.address.addressLocality = employer.address.city;
            data.address.postalCode = employer.address.zipCode;
            data.address.streetAddress = employer.address.street1;
          }
          if('phone_number' in employer && employer.phone_number){
            data.phone_number = employer.phone_number;
          }
          if('from_duration' in employer && employer.from_duration){
            let presentEmployer = moment.utc();
            let fromDuration = moment.utc(employer.from_duration);
            if(presentEmployer.diff(fromDuration, 'months') > 24){
              data.setDuration = 3;
            } else {
              data.setDuration = 2;
            }
          }
          this.applicationJson.worksFor = Object.assign(this.applicationJson.worksFor, data);
        } else {
          let dataWorkedAt: any = {
            "@type": "Organization",
            address: {
              "@type": "PostalAddress"
            }
          };
          dataWorkedAt.name = employer.employer_name;
          dataWorkedAt.url = "";
          dataWorkedAt.preferred = employer.preferred;
          if('phone_number' in employer && employer.phone_number){
            dataWorkedAt.phone_number = employer.phone_number;
          }
          if ('address' in employer && employer.address) {
            dataWorkedAt.address.addressLocality = employer.address.city;
            dataWorkedAt.address.postalCode = employer.address.zipCode;
            dataWorkedAt.address.streetAddress = employer.address.street1;
          } else {
            dataWorkedAt.address.streetAddress = "";
            dataWorkedAt.address.postalCode = "";
            dataWorkedAt.address.addressLocality = "";
          }
          if (!('preferred' in employer && employer.preferred)){
            if('to_duration' in employer && employer.to_duration){
              let presentEmployer = moment.utc();
              let toDuration = moment.utc(employer.to_duration);
              dataWorkedAt.setDuration = presentEmployer.diff(toDuration, 'months');
            }
          }
          this.applicationJson.workedAt.push(dataWorkedAt);
        }
      }
    }
    console.log("Generate Application JSON", this.applicationJson);
  }

  closingPdf(){
    this.showPdf = false;
  }

  callPrintPdf(){
    this.invokePdf == 'callingPdf' ? this.invokePdf = 'swapVariable' : this.invokePdf = 'callingPdf';
  }

}
