import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ChartsModule } from 'ng4-charts';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import {NgxPaginationModule} from 'ngx-pagination';
import { AvatarModule } from 'ngx-avatar';
import { GaugeChartModule } from 'angular-gauge-chart';

import { AppComponent } from './app.component';
import { AppRoutingModule, RoutingComponents} from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SignuppageComponent } from './signuppage/signuppage.component';
import { AuthService } from './auth/auth.service';
import { NgxPlaidLinkModule } from 'ngx-plaid-link';
import { BackendService } from './backend/backend.service';
import { HttpModule } from '@angular/http';
import { AngularWebStorageModule } from 'angular-web-storage';
import { CongratPageComponent } from './congrat-page/congrat-page.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { GeneratereportPageComponent } from './generatereport-page/generatereport-page.component';
import { OpenReportComponent } from './open-report/open-report.component';
import { EditProfileModalComponent } from './edit-profile-modal/edit-profile-modal.component';
import { TrendsComponent } from './trends/trends.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { ReportTransactionComponent } from './report-transaction/report-transaction.component';
import { ReportSidebarComponent } from './report-sidebar/report-sidebar.component';
import { AddPropertyModalComponent } from './add-property-modal/add-property-modal.component';
import {MoneyFormatterPipe} from "./common/moneyformatter.pipe";
import {DateDiffFormatPipe} from "./common/datediffformat.pipe";
import {Filter} from "./common/filter.pipe";
import {SafeHtml} from "./common/safehtml.pipe";
import { TransactionsModalComponent } from './transactions-modal/transactions-modal.component';
import { DateOrdering } from './common/dateOrdering.pipe';
import { ProfileComponent } from './profile/profile.component';
import { PhoneNumberPipe } from './common/phoneNumber.pipe';
import { SearchFilterPipe } from './common/searchFilter.pipe';
import {Empty} from './common/empty.pipe';
import { QrGenerateModalComponent } from './qr-generate-modal/qr-generate-modal.component';
import { LooseCurrencyPipe } from './common/currencyformatter.pipe';
import { ManageAccountComponent } from './manage-account/manage-account.component';
import { AddAddressModalComponent } from './add-employeraddress-modal/add-address-modal.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { PdfComponent } from './pdf/pdf.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { AddLoanModalComponent } from './add-loan-modal/add-loan-modal.component';
import { AddInvestmentModalComponent } from './add-investment-modal/add-investment-modal.component';
import { NisaanComponent } from './nisaan/nisaan.component';
import { GaugeChartComponent } from './guage-chart/guage-chart.component';
import { VerifyLoanDataComponent } from './verify-loan-data/verify-loan-data.component';
import { Sum } from './common/sum.pipe';



/* Custom angular notifier options */
const customNotifierOptions: NotifierOptions = {
	position: {
		horizontal: {
			position: 'middle',
			distance: 12
		},
		vertical: {
			position: 'top',
			distance: 12,
			gap: 10
		}
	},
};

const avatarColors = ["#FFB6C1", "#2c3e50", "#95a5a6", "#f39c12", "#1abc9c"];

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    SidebarComponent,
    SignuppageComponent,
    CongratPageComponent,
    GeneratereportPageComponent,
    OpenReportComponent,
    EditProfileModalComponent,
    TrendsComponent,
    TransactionsComponent,
    ReportTransactionComponent,
    ReportSidebarComponent,
    AddPropertyModalComponent,
    MoneyFormatterPipe,
    DateDiffFormatPipe,
    Sum,
    Filter,
    SafeHtml,
    Empty,
    PhoneNumberPipe,
    TransactionsModalComponent,
    DateOrdering,
    SearchFilterPipe,
    ProfileComponent,
    QrGenerateModalComponent,
    LooseCurrencyPipe,
    ManageAccountComponent,
    AddAddressModalComponent,
    EmailVerificationComponent,
    PdfComponent,
    AddLoanModalComponent,
    AddInvestmentModalComponent,
    NisaanComponent,
    GaugeChartComponent,
    VerifyLoanDataComponent
  
  ],
  entryComponents: [
    TransactionsModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgxPlaidLinkModule,
    HttpModule,
    AngularWebStorageModule,
    NgxSpinnerModule,
    ChartsModule,
    GaugeChartModule,
    HttpClientModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NotifierModule.withConfig( customNotifierOptions ),
    ProgressbarModule.forRoot(),
    AvatarModule.forRoot({
      colors: avatarColors
    }),
    NgxPaginationModule,
    PopoverModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
    GaugeChartModule
  ],
  providers: [AuthService,BackendService, BsModalRef, MoneyFormatterPipe, LooseCurrencyPipe,DateOrdering],
  bootstrap: [AppComponent]
})
export class AppModule { }
