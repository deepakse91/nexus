import {Component, Input,OnChanges,SimpleChanges, OnInit, TemplateRef, ViewChild, EventEmitter, Output} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Moment} from "moment";
import * as moment from "moment";
import {NgxSpinnerService} from "ngx-spinner";
import {AuthService} from "../auth/auth.service";
import {BackendService} from "../backend/backend.service";
import 'chart.piecelabel.js';
import {TransactionsModalComponent} from '../transactions-modal/transactions-modal.component';
import {BaseChartDirective} from 'ng4-charts';
import { LooseCurrencyPipe } from '../common/currencyformatter.pipe';
import { NotifierService } from 'angular-notifier';
import { TranslateService } from '@ngx-translate/core';
import Utils from '../common/utils';
import { DateOrdering } from '../common/dateOrdering.pipe';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css']
})
export class TrendsComponent implements OnInit,OnChanges {
  modalRef: BsModalRef;
  transactions: any = [];
  balanceArr: any = [];
  @ViewChild('transactionModal') transactionModal;
  @ViewChild("baseChart") chart: BaseChartDirective;
  showNoTransactionsWarning: boolean = false;
  graphValue = 'income_over_time';
  user:any;
  allAccounts:any;
  dateFilterArray: any = [];
  G_Spending:any;
  G_Income:any;
  G_Net_Income:any;
  G_Assets:any;
  G_PROPERTY:any;
  G_Debts:any;
  G_Net_Worth:any;
  graphs:any;

  DATE_FORMAT: string = 'YYYY-MM-DD';
  @Input("report") report: any = null;
  @Input("propertyAttribute") propertyAttribute: any;
  @Input("reloadScreen") reloadScreen :any;
  @Input("controlData") controlData :any;
  @Input("updatedReport") updatedReport: any;
  @Input("sidebarReload") sidebarReload: any;
  @Output() tranxDataEmit = new EventEmitter<any>();
  @Output() addAccountHandlerEmit = new EventEmitter<any>();
  accounts: any = [];
  accountsForBlur:any = [];
  allProperty:any;
  private readonly notifier: NotifierService;

  constructor(private authService: AuthService, private modalService: BsModalService, private spinnerService: NgxSpinnerService, 
    private backend: BackendService, private currency : LooseCurrencyPipe, private notifierService: NotifierService,public translate: TranslateService, private dateOrderPipe: DateOrdering) {
      this.notifier = notifierService;
      let G_TRANSACTIONS;
      translate.get('G_TRANSACTIONS').subscribe(value => G_TRANSACTIONS = value);

      this.graphs = {
        "income_over_time": {
          name: 'G_INCOME_OVER_TIME', 
          translationKey:"G_INCOME_OVER_TIME",
          totalAmountNameKey : 'G_TOTAL_INCOME_FOR_PERIOD',
          avgAmountNameKey: 'G_AVERAGE_INCOME',
          load: false,
          showFromOption: true,
          accountsFilter: 'all',
          datesFilter: '12months',
          barOptions: {
            maintainAspectRatio: false,
            scaleShowHorizontalLines: true,
            responsive: true,
            layout: {
              padding: {
                bottom: 60  //set that fits the best
              }
            },
            tooltips: {
              bodyFontSize: 24,
              bodySpacing: 4,
              xPadding: 16,
              yPadding: 10,
              xAlign: "center",
              yAlign: 'top',
              custom: function (tooltip) {
                if (!tooltip) return;
                // disable displaying the color box;
                tooltip.displayColors = false;
              },
              callbacks: {
                label: function (tooltipItem, data) {
                  return '$' + tooltipItem.yLabel.toLocaleString();
                },
                footer: function (tooltipItem, data) {
                  let index = tooltipItem[0].index;
                  let t = data.datasets[0].transactions[index].length;
                  return t + `${G_TRANSACTIONS}`;
                }
              }
            },
            hover: {
              onHover: function (e) {
                let point = this.getElementAtEvent(e);
                if (point.length) {
                  e.target.style.cursor = 'pointer';
                } else e.target.style.cursor = 'default';
              }
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  autoSkip: false,
                  callback: function(value, index, values) {
                    if(parseInt(value) < 0 ||  parseInt(value) >= 0){
                      return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                    }
                  }
                }
              }]
            }
          },
          colors: [
            {
              backgroundColor: 'rgb(0, 194, 86)'
            }
          ],
          labels: [],
          type: 'bar',
          data: [],
          totalAmount : 0,
          avgAmount : 0,
        },
        "spending_over_time": {
          name: 'Spending Over Time',
          translationKey:"G_SPENDING_OVER_TIME",
          totalAmountName : 'Total Spending for period',
          totalAmountNameKey : 'G_TOTAL_SPENDING_FOR_PERIOD',
          avgAmountName : 'Average Spending',
          avgAmountNameKey : 'G_AVERAGE_SPENDING',
          load: false,
          showFromOption: true,
          accountsFilter: 'all',
          accountsFilterkey: 'G_ALL',
          datesFilter: '12months',
          datesFilterKey: 'G_12MONTHS',
          barOptions: {
            maintainAspectRatio: false,
            scaleShowHorizontalLines: true,
            responsive: true,
            layout: {
              padding: {
                bottom: 60  //set that fits the best
              }
            },
            tooltips: {
              bodyFontSize: 24,
              bodySpacing: 4,
              xPadding: 16,
              yPadding: 10,
              xAlign: "center",
              yAlign: 'top',
              custom: function (tooltip) {
                if (!tooltip) return;
                // disable displaying the color box;
                tooltip.displayColors = false;
              },
              callbacks: {
                label: function (tooltipItem, data) {
                  return '$' + tooltipItem.yLabel.toLocaleString();
                },
                footer: function (tooltipItem, data) {
                  let index = tooltipItem[0].index;
                  let t = data.datasets[0].transactions[index].length;
                  return t + `${G_TRANSACTIONS}`;
                }
              }
            },
            hover: {
              onHover: function (e) {
                let point = this.getElementAtEvent(e);
                if (point.length) e.target.style.cursor = 'pointer';
                else e.target.style.cursor = 'default';
              }
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  autoSkip: false,
                  callback: function(value, index, values) {
                    if(parseInt(value) < 0 ||  parseInt(value) >= 0){
                      return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                    }
                  }
                }
              }]
            }
          },
          colors: [
            {
              backgroundColor: 'rgb(255, 107, 113)'
            }
          ],
          labels: [],
          type: 'bar',
          data: [],
          totalAmount : 0,
          avgAmount : 0,
        },
        "net_income_over_time": {
          name: 'Net Income Over Time',
          translationKey:"G_NET_INCOME_OVER_TIME",
          totalAmountName : 'Total Net income for period',
          totalAmountNameKey : 'G_TOTAL_NET_INCOME_FOR_PERIOD',
          avgAmountName : 'Average net income',
          avgAmountNameKey : 'G_AVERAGE_NET_INCOME',
          load: false,
          showFromOption: false,
          accountsFilter: 'all',
          accountsFilterkey: 'G_ALL',
          datesFilter: '12months',
          datesFilterKey: 'G_12MONTHS',
          barOptions: {
            maintainAspectRatio: false,
            scaleShowHorizontalLines: true,
            responsive: true,
            layout: {
              padding: {
                bottom: 70  //set that fits the best
              }
            },
            scales: {
              xAxes: [{
                stacked: true
              }],
              yAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                  autoSkip: false,
                  callback: function(value, index, values) {
                    if(parseInt(value) < 0 ||  parseInt(value) >= 0){
                      return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                    }
                  }
                }
              }]
            },
            tooltips: {
              titleFontSize : 20,
              bodyFontSize: 18,
              bodySpacing: 4,
              xPadding: 16,
              yPadding: 6,
              xAlign: "center",
              yAlign: "top",
              custom: function (tooltip) {
                if (!tooltip) return;
    
                if (('dataPoints' in tooltip && tooltip.dataPoints && tooltip.dataPoints[0].datasetIndex)) {
                  tooltip.opacity = 1;
                }
                // disable displaying the color box;
                tooltip.displayColors = false;
              },
              callbacks: {
                beforeTitle: function (tooltipItem, data) {
                  if(data.datasets[tooltipItem[0].datasetIndex].label){
                    let title = data.datasets[tooltipItem[0].datasetIndex].label;
                    return title;
                  }
                },
                title: () => null,
                beforeLabel: function (tooltipItem, data) {
                  let modifiedLabels = data.labels[tooltipItem.index];
                  return modifiedLabels;
                },
                label: function (tooltipItem, data) {
                  if (tooltipItem && tooltipItem.datasetIndex >= 0) {
                    return '$' + tooltipItem.yLabel.toLocaleString();
                  }
                  return '';
                },
                footer: function (tooltipItem, data) {
                  let index = tooltipItem[0].index;
                  if (data.datasets[tooltipItem[0].datasetIndex].transactions[index] && data.datasets[tooltipItem[0].datasetIndex].transactions[index].length > 0) {
                    let t = data.datasets[tooltipItem[0].datasetIndex].transactions[index].length;
                    return t + `${G_TRANSACTIONS}`;
                  }
                }
              }
            },
            hover: {
              onHover: function (e) {
                let point = this.getElementAtEvent(e);
                if (point.length) e.target.style.cursor = 'pointer';
                else e.target.style.cursor = 'default';
              }
            },
          },
          colors: [
            {
              backgroundColor: '#717171'
            },
            {
              backgroundColor: 'rgb(0, 194, 86)'
            },
            {
              backgroundColor: 'rgb(255, 107, 113)'
            }
          ],
          labels: [],
          type: 'bar',
          data: [],
          totalAmount : 0,
          avgAmount : 0,
        },
        "debts_over_time": {
          name: 'Debts Over Time',
          translationKey:"G_DEBTS_OVER_TIME",
          totalAmountName : 'Lowest Debt',
          totalAmountNameKey : 'G_LOWEST_DEBT',
          avgAmountName : 'Highest debt',
          avgAmountNameKey : 'G_HIGHEST_DEBT',
          load: false,
          showFromOption: true,
          accountsFilter: 'all',
          accountsFilterKey: 'G_ALL',
          datesFilter: '12months',
          datesFilterKey: 'G_12MONTHS',
          barOptions: {
            maintainAspectRatio: false,
            scaleShowHorizontalLines: true,
            responsive: true,
            layout: {
              padding: {
                bottom: 60  //set that fits the best
              }
            },
            tooltips: {
              enabled: true,
              bodyFontSize: 24,
              bodySpacing: 4,
              xPadding: 16,
              yPadding: 10,
              xAlign: "center",
              yAlign: 'top',
              custom: function (tooltip) {
                if (!tooltip) return;
                // disable displaying the color box;
                tooltip.displayColors = false;
              },
              callbacks: {
                label: function (tooltipItem, data) {
                  return '$' + tooltipItem.yLabel.toLocaleString();
                },
                footer: function (tooltipItem, data) {
                  let index = tooltipItem[0].index;
                  let t = data.datasets[0].transactions[index].length;
                  // return t + " Transactions";
                }
              }
            },
            hover: {
              onHover: function (e) {
                let point = this.getElementAtEvent(e);
                if (point.length) e.target.style.cursor = 'default';
                else e.target.style.cursor = 'default';
              }
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  autoSkip: false,
                  callback: function(value, index, values) {
                    if(parseInt(value) < 0 ||  parseInt(value) >= 0){
                      return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                    }
                  }
                }
              }]
            }
          },
          colors: [
            {
              backgroundColor: 'rgb(255, 107, 113)'
            }],
          labels: [],
          type: 'bar',
          data: [],
          totalAmount : 0,
          avgAmount : 0,
        },
        "assets_over_time": {
          name: 'Assets Over Time',
          translationKey:"G_ASSETS_OVER_TIME",
          totalAmountName : 'Highest Assets for period',
          totalAmountNameKey : 'G_CURRENT_ASSETS_FOR_PERIOD',
          avgAmountName : 'Average assets',
          avgAmountNameKey : 'G_AVERAGE_ASSETS',
          load: false,
          showFromOption: true,
          accountsFilter: 'all',
          accountsFilterKey: 'G_ALL',
          datesFilter: '12months',
          datesFilterKey: 'G_12MONTHS',
          barOptions: {
            maintainAspectRatio: false,
            scaleShowHorizontalLines: true,
            responsive: true,
            layout: {
              padding: {
                bottom: 60  //set that fits the best
              }
            },
            tooltips: {
              enabled: true,
              bodyFontSize: 24,
              bodySpacing: 4,
              xPadding: 16,
              yPadding: 10,
              xAlign: "center",
              yAlign: 'top',
              custom: function (tooltip) {
                if (!tooltip) return;
                // disable displaying the color box;
                tooltip.displayColors = false;
              },
              callbacks: {
                beforeTitle: function (tooltipItem, data) {
                  if(data.datasets[tooltipItem[0].datasetIndex].label){
                    let title = data.datasets[tooltipItem[0].datasetIndex].label;
                    return title;
                  }
                },
                title: () => null,
                beforeLabel: function (tooltipItem, data) {
                  let modifiedLabels = data.labels[tooltipItem.index];
                  return modifiedLabels;
                },
                label: function (tooltipItem, data) {
                  return '$' + tooltipItem.yLabel.toLocaleString();
                },
                footer: function (tooltipItem, data) {
                  let index = tooltipItem[0].index;
                  let t = data.datasets[0].transactions[index].length;
                  // return t + `${G_TRANSACTIONS}`;
                }
              }
            },
            hover: {
              onHover: function (e) {
                let point = this.getElementAtEvent(e);
                if (point.length) e.target.style.cursor = 'default';
                else e.target.style.cursor = 'default';
              }
            },
            scales: {xAxes: [{
              stacked: true
            }],
              yAxes: [{
                stacked: true,
                ticks: {
                  autoSkip: false,
                    min: 0,
                    callback: function(value, index, values) {
                    if(parseInt(value) < 0 ||  parseInt(value) >= 0){
                      return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                    }
                  }
                }
              }]
            }
          },
          colors: [
            {
              backgroundColor: 'rgb(0, 194, 86)'
            },
            {
              backgroundColor: 'orange'
            }
          ],
          labels: [],
          type: 'bar',
          data: [],
          totalAmount : 0,
          avgAmount : 0,
        },
        "networth_over_time": {
          name: 'NetWorth Over Time',
          translationKey:"G_NETWORTH_OVER_TIME",
          totalAmountName : 'Lowest Net Worth',
          totalAmountNameKey : 'G_LOWEST_NET_WORTH',
          avgAmountName : 'Highest Net Worth',
          avgAmountNameKey : 'G_HIGHEST_NET_WORTH',
          load: false,
          showFromOption: false,
          accountsFilter: 'all',
          accountsFilterKey: 'G_ALL',
          datesFilter: '12months',
          datesFilterKey: 'G_12MONTHS',
          barOptions: {
            maintainAspectRatio: false,
            scaleShowHorizontalLines: true,
            responsive: true,
            layout: {
              padding: {
                bottom: 60  //set that fits the best
              }
            },
            tooltips: {
              enabled: true,
              bodyFontSize: 24,
              bodySpacing: 4,
              xPadding: 16,
              yPadding: 10,
              xAlign: "center",
              yAlign: 'top',
              custom: function (tooltip) {
                if (!tooltip) return;
                // disable displaying the color box;
                tooltip.displayColors = false;
              },
              callbacks: {
                beforeTitle: function (tooltipItem, data) {
                  if(data.datasets[tooltipItem[0].datasetIndex].label){
                    let title = data.datasets[tooltipItem[0].datasetIndex].label;
                    return title;
                  }
                },
                title: () => null,
                beforeLabel: function (tooltipItem, data) {
                  let modifiedLabels = data.labels[tooltipItem.index];
                  return modifiedLabels;
                },
                label: function (tooltipItem, data) {
                  if (tooltipItem && tooltipItem.datasetIndex >= 0) {
                    return '$' + tooltipItem.yLabel.toLocaleString();
                  }
                  return '';
                },
                footer: function (tooltipItem, data) {
                  let index = tooltipItem[0].index;
                  if (data.datasets[tooltipItem[0].datasetIndex].transactions[index] && data.datasets[tooltipItem[0].datasetIndex].transactions[index].length > 0) {
                    let t = data.datasets[tooltipItem[0].datasetIndex].transactions[index].length;
                    // return t + `${G_TRANSACTIONS}`;
                  }
                }
              }
            },
            hover: {
              onHover: function (e) {
                let point = this.getElementAtEvent(e);
                if (point.length) e.target.style.cursor = 'default';
                else e.target.style.cursor = 'default';
              }
            },
            scales: {
              xAxes: [{
                stacked: true
              }],
              yAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                  autoSkip: false,
                  callback: function(value, index, values) {
                    if(parseInt(value) < 0 ||  parseInt(value) >= 0){
                      return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                    }
                  }
                }
              }]
            },
          },
          colors: [
            {
              backgroundColor: '#717171'
            },
            {
              backgroundColor: 'rgb(0, 194, 86)'
            },
            {
              backgroundColor: 'rgb(255, 107, 113)'
            },
            {
              backgroundColor: 'orange'
            }
          ],
          labels: [],
          type: 'bar',
          data: [],
          totalAmount : 0,
          avgAmount : 0,
        },
        "spending_by_category": {
          name: 'Spending By Category for past 12 months',
          translationKey:"G_SPENDING_BY_CATEGORY",
          load: false,
          showFromOption: true,
          accountsFilter: 'all',
          accountsFilterKey: 'G_ALL',
          datesFilter: '12months',
          datesFilterKey: 'G_12MONTHS',
          barOptions: {
            maintainAspectRatio: false,
            responsive: true,
            layout: {
              padding: {
                bottom: 30  //set that fits the best
              }
            },
            tooltips: {
              titleFontSize: 20,
              bodySpacing: 4,
              xPadding: 12,
              yPadding: 10,
              xAlign: "center",
              yAlign: 'top',
              mode: 'label',
              position: 'cursor',
              intersect:true,
              custom: function (tooltip) {
                if (!tooltip) return;
                // disable displaying the color box;
                tooltip.displayColors = false;
              },
              callbacks: {
                title: function (tooltipItem, data) {
                  let tooltipIndex = tooltipItem.index ? tooltipItem.index : tooltipItem[0].index
                  let amountData = data.datasets[0].data[tooltipIndex];
                  return '$' + ((amountData).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                },
                label: function (tooltipItem, data) {
                  return "";
                },
                footer: function (tooltipItem, data) {
                  let index = tooltipItem[0].index;
                  let t = data.datasets[1].data[index].length;
                  return t + `${G_TRANSACTIONS}`;
                }
              }
            },
            cutoutPercentage: 70,
            hover: {
              onHover: function (e) {
                let point = this.getElementAtEvent(e);
                if (point.length) {
                  e.target.style.cursor = 'pointer';
                } else e.target.style.cursor = 'default';
              }
            },
            legend: {
              display: false,
              labels: {
                fontColor: '#000',
                fontSize: 16
              },
              position: 'right'
            },
            pieceLabel:
              {
                segment: true,
                position: 'outside',
                overlap: false,
                fontColor: '#000',
                fontSize: 12,
                textMargin: 12,
                outsidePadding: 0,
                render: function (args) {
                  return args.label;
                }
              },
            elements: {
              center: {
                text: '$',
                color: '#36A2EB', //Default black
                fontStyle: 'Helvetica', //Default Arial
                sidePadding: 15 //Default 20 (as a percentage)
              }
            },
            onResize: function (chart, size) {
              if (size.width > 350) {
                chart.options.legend.position = 'right';
                chart.options.legend.fontSize = 16;
                chart.options.pieceLabel.textMargin = 12;
              } else if (size.width < 350) {
                chart.options.legend.position = 'top';
                chart.options.legend.fontSize = 6;
                chart.options.pieceLabel.textMargin = 8;
                chart.height = 300
              }
            }
          },
          colors: [
            {
              backgroundColor: [],
              borderWidth : 1
            },
               ],
          labels: [],
          type: 'doughnut',
          data: [],
          transactions: [],
          level: 0,
          labelHierachy: []
        }
      };
  }

  drillDownPieChart(index, type) {
    if (type == 'RESET_PIE') {
      this.graphs.spending_by_category.level = 0;
      let allFirstTranx = this.graphs.spending_by_category.transactions[this.graphs.spending_by_category.level];
      let selectedPieTranx = [];
      for (let i = 0; i < allFirstTranx.length; i++) {
        selectedPieTranx = selectedPieTranx.concat(allFirstTranx[i]);
      }
      this.createPieChartOverTimeGraph(selectedPieTranx, 0);
    } else {
      if (this.graphs.spending_by_category.level < 1) {
        let selectedPieTranx = this.graphs.spending_by_category.transactions[this.graphs.spending_by_category.level][index];
        
        // collection of category
        let collectionOfCatog = selectedPieTranx.map(t => t.category);

        // find repeated category
        let collectRepeatedCatog = [...new Set(collectionOfCatog)];
        
        // if only one category repeating in all transaction, then show modal.
        if(collectRepeatedCatog.length == 1){
            let initialState: any = {transactions: selectedPieTranx};
            this.modalRef = this.modalService.show(
              TransactionsModalComponent,
              Object.assign({}, {class: 'gray modal-lg', initialState}));
              return;
        }

        // trying to find how many string in transaction's category and then find which category have more string
        let catg;
        let count = 0;
        for (let t of selectedPieTranx) {
          catg = t.category.split(',');
          if (catg.length > count) {
            count = catg.length
          }
        }
        //if finalCatg have only 2 string and our lavel is 1; then i reset level with 0;
        if ((count - this.graphs.spending_by_category.level) == 1) {
          this.graphs.spending_by_category.level = 0;
          let allFirstTranx = this.graphs.spending_by_category.transactions[this.graphs.spending_by_category.level];
          let selectedPieTranx = [];
          for (let i = 0; i < allFirstTranx.length; i++) {
            selectedPieTranx = selectedPieTranx.concat(allFirstTranx[i]);
          }
          this.createPieChartOverTimeGraph(selectedPieTranx, 0);
        } else {
          this.graphs.spending_by_category.labelHierachy.push(this.graphs.spending_by_category.labels[index]);
          this.createPieChartOverTimeGraph(selectedPieTranx, this.graphs.spending_by_category.level + 1);
        }

      } else {
        // showing transaction modal
        let allFirstTranx = this.graphs.spending_by_category.transactions[this.graphs.spending_by_category.level];
        let initialState: any = {transactions: allFirstTranx[index]};
        if('transactions' in initialState && initialState.transactions && initialState.transactions.length > 0){
          this.modalRef = this.modalService.show(
            TransactionsModalComponent,
            Object.assign({}, {class: 'gray modal-lg', initialState}));
         } else{
          this.translate.get('G_NO_TRANS')
          .subscribe((value)=>{
            this.notifier.notify('warning', value);
         });
        } 
      }
    }
  }

  onChartClick(event) {
    console.log('onChartClick', event);
    if (event && event.active
      && event.active.length > 0
      && (event.active[0]._chart.config.type === 'doughnut' || event.active[0]._chart.config.type === 'bar')
      && event.active[0]._chart.config.data
      && event.active[0]._chart.config.data.datasets
      && event.active[0]._chart.config.data.datasets.length > 0
    ) {
      let stopClickEvent = ["DEBTS", "BALANCE", "NETWORTH", "DEUDAS", "BALANCE", "VALOR NETO"];
      if (event.active[0]._chart.config.type === 'bar') {
        if (!(stopClickEvent.includes(event.active[0]._model.datasetLabel || 
          event.active[0]._chart.config.data.datasets[2].label))) {
          const chart = event.active[0]._chart;
          const activePoints = chart.getElementAtEvent(event.event);
          const clickedElementIndex = activePoints[0]._datasetIndex;
          let initialState: any = {transactions: event.active[clickedElementIndex]._chart.config.data.datasets[clickedElementIndex].transactions[event.active[clickedElementIndex]._index]};
         if('transactions' in initialState && initialState.transactions && initialState.transactions.length > 0){
          this.modalRef = this.modalService.show(
            TransactionsModalComponent,
            Object.assign({}, {class: 'gray modal-lg', initialState}));
         } else{
          this.translate.get('G_NO_TRANS')
          .subscribe((value)=>{
            this.notifier.notify('warning', value);
         });
         }         
        }
      } else {
        this.drillDownPieChart(event.active[0]._index, null);
      }
    }
  };

  ngOnInit() {
    this.user = this.authService.getUserProfile();
    console.log('fromTends-user', this.user);
    console.log('trend-report', this.report);
    this.allAccounts = [];
    if(this.report){
      this.allAccounts = this.report.accounts;
    } else if(this.user){
      this.allAccounts = this.user.accounts;
    }
    this.allProperty = this.user.properties;
    console.log('this.allaccounts', this.user);
    console.log('this.allProperty', this.allProperty);

    this.translate.get('G_SPENDING').subscribe(value => this.G_Spending = value);
    this.translate.get('G_INCOME').subscribe(value => this.G_Income = value);
    this.translate.get('G_NET_INCOME').subscribe(value => this.G_Net_Income = value);
    this.translate.get('G_BALANCE').subscribe(value => this.G_Assets = value);
    this.translate.get('G_DEBTS').subscribe(value => this.G_Debts = value);
    this.translate.get('G_NET_WORTH').subscribe(value => this.G_Net_Worth = value);
    this.translate.get('MA_PROPERTY').subscribe(value => this.G_PROPERTY = value);
    let start_date: any = null;
    let end_date: any = null;
    this.dateFilterArray = [];
    this.dateFilterArray.push(
        {
          key: 'Last 7 days',
          translateKeyValue:"G_LAST_7_DAYS",
          value: '7days',
        },
        {
          key: 'Last 14 days',
          translateKeyValue:"G_LAST_14_DAYS",
          value: '14days',
        },
        {
          key: 'This month',
          translateKeyValue:"G_THIS_MONTH",
          value: 'thismonth',
        },
        {
          key: 'Last 3 months',
          translateKeyValue:"G_LAST_3_MONTHS",
          value: '3months',
        }
    )
    if (this.report) {

      console.log('Inside report trend component:', this.report);
      if (this.report.trans_interval === 6) {
        this.dateFilterArray.push({
          key: 'Last 6 months',
          translateKeyValue:"G_LAST_6_MONTHS",
          value: '6months',
        });

      } else if (this.report.trans_interval === 12) {
        this.dateFilterArray.push(
          {
          key: 'Last 6 months',
          translateKeyValue:"G_LAST_6_MONTHS",
          value: '6months',
          },
          {
            key: 'Last 6 months',
            translateKeyValue:"G_LAST_12_MONTHS",
            value: '12months',
          }
        );
      }

      // Set default value
      for (let graph of this.getKeys(this.graphs)) {
        this.graphs[graph].datesFilter = this.dateFilterArray[this.dateFilterArray.length - 1].value;
      }
      start_date = moment.utc().subtract(11, this.report.trans_duration).format(this.DATE_FORMAT);
      end_date = moment.utc().format(this.DATE_FORMAT);

    } else {
      this.dateFilterArray.push(
        {
          key: 'Last 6 months',
          translateKeyValue:"G_LAST_6_MONTHS",
          value: '6months'
        },
        {
          key: 'Last 6 months',
          translateKeyValue:"G_LAST_12_MONTHS",
          value: '12months'
        },
        {
          key: 'This year',
          translateKeyValue:"G_THIS_YEAR",
          value: 'thisyear'
        },
        {
          key: 'Last year',
          translateKeyValue:"G_LAST_YEAR",
          value: 'lastyear'
        }
      );

      
      if (this.allAccounts && this.allAccounts.length > 0) {
        this.accounts = [];
        for (let us of this.allAccounts) {
          if ('accounts' in us && us.accounts) {
            for (let account of us.accounts) {
              account.inst_name = us.inst_name;
              this.accounts.push(account);
            }
          }
        }
      }

      start_date = moment.utc().add(-11, 'months').startOf('month').format(this.DATE_FORMAT);
      end_date = moment.utc().format(this.DATE_FORMAT);
    }

    //get accounts
    if (this.allAccounts && this.allAccounts.length > 0) {
      this.accountsForBlur = [];
      for (let us of this.allAccounts) {
        if ('accounts' in us && us.accounts) {
          for (let account of us.accounts) {
            if(('type' in account && account.type == "depository") || ('type' in account && account.type == "credit")){
              this.accountsForBlur.push(account);
            }
          }
        }
      }
    }

    // Get transactions also
  //  this.spinnerService.show();
    return this.populateTransactions(start_date, end_date, null, 'all');


  }
 
  

 
 
  ngOnChanges(changes: SimpleChanges) {
    if ('reloadScreen' in changes && changes.reloadScreen.previousValue != null) {
      this.showNoTransactionsWarning = false;
      this.ngOnInit();
    }

    if ('controlData' in changes && changes.controlData.previousValue != null) {
      this.showNoTransactionsWarning = true;
    }

    if('updatedReport' in changes && 'currentValue' in changes.updatedReport && changes.updatedReport.currentValue){
      this.report = null;
      this.report = this.updatedReport;
      this.ngOnInit();
    }

    if ('propertyAttribute' in changes && changes.propertyAttribute && 'currentValue' in changes.propertyAttribute && changes.propertyAttribute.currentValue) {
      this.ngOnInit();
    }

    if ('sidebarReload' in changes && changes.sidebarReload != null) {
      this.ngOnInit();
   }
  }

  // dynamic color generator
  dynamicColors(){
    let r = Math.floor((Math.random() * 255) - 30);
    let g = Math.ceil((Math.random() * 255) + 30);
    let b = Math.round((Math.random() * 255));
    return "rgba(" + r + "," + g + "," + b + "," + 0.8 + ")";
  };

  getColor(){
    if(this.graphs.spending_by_category.data && this.graphs.spending_by_category.data.length > 0){
      this.graphs.spending_by_category.colors[0].backgroundColor.length = 0;
      for(let i in this.graphs.spending_by_category.data[0].data){
        let returnColor =  this.dynamicColors();
        this.graphs.spending_by_category.colors[0].backgroundColor.push(returnColor)
      }
      console.log('sampleColor', this.graphs.spending_by_category.colors);
      console.log('pie-color', this.graphs.spending_by_category.colors[0].backgroundColor);
    }
  }
  // deep cloning 
  genericCloneMethod(arrayData) {
    let arrr = [];
    for (let obj of arrayData) {
      let b = null;
          b = Object.assign({},obj);
          arrr.push(b)
    }
    return arrr;
  }


  getKeys(map) {
    return Array.from(Object.keys(map));
  }

  //spending over time
  createSpendingOverTimeGraph(dates, transactions) {
    let SpendFilterData = Utils.filterSpendingTransactions(transactions);
    let spendingData = Utils.groupGraphDataByFormat(SpendFilterData,dates,'amount',null);
    
    let amountArr = Object.values(spendingData).map((t: any) => Math.abs(t.amount));
    let sumOfAmount = amountArr.reduce((a, b) => a + b);    
    this.graphs.spending_over_time.totalAmount = sumOfAmount;
    this.graphs.spending_over_time.avgAmount = sumOfAmount / amountArr.length;

    this.graphs.spending_over_time.labels.length = 0;
    this.graphs.spending_over_time.labels.push(...Object.keys(spendingData));
    this.graphs.spending_over_time.data = [{
      data: Object.values(spendingData).map((t: any) => Math.abs(t.amount)),
      label: `${this.G_Spending}`,
      transactions: Object.values(spendingData).map((t: any) => t.transactionArr)
    }];

    this.graphs.spending_over_time.load = true;
  }

  // income over time
  createIncomeOverTimeGraph(dates, transactions) {

    let incomeDataArr = Utils.filterIncomeTransactions(transactions);
    let chartData = Utils.groupGraphDataByFormat(incomeDataArr,dates,'amount',null);
    
    // sum of chartdata amount
    let amountArr = Object.values(chartData).map((t: any) => Math.abs(t.amount));
    let sumOfAmount = amountArr.reduce((a, b) => a + b);    
    this.graphs.income_over_time.totalAmount = sumOfAmount;
    this.graphs.income_over_time.avgAmount = sumOfAmount / amountArr.length;

    this.graphs.income_over_time.labels.length = 0;
    this.graphs.income_over_time.labels.push(...Object.keys(chartData));
    this.graphs.income_over_time.data = [{
      data: Object.values(chartData).map((t: any) => Math.abs(t.amount)),
      label: `${this.G_Income}`,
      transactions: Object.values(chartData).map((t: any) => t.transactionArr)
    }];
    this.graphs.income_over_time.load = true;
    this.spinnerService.hide();
  };

  //Net Income over time
  createTotalOverTimeGraph(dates, transactions) {
    
    let avgData = [];
    let incomeData: any = [];
    let spendData: any = [];

    // income calculation
    let filterData = Utils.filterIncomeTransactions(transactions);
    let chartData = Utils.groupGraphDataByFormat(filterData,dates,'amount',null);

    // Spending calculation
    let spendDataArr = Utils.filterSpendingTransactions(transactions);
    let cloneSpendDataArr = this.genericCloneMethod(spendDataArr);
    let spendingData = Utils.groupGraphDataByFormat(cloneSpendDataArr,dates,'amount',null);

    // averageData calculation
    incomeData = Object.values(chartData).map((a: any) => a.amount);
    spendData = Object.values(spendingData).map((a: any) => a.amount * -1);
    for (let i = 0; i < incomeData.length; i++) {
      avgData.push(incomeData[i] + spendData[i]);
    }

    // net total income and its avg
    let sumOfAmount = avgData.reduce((a, b) => a + b);    
    this.graphs.net_income_over_time.totalAmount = sumOfAmount;
    this.graphs.net_income_over_time.avgAmount = sumOfAmount / avgData.length;

    // add income and spend transaction in one variable
    let incomeTransArr = Object.values(chartData).map((t: any) => t.transactionArr);
    let spendingTransArr = Object.values(spendingData).map((t: any) => t.transactionArr);
    let avgTransactionArr = []

    for (let i = 0; i < incomeTransArr.length; i++) {
      let incomeKey = null;
      let spendKey = null;

      incomeKey = incomeTransArr[i];
      spendKey = spendingTransArr[i];

      avgTransactionArr.push(incomeKey.concat(spendKey));
    }
   
    this.graphs.net_income_over_time.labels.length = 0;
    this.graphs.net_income_over_time.labels.push(...Object.keys(chartData));
    this.graphs.net_income_over_time.data = [
      {
        data: avgData,
        label: `${this.G_Net_Income}`,
        type: "line",
        borderColor: "#717171",
        fill: false,
        lineTension: 0,
        pointRadius: 4,
        pointBackgroundColor: "#717171",
        transactions: avgTransactionArr
      },
      {
        data: incomeData,
        label: `${this.G_Income}`,
        transactions: Object.values(chartData).map((t: any) => t.transactionArr)
      },
      {        
        data: spendData,
        label: `${this.G_Spending}`,
        transactions: Object.values(spendingData).map((t: any) => t.transactionArr)
      }
    ];

    this.graphs.net_income_over_time.load = true;
    this.spinnerService.hide();
  }

  // this function is used to create snapshot of balance based on dateformat
  createBalanceSnapshotByDateFormat(dates,filteredBalances){
    let start_date: Moment = moment.utc(dates.start_date);
    let end_date: Moment = moment.utc(dates.end_date);
    let dateFormat = 'MMM YYYY'; // Default to monthy view
    let interval: any = 1;
    let duration = 'month';
    if ((end_date.diff(start_date, 'months') == 0) || (end_date.diff(start_date, 'days') < 15)) {
      dateFormat = 'MM-DD-YYYY';
      duration = 'days';
    }else{
      end_date = end_date.endOf('month');
    }

    // First group balances by account id. Since it might happen that one account has balance for the
    // need date and other does not
    let balancesByAccount = {};
    for(let balance of filteredBalances){
      if(!(balance.account_id in balancesByAccount)){
        balancesByAccount[balance.account_id] = [];
      }
      balancesByAccount[balance.account_id].push(balance);
    }
    console.log('balancesByAccount',balancesByAccount);

    let groupedFilteredBalances={};
    
    for(let account of Object.keys(balancesByAccount)){
      let filteredAccountBalances = balancesByAccount[account];
      let notAvailableBalanceMapping = {};
      let groupedFilteredAccountBalances = {};
      let loop_start_date = moment.utc(dates.start_date);
      for (let l = loop_start_date; l.isSameOrBefore(end_date); l.add(interval, duration)) {
        // for each account 
        let formatedDate = l.format(dateFormat);
        let filteredBalancesByFormat = filteredAccountBalances.filter(t=>t.date && moment.utc(t.date).format(dateFormat) === formatedDate);
        console.log('filteredBalancesByFormat',formatedDate,filteredBalancesByFormat);
        // If duration is month only keep those balances of last day of month/last date available balance in that month
        if(duration === 'month'){
          // Now sort it by date
          if(filteredBalancesByFormat && filteredBalancesByFormat.length > 0){
            filteredBalancesByFormat = this.dateOrderPipe.transform(filteredBalancesByFormat,'-date');
            // Get date of last available date and get all transactions of that date
            // Since we can have multiple account balances for that date
            let dateToConsider = filteredBalancesByFormat[0].date;
            groupedFilteredAccountBalances[formatedDate] = filteredBalancesByFormat.filter(t=>t.date === dateToConsider);
          }
          
        }else{
          groupedFilteredAccountBalances[formatedDate] = filteredBalancesByFormat;
          if(!filteredBalancesByFormat || filteredBalancesByFormat.length === 0){
            // Get date of last available date and get all transactions of that date
            // Since we can have multiple account balances for that date
            let filteredBalancesByFormat = this.dateOrderPipe.transform(filteredAccountBalances,'-date');
            // Get date of last available date and get all transactions of that date
            // Since we can have multiple account balances for that date
            notAvailableBalanceMapping[formatedDate] = moment.utc(filteredBalancesByFormat[0].date).format(dateFormat);
          }
        }
      }

      // Check if any date/month transaction is not present that consider last available
      if(Object.keys(notAvailableBalanceMapping).length > 0){
        console.log('notAvailableBalanceMapping',notAvailableBalanceMapping);
        for(let durationKey of Object.keys(notAvailableBalanceMapping)){
          groupedFilteredAccountBalances[durationKey] = groupedFilteredAccountBalances[notAvailableBalanceMapping[durationKey]];
        }
      }
      console.log('groupedFilteredAccountBalances',groupedFilteredAccountBalances);
      for(let gfab of Object.keys(groupedFilteredAccountBalances)){
        if(!(gfab in groupedFilteredBalances)){
          groupedFilteredBalances[gfab]=[];
        }
        groupedFilteredBalances[gfab].push(...groupedFilteredAccountBalances[gfab]);
      }
    }
    return groupedFilteredBalances;
  }

  //Debts over time
  createDebtOverTimeGraph(dates, balanceArr) {
    let start_date: Moment = moment.utc(dates.start_date);
    let end_date: Moment = moment.utc(dates.end_date);
    // for spending transaction
    let filteredDepoTrans = balanceArr.filter(t => ((t.balance < 0 && t.account_type == "depository") || (t.balance > 0 && t.account_type == "credit")) && t.date && (moment.utc(t.date).isBetween(start_date, end_date, null, '[]')));
    
    let balanceMapping = this.createBalanceSnapshotByDateFormat(dates,filteredDepoTrans);
    console.log('balanceMapping:',balanceMapping);
    let debtsData = Utils.createEmptyGraphDataByDuration(dates);
    if(balanceMapping && Object.keys(balanceMapping).length>0){
      for(let key of Object.keys(debtsData)){
        if(key in balanceMapping && balanceMapping[key]){
          debtsData[key].transactionArr = balanceMapping[key];
          debtsData[key].amount = balanceMapping[key].reduce((sum, t) => {
            return sum + (Math.round(Math.abs(t.balance) * 100) / 100)
          },0);
        }
      }
    }
    
    // Debts total income and its avg
    let amountArr = Object.values(debtsData).map((t: any) => Math.abs(t.amount));
    let maxAmount =  Math.max(...amountArr);
    let minAmount =  Math.min.apply(null, amountArr.filter(Boolean));
    minAmount =  minAmount == Infinity ? 0 :  minAmount   
    this.graphs.debts_over_time.totalAmount = minAmount;
    this.graphs.debts_over_time.avgAmount = maxAmount;

    this.graphs.debts_over_time.labels.length = 0;
    this.graphs.debts_over_time.labels.push(...Object.keys(debtsData));
    this.graphs.debts_over_time.data = [
      {
        data: Object.values(debtsData).map((t: any) => t.amount),
        label: `${this.G_Debts}`,
        transactions: Object.values(debtsData).map((t: any) => t.transactionArr)
      }
    ];
    this.graphs.debts_over_time.load = true;
    this.spinnerService.hide();
  }

  
  // This function is used to calculate properties data based on duration
  calculatePropertiesData(dates,allProperties){
    let graphData=Utils.createEmptyGraphDataByDuration(dates);

    let start_date: Moment = moment.utc(dates.start_date);
    let dateAndDuration = Utils.calculateDuration(dates);
    // loop all properties
    for(let property of allProperties){
      // Get when propery was created
      let created_at = moment.utc(property.created_at);
      // We need to check if this property was created after the start date of the dates
      if(created_at.isSameOrAfter(start_date)){
        // Add this property to all the formats
        let propertyAdded = false;
        for(let time of Object.keys(graphData)){
          // Once property is added we want to keep it adding for rest of all the dates
          if(time === created_at.format(dateAndDuration.dateFormat) || propertyAdded){
            graphData[time].amount += property.estimated_value;
            graphData[time].transactionArr.push(property);
            propertyAdded = true;
          }
        }
      }
    }
    return graphData;
  }

  // This function is used to calculate asset data based on balances
  // We take the balance present on that day/month as part of calculation
  calculateAssetsData(dates,balanceArr){
    let start_date: Moment = moment.utc(dates.start_date);
    let end_date: Moment = moment.utc(dates.end_date);
    let filteredBalances = balanceArr.filter(t => t.account_type == "depository" && t.balance > 0 && t.date && (moment.utc(t.date).isBetween(start_date, end_date, null, '[]')));
    console.log('filteredBalances',filteredBalances);
    

    let balanceMapping = this.createBalanceSnapshotByDateFormat(dates,filteredBalances);
    console.log('balanceMapping:',balanceMapping);
    let assetsData=Utils.createEmptyGraphDataByDuration(dates);
    if(balanceMapping && Object.keys(balanceMapping).length>0){
      for(let key of Object.keys(assetsData)){
        if(key in balanceMapping && balanceMapping[key]){
          assetsData[key].transactionArr = balanceMapping[key];
          assetsData[key].amount = balanceMapping[key].reduce((sum, t) => {
            return sum + (Math.round(t.balance * 100) / 100);
          },0);
        }
      }
    }

    console.log('AssestData', assetsData);
    return assetsData;
  }

  //Assets Over Time
  createAssetsOverTimeGraph(dates, balanceArr, allProperties) {
    let assetsData = this.calculateAssetsData(dates,balanceArr);
    console.log('AssestData', assetsData);

    let propertiesData = this.calculatePropertiesData(dates,allProperties);
    console.log('propertiesData', propertiesData);
    
    // Assets total income and its avg
    let amountArr = Object.values(assetsData).map((t: any) => Math.abs(t.amount));
    let propArr = Object.values(propertiesData).map((t: any) => t.amount);
    let merdedData = [];
    for(let i=0; i < amountArr.length; i++){
      merdedData.push(amountArr[i] + propArr[i]);
    }

    let sumOfAmount = merdedData.reduce((a, b) => a + b); 
    let maxAmount = Math.max(...merdedData);   
    this.graphs.assets_over_time.totalAmount = maxAmount;
    this.graphs.assets_over_time.avgAmount = sumOfAmount / amountArr.length;

    console.log('assets-amountArr', amountArr);

    // this.graphs.income_over_time.data[0].transaction.length= 0;
    this.graphs.assets_over_time.labels.length = 0;
    this.graphs.assets_over_time.labels.push(...Object.keys(assetsData));
    this.graphs.assets_over_time.data = [{
      data: Object.values(assetsData).map((t: any) => t.amount),
      label: `${this.G_Assets}`,
      transactions: Object.values(assetsData).map((t: any) => t.transactionArr)
    },
    {        
      data: Object.values(propertiesData).map((t: any) => t.amount),
      label: `${this.G_PROPERTY}`,
      transactions: Object.values(propertiesData).map((t: any) => t.transactionArr)
    }
  ];
    this.graphs.assets_over_time.load = true;
    this.spinnerService.hide();
  }

  // This function is used to merge assets data and properties data
  mergeAssetAndProperties(assetsData, propertiesData){
    let mergedData = assetsData;
    if(propertiesData){
      for(let timeKey of Object.keys(propertiesData)){
        if(!Utils.isNullOrEmpty(assetsData,timeKey)){
          assetsData[timeKey].amount = Math.abs(assetsData[timeKey].amount) + propertiesData[timeKey].amount;
          assetsData[timeKey].transactionArr.push(...propertiesData[timeKey].transactionArr);
        }
      }
    }
    return mergedData;
  }

  //NetWorth Over Time
  createNetWorthOverTimeGraph(dates, balanceArr, allProperties) {
    let start_date: Moment = moment.utc(dates.start_date);
    let end_date: Moment = moment.utc(dates.end_date);

    

    let assetsData = this.calculateAssetsData(dates,balanceArr);
    
    let propertiesData = this.calculatePropertiesData(dates,allProperties);
    console.log('propertiesData', propertiesData);
    assetsData = this.mergeAssetAndProperties(assetsData,propertiesData);

    let netWorthData = [];
    let assetsArray: any = [];
    let debtArray: any = [];

    //  Net Worth over time=>  the assets over time minus the debts over time (debts ove time is the like assets but for NEGATIVE values, meaning, you owe money to someone )bank, credit card, mortgage, loan)
    let filteredDebtsTransactions = balanceArr.filter(t => (t.balance < 0 && t.account_type == "depository" || t.balance > 0 && t.account_type == "credit") && t.date && (moment.utc(t.date).isBetween(start_date, end_date, null, '[]')));
    let balanceMapping = this.createBalanceSnapshotByDateFormat(dates,filteredDebtsTransactions);
    console.log('balanceMapping:',balanceMapping);
    let debtsData = Utils.createEmptyGraphDataByDuration(dates);
    if(balanceMapping && Object.keys(balanceMapping).length>0){
      for(let key of Object.keys(debtsData)){
        if(key in balanceMapping && balanceMapping[key]){
          debtsData[key].transactionArr = balanceMapping[key];
          debtsData[key].amount = balanceMapping[key].reduce((sum, t) => {
            return sum + (Math.round(Math.abs(t.balance) * 100) / 100)
          },0);
        }
      }
    }
     // averageData calculation
     assetsArray = Object.values(assetsData).map((a: any) => a.amount);
     debtArray = Object.values(debtsData).map((a: any) =>Math.abs(a.amount) * -1);
     for (let i = 0; i < assetsArray.length; i++) {
       netWorthData.push(assetsArray[i] + debtArray[i]);
     }
     // netWorthTransactionArr
     let assetsTransArr = Object.values(assetsData).map((t: any) => t.transactionArr);
     let debtTransArr = Object.values(debtsData).map((t: any) => t.transactionArr);
     let netWorthTransactionArr = []
 
     for (let i = 0; i < assetsTransArr.length; i++) {
       let incomeKey = null;
       let spendKey = null;
 
       incomeKey = assetsTransArr[i];
       spendKey = debtTransArr[i];
 
       netWorthTransactionArr.push(incomeKey.concat(spendKey));
     }
    
    // networth min and max value  
    this.graphs.networth_over_time.totalAmount = Math.min(...netWorthData);
    this.graphs.networth_over_time.avgAmount = Math.max(...netWorthData);

    this.graphs.networth_over_time.labels.length = 0;
    this.graphs.networth_over_time.labels.push(...Object.keys(assetsData));
    this.graphs.networth_over_time.data = [
      {
        data: netWorthData,
        label: `${this.G_Net_Worth}`,
        // label : 'T_NET_WORTH
        type: "line",
        borderColor: "#717171",
        fill: false,
        lineTension: 0,
        pointRadius: 4,
        pointBackgroundColor: "#717171",
        transactions: netWorthTransactionArr
      },
      {
        data: assetsArray,
        label: `${this.G_Assets}`,
        // label : 'T_ASSETS
        transactions: Object.values(assetsData).map((t: any) => t.transactionArr)
      },
      {
        data: debtArray,
        label: `${this.G_Debts}`,
        // label : 'T_DEBT
        transactions: Object.values(debtsData).map((t: any) => t.transactionArr)
      }
    ];
    this.graphs.networth_over_time.load = true;
    this.spinnerService.hide();
  }

  // pie chart data over time
  createPieChartOverTimeGraph(transactions, level) {
    let PiechartData: any = {};
    let total = 0;

    let SpendFilterData = Utils.filterSpendingTransactions(transactions);

    let category;

    for (let t of SpendFilterData) {
      total += Math.abs(t.amount);
      if (t.category) {
        // This is done to get first level category
        let catg = t.category.split(',');

        if (catg.length >= level) {
           catg.length == level ?  category = catg[0] : category = catg[level];
        }
        if (category in PiechartData) {
          PiechartData[category].amount +=Math.abs(t.amount);
          PiechartData[category].transactions.push(t);
        } else {
          PiechartData[category] = {
            amount: Math.abs(t.amount),
            transactions: [t]
          };
        }
      }
    }

    // calculate percentage
    for (let category of Object.keys(PiechartData)) {
      let percentage = (PiechartData[category].amount * 100) / total;
     // console.log(percentage);
      if (percentage < 5) {
        // Club into others category
        if (!('Others' in PiechartData)) {
          PiechartData.Others = {
            amount: 0,
            transactions: []
          }
        }
        PiechartData.Others.amount += PiechartData[category].amount;
        PiechartData.Others.transactions.push(...PiechartData[category].transactions);
        delete PiechartData[category];
      }
    }

    this.graphs.spending_by_category.barOptions.elements.center.text = this.currency.transform(total,'USD',true,'1.0-0');
    this.graphs.spending_by_category.labels.length = 0;
    this.graphs.spending_by_category.labels.push(...Object.keys(PiechartData));

    if (level === 0) {
      this.graphs.spending_by_category.labelHierachy = [];
      this.graphs.spending_by_category.transactions = [];
    }
    this.graphs.spending_by_category.transactions.push(Object.values(PiechartData).map((t: any) => t.transactions));
    this.graphs.spending_by_category.level = level;

    // We are keeping in dataset at index 1
    this.graphs.spending_by_category.data = [{
      data: Object.values(PiechartData).map((t: any) => (t.amount)),
      transactions: Object.values(PiechartData).map((t: any) => t.transactions)
    }, {
      data: Object.values(PiechartData).map((t: any) => t.transactions)
    }];
    this.getColor();
    this.graphs.spending_by_category.load = true;
  };
  
  getstartenddate(graph) {
    let start_date = '';
    let end_date = '';
    let passVar = this.graphs[graph].datesFilter;
    switch (passVar) {
      case '7days':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(6, 'days').format(this.DATE_FORMAT);
        break;
      case '12months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(11, 'months').startOf('month').format(this.DATE_FORMAT);
        break;
      case '14days':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(13, 'days').format(this.DATE_FORMAT);
        break;
      case 'thismonth':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().startOf('month').format(this.DATE_FORMAT);
        break;
      case '3months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(2, 'months').startOf('month').format(this.DATE_FORMAT);
        break;
      case '6months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(5, 'months').startOf('month').format(this.DATE_FORMAT);
        break;
      case 'thisyear':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().startOf('year').format(this.DATE_FORMAT);
        break;
      case 'lastyear':
        end_date = moment.utc().subtract('1', 'year').endOf('year').format(this.DATE_FORMAT);
        start_date = moment.utc().subtract('1', 'year').startOf('year').format(this.DATE_FORMAT);
        break;
    }
    return {start_date: start_date, end_date: end_date};
  }

  populateTransactions(start_date, end_date, accounts, graph) {
    console.log('trends-startDate', start_date);
    console.log('trends-end_date', end_date);
    console.log('populateTransactions Data', accounts);

    // Make get transactions call
    let path = '/users/transactions';
    let qp = [];
    if (start_date) {
      qp.push('start_date=' + start_date);
    }
    if (end_date) {
      qp.push('end_date=' + end_date);
    }
    if (accounts) {
      qp.push('account_ids=' + accounts);
    }

    if (qp && qp.length > 0) {
      path += '?' + qp.join('&');
    }
    console.log(path);
    let token: string = '';
    if (this.report) {
      token = this.report.token;
    } else {
      token = this.authService.getAccessTokenAndID().accessToken;
    }
    this.backend.get(path, token)
      .subscribe(data => {
        let allTransactions = data.json();
        console.log('allTransactionDetails', allTransactions);
        this.transactions = [];
        this.transactions = allTransactions.transactions;
        console.log('trends-showNoTransactionsWarning', this.showNoTransactionsWarning);

        if (graph == 'all') {
          this.tranxDataEmit.emit(allTransactions);
        }
        

        // transform the date into date format
        for (let t of allTransactions.balances) {
          t.date = moment.utc(t.date).format('YYYY-MM-DD')
        }

        this.balanceArr = allTransactions.balances;

        if (graph == 'all' && !(this.transactions && this.transactions.length > 0 || this.balanceArr && this.balanceArr.length > 0)) {
          this.showNoTransactionsWarning = true;
          console.log('trends-showNoTransactionsWarning', this.showNoTransactionsWarning);
        }

        this.spinnerService.hide();

        let dates: any = {start_date, end_date};
        console.log("dates", dates);
        
        let allProperty = [];
        if(this.report){
          allProperty = this.report.properties;
        }else{
          allProperty = this.allProperty;
        }

        //Clone transactions data
        let clonedTransaction = this.genericCloneMethod(this.transactions);
        let clonedProperties =  this.genericCloneMethod(allProperty);
        let clonedBalanceArr = this.genericCloneMethod(this.balanceArr);

        //  this.renderGraph(dates);
        if (graph == 'all' || graph == 'income_over_time') {
          this.createIncomeOverTimeGraph(dates, clonedTransaction);
        }
        if (graph == 'all' || graph == 'spending_over_time') {
          this.createSpendingOverTimeGraph(dates, clonedTransaction);
        }
        if (graph == 'all' || graph == 'net_income_over_time') {
          this.createTotalOverTimeGraph(dates, clonedTransaction);
        }
        if (graph == 'all' || graph == 'debts_over_time') {
          this.createDebtOverTimeGraph(dates, clonedBalanceArr);
        }
        if (graph == 'all' || graph == 'assets_over_time') {
          this.createAssetsOverTimeGraph(dates, clonedBalanceArr, clonedProperties);
        }
        if (graph == 'all' || graph == 'networth_over_time') {
          this.createNetWorthOverTimeGraph(dates, clonedBalanceArr, clonedProperties);
        }
        if (graph == 'all' || graph == 'spending_by_category') {
          this.createPieChartOverTimeGraph(clonedTransaction, 0);
        }

      });
  }

  filterForTransactions(event, graph) {
    this.graphs[graph].load = false;
    this.transactions = [];
    this.balanceArr = [];
    let dates: any = this.getstartenddate(graph);
    console.log("dates", dates);
    let selectedAccountsFilter = this.graphs[graph].accountsFilter;
    console.log('accounts filter', selectedAccountsFilter);
    let accountsFilter = null;
    if (selectedAccountsFilter == 'all') {
      accountsFilter = null;
    } else {
      accountsFilter = selectedAccountsFilter.account_id;
    }
    this.populateTransactions(dates.start_date, dates.end_date, accountsFilter, graph);
  }

  showGraph(item) {
    if (item) {
      this.graphValue = item;
    }
  }

  addAccountHandler(refrenceName) {
    this.addAccountHandlerEmit.emit(refrenceName);
  } 

}
