import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-add-loan-modal',
  templateUrl: './add-loan-modal.component.html',
  styleUrls: ['./add-loan-modal.component.css']
})
export class AddLoanModalComponent implements OnInit {

  constructor( public modalRef : BsModalRef) { }

  ngOnInit() {
  }

}
