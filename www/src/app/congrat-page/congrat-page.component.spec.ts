import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongratPageComponent } from './congrat-page.component';

describe('CongratPageComponent', () => {
  let component: CongratPageComponent;
  let fixture: ComponentFixture<CongratPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongratPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongratPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
