import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import {BackendService} from '../backend/backend.service';
import {SessionStorageService} from "angular-web-storage";
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-congrat-page',
  templateUrl: './congrat-page.component.html',
  styleUrls: ['./congrat-page.component.css']
})
export class CongratPageComponent implements OnInit {
  user: any;
  accounts : any = {};
  firstStar = true;
  secondStar = true;
  thirdStar = true;
  fourthStar = true;
  fifthStar = true;
  accountsLength:any;
  private readonly notifier: NotifierService;

  constructor(private authService: AuthService,private backend: BackendService, public session: SessionStorageService,
     public router: Router, private spinnerService: NgxSpinnerService, private notifierService: NotifierService, public translate: TranslateService) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.spinnerService.show();
    this.user = this.authService.getUserProfile();
    let tokenID = this.authService.getAccessTokenAndID();
    this.accounts = this.user.accounts;
    console.log('accounts',this.accounts);
    this.accountsLength = this.accounts.length;
    if(this.accountsLength === 1){
      let path = "/users/" + tokenID.idTokenPayload.sub;
      this.backend.get(path, tokenID.accessToken)
        .subscribe(data => {
          console.log('getUserProfile', data.json());
          this.user = Object.assign(this.user, data.json());
          this.authService.setDataInCache(this.authService.USER_PROFILE, this.user);
          this.spinnerService.hide();
        },
        error => {
          console.log(error);
          this.router.navigate(['/']);
          let errorVar = error.json();
          this.translate.get('DASH_ERROR')
          .subscribe((value) => {
            this.notifier.notify('error', value + errorVar["message"]);
          })
          this.spinnerService.hide();
        });
    }

    switch(this.accountsLength) {
      case 1: {
         this.firstStar = false;
         this.secondStar = true;
         this.thirdStar = true;
         this.fourthStar = true;
         this.fifthStar = true;
         this.accountsLength = this.accountsLength + 'st';
         break;
      }
      case 2: {
         this.firstStar = false;
         this.secondStar = false;
         this.thirdStar = true;
         this.fourthStar = true;
         this.fifthStar = true;
         this.accountsLength = this.accountsLength + 'nd';
         break;
      }
      case 3: {
         this.firstStar = false;
         this.secondStar = false;
         this.thirdStar = false;
         this.fourthStar = true;
         this.fifthStar = true;
         this.accountsLength = this.accountsLength + 'rd';
         break;
      }
      case 4: {
         this.firstStar = false;
         this.secondStar = false;
         this.thirdStar = false;
         this.fourthStar = false;
         this.fifthStar = true;
         this.accountsLength = this.accountsLength + 'th';
         break;
      }
      case 5: {
         this.firstStar = false;
         this.secondStar = false;
         this.thirdStar = false;
         this.fourthStar = false;
         this.fifthStar = false;
         this.accountsLength = this.accountsLength + 'th';
         break;
      }
      default: {
         this.firstStar = false;
         this.secondStar = false;
         this.thirdStar = false;
         this.fourthStar = false;
         this.fifthStar = false;
         this.accountsLength = this.accountsLength + 'th';
         break;
      }
    }
    this.spinnerService.hide();
  }

  goToDashboard(event){
    console.log(event);
    this.router.navigate(['/dashboard']); // go to the dashboard
  }

}
