import {Component, Input, OnInit, OnChanges, SimpleChanges, EventEmitter, Output} from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {NgxSpinnerService} from "ngx-spinner";
import {BackendService} from "../backend/backend.service";
import {Moment} from "moment";
import * as moment from "moment";

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css'],
})
export class TransactionsComponent implements OnInit, OnChanges {
  transactions: any = [];
  @Input("reloadScreen") reloadScreen :any;
  accounts: any = [];
  accountsForBlur:any = [];
  DATE_FORMAT: string = 'YYYY-MM-DD';
  selectedAccountFilter: any = 'all';
  selectedDateFilter: string = 'default';
  orderByField = '-date';
  datePrefrence :string = null;
  showNoTransactionsWarning: boolean = false;
  firstTimeWarning:boolean = false;
  selectedAccId:any;
  page: number = 1;
  searchText:string = "";
  searchValue:string = "";
  canParseAccounts:boolean = true;
  allAccounts:any;
  user:any;

  @Input("accountId") accountId:any;
  @Input("controlData") controlData :any;
  @Output() addAccountHandlerEmit = new EventEmitter<any>();

  constructor(private authService: AuthService,private spinnerService: NgxSpinnerService,private backend: BackendService) { }

  ngOnChanges(changes: SimpleChanges){
    if(changes.accountId && changes.accountId.currentValue){
      this.selectedAccId = changes.accountId.currentValue;
      this.selectedDateFilter = 'default';
  
      let start_date = moment.utc().add('-7','year').format(this.DATE_FORMAT);
      let end_date = moment.utc().format(this.DATE_FORMAT);
     
      // trying to match acc id with our caputured accId 
      if(this.canParseAccounts){
        for(let acc of this.allAccounts) {
          if('accounts' in acc && acc.accounts) {
            for(let account of acc.accounts) {
              if(account.account_id == changes.accountId.currentValue){
                return this.populateTransactions(start_date, end_date, changes.accountId.currentValue,false);            
              }          
            }
          }
        }
      }
    }
     // reload
     if(changes.reloadScreen.currentValue != changes.reloadScreen.previousValue){
      this.firstTimeWarning = false;
      this.ngOnInit();      
    }

    if ('controlData' in changes && changes.controlData.previousValue != null) {
      this.firstTimeWarning = true;
   }
  }

  ngOnInit() {
    this.user = this.authService.getUserProfile();
    this.allAccounts = [];
    this.allAccounts = this.user.accounts;

    let messageError = Object.create(null);
    if (this.allAccounts.length == 1 && typeof this.allAccounts[0] === 'string') {
      messageError = JSON.parse(this.allAccounts[0]);
      if (messageError.message == "ITEM_LOGIN_REQUIRED") {
        this.canParseAccounts = false;
        alert("Accounts can't be fetched, retry changing credentials");
      }
    }

    if (this.allAccounts && this.allAccounts.length > 0) {
      this.accounts = [];
      this.accountsForBlur = [];
      for(let us of this.allAccounts) {
        if('accounts' in us && us.accounts) {
          for(let account of us.accounts) {
            account.inst_name = us.inst_name;
            this.accounts.push(account);
            if(('type' in account && account.type == "depository") || ('type' in account && account.type == "credit")){
              this.accountsForBlur.push(account);
            }
          }
        }
      }

      // Get transactions also
      let start_date = moment.utc().add('-7','year').format(this.DATE_FORMAT);
      let end_date = moment.utc().format(this.DATE_FORMAT);
      return this.populateTransactions(start_date, end_date, null,true)
    }
  }
  
  populateTransactions(start_date, end_date, accounts,firstload) {
    this.spinnerService.show();
    console.log('populateTransactions Data', accounts);

    // Make get transactions call
    let path = '/users/transactions';
    let qp = [];
    if(start_date){
      qp.push('start_date='+start_date);
    }
    if(end_date){
      qp.push('end_date='+end_date);
    }
    if(accounts){
      qp.push('account_ids='+accounts);
    }

    if(qp && qp.length>0) {
      path +='?'+qp.join('&');
    }
    console.log(path);
    this.backend.get(path,this.authService.getAccessTokenAndID().accessToken)
      .subscribe(data => {
        let allTransactions = null;
            allTransactions = data.json();
        this.spinnerService.show()

        this.showNoTransactionsWarning = false;
        this.firstTimeWarning = false;
        console.log('transssallTransactions', allTransactions);
        
        // don't delete this code, it will be use in future.........!!

        //changing date format according to userLoggedIn
        // if(allTransactions.transactions && allTransactions.transactions.length > 0){}
          // for(let t of allTransactions.transactions){
          //   if(this.authService.getUserProfile().preferences == 'uk'){
          //     this.datePrefrence = 'uk'
          //     t.date = moment.utc(t.date).format('DD-MM-YYYY');
          //   }else{
          //     this.datePrefrence = 'us'
          //     t.date = moment.utc(t.date).format('MM-DD-YYYY');
          //   }
          // }

           let clonedTransaction = [];
            for (let obj of allTransactions.transactions) {
              let b = null;
              b = Object.assign({},obj);
              clonedTransaction.push(b);
            }


          for(let t of clonedTransaction){
             if(t.account_type == 'depository' || t.account_type == 'credit' || t.account_type == 'brokerage'){
               if(t.amount < 0){
                 t.amount = (t.amount * -1).toFixed(2);
               }else{
                t.amount = (t.amount * -1).toFixed(2);
               }
             }
          }

          this.transactions = clonedTransaction;
          
          if(firstload && !(this.transactions && this.transactions.length>0)){
            this.firstTimeWarning = true;
          }

          if(!firstload && !(this.transactions && this.transactions.length>0)){
            this.showNoTransactionsWarning = true;
          }

        this.spinnerService.hide();
      },
      (err) => {
              console.log('got error', err.json());
              this.spinnerService.hide()
       });
  }

  getstartenddate() {
    let start_date = '';
    let end_date = '';
    let passVar = this.selectedDateFilter;
    switch (passVar) {
      case '7days':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(6, 'days').format(this.DATE_FORMAT);
        break;
      case '12months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(11, 'months').startOf('month').format(this.DATE_FORMAT);
        break;
      case '14days':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(13, 'days').format(this.DATE_FORMAT);
        break;
      case 'thismonth':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().startOf('month').format(this.DATE_FORMAT);
        break;
      case '3months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(2, 'months').format(this.DATE_FORMAT);
        break;
      case '6months':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract(5, 'months').format(this.DATE_FORMAT);
        break;
      case 'thisyear':
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().startOf('year').format(this.DATE_FORMAT);
        break;
      case 'lastyear':
        end_date = moment.utc().subtract('1','year').endOf('year').format(this.DATE_FORMAT);
        start_date = moment.utc().subtract('1','year').startOf('year').format(this.DATE_FORMAT);
        break;
      default:
        end_date = moment.utc().format(this.DATE_FORMAT);
        start_date = moment.utc().subtract('7','year').startOf('year').format(this.DATE_FORMAT);
        
        break;
    }
    return {start_date: start_date, end_date: end_date};
  }

  filterForTransactions(event,load) {
    this.spinnerService.show();
    this.transactions = [];
    let dates: any = this.getstartenddate();
    console.log("dates", dates);

    let selectedAccountsFilter = this.selectedAccountFilter;
    console.log('accounts filter',selectedAccountsFilter);
    let accountsFilter = null;
    if(selectedAccountsFilter == 'all'){
      accountsFilter = null;
    }else {
      accountsFilter = selectedAccountsFilter.account_id;
    }
    this.populateTransactions(dates.start_date, dates.end_date, accountsFilter,load);
  }

  orderByDate(){
    if(this.orderByField && this.orderByField === '-date'){
      this.orderByField = 'date';
    }else{
      this.orderByField = '-date';
    }
  }

  //datePrefrence method
  setDatePrefrence(e){
   console.log('setDatePrefrence',this.datePrefrence);
   this.spinnerService.show();
   let data:any = {}
   if(this.datePrefrence){
    data.preferences = this.datePrefrence;
   }
   let path = "/users/" + this.authService.getAccessTokenAndID().idTokenPayload.sub;
    this.backend.put(data, path, this.authService.getAccessTokenAndID().accessToken)
    .subscribe(res => {
      console.log('preferencesRes', res);
      let clonedTrans = [...this.transactions];
       if(res.status == 204){
        this.transactions = [];
          for(let t of clonedTrans){
            if(this.datePrefrence == 'uk'){
              t.date = moment.utc(t.date, 'MM-DD-YYYY').format('DD-MM-YYYY')            
            }
            else{
              t.date = moment.utc(t.date, 'DD-MM-YYYY').format('MM-DD-YYYY')
            }
          }
       }
       //update transactions
       this.transactions = [...this.transactions,...clonedTrans];
        this.spinnerService.hide();
      },
      err => {
        console.log('preferenceError', err)
        this.spinnerService.hide();
      });
  }

  addAccountHandler(refrenceName) {
    this.addAccountHandlerEmit.emit(refrenceName);
  }
}
