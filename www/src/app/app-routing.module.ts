import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignuppageComponent } from './signuppage/signuppage.component';
import { CongratPageComponent } from './congrat-page/congrat-page.component';
import { GeneratereportPageComponent } from './generatereport-page/generatereport-page.component';
import { OpenReportComponent } from './open-report/open-report.component';
import { ManageAccountComponent } from './manage-account/manage-account.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { GaugeChartComponent } from './guage-chart/guage-chart.component';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'signup', component: SignuppageComponent},
  { path: 'congratpage', component: CongratPageComponent},
  { path: 'report/:id', component: GeneratereportPageComponent},
  { path: 'report/:id/open', component: OpenReportComponent},
  { path: 'manageaccount', component: ManageAccountComponent},
  { path: 'email-verification', component: EmailVerificationComponent},
  { path: 'guage-chart', component:GaugeChartComponent }
];

export const RoutingComponents = [ LoginComponent]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
