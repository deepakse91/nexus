import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-add-investment-modal',
  templateUrl: './add-investment-modal.component.html',
  styleUrls: ['./add-investment-modal.component.css']
})
export class AddInvestmentModalComponent implements OnInit {

  constructor( public modalRef : BsModalRef) { }

  ngOnInit() {
  }

}
