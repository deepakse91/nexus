import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyLoanDataComponent } from './verify-loan-data.component';

describe('VerifyLoanDataComponent', () => {
  let component: VerifyLoanDataComponent;
  let fixture: ComponentFixture<VerifyLoanDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyLoanDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyLoanDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
