import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BackendService } from '../backend/backend.service';
import { AuthService } from "../auth/auth.service";
import { NotifierService } from 'angular-notifier';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import Utils from '../common/utils';

@Component({
  selector: 'app-verify-loan-data',
  templateUrl: './verify-loan-data.component.html',
  styleUrls: ['./verify-loan-data.component.css' ,'../global/global.css']
})
export class VerifyLoanDataComponent implements OnInit {

  @Output() verifyChildEvent = new EventEmitter();
  private readonly notifier: NotifierService;
  modalRef: BsModalRef;

  displayCurrentDiv: string = 'welcomeDiv';
  userAttributes: any;
  authResultInfo: any;
  DATE_FORMAT: string = 'YYYY-MM-DD';
  previousEmployer:any = 0;
  previousEmployerData = {
    'preferred':0,
    'present':0,
    'from_duration_in_date':null,
    'dateDuration': 0,
    'address': {
      street1: null,
      city: null,
      street2: null,
      state: null,
      zip: null
    },
    'to_duration':null,
    'from_duration':null,
    'email':null,
    'phone_number':null,
    'contact_person':null,
    'title':null,
    'occupation':null,
    'income':null,
    'self_employed':null,
    'employer_name':null
  };

  constructor(private notifierService: NotifierService,
    private authService: AuthService,
    private backend: BackendService,
    private modalService: BsModalService,
    private spinnerService: NgxSpinnerService,
    public translate: TranslateService,
  ) {
    this.notifier = notifierService;

    // this is default langugage
    translate.setDefaultLang('en');

    // this language is to set by your prefered browser language
    let browserLang = translate.getBrowserLang();
    translate.use(browserLang);
  }

  ngOnInit() {
    // Clone this
    this.userAttributes = Utils.cloneJSON(this.authService.getUserProfile());
    if (this.userAttributes.dob) {
      this.userAttributes.dobInDate = moment.utc(this.userAttributes.dob).toDate();
    } else {
      this.userAttributes.dobInDate = null;
    }
    if (!this.userAttributes.address) {
      this.userAttributes.address = {
        street: null,
        city: null,
        street2: null,
        country: null,
        state: null,
        zip: null
      }
    }

    if ('employers' in this.userAttributes && this.userAttributes.employers && this.userAttributes.employers.length > 0) {
      for (let emp of this.userAttributes.employers) {
          if (emp.from_duration) {
            emp.from_duration_in_date = moment.utc(emp.from_duration).toDate();
          } else {
            emp.from_duration_in_date = null;
          }

          if ('self_employed' in emp && emp.self_employed) {
            emp.self_employed = true;
          } else {
            emp.self_employed = false;
          }

          if (!emp.address) {
            emp.address = {
              street1: null,
              city: null,
              street2: null,
              state: null,
              zip: null
            }
          }
      }
    }

    this.authResultInfo = this.authService.getAccessTokenAndID();
  }

  changeModalDiv(divName: any) {
    this.displayCurrentDiv = divName;
  }

  /* Update profile Code */
  updateUserProfile(divName: any) {
    let data: any = {};
    let requiredField = true;
    if (!Utils.isNullOrEmpty(this.userAttributes, 'first_name')) {
      data.first_name = this.userAttributes.first_name;
    } else {
      requiredField = false;
    }
    if ('last_name' in this.userAttributes) {
      data.last_name = this.userAttributes.last_name;
      if (data.last_name == '') {
        data.last_name = null;
      }
    }
    if (!Utils.isNullOrEmpty(this.userAttributes, 'dobInDate')) {
      data.dob = moment.utc(this.userAttributes.dobInDate).format(this.DATE_FORMAT);
      this.userAttributes.dob = data.dob;
    }
    if (!Utils.isNullOrEmpty(this.userAttributes, 'address')) {
      data.address = JSON.parse(JSON.stringify(this.userAttributes.address));
    }
    if (!Utils.isNullOrEmpty(this.userAttributes, 'emails')) {
      data.emails = this.userAttributes.emails;
    }
    if (!Utils.isNullOrEmpty(this.userAttributes, 'phone_numbers')) {
      data.phone_numbers = this.userAttributes.phone_numbers;
    }
    if (!Utils.isNullOrEmpty(this.userAttributes, 'home_phone')) {
      data.home_phone = this.userAttributes.home_phone;
    }
    console.log("Data : ", data);

    this.spinnerService.show();
    let path = "/users/" + this.authResultInfo.idTokenPayload.sub;
    if (requiredField) {
      this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(res => {
        this.spinnerService.hide();
        // translate error message
        this.translate.get('EPM_UPDATE_PROFILE')
          .subscribe((value) => {
            this.notifier.notify('success', value);
          })
        this.displayCurrentDiv = divName;
        let response = res.json();
        console.log('Response: ', response);
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
      },
        err => {
          this.spinnerService.hide();
          let errorVar = err.json();
          console.log('Error: ', errorVar["message"]);
          // translate error message
          this.translate.get('EPM_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + errorVar["message"]);
            })
        });
    } else {
      this.spinnerService.hide();
      // translate error message
      this.translate.get('EPM_FIRSTNAME_REQUIRED')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
    }
  }

  setEmployerDetails(divName:any) {
    let data: any = {
      'account_verified': 1,
      'update_account_status': true
    };
    this.userAttributes.account_verified = 1;
    let requiredField = true;
    if ('employers' in this.userAttributes && this.userAttributes.employers) {
      for (let index of this.userAttributes.employers) {
        if (Utils.isNullOrEmpty(index, 'employer_name')) {
          requiredField = false;
        }
        if (index.from_duration_in_date) {
          index.from_duration = moment.utc(index.from_duration_in_date).format(this.DATE_FORMAT);
        } else {
          delete index.from_duration_in_date;
          delete index.from_duration;
        }
        delete index.user_id;
        delete index.years;
        delete index.months;
        delete index.created_at;
      }
      data.employers = JSON.parse(JSON.stringify(this.userAttributes.employers));
    }

    for (let index of data.employers) {
      if (!index.address) {
        delete index.address;
      }
      if (!index.phone_number) {
        delete index.phone_number;
      }
      if (!index.email) {
        delete index.email;
      }
      if (!index.contact_person) {
        delete index.contact_person;
      }
      if (!index.title) {
        delete index.title;
      }
      delete index.from_duration_in_date;
      delete index.to_duration_in_date;
      delete index.seeTransaction;
      delete index.dateDuration;
      if (!index.from_duration) {
        delete index.from_duration;
      }
      if (!index.to_duration) {
        delete index.to_duration;
      }
      if (!index.income) {
        delete index.income;
      }
    }

    let path = "/users/" + this.authResultInfo.idTokenPayload.sub;
    if (requiredField) {
      this.spinnerService.show();
      this.backend.put(data, path, this.authResultInfo.accessToken).subscribe(res => {
        this.spinnerService.hide();
        this.translate.get('PC_EMPL_UPDATE_SUCCES')
          .subscribe((value) => {
            this.notifier.notify('success', value);
          });
        let response = res.json();
        console.log('Response#: ', response);
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        if(divName == 'previousEmployerDiv' && this.previousEmployer < 24){
          this.displayCurrentDiv = divName;
        } else {
          this.closeModal('reload');
        }
      },
        err => {
          this.spinnerService.hide();
          let errorVar = err.json();
          console.log('Error: ', errorVar["message"]);
          this.translate.get('DASH_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + ": " + errorVar["message"]);
            })
        });
    } else {
      this.spinnerService.hide();
      this.translate.get('PC_SELECET_ATLEAST_ONE')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
    }
  }

  closeModal(event) {
    this.verifyChildEvent.emit(event);
  }

  // restrict alpha character and allow only number
  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // restrict special character 
  alphabetOnly(event: any): boolean {
    let regex = new RegExp("^[a-zA-Z ]*$");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
      event.preventDefault();
      return false;
    }
  }

  calculateDuration(event:Date, index){
    let now = moment.utc();
    let fromDuration = moment.utc(event);
    let year = now.diff(fromDuration, 'year');
    let months = now.diff(fromDuration, 'months');
    let days = now.diff(fromDuration, 'days');
    let dateDiffrence = Utils.diffYearMonthes(year, months, days);

    if(index == 'previousEmployer'){
      this.previousEmployerData.dateDuration = dateDiffrence;
    } else {
      this.userAttributes.employers[index].dateDuration = dateDiffrence;
    }
    this.previousEmployer = months;
  }

  setPreviousEmployerDetails(preData:any){
    let data: any = {
      'employers': []
    };
    let requiredField = true;
    if (Utils.isNullOrEmpty(preData, 'employer_name')) {
      requiredField = false;
    }
    if (preData.from_duration_in_date) {
      preData.from_duration = moment.utc(preData.from_duration_in_date).format(this.DATE_FORMAT);
    }

    this.userAttributes.employers.push(preData);
    let emp = [];
    emp.push(preData);
    data.employers = JSON.parse(JSON.stringify(emp));
    for (let index of data.employers) {
      delete index.from_duration_in_date;
      delete index.to_duration_in_date;
      delete index.dateDuration;  
    }

    if (requiredField) {
      this.spinnerService.show();
      this.backend.post(data, '/users/employers', this.authResultInfo.accessToken).subscribe(res => {
        this.spinnerService.hide();
        this.translate.get('PC_EMPL_UPDATE_SUCCES')
          .subscribe((value) => {
            this.notifier.notify('success', value);
          });
        let response = res.json();
        console.log('Response#: ', response);
        this.authService.setDataInCache(this.authService.USER_PROFILE, this.userAttributes);
        this.closeModal('reload');
      },
        err => {
          this.spinnerService.hide();
          let errorVar = err.json();
          console.log('Error: ', errorVar["message"]);
          this.translate.get('DASH_ERROR')
            .subscribe((value) => {
              this.notifier.notify('error', value + ": " + errorVar["message"]);
            })
        });
    } else {
      this.spinnerService.hide();
      this.translate.get('PC_SELECET_ATLEAST_ONE')
        .subscribe((value) => {
          this.notifier.notify('error', value);
        })
    }
  }

}
