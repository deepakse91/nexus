// auth.service.ts
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { Router } from '@angular/router';
import Auth0Lock from 'auth0-lock';
import { SessionStorageService } from 'angular-web-storage';
import { Http, Headers} from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { BackendService } from '../backend/backend.service';
import { NotifierService } from 'angular-notifier';
import { Cookie } from 'ng2-cookies/ng2-cookies';

(window as any).global = window;

@Injectable()
export class AuthService {
  USER_PROFILE: string="userProfile";
  ACCOUNT: string = 'accounts';
  TOKEN: string = 'tokens';
  HOME_PAGE: string = '/dashboard';
  BASE_64 : string = 'base64';
  private readonly notifier: NotifierService;

  auth0Options = {
    container:'logincontainer',
    theme: {
      logo: '/assets/nexus-log.png',
      primaryColor: '#8DC740'
    },
    languageDictionary: {
      title: "",
      signUpTitle:""
    },
    rememberLastLogin: false,
    auth: {
      redirectUrl: environment.auth.redirect,
      responseType: 'token id_token',
      audience: `https://${environment.auth.domain}/api/v2/`,
      params: {
        scope: environment.auth.scope
      }
    },
    autoclose: true,
    oidcConformant: true,
  };

  lock = new Auth0Lock(
    environment.auth.clientID,
    environment.auth.domain,
    this.auth0Options
  );

  constructor(public router: Router, public session:SessionStorageService, private http:Http, private spinnerService: NgxSpinnerService, private backend : BackendService,private notifierService: NotifierService) {
    this.notifier = notifierService;
    
    this.lock.on('authenticated', (authResult: any) => {
      console.log('authResult Info', authResult);
      this.lock.hide();
      this.handleSuccessAuth(authResult);
    }); // ...finish implementing authenticated

    this.lock.on('authorization_error', error => {
      console.log('something went wrong', error);
      if(error && 'errorDescription' in error && error.errorDescription){
        this.notifier.notify('error',error.errorDescription);
      }
      this.lock.show();
    });
  }

  handleSuccessAuth(authResult){
    this.spinnerService.show();
    this.lock.hide();
    this.setDataInCache(this.TOKEN, authResult);

    let userProfile: any = {
      id: authResult.idTokenPayload.sub
    };

    // Get userDetails
    let path ="/users/" + authResult.idTokenPayload.sub+'?expand=accounts&sync=true';
    this.backend.get(path, authResult.accessToken)
      .subscribe(data => {
          console.log('getUserProfile', data.json());
          userProfile = Object.assign(userProfile, data.json());
          
          // if pricture has 'NA
          if(userProfile.picture == "NA"){
            let collectName = [];
            if('first_name' in userProfile && userProfile.first_name){
              collectName.push(userProfile.first_name[0]);
            }
            if('last_name' in userProfile && userProfile.last_name){
              collectName.push(userProfile.last_name[0]);
            }            
            userProfile.picture = collectName.join('');
          }

          this.setDataInCache(this.USER_PROFILE, userProfile);
          this.spinnerService.hide();
          this.router.navigate([this.HOME_PAGE]); // go to the home route
        },
        error => {
          console.log(error);
          this.router.navigate(['/']);
          let errorVar = error.json();
          this.notifier.notify('error', "Message from Server: " + errorVar["message"]);
          this.spinnerService.hide();
        });
  }

  showLogin() {
    this.lock.show();
  }

  checkAuthentication(redirect,hash){
    let that = this;
    this.spinnerService.show();
    this.lock.hide();
    this.lock.checkSession({}, function(err, authResult) {
      // handle error or new tokens
      console.log(err,redirect,hash);
      if(!(hash && hash.includes('access_token'))) {
        if (err) {
          if (redirect) {
            that.router.navigate(['/']);
          } else {
            that.lock.show();
            that.spinnerService.hide();
            return;
          }

        }

        console.log(authResult);
        if (authResult) {
          that.handleSuccessAuth(authResult);
        }
      }
    });
  }
  
  getUserProfile(){
   // console.log("Session Call$$$$", this.session.get(this.USER_PROFILE));
    return this.session.get(this.USER_PROFILE);
  }

  getAccessTokenAndID(){
    return this.session.get(this.TOKEN);
  }

  getDataFromCache(key){
    return this.session.get(key);
  }

  setDataInCache(key,data){
    this.session.set(key,data,2,'h');
  }

  sessionCleared(){
    this.session.remove(this.USER_PROFILE);
    this.session.remove(this.ACCOUNT);
    this.session.remove(this.TOKEN);
    this.session.remove(this.HOME_PAGE);
  }

  cookieClear (){
    Cookie.delete(this.USER_PROFILE);
    Cookie.delete(this.ACCOUNT);
    Cookie.delete(this.TOKEN);
    Cookie.delete(this.HOME_PAGE);
  }
  getUserLogout(){
   this.sessionCleared();
   this.cookieClear();
   this.session.clear();
   this.lock.logout({
     returnTo: environment.auth.redirect
   });
   return;
  }
  login() {
    // Auth0 authorize request
    this.lock.show();
  }
}
