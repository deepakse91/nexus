import {Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnChanges {
  @Input()
  user: any;
  userToken : any;
  @Output() hitProfile = new EventEmitter();

  constructor(private authService: AuthService,
    public router: Router) {
      
    }

  ngOnInit() {
    console.log('inside header');
    this.userToken = this.authService.getDataFromCache('tokens');
    if(!this.userToken){
      this.router.navigate(['/']);
    }
    console.log('userInfo', this.user);
  }
  
  logout(){
    this.authService.getUserLogout();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("In header ngOnChanges");
    this.user = this.authService.getUserProfile();
  }
  profileSection(){
    this.hitProfile.emit('hitprofile');

  }

}
