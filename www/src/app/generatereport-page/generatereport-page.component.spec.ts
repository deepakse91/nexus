import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratereportPageComponent } from './generatereport-page.component';

describe('GeneratereportPageComponent', () => {
  let component: GeneratereportPageComponent;
  let fixture: ComponentFixture<GeneratereportPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratereportPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratereportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
