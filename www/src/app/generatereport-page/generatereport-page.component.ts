import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {BackendService} from "../backend/backend.service";
import {NgxSpinnerService} from "ngx-spinner";
import {NotifierService} from "angular-notifier";
import * as moment from 'moment'
import {of} from "rxjs";
import {mergeMap} from "rxjs/operators";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-generatereport-page',
  templateUrl: './generatereport-page.component.html',
  styleUrls: ['./generatereport-page.component.css']
})
export class GeneratereportPageComponent implements OnInit {

  id: string = null;
  notifier: any;
  report: any = null;

  constructor(public router: Router,private route: ActivatedRoute,
    private spinnerService: NgxSpinnerService,
    private notifierService: NotifierService,
    public translate: TranslateService,
    private backend: BackendService) {
    this.notifier = notifierService;
    {
          
        // this is default langugage
        translate.setDefaultLang('en');

        // this language is to set by your prefered browser language
        let browserLang = translate.getBrowserLang();
        translate.use(browserLang);
      

    }
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id");
    console.log('report',this.id);

    this.spinnerService.show();
    // Get report details
    this.backend.get(`/reports/${this.id}?expand=users`, null)
      .subscribe(data=>{
        this.report = data.json();
        console.log('report Data', this.report);
       this.translate.get('GP_TIME START')
       .subscribe((value)=>{
         this.notifier.notify('success', value);
       })

        this.spinnerService.hide();
      },
      error =>{
        console.log('error', error);
        this.spinnerService.hide();
        let errorDetails = error.json();
        this.notifier.notify('error', errorDetails.message);
      });
  }

  open(){
    this.router.navigate([`/report/${this.id}/open`]);
  }

}
