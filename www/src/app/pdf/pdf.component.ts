import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import Utils from '../common/utils';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit, OnChanges {
  
  @Input("applicationJson") applicationJson: any;
  @Input("invokePdf") invokePdf: any;
  user:any;

  constructor(private spinnerService: NgxSpinnerService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if('applicationJson' in changes && 'currentValue' in changes.applicationJson && changes.applicationJson.currentValue){
      this.user = changes.applicationJson.currentValue;
      if(!('birthDate' in this.user && this.user.birthDate)){
        this.user.birthDate = "";
      }

      // for email
      this.user.userEmail = "";
      if(!Utils.isNullOrEmpty(this.user, 'email') && this.user.email.length > 0){
        this.user.userEmail = this.user.email[0];
      }
      if('address' in this.user && this.user.address && 'street2' in this.user.address && this.user.address.street2){
        this.user.address.streetAddress = this.user.address.streetAddress + ', ' + this.user.address.street2;
      }

      // for previous address
      this.user['previous addresses'][0].fullAddress = "";
      if(!Utils.isNullOrEmpty(this.user, 'previous addresses') && this.user['previous addresses'].length > 0){
        this.user['previous addresses'][0].fullAddress = Utils.constructAddress(this.user['previous addresses'][0]);
      }

      // for current employer
      this.user.worksFor.empAddress = "";
      if('worksFor' in this.user && 'address' in this.user.worksFor && Object.keys(this.user.worksFor.address).length > 0){
        this.user.worksFor.empAddress =  Utils.constructAddress(this.user.worksFor.address);
      }
      
      // for previous employer
      this.user.preEmployer = {
        name:'',
      };
      if('worksFor' in this.user && 'setDuration' in this.user.worksFor && this.user.worksFor.setDuration > 2){
        if(this.user.workedAt && this.user.workedAt.length > 0){
          let workedAtByDuration = this.user.workedAt.sort(function (a, b) { return (a.setDuration - b.setDuration); });
          if(workedAtByDuration && workedAtByDuration.length > 0){
            this.user.preEmployer.name = workedAtByDuration[0].name;
          }
        }
      }
      console.log('User', this.user);
    }

    if('invokePdf' in changes && 'currentValue' in changes.invokePdf && changes.invokePdf.currentValue && typeof changes.invokePdf.currentValue == 'string'){
      this.printPdf();
    }
  }

  // for print
  printPdf() {
    window.print()
  }
}
