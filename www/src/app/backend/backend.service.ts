import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {environment} from "../../environments/environment";
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class BackendService {

  public static API_BASE_URL = environment.apibaseurl;
  public static NEXUS_SCORE_METER_URL = environment.nexusscoremeterurl;

  constructor(private http: Http, private _http : HttpClient) { }

  public post(data, path, token) {
    console.log('Inside post');
    let headers = new Headers();
    headers.append("Authorization", token);
    headers.append("content-type", "application/json");
    let body = data;
    let url = BackendService.API_BASE_URL + path;

    return this.http.post(url, body, {
      headers: headers
    });

  };

  public postHandlerForMeter(data) {
    console.log('Inside post');
    let headers = new Headers();
    headers.append("content-type", "application/json");
    let body = data;
    let url = BackendService.NEXUS_SCORE_METER_URL;
    return this.http.post(url, body, {
      headers: headers
    });

  };

  public delete(path, token) {
    console.log('Inside delete');
    let headers = new Headers();
    headers.append("Authorization", token);
    headers.append("content-type", "application/json");
    let url = BackendService.API_BASE_URL + path;

    return this.http.delete(url, {headers: headers});

  };

  public put(data, path, token) {
    console.log('Inside put');
    let headers = new Headers();
    headers.append("Authorization", token);
    let body = data;
    headers.append("content-type", "application/json");
    let url = BackendService.API_BASE_URL + path;

    return this.http.put(url, body, {
      headers: headers
    });

  };

  public get(path, token) {
    console.log('Inside get');
    let headers = new Headers();
    if(token) {
      headers.append("Authorization", token);
    }
    headers.append("content-type", "application/json");
    let url = BackendService.API_BASE_URL + path;

    return this.http.get(url, {
      headers: headers
    });
  };

  public syncTransaction(path, token) {
    console.log('Inside syncTransaction');
    let headers = new Headers();
    headers.append("Authorization", token);
    headers.append("content-type", "application/json");
    headers.append("Access-Control-Allow-Origin", "*");
    headers.append('Access-Control-Allow-Methods','GET, PUT, POST, DELETE');
    headers.append('Access-Control-Allow-Headers','Origin, Content-Type, Accept,X-Requested-With');

    let url = BackendService.API_BASE_URL + path;

    return this.http.post(url,{ headers: headers });
  };

}

