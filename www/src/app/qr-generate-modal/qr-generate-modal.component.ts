import { Component, OnInit, EventEmitter, Output, Input,} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {BackendService} from '../backend/backend.service';
import {NotifierService} from 'angular-notifier';
import { AuthService } from '../auth/auth.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-qr-generate-modal',
  templateUrl: './qr-generate-modal.component.html',
  styleUrls: ['./qr-generate-modal.component.css']
})
export class QrGenerateModalComponent implements OnInit {
  @Output() childEvent = new EventEmitter();
  @Input("qrCodeData") qrCodeData:any;
  generateReportId:any;
  emailValue: any;
  emailValidate;
  authResultInfo: any;
  private readonly notifier: NotifierService;

  constructor(private spinnerService: NgxSpinnerService,
              private backend: BackendService,
              private authService: AuthService,
              private notifierService: NotifierService,
              public translate: TranslateService) 
            {
              this.notifier = notifierService; 
            }

  ngOnInit() {
    this.generateReportId = this.qrCodeData.id;
    this.authResultInfo = this.authService.getAccessTokenAndID();
    this.translate.get('QR_TIME_START')
      .subscribe((value)=>{
      this.notifier.notify('success', value);
    })
    this.emailValidate = new FormGroup({
      emailId: new FormControl("", Validators.compose([
        Validators.required,
        Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")
      ]))
    });
  }

  closeModal(){
    this.childEvent.emit();
  }

  sendEmail() {
    this.spinnerService.show();
    let base64Data = this.authService.getDataFromCache(this.authService.BASE_64);
    
    console.log('base64Data', base64Data);

    let data = {
      email: this.emailValue,
      snapshot : base64Data
    }

    let path = "/reports/" + this.generateReportId + "/send";
    this.backend.post(data, path, this.authResultInfo.accessToken).subscribe(res => {
        this.spinnerService.hide();
        console.log('Response: ', res);
             this.translate.get('QR_SUCCESS_SENT')
           .subscribe((value)=>{
            this.notifier.notify('success', value);
          })
      },
      err => {
        this.spinnerService.hide();
        let errorVar = err.json();
        console.log('Error: ', errorVar["message"]);
        this.translate.get('DASH_ERROR')
           .subscribe((value)=>{
          this.notifier.notify('error', value + ": " + errorVar["message"]);
          })
      });
  }

  goToReport() {
    window.open(`/report/${this.generateReportId}`, "_blank"); // go to the Report page
  }

}
