import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrGenerateModalComponent } from './qr-generate-modal.component';

describe('QrGenerateModalComponent', () => {
  let component: QrGenerateModalComponent;
  let fixture: ComponentFixture<QrGenerateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrGenerateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrGenerateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
