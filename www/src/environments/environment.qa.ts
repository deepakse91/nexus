// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment:any = {
  production: false,
  auth: {
    clientID: 'Kbxz3nlJcoNv6boi2FDxNL1USuyW56tZ',
    domain: 'nexus-qa.auth0.com', // e.g., you.auth0.com
    audience: 'https://qa.nexusscore.com/', // e.g., http://localhost:3001
    redirect: 'https://qa.nexusscore.com/',
    scope: 'openid profile email'
  },
  placid: {
    apiid: '58b5868dead2e3931a909a866b60ca',
    environment:"production",
    productArray: ['transactions','assets'],
  },
  apibaseurl: 'https://api-qa.nexusscore.com',
  nexusscoremeterurl : 'https://aclaronexusscore.azurewebsites.net/CheckScore',
  wsapiurl:'wss://5ibetihoj4.execute-api.us-east-1.amazonaws.com/qa'
};
