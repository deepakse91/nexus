export const environment: any = {
  production: true,
  auth: {
    clientID: 'p5CRBYofRGuyAp1yb3ccjDGXtNlIFlKx',
    domain: 'nexusscore.auth0.com', // e.g., you.auth0.com
    audience: 'https://www.nexusscore.com/', // e.g., http://localhost:3001, localhost:4200
    redirect: 'https://www.nexusscore.com/',
    scope: 'openid profile email'
  },
  placid: {
    apiid: '58b5868dead2e3931a909a866b60ca',
    environment: 'production',
    productArray: ['transactions', 'assets']
  },
  apibaseurl: 'https://api.nexusscore.com',
  nexusscoremeterurl : 'https://aclaronexusscore.azurewebsites.net/CheckScore',
  wsapiurl:'wss://4xneg2l8kk.execute-api.us-east-1.amazonaws.com/prod'
};
