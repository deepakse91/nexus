// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  auth: {
    clientID: '7q6hiRHKiZlGvgqGGtEHpt8DTWHNWtTH',
    domain: 'aclaro.auth0.com', // e.g., you.auth0.com
    audience: 'https://localhost:4200', // e.g., http://localhost:3001
    redirect: 'http://localhost:4200/',
    scope: 'openid profile email'
  },
  placid: {
    apiid: '58b5868dead2e3931a909a866b60ca',
    environment:"sandbox",
    productArray: ['transactions','assets']
  },
  apibaseurl: 'https://api-dev.nexusscore.com',
  nexusscoremeterurl : 'https://aclaronexusscore.azurewebsites.net/CheckScore',
  wsapiurl:'wss://vv6govd0m0.execute-api.us-east-1.amazonaws.com/dev'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
